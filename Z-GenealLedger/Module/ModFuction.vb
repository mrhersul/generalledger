﻿Imports VB = Microsoft.VisualBasic
Imports System.IO
Imports System.Security.Cryptography
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data.OleDb
'For time server
Imports System.Runtime.InteropServices

Public Class ModFuction
    Public Function getValue(ByVal Query As String, ByVal ReturnColumn As String) As Long
        cmd = New SqlClient.SqlCommand(Query, Cn)
        dr = cmd.ExecuteReader()
        If dr.Read Then
            getValue = dr.Item(ReturnColumn)
            dr.Close()
        Else
            getValue = 0
        End If
        Return getValue
    End Function


    Public Function pecahdata(ByVal pecah As String, ByVal urutan As Integer) As String
        Dim pisahdata() As String
        Dim hasil As String
        pisahdata = Split(pecah, "-", , vbTextCompare)
        If UBound(pisahdata) > 0 Then
            hasil = pisahdata(urutan)
        Else
            hasil = pecah
        End If
        Return hasil
    End Function

    Public Function pecahdatadesimal(ByVal pecah As String, ByVal urutan As Integer) As String
        Dim pisahdata() As String
        Dim hasil As String
        pisahdata = Split(pecah, ",", , vbTextCompare)
        If UBound(pisahdata) > 0 Then
            hasil = pisahdata(urutan)
        Else
            hasil = pecah
        End If
        Return hasil
    End Function


    Public Function Isi(ByRef Teks As Object) As Boolean
        If Teks.ToString = "" Then
            Isi = False
            MsgBox("Data tidak boleh kosong, Silahkan cek lagi!", vbExclamation, "Peringatan")
            Exit Function
        Else
            Isi = True
        End If
    End Function

    Public Function Generate(ByVal xkode As String, ByVal Nama_Tabel As String, ByVal Nama_Field As String) As String
        dr.Close()
        SQL = ""
        SQL = "Select no_transaksi From " & Nama_Tabel & " where unitbisnis_id='" & Trim(frmKasInDetail.txtIdUnit.Text) & "' and substring(" & Nama_Field & ",2,4)='" & xytahun & "' and substring(" & Nama_Field & ",1,1)='" & xkode & "' order by " & Nama_Field & " desc"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()

        If dr.HasRows Then
            With dr.Item(1)
                VarStr = Val(Microsoft.VisualBasic.Right(dr.Item(1), 7)) + 1
                VarStr1 = "0000000" & Trim(Str(VarStr))
                VarStr2 = Microsoft.VisualBasic.Right(VarStr1, 7)
                Generate = VarStr2

            End With
        Else
            Generate = "0000001"
        End If
    End Function

    Public Function GenerateJurnal(ByVal xkode As String, ByVal Nama_Tabel As String, ByVal Nama_Field As String) As String
        dr.Close()
        SQL = ""
        SQL = "Select no_transaksi From " & Nama_Tabel & " where unitbisnis_id=" & xystrkdunit & " and substring(" & Nama_Field & ",31,2)='" & thnz & "' and substring(" & Nama_Field & ",1,3)='" & xkode & "' order by " & Nama_Field & " desc"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()

        If dr.HasRows Then
            With dr.Item(0)
                VarStr = Val(Microsoft.VisualBasic.Right(dr.Item(0), 6)) + 1
                VarStr1 = "000000" & Trim(Str(VarStr))
                VarStr2 = Microsoft.VisualBasic.Right(VarStr1, 6)
                GenerateJurnal = VarStr2

            End With
        Else
            GenerateJurnal = "000001"
        End If
    End Function


    Public Function GenerateJurnalUmum(ByVal xkode As String, ByVal Nama_Tabel As String, ByVal Nama_Field As String) As String

        dr.Close()
        SQL = ""
        SQL = "Select top 1 no_transaksi_kas From " & Nama_Tabel & " where unitbisnis_id=" & xystrkdunit & " and Year(tanggal)=" & thnzx & " and substring(no_transaksi_kas,1,3)='" & xkode & "' order by no_transaksi_kas desc"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()

        If dr.HasRows Then
            With dr.Item(0)
                VarStr = Val(Microsoft.VisualBasic.Right(dr.Item(0), 6)) + 1
                VarStr1 = "000000" & Trim(Str(VarStr))
                VarStr2 = Microsoft.VisualBasic.Right(VarStr1, 6)
                GenerateJurnalUmum = VarStr2

            End With
        Else
            GenerateJurnalUmum = "000001"
        End If
    End Function

    Public Function GenerateJurnalUmumBayarBeli(ByVal xkode As String, ByVal Nama_Tabel As String, ByVal Nama_Field As String) As String

        dr.Close()
        SQL = ""
        SQL = "Select top 1 no_transaksi_kwt From " & Nama_Tabel & " where cabang_id=" & xystrkdunit & " and Year(tgl_kuitansi)=" & thnzx & " and  Month(tgl_kuitansi) =" & blnzx & " and substring(no_transaksi_kwt,1,3)='" & xkode & "' order by no_transaksi_kwt desc"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()

        If dr.HasRows Then
            With dr.Item(0)
                VarStr = Val(Microsoft.VisualBasic.Right(dr.Item(0), 6)) + 1
                VarStr1 = "000000" & Trim(Str(VarStr))
                VarStr2 = Microsoft.VisualBasic.Right(VarStr1, 6)
                GenerateJurnalUmumBayarBeli = VarStr2

            End With
        Else
            GenerateJurnalUmumBayarBeli = "000001"
        End If
    End Function


    Public Function GenerateKasBankMasuk(ByVal xkode As String, ByVal Nama_Tabel As String, ByVal Nama_Field As String) As String

        dr.Close()
        SQL = ""
        SQL = "Select top 1 no_transaksi From " & Nama_Tabel & " where unitbisnis_id=" & xystrkdunit & " and Year(tgl_transaksi)=" & thnzx & " and substring(no_transaksi,1,3)='" & xkode & "' order by no_transaksi desc"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()

        If dr.HasRows Then
            With dr.Item(0)
                VarStr = Val(Microsoft.VisualBasic.Right(dr.Item(0), 6)) + 1
                VarStr1 = "000000" & Trim(Str(VarStr))
                VarStr2 = Microsoft.VisualBasic.Right(VarStr1, 6)
                GenerateKasBankMasuk = VarStr2

            End With
        Else
            GenerateKasBankMasuk = "000001"
        End If
    End Function


    Public Function statusx(ByVal strstatus As String) As String
        Select Case strstatus
            Case "OPEN"
                vstatusx = 0
            Case "CLOSED"
                vstatusx = 1
            Case "CANCEL"
                vstatusx = 2
        End Select
        statusx = vstatusx
    End Function

    Public Function unstatusx(ByVal strunstatus As String) As String
        Select Case strunstatus
            Case 0
                vunstatusx = "OPEN"
            Case 1
                vunstatusx = "CLOSED"
            Case 2
                vunstatusx = "CANCEL"
        End Select
        unstatusx = vunstatusx
    End Function

    Function TerbilangIndonesia(ByRef Indx As String) As String

        Dim satu(10) As String
        Dim dua(10) As String
        Dim tiga(10) As String
        Dim ratus As String
        Dim ribu As String
        Dim juta As String
        Dim Millyar As String
        Dim Trilliun As String

        satu(0) = "Nol" : satu(1) = "Satu" : satu(2) = "Dua" : satu(3) = "Tiga" : satu(4) = "Empat" : satu(5) = "Lima" : satu(6) = "Enam" : satu(7) = "Tujuh" : satu(8) = "Delapan" : satu(9) = "Sembilan"
        dua(0) = "Sepuluh" : dua(1) = "Sebelas" : dua(2) = "Dua belas" : dua(3) = "Tiga belas" : dua(4) = "Empat Belas" : dua(5) = "Lima Belas" : dua(6) = "Enam Belas" : dua(7) = "Tujuh belas" : dua(8) = "Delapan belas" : dua(9) = "Sembilan belas"
        tiga(2) = "Dua puluh" : tiga(3) = "Tiga puluh" : tiga(4) = "Empat puluh" : tiga(5) = "Lima puluh" : tiga(6) = "Enam Puluh" : tiga(7) = "Tujuh Puluh" : tiga(8) = "Delapan puluh" : tiga(9) = "Sembilan puluh"
        ratus = "ratus" : ribu = "ribu" : juta = "juta"
        Millyar = "millyar" : Trilliun = "trilliun"

        Dim inp, BhsNilai As String

        inp = CStr(Val(Indx))


        Select Case Len(inp)
            Case 1
                BhsNilai = satu(CInt(Indx))

            Case 2
                If Int(CDbl(VB.Right(inp, 1))) > 0 And CDbl(VB.Left(inp, 1)) > 1 Then BhsNilai = TerbilangIndonesia(CStr(Int(CDbl(VB.Right(inp, 1)))))
                If CDbl(VB.Left(inp, 1)) > 1 Then BhsNilai = tiga(CInt(VB.Left(inp, 1))) & BhsNilai
                If CDbl(VB.Left(inp, 1)) = 1 Then BhsNilai = dua(CInt(VB.Right(inp, 1)))
                BhsNilai = Replace(BhsNilai, "Satu ribu", "Seribu")

            Case 3
                BhsNilai = satu(10)
                If Int(CDbl(VB.Right(inp, 2))) > 0 Then BhsNilai = TerbilangIndonesia(CStr(Int(CDbl(VB.Right(inp, 2)))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 1)))) & ratus & BhsNilai

            Case 4
                If Int(CDbl(VB.Right(inp, 3))) > 0 Then BhsNilai = TerbilangIndonesia(CStr(Int(CDbl(VB.Right(inp, 3)))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 1)))) & ribu & BhsNilai
                BhsNilai = Replace(BhsNilai, "Satu ribu", "Seribu")

            Case 5
                If Int(CDbl(VB.Right(inp, 3))) > 0 Then BhsNilai = TerbilangIndonesia(CStr(Int(CDbl(VB.Right(inp, 3)))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 2)))) & ribu & BhsNilai

            Case 6
                If CInt(VB.Right(inp, 3)) > 0 Then BhsNilai = TerbilangIndonesia(CStr(CInt(VB.Right(inp, 3))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 3)))) & ribu & BhsNilai

            Case 7
                If CInt(VB.Right(inp, 6)) > 0 Then BhsNilai = TerbilangIndonesia(CStr(CInt(VB.Right(inp, 6))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 1)))) & juta & BhsNilai

            Case 8
                If CInt(VB.Right(inp, 6)) > 0 Then BhsNilai = TerbilangIndonesia(CStr(CInt(VB.Right(inp, 6))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 2)))) & juta & BhsNilai

            Case 9
                If CInt(VB.Right(inp, 6)) > 0 Then BhsNilai = TerbilangIndonesia(CStr(CInt(VB.Right(inp, 6))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 3)))) & juta & BhsNilai

            Case 10
                If CInt(VB.Right(inp, 9)) > 0 Then BhsNilai = TerbilangIndonesia(CStr(CInt(VB.Right(inp, 9))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 1)))) & Millyar & BhsNilai

            Case 11
                If CInt(VB.Right(inp, 9)) > 0 Then BhsNilai = TerbilangIndonesia(CStr(CInt(VB.Right(inp, 9))))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 2)))) & Millyar & BhsNilai

            Case 12
                If Val(VB.Right(inp, 9)) > 0 Then BhsNilai = TerbilangIndonesia(VB.Right(inp, 9))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 3)))) & Millyar & BhsNilai

            Case 13
                If Val(VB.Right(inp, 12)) > 0 Then BhsNilai = TerbilangIndonesia(VB.Right(inp, 12))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 1)))) & Trilliun & BhsNilai

            Case 14
                If Val(VB.Right(inp, 12)) > 0 Then BhsNilai = TerbilangIndonesia(VB.Right(inp, 12))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 2)))) & Trilliun & BhsNilai

            Case 15
                If Val(VB.Right(inp, 12)) > 0 Then BhsNilai = TerbilangIndonesia(VB.Right(inp, 12))
                BhsNilai = TerbilangIndonesia(Int(CDbl(VB.Left(inp, 3)))) & Trilliun & BhsNilai
        End Select

        BhsNilai = Replace(BhsNilai, "Satu ratus", "Seratus")
        BhsNilai = Trim(BhsNilai)

        TerbilangIndonesia = " " & BhsNilai & " "
        TerbilangIndonesia = Replace(TerbilangIndonesia, " ", " ")

    End Function

    Function BuatKomaSatu(ByVal desc As String) As String
        If Left(desc, 1) = "'" And Right(desc, 1) = "'" Then
            Dim Panjang As Integer
            Panjang = Len(desc) - 2
            BuatKomaSatu = "'" & Replace(Mid(desc, 2, Panjang), "'", "''") & "'"
        Else
            BuatKomaSatu = Replace(desc, "'", "''")
        End If
    End Function

    Function NullToStr(ByVal inputVar As Object) As String
        If Trim(inputVar & "") = "" Then
            NullToStr = ""
        Else
            NullToStr = "" & inputVar & ""
        End If
    End Function


    'txt_encrypt.text = EncryptText(txt_decrypt.text)
    Public Function EncryptText(ByVal SourceText As System.String) As System.String
        Dim MyKey As String = "Powered by F1KAR"
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim strResult As System.String = ""

        Try
            Dim bykey() As Byte = System.Text.Encoding.UTF8.GetBytes(Strings.Left(MyKey, 8))
            Dim InputByteArray() As Byte = System.Text.Encoding.UTF8.GetBytes(SourceText)
            Dim des As New DESCryptoServiceProvider
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write)
            cs.Write(InputByteArray, 0, InputByteArray.Length)
            cs.FlushFinalBlock()
            strResult = Convert.ToBase64String(ms.ToArray())
        Catch ex As Exception
            'Throw New Exception
            Return strResult
        End Try
        Return strResult
    End Function
    'txt_decrypt.text = DecryptText(txt_encrypt.text)
    Public Function DecryptText(ByVal Chippedtexta As System.String) As System.String
        Dim mykey As String = "Powered by F1KAR"
        Dim iv() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputbytearray(Chippedtexta.Length) As Byte
        Dim strresult As System.String
        Try
            Dim bykey() As Byte = System.Text.Encoding.UTF8.GetBytes(Strings.Left(mykey, 8))
            Dim des As New DESCryptoServiceProvider
            inputbytearray = Convert.FromBase64String(Chippedtexta)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(bykey, iv), CryptoStreamMode.Write)
            cs.Write(inputbytearray, 0, inputbytearray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            strresult = encoding.GetString(ms.ToArray())
            Return strresult
        Catch ex As Exception
            'Throw New Exception
            'MsgBox("File Config was Broken..!!")
            Return vbFalse
        End Try
    End Function

    'fungsi untuk write file .ini
    Private Declare Unicode Function WritePrivateProfileString Lib "kernel32" _
    Alias "WritePrivateProfileStringW" (ByVal lpSection As String, ByVal lpParamName As String, _
    ByVal lpParamVal As String, ByVal lpFileName As String) As Int32

    'procedure untuk write .ini
    Public Sub writeini(ByVal iniFilename As String, ByVal section As String, ByVal ParamName As String, ByVal ParamVal As String)
        'menanggil fungsi WritePrivateProfilString untuk write file .ini
        Dim result As Integer = WritePrivateProfileString(section, ParamName, ParamVal, iniFilename)
    End Sub

    'function untuk read file .ini
    Private Declare Unicode Function GetPrivateProfileString Lib "kernel32" _
    Alias "GetPrivateProfileStringW" (ByVal lpSection As String, ByVal lpParamName As String, _
    ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Int32, _
    ByVal lpFilename As String) As Int32

    'function untuk read file .ini
    Public Function readini(ByVal iniFileName As String, ByVal Section As String, ByVal ParamName As String, ByVal ParamDefault As String) As String
        Dim ParamVal As String = Space$(1024)
        Dim LenParamVal As Long = GetPrivateProfileString(Section, ParamName, ParamDefault, ParamVal, Len(ParamVal), iniFileName)
        'mengembalikan nilai yang sudah didapatkan
        readini = Strings.Left(ParamVal, LenParamVal)
    End Function

    Public Function SQLQry(ByVal Query As String, ByVal cmd As SqlCommand, ByVal conn As SqlConnection, Optional ByVal cmdTO As Integer = 0) As SqlClient.SqlCommand
        Try
            With cmd
                .Connection = Cn
                .CommandType = CommandType.Text
                .CommandTimeout = cmdTO
                .CommandText = Query
            End With
            SQLQry = cmd
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
            SQLQry = Nothing
        End Try
        Return SQLQry
    End Function

    'Public Sub ExecuteNonQuery(ByVal Query As String)

    '    Try
    '        With cmd
    '            .Connection = Cn
    '            .CommandType = CommandType.Text
    '            .CommandText = Query
    '            .ExecuteNonQuery()
    '        End With
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Public Sub IUDQuery(ByRef Query As String)
        Try
            With cmd
                .CommandText = Query
                .CommandType = CommandType.Text
                .Connection = Cn
                .ExecuteNonQuery()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Exception")
            MsgBox("Tidak dapat terhubung dengan database, aplikasi akan ditutup", MsgBoxStyle.Exclamation, "Tidak terhubung dengan database")
            'End
        End Try
    End Sub

    Public Sub IUDQuery2(ByRef Query As String)
        Try
            With Cmd
                .CommandText = Query
                .CommandType = CommandType.Text
                .Connection = Cn
                .ExecuteNonQuery()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Exception")
            MsgBox("Tidak dapat terhubung dengan database, aplikasi akan ditutup", MsgBoxStyle.Exclamation, "Tidak terhubung dengan database")
            'End
        End Try
    End Sub

    Public Function GetData(ByVal FileName As String) As List(Of String)
        Dim valueList As New List(Of String)
        Using cn As New OleDbConnection With
            {
                .ConnectionString = ConnectionString(FileName)
            }
            Using cmd As OleDbCommand = New OleDbCommand("SELECT bNumber FROM [Table_1$]", cn)
                cn.Open()
                Dim reader As OleDbDataReader = cmd.ExecuteReader
                While reader.Read
                    valueList.Add(reader.GetString(0))
                End While
            End Using
        End Using
        Return valueList
    End Function
    Public Function ConnectionString(ByVal FileName As String) As String
        Dim Builder As New OleDbConnectionStringBuilder
        If IO.Path.GetExtension(FileName).ToUpper = ".XLS" Then
            Builder.Provider = "Microsoft.Jet.OLEDB.4.0"
            Builder.Add("Extended Properties", "Excel 8.0;IMEX=1;HDR=Yes;")
        Else
            Builder.Provider = "Microsoft.ACE.OLEDB.12.0"
            Builder.Add("Extended Properties", "Excel 12.0;IMEX=1;HDR=Yes;")
        End If

        Builder.DataSource = FileName

        Return Builder.ConnectionString

    End Function
End Class

