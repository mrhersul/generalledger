﻿Module ModDeclare
    Public Tabel As DataTable
    Public VarStr, VarStr1, VarStr2, VarStr3 As String
    
    Public MyPass As String
    Public MyLevel As String
    Public myName As String
    Public myIDCabang As Long
    Public myStsKary As String
    Public myActive As Long
    Public myIDOrganisasi As Long
    Public myIDParent As Long
    Public myKdBisnis As String
    Public myNmBisnis As String
    Public myID As Long
    Public myIDSub As String
    Public xyInt As Integer
    Public strKB As Integer
    Public blnAdd As Boolean
    Public blnEdit As Boolean
    Public strFullName As String
    Public strDept As String
    Public strActive As String
    Public strUsername As String
    Public strPassword As String
    Public strKaryawan As String
    Public strLevel As String
    Public strNoBukti As String
    Public strDes As String
    Public strStatus As String
    Public pstatus As String
    Public strRemaks As String
    Public xDate
    Public xnominal As Double
    Public tglsystem
    Public strnik As String
    Public strsupp As String
    Public strcode As String
    Public strName As String
    Public strTitle As String
    Public strstoreID As String
    Public strstoredesc As String
    Public straddress As String
    Public strprovince As String
    Public strspvname As String
    Public strSKU As String
    Public strSKUDesc As String
    Public strUnit As String
    Public strCost As Double
    Public strCategory As String
    Public strKodeKategori As String
    Public svCategoryCode As String
    Public strSuppID As String
    Public strKodeSupp As String
    Public svKodeSupp As String
    Public Cursor = Cursors.WaitCursor
    Public xyDept As String
    Public chkQty As Double
    Public pstrsupp As String
    Public myUser As String
    Public strID As String
    Public blnUnitKasOut As Boolean
    Public blnUnitBankOut As Boolean
    Public blnUnitKasIn As Boolean
    Public blnUnitBankIn As Boolean
    Public blnCoaBankIn As Boolean
    Public blnCoaBankOut As Boolean
    Public blnCoaKasIn As Boolean
    Public blnCoaKasOut As Boolean
    Public blnCoaGridKasIn As Boolean
    Public blnCoaGridKasOut As Boolean
    Public blnCoaGridBankIn As Boolean
    Public blnCoaGridBankOut As Boolean
    Public blnUnitRptNeraca As Boolean
    Public blnUnitRptNeraca2 As Boolean
    Public blnUnitRptRL As Boolean
    Public blnParent As Boolean
    Public blnCoaGridJurnalPiutang As Boolean
    Public blnUnitCLosing As Boolean
    Public blnUnitUnCLosing As Boolean
    Public strNoTransaksi As String
    Public ds1 As New DataSetGL
    Public xyzID As Long
    Public svID As Integer
    Public svCoaKode As String
    Public svTotal As Double
    Public svKet As String
    Public vstatusx As Integer
    Public vunstatusx As String
    Public vbln As String
    Public svConvertion As Single
    Public svUnitSmall As String
    Public svQtySmall As Single
    Public svCostSmall
    Public svTotalSmall
    Public strKodeJenis As String
    Public svJenisCode As String
    Public blngroup As Boolean
    Public blnunitbisnis As Boolean
    Public blnUnitTransfer As Boolean
    Public blnCoaTransfer As Boolean
    Public blnCoaGridTransfer As Boolean
    Public strcari As String
    Public blnUnitJurnal As Boolean
    Public blnCoaGridJurnal As Boolean
    Public blnUnitSesuai As Boolean
    Public blnCoaGridSesuai As Boolean
    Public blnCoaSetting As Boolean
    Public svDebet As Double
    Public svCredit As Double
    Public thnz As String
    Public blnz As String
    Public tglz As String
    Public blnzx As Integer
    Public thnzx As Long
    Public myIDUnit As String
    Public xystrkdunit As Long
    Public xyunitID As Integer
    Public blnbayarbeli As Boolean
    Public blnbayarjual As Boolean
    Public strNoTransaksi2 As String
    Public strCallPhoto As String
    Public dscoa As New DataAkun
    Public dscoa2 As New DataAkunDua
    Public dscoa3 As New DataSetJurnal
    Public dscoa_aktiva As New DataAkunAktiva
    Public blnfrmCoa As Boolean
    Public xyzNotrans As String
    Public stsposting As Integer
    Public xyzCoa As String
    Public xyzNoTransaksi As String
    Public blnMutasi As Boolean

    Public fcoag As New frmGroupCoaBrowse
    Public fcoab As New frmCoaBrowse
    Public fcib As New frmKasBankTransferBrowse
    Public fkib As New frmKasInBrowse
    Public fkob As New frmKasOutBrowse
    Public fbib As New frmBankInBrowse
    Public fbob As New frmBankOutBrowse
    Public fjub As New frmJurnalUmumBrowse
    Public fjsb As New frmJurnalSesuaiBrowse
    Public fscb As New frmSettingCoaBrowse
    Public fscp As New frmSettingCoaprint
    Public frj As New frmRekapJurnal

    Public blnRptKasMasuk As Boolean
    Public blnRptKasKeluar As Boolean
    Public blnRptBankMasuk As Boolean
    Public blnRptBankKeluar As Boolean

End Module
