﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Module ModMain
    'Public Konek As New ModFuction
    'Public db_string As String
    'Public SQL As String
    'Public Cn As SqlClient.SqlConnection
    'Public cmd, cmdS As New SqlClient.SqlCommand
    'Public dr, drLoc As SqlClient.SqlDataReader
    Public BeginCommit, BeginCommitLoc As SqlClient.SqlTransaction
    'Public Da As SqlClient.SqlDataAdapter
    ''Public cmdBld As NpgsqlCommandBuilder
    'Public daLoc As SqlClient.SqlDataAdapter
    ''Public cmdBldLoc As NpgsqlCommandBuilder
    'Public transaction As SqlClient.SqlTransaction

    Public xytahun
    Public Konek As New ModFuction
    Public SQL As String
    Public Cn As SqlClient.SqlConnection
    Public Cmd As SqlClient.SqlCommand
    Public Cmd1 As SqlClient.SqlCommand
    Public Da As SqlClient.SqlDataAdapter
    Public Ds As DataSet
    Public Dt As DataTable
    Public Dt0 As DataTable
    Public Dt1 As DataTable
    Public Dt2 As DataTable
    Public Dt3 As DataTable
    Public dr As SqlClient.SqlDataReader
    Public dr0 As SqlClient.SqlDataReader
    Public dr1 As SqlClient.SqlDataReader
    Public dr2 As SqlClient.SqlDataReader
    Public dr3 As SqlClient.SqlDataReader
    Public dr4 As SqlClient.SqlDataReader
    Public myTrans As SqlTransaction
    Public draktiva1 As SqlClient.SqlDataReader
    Public Dtaktiva1 As DataTable
    Public draktiva2 As SqlClient.SqlDataReader
    Public Dtaktiva2 As DataTable
    Public draktiva3 As SqlClient.SqlDataReader
    Public Dtaktiva3 As DataTable

    Public Function OpenConn() As Boolean
        Dim FilePath As String = Application.StartupPath & "\setting.ini"
        Dim FileName As String = System.IO.Path.GetFileName(FilePath)
        Dim decServerName As String
        Dim decLogin As String
        Dim decPass As String
        Dim decData As String

        If System.IO.File.Exists(FileName) Then
            Try

                decServerName = Konek.readini(FilePath, "Koneksi Local", "ServerName", "")
                decLogin = Konek.readini(FilePath, "Koneksi Local", "Login", "")
                decPass = Konek.readini(FilePath, "Koneksi Local", "Password", "")
                decData = Konek.readini(FilePath, "Koneksi Local", "Database", "")

                ' Koneksi Local
                Cn = New SqlClient.SqlConnection( _
                    "server='" & Konek.DecryptText(decServerName) & "';database='" & Konek.DecryptText(decData) & "';user id='" & Konek.DecryptText(decLogin) & "';password='" & _
                    Konek.DecryptText(decPass) & "';multipleactiveresultsets=true")
                Cn.Open()
                If Cn.State <> ConnectionState.Open Then
                    Return False
                Else
                    Return True
                End If

            Catch
                Return False
            End Try
        Else
            Return False
        End If
    End Function



End Module