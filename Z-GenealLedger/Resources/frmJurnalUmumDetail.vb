﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmJurnalUmumDetail
    Public curcol, currow As Integer
    Dim strIDHdr As Integer
    Dim strTipe As String
    Dim svIdKar As Long
    Dim svIdPersonal As Long
    Dim strTipeTransaksi As String
    Dim svKodeTrans As String
    Dim xynominal As Double
    Dim xydk As String
    Dim xyceknominal As Double

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub frmJurnalUmumDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DGView.Rows.Clear()
        ClearData()

        If blnAdd Then
            txtkdUnit.Text = myKdBisnis
            txtNamaUnit.Text = myNmBisnis
            txtIdUnit.Text = myIDUnit
            txtDate.Value = Now
            BtnSave.Enabled = True
            DGView.Rows.Add()
        Else
            If blnEdit Then
                BtnSave.Enabled = True
            Else
                BtnSave.Enabled = False
            End If
            ShowData()
        End If

        SettingGrid()
    End Sub

    Sub ClearData()
        txtNoTrans.Text = ""
        txtIDHdr.Text = "'"
        txtkdUnit.Text = ""
        txtNamaUnit.Text = ""
        txtIdUnit.Text = ""
        txtKet.Text = ""
        txtDebet.Text = 0
        txtCredit.Text = 0
        txtBalance.Text = 0
        txtKet.Text = ""
        cboTipe1.Text = ""
        cboTipe2.Text = ""
    End Sub

    Sub SettingGrid()
        DGView.Columns(0).ReadOnly = False
        DGView.Columns(2).ReadOnly = True
        DGView.Columns(3).ReadOnly = False
        DGView.Columns(4).ReadOnly = False
        DGView.Columns(5).ReadOnly = False
    End Sub

    Sub ShowData()

        SQL = ""
        SQL = " select a.id,a.no_transaksi,a.tgl_transaksi,a.unitbisnis_id,c.kd_unitbisnis,c.nama_unitbisnis,a.tipe_transaksi,a.tipe_transaksi2,"
        SQL = SQL & " a.ttl_debet,a.ttl_credit,a.keterangan,a.confirm,b.acc_kode as acc_codedtl,"
        SQL = SQL & " case b.acc_kode when 6 then f.kd_karyawan COLLATE Latin1_General_CI_AS else e.kode COLLATE Latin1_General_CI_AS end as kodedtl,"
        SQL = SQL & " case b.acc_kode when 6 then g.nama_lengkap COLLATE Latin1_General_CI_AS else e.nama COLLATE Latin1_General_CI_AS end as namadtl,"
        SQL = SQL & " e.type_perkiraan, b.debet, b.credit, b.karyawan_id, b.karyawan_personal_id"
        SQL = SQL & " from acc_jurnal_hd a inner join acc_jurnal_dt b on a.id=b.header_id"
        SQL = SQL & " left join unitbisnis_nama c on a.unitbisnis_id=c.id "
        SQL = SQL & " left join acc_perkiraan_nama e on b.acc_kode=e.id "
        SQL = SQL & " left join karyawan_mutasi_organisasi f on b.karyawan_id=f.karyawan_id and f.acc_piutang_usaha_id=b.acc_kode "
        SQL = SQL & " left join personal g on b.karyawan_personal_id = g.id "
        SQL = SQL & " where a.id='" & Trim(strID) & "'"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read
                txtNoTrans.Text = Trim(dr.Item("no_transaksi").ToString())
                txtIDHdr.Text = Trim(dr.Item("id").ToString())
                txtDate.Value = dr.Item("tgl_transaksi").ToString()
                txtkdUnit.Text = Trim(dr.Item("kd_unitbisnis").ToString())
                txtNamaUnit.Text = Trim(dr.Item("nama_unitbisnis").ToString())
                txtIdUnit.Text = Trim(dr.Item("unitbisnis_id").ToString())
                txtKet.Text = Trim(dr.Item("keterangan").ToString())
                txtDebet.Text = Format(CDbl(dr.Item("ttl_debet").ToString()), "N2")
                txtCredit.Text = Format(CDbl(dr.Item("ttl_credit").ToString()), "N2")
                txtBalance.Text = Format(CDbl(txtDebet.Text - txtCredit.Text), "N2")
                cboTipe1.Text = Trim(dr.Item("tipe_transaksi").ToString())
                cboTipe2.Text = Trim(dr.Item("tipe_transaksi2").ToString())

                xyceknominal = dr.Item("debet").ToString
                If xyceknominal <> 0 Then
                    xynominal = dr.Item("debet").ToString
                    xydk = "D"
                Else
                    xynominal = dr.Item("credit").ToString
                    xydk = "K"
                End If

                DGView.Rows.Add(dr.Item("kodedtl").ToString, "", dr.Item("namadtl").ToString, xydk, xynominal, "", dr.Item("acc_codedtl").ToString, dr.Item("karyawan_id").ToString, dr.Item("karyawan_personal_id").ToString)

            End While
        End If
        DGView.Rows.Add()

        hitungtotal()

        dr.Close()

    End Sub


    Private Sub DGView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellClick
        On Error Resume Next
        If DGView.Columns(e.ColumnIndex).HeaderText = "FIND" Then
            frmCombo.ShowDialog()
        ElseIf DGView.Columns(e.ColumnIndex).HeaderText = "ACTION" Then
            DGView.Rows.Remove(DGView.CurrentRow)
            hitungtotal()
        End If
    End Sub

    Private Sub DGView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGView.CellFormatting
        If e.ColumnIndex = 4 AndAlso e.RowIndex <> DGView.NewRowIndex Then
            e.Value = CDec(e.Value).ToString("N2")
            e.FormattingApplied = True
        End If
       
    End Sub

    Private Sub DGView_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.CurrentCellChanged
        Try
            curcol = DGView.CurrentCell.ColumnIndex
            currow = DGView.CurrentCell.RowIndex
        Catch ex As Exception
            curcol = 0
            currow = 0
        End Try
    End Sub

    Private Sub DGView_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles DGView.EditingControlShowing

        'RemoveHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress

        If DGView.CurrentCell.ColumnIndex = 4 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress
        End If


        If DGView.CurrentCell.ColumnIndex = 3 Then
            Dim combo As ComboBox = CType(e.Control, ComboBox)
            If (combo IsNot Nothing) Then
                ' Remove an existing event-handler, if present, to avoid 
                ' adding multiple handlers when the editing control is reused.
                RemoveHandler combo.SelectionChangeCommitted, New EventHandler(AddressOf ComboBox_SelectionChangeCommitted)

                ' Add the event handler. 
                AddHandler combo.SelectionChangeCommitted, New EventHandler(AddressOf ComboBox_SelectionChangeCommitted)
            End If
        End If

    End Sub

    Private Sub TextBox_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

        If (Not Char.IsControl(e.KeyChar) _
                   AndAlso (Not Char.IsDigit(e.KeyChar) _
                   AndAlso (e.KeyChar <> Microsoft.VisualBasic.ChrW(46)))) Then
            e.Handled = True
        End If

    End Sub

    Private Sub DGView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGView.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                DGView.ClearSelection()
                Try
                    If curcol = 0 Then
                        If blnCoaGridJurnal = True Then
                            SQL = "select * from acc_perkiraan_nama where kode='" & Trim(DGView.Item(0, currow).Value) & "'"
                            Cmd = New SqlClient.SqlCommand(SQL, Cn)
                            dr = Cmd.ExecuteReader()
                        Else
                            MsgBox("Silakan Klik Tombol FIND", vbInformation)
                            Exit Sub
                        End If

                        If dr.HasRows Then
                            With DGView
                                While dr.Read
                                    .Item(2, currow).Value = Trim(dr.Item("nama").ToString())
                                    .Item(6, currow).Value = Trim(dr.Item("id").ToString())
                                End While
                                DGView.CurrentCell = DGView(3, currow)
                            End With
                            dr.Close()
                            Exit Sub
                        Else
                            MsgBox("Data dengan kode tsb tidak ada", vbInformation)
                            DGView.CurrentCell = DGView(0, currow)
                            Exit Sub
                        End If

                    ElseIf curcol = 3 Then
                        hitungtotal()
                        DGView.CurrentCell = DGView(4, currow)
                    ElseIf curcol = 4 Then
                        DGView.Rows.Add()
                        hitungtotal()
                        DGView.CurrentCell = DGView(0, currow + 1)
                    End If

                Catch ex As Exception
                    Exit Try
                End Try
        End Select
    End Sub

    Sub hitungtotal()
        Dim tdebet As Double = 0
        Dim tcredit As Double = 0
        txtDebet.Text = 0
        txtCredit.Text = 0

        For t As Integer = 0 To DGView.Rows.Count - 1
            If DGView.Rows(t).Cells(3).Value = "D" Then
                tdebet = tdebet + (DGView.Rows(t).Cells(4).Value)
            Else
                tcredit = tcredit + (DGView.Rows(t).Cells(4).Value)
            End If
        Next

        txtDebet.Text = Format(tdebet, "N2")
        txtCredit.Text = Format(tcredit, "N2")
        txtBalance.Text = Format(CDbl(txtDebet.Text - txtCredit.Text), "N")
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        blnz = txtDate.Value.ToString("MM")
        thnz = txtDate.Value.ToString("yy")
        xystrkdunit = txtIdUnit.Text

        blnzx = Month(txtDate.Value)
        thnzx = Year(txtDate.Value)

        '=== Dimatikan sementara tunggu konfirmasi prioritas
        'SQL = ""
        'SQL = " Select * From acc_periode_setting Where id_unitbisnis = " & txtIdUnit.Text & " and datediff(month,tgl_berjalan,'" & Format(txtDate.Value, "yyyy-MM-dd") & "') >= 0"

        'Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
        'dr1 = Cmd1.ExecuteReader()

        'If dr1.HasRows Then
        '    dr1.Close()
        'Else
        '    MsgBox("Bulan yang anda input sudah di closing, Silakan ganti tangal menjadi bulan berjalan", vbInformation)
        '    dr1.Close()
        '    Exit Sub
        'End If


        If txtBalance.Text <> 0 Or txtBalance.Text = "" Then
            MsgBox("Total Kredit dan Debet Belum Sama, Silakan Cek Kembali", vbInformation)
            Exit Sub
        End If

        If txtkdUnit.Text = "" Or txtDebet.Text = "" Or txtCredit.Text = "" Then
            MsgBox("Masih Ada Data Yang Kosong...", MsgBoxStyle.Critical)
            Exit Sub
        End If


        If blnAdd = True Then

            IsConError = True
            dr.Close()
            SQL = "Insert Into acc_jurnal_hd (confirm,tipe_transaksi,no_transaksi,tgl_transaksi,unitbisnis_id,unitbisnis_level,ttl_debet,ttl_credit,tanggal_buat,user_buat,keterangan,status,is_trash,tipe_transaksi2)"
            SQL = SQL & " Values(0,'" & cboTipe1.Text & "','" & txtNoTrans.Text & "','" & Format(txtDate.Value, "yyyy-MM-dd") & "'," & txtIdUnit.Text & ",0,"
            SQL = SQL & " " & CDbl(txtDebet.Text) & "," & CDbl(txtCredit.Text) & ",'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "','" & myID & "','" & Konek.BuatKomaSatu(txtKet.Text) & "',0,0,'" & cboTipe2.Text & "')"
            Konek.IUDQuery(SQL)


            SQL = ""
            SQL = "Select * From acc_jurnal_hd Where no_transaksi = '" & txtNoTrans.Text & "'"

            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()

            If dr.HasRows Then

                While dr.Read
                    strIDHdr = Trim(dr.Item("id").ToString())
                End While
                dr.Close()
            End If


        ElseIf blnEdit = True Then
            IsConError = True

            strIDHdr = txtIDHdr.Text

            dr.Close()
            SQL = "update acc_jurnal_hd set tgl_transaksi='" & Format(txtDate.Value, "yyyy-MM-dd HH:mm:ss") & "',ttl_debet=" & CDbl(txtDebet.Text) & ",ttl_credit=" & CDbl(txtCredit.Text) & ","
            SQL = SQL & " keterangan='" & Konek.BuatKomaSatu(txtKet.Text) & "',tanggal_ubah='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',user_ubah=" & myID & ""
            SQL = SQL & " where no_transaksi='" & Trim(txtNoTrans.Text) & "'"
            Konek.IUDQuery(SQL)


            dr.Close()
            SQL = " delete from acc_jurnal_dt where header_id='" & strIDHdr & "'"
            Konek.IUDQuery(SQL)

            dr.Close()
            SQL = " delete from acc_jurnal where no_transaksi_kas='" & txtNoTrans.Text & "'"
            Konek.IUDQuery(SQL)


        End If


        For x As Integer = 0 To DGView.Rows.Count - 1
            If DGView.Rows(x).Cells(0).Value <> "" Then
                svID = DGView.Rows(x).Cells(6).Value

                If DGView.Rows(x).Cells(3).Value = "D" Then
                    svDebet = DGView.Rows(x).Cells(4).Value
                    svCredit = 0
                ElseIf DGView.Rows(x).Cells(3).Value = "K" Then
                    svDebet = 0
                    svCredit = DGView.Rows(x).Cells(4).Value
                End If

                svIdKar = DGView.Rows(x).Cells(7).Value
                svIdPersonal = DGView.Rows(x).Cells(8).Value

                'Menyimpan Data Ke Tabel Detail
                dr.Close()
                SQL = "Insert Into acc_jurnal_dt (header_id,acc_kode,debet,credit,tanggal_buat,user_buat,status,is_trash,karyawan_id,karyawan_personal_id) " & _
                     "Values(" & strIDHdr & "," & svID & "," & svDebet & "," & svCredit & ",'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",0,0," & svIdKar & "," & svIdPersonal & ")"
                Konek.IUDQuery(SQL)


                dr.Close()
                SQL = ""
                SQL = "Insert Into acc_jurnal (transaksi_id,jenis,tanggal,unitbisnis_id,acc_coa_id,nilai_total,nilai_debet,nilai_kredit,"
                SQL = SQL & " no_urut,keterangan,status,is_trash,tanggal_buat,user_buat,posting,no_transaksi_kas) "
                SQL = SQL & " Values(" & strIDHdr & ",'" & cboTipe1.Text & "','" & Format(txtDate.Value, "yyyy-MM-dd") & "'," & txtIdUnit.Text & "," & svID & "," & CDbl(txtBalance.Text) & ","
                SQL = SQL & " " & svDebet & "," & svCredit & ",0,'" & Konek.BuatKomaSatu(txtKet.Text) & "',0,0,'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",0,'" & txtNoTrans.Text & "')"
                Konek.IUDQuery(SQL)

            End If
        Next


        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        blnAdd = False
        blnEdit = False

        Me.Close()
        Exit Sub

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitJurnal = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub cboTipe1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipe1.SelectedIndexChanged
        If blnAdd Then
            cboTipe2.Items.Clear()
            cboTipe2.Text = ""
            Select Case cboTipe1.SelectedIndex
                Case 0
                    cboTipe2.Items.Add("KAS MASUK CABANG")
                    cboTipe2.Items.Add("KAS MASUK PERANTARA")
                Case 1
                    cboTipe2.Items.Add("KAS KELUAR CABANG")
                    cboTipe2.Items.Add("KAS KELUAR PERANTARA")
                Case 2
                    cboTipe2.Items.Add("BANK MASUK")
                    cboTipe2.Text = "BANK MASUK"
                Case 3
                    cboTipe2.Items.Add("BANK KELUAR")
                    cboTipe2.Text = "BANK KELUAR"
            End Select
        End If
    End Sub

    Private Sub cboTipe2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipe2.SelectedIndexChanged

        Select Case cboTipe2.Text
            Case "KAS MASUK CABANG"
                svKodeTrans = "KMC"
            Case "KAS MASUK PERANTARA"
                svKodeTrans = "KMP"
            Case "KAS KELUAR CABANG"
                svKodeTrans = "KKC"
            Case "KAS KELUAR PERANTARA"
                svKodeTrans = "KKP"
            Case "BANK MASUK"
                svKodeTrans = "BM."
            Case "BANK KELUAR"
                svKodeTrans = "BK."
        End Select

        CallNoTransaksi()
  
    End Sub

    Sub CallNoTransaksi()
        blnz = txtDate.Value.ToString("MM")
        thnz = txtDate.Value.ToString("yy")
        tglz = txtDate.Value.ToString("dd")
        xystrkdunit = txtIdUnit.Text

        blnzx = Month(txtDate.Value)
        thnzx = Year(txtDate.Value)

        strNoTransaksi = svKodeTrans & "/" & txtkdUnit.Text & "/" & thnz & "/" & blnz & "/" & tglz & "/" & Konek.GenerateJurnalUmum(svKodeTrans, "acc_jurnal_hd", "tgl_transaksi") & ""
        txtNoTrans.Text = strNoTransaksi

    End Sub

    Private Sub ComboBox_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim combo As ComboBox = CType(sender, ComboBox)
        Console.WriteLine("Row: {0}, Value: {1}", DGView.CurrentCell.RowIndex, combo.SelectedItem)
    End Sub

    Private Sub DGView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellContentClick

    End Sub

    Private Sub DGView_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles DGView.CellPainting
        If e.ColumnIndex = 1 AndAlso e.RowIndex >= 0 Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            e.Graphics.DrawImage(My.Resources.find, CInt((e.CellBounds.Width / 2) - (My.Resources.find.Width / 2)) + e.CellBounds.X, CInt((e.CellBounds.Height / 2) - (My.Resources.find.Height / 2)) + e.CellBounds.Y)
            e.Handled = True
        End If
    End Sub
End Class