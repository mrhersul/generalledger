﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInformasiKaryawan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInformasiKaryawan))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TextBoxX6 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX5 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX4 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX3 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TextBoxX19 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX18 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX17 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX16 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX15 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX14 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX13 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX12 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX11 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX10 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX9 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX8 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX7 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TextBoxX27 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX31 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX26 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX30 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX25 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX29 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX24 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX28 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX23 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX22 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX21 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX20 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.TextBoxX32 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX37 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX31 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX36 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX30 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX35 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX34 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX28 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX33 = New DevComponents.DotNetBar.LabelX()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.LabelX32 = New DevComponents.DotNetBar.LabelX()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker()
        Me.TextBoxX29 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX39 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX33 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX40 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX34 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX41 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX42 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX35 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX43 = New DevComponents.DotNetBar.LabelX()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.LabelX38 = New DevComponents.DotNetBar.LabelX()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TextBoxX41 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextBoxX40 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX50 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX36 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX45 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX37 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX46 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX38 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX47 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX48 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX39 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX49 = New DevComponents.DotNetBar.LabelX()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.LabelX44 = New DevComponents.DotNetBar.LabelX()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.LabelX60 = New DevComponents.DotNetBar.LabelX()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.LabelX59 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX45 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextBoxX46 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX56 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX57 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX47 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX58 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX42 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextBoxX43 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX53 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX54 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX44 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX55 = New DevComponents.DotNetBar.LabelX()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.LabelX51 = New DevComponents.DotNetBar.LabelX()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.LabelX52 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.TextBoxX6)
        Me.GroupBox1.Controls.Add(Me.LabelX7)
        Me.GroupBox1.Controls.Add(Me.TextBoxX5)
        Me.GroupBox1.Controls.Add(Me.LabelX6)
        Me.GroupBox1.Controls.Add(Me.TextBoxX4)
        Me.GroupBox1.Controls.Add(Me.LabelX5)
        Me.GroupBox1.Controls.Add(Me.TextBoxX3)
        Me.GroupBox1.Controls.Add(Me.LabelX4)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.LabelX3)
        Me.GroupBox1.Controls.Add(Me.TextBoxX2)
        Me.GroupBox1.Controls.Add(Me.LabelX1)
        Me.GroupBox1.Controls.Add(Me.TextBoxX1)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(985, 328)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "INFORMASI KARYAWAN"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(720, 38)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(206, 254)
        Me.PictureBox1.TabIndex = 162
        Me.PictureBox1.TabStop = False
        '
        'TextBoxX6
        '
        '
        '
        '
        Me.TextBoxX6.Border.Class = "TextBoxBorder"
        Me.TextBoxX6.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX6.Location = New System.Drawing.Point(181, 278)
        Me.TextBoxX6.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX6.Name = "TextBoxX6"
        Me.TextBoxX6.Size = New System.Drawing.Size(191, 29)
        Me.TextBoxX6.TabIndex = 161
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(21, 273)
        Me.LabelX7.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(152, 28)
        Me.LabelX7.TabIndex = 160
        Me.LabelX7.Text = "STATUS"
        '
        'TextBoxX5
        '
        '
        '
        '
        Me.TextBoxX5.Border.Class = "TextBoxBorder"
        Me.TextBoxX5.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX5.Location = New System.Drawing.Point(182, 241)
        Me.TextBoxX5.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX5.Name = "TextBoxX5"
        Me.TextBoxX5.Size = New System.Drawing.Size(433, 29)
        Me.TextBoxX5.TabIndex = 159
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(22, 237)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(152, 28)
        Me.LabelX6.TabIndex = 158
        Me.LabelX6.Text = "JABATAN"
        '
        'TextBoxX4
        '
        '
        '
        '
        Me.TextBoxX4.Border.Class = "TextBoxBorder"
        Me.TextBoxX4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX4.Location = New System.Drawing.Point(181, 204)
        Me.TextBoxX4.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX4.Name = "TextBoxX4"
        Me.TextBoxX4.Size = New System.Drawing.Size(433, 29)
        Me.TextBoxX4.TabIndex = 157
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(22, 201)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(152, 28)
        Me.LabelX5.TabIndex = 156
        Me.LabelX5.Text = "DEPARTEMEN"
        '
        'TextBoxX3
        '
        '
        '
        '
        Me.TextBoxX3.Border.Class = "TextBoxBorder"
        Me.TextBoxX3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX3.Location = New System.Drawing.Point(181, 167)
        Me.TextBoxX3.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX3.Name = "TextBoxX3"
        Me.TextBoxX3.Size = New System.Drawing.Size(433, 29)
        Me.TextBoxX3.TabIndex = 155
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(21, 165)
        Me.LabelX4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(152, 28)
        Me.LabelX4.TabIndex = 154
        Me.LabelX4.Text = "UNIT BISNIS"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(181, 129)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 29)
        Me.DateTimePicker1.TabIndex = 153
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(22, 129)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(152, 28)
        Me.LabelX3.TabIndex = 152
        Me.LabelX3.Text = "TGL GABUNG"
        '
        'TextBoxX2
        '
        '
        '
        '
        Me.TextBoxX2.Border.Class = "TextBoxBorder"
        Me.TextBoxX2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX2.Location = New System.Drawing.Point(181, 84)
        Me.TextBoxX2.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX2.Name = "TextBoxX2"
        Me.TextBoxX2.Size = New System.Drawing.Size(433, 29)
        Me.TextBoxX2.TabIndex = 151
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(21, 84)
        Me.LabelX1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(152, 28)
        Me.LabelX1.TabIndex = 150
        Me.LabelX1.Text = "KODE KARYAWAN"
        '
        'TextBoxX1
        '
        '
        '
        '
        Me.TextBoxX1.Border.Class = "TextBoxBorder"
        Me.TextBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX1.Location = New System.Drawing.Point(181, 47)
        Me.TextBoxX1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX1.Name = "TextBoxX1"
        Me.TextBoxX1.Size = New System.Drawing.Size(433, 29)
        Me.TextBoxX1.TabIndex = 149
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(21, 47)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(152, 28)
        Me.LabelX2.TabIndex = 148
        Me.LabelX2.Text = "NAMA KARYAWAN"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(1003, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(405, 328)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ACTION"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Red
        Me.Button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.Location = New System.Drawing.Point(25, 207)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(354, 58)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "RESIGN"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(25, 135)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(354, 58)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "KARIR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button1.Location = New System.Drawing.Point(25, 64)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(354, 58)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "CATATAN KARYAWAN"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 360)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1396, 431)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.TextBoxX19)
        Me.TabPage1.Controls.Add(Me.LabelX22)
        Me.TabPage1.Controls.Add(Me.TextBoxX18)
        Me.TabPage1.Controls.Add(Me.LabelX21)
        Me.TabPage1.Controls.Add(Me.TextBoxX17)
        Me.TabPage1.Controls.Add(Me.LabelX20)
        Me.TabPage1.Controls.Add(Me.TextBoxX16)
        Me.TabPage1.Controls.Add(Me.LabelX19)
        Me.TabPage1.Controls.Add(Me.TextBoxX15)
        Me.TabPage1.Controls.Add(Me.LabelX18)
        Me.TabPage1.Controls.Add(Me.TextBoxX14)
        Me.TabPage1.Controls.Add(Me.LabelX17)
        Me.TabPage1.Controls.Add(Me.TextBoxX13)
        Me.TabPage1.Controls.Add(Me.LabelX16)
        Me.TabPage1.Controls.Add(Me.TextBoxX12)
        Me.TabPage1.Controls.Add(Me.LabelX15)
        Me.TabPage1.Controls.Add(Me.TextBoxX11)
        Me.TabPage1.Controls.Add(Me.LabelX14)
        Me.TabPage1.Controls.Add(Me.TextBoxX10)
        Me.TabPage1.Controls.Add(Me.LabelX13)
        Me.TabPage1.Controls.Add(Me.TextBoxX9)
        Me.TabPage1.Controls.Add(Me.LabelX12)
        Me.TabPage1.Controls.Add(Me.TextBoxX8)
        Me.TabPage1.Controls.Add(Me.LabelX11)
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.TextBoxX7)
        Me.TabPage1.Controls.Add(Me.LabelX8)
        Me.TabPage1.Location = New System.Drawing.Point(4, 30)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1388, 397)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "PERSONAL"
        '
        'TextBoxX19
        '
        '
        '
        '
        Me.TextBoxX19.Border.Class = "TextBoxBorder"
        Me.TextBoxX19.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX19.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX19.Location = New System.Drawing.Point(876, 179)
        Me.TextBoxX19.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX19.Name = "TextBoxX19"
        Me.TextBoxX19.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX19.TabIndex = 190
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(716, 179)
        Me.LabelX22.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(152, 28)
        Me.LabelX22.TabIndex = 189
        Me.LabelX22.Text = "EMAIL 2"
        '
        'TextBoxX18
        '
        '
        '
        '
        Me.TextBoxX18.Border.Class = "TextBoxBorder"
        Me.TextBoxX18.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX18.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX18.Location = New System.Drawing.Point(876, 143)
        Me.TextBoxX18.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX18.Name = "TextBoxX18"
        Me.TextBoxX18.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX18.TabIndex = 188
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX21.ForeColor = System.Drawing.Color.Black
        Me.LabelX21.Location = New System.Drawing.Point(716, 143)
        Me.LabelX21.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(152, 28)
        Me.LabelX21.TabIndex = 187
        Me.LabelX21.Text = "EMAIL"
        '
        'TextBoxX17
        '
        '
        '
        '
        Me.TextBoxX17.Border.Class = "TextBoxBorder"
        Me.TextBoxX17.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX17.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX17.Location = New System.Drawing.Point(876, 107)
        Me.TextBoxX17.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX17.Name = "TextBoxX17"
        Me.TextBoxX17.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX17.TabIndex = 186
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(716, 107)
        Me.LabelX20.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(152, 28)
        Me.LabelX20.TabIndex = 185
        Me.LabelX20.Text = "NOMOR TELEPON 2"
        '
        'TextBoxX16
        '
        '
        '
        '
        Me.TextBoxX16.Border.Class = "TextBoxBorder"
        Me.TextBoxX16.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX16.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX16.Location = New System.Drawing.Point(876, 69)
        Me.TextBoxX16.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX16.Name = "TextBoxX16"
        Me.TextBoxX16.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX16.TabIndex = 184
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(716, 69)
        Me.LabelX19.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(152, 28)
        Me.LabelX19.TabIndex = 183
        Me.LabelX19.Text = "NOMOR TELEPON"
        '
        'TextBoxX15
        '
        '
        '
        '
        Me.TextBoxX15.Border.Class = "TextBoxBorder"
        Me.TextBoxX15.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX15.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX15.Location = New System.Drawing.Point(221, 357)
        Me.TextBoxX15.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX15.Name = "TextBoxX15"
        Me.TextBoxX15.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX15.TabIndex = 182
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(43, 357)
        Me.LabelX18.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(152, 28)
        Me.LabelX18.TabIndex = 181
        Me.LabelX18.Text = "GOLONGAN DARAH"
        '
        'TextBoxX14
        '
        '
        '
        '
        Me.TextBoxX14.Border.Class = "TextBoxBorder"
        Me.TextBoxX14.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX14.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX14.Location = New System.Drawing.Point(221, 321)
        Me.TextBoxX14.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX14.Name = "TextBoxX14"
        Me.TextBoxX14.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX14.TabIndex = 180
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(43, 321)
        Me.LabelX17.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(170, 28)
        Me.LabelX17.TabIndex = 179
        Me.LabelX17.Text = "DAERAH ASAL / SUKU"
        '
        'TextBoxX13
        '
        '
        '
        '
        Me.TextBoxX13.Border.Class = "TextBoxBorder"
        Me.TextBoxX13.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX13.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX13.Location = New System.Drawing.Point(221, 285)
        Me.TextBoxX13.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX13.Name = "TextBoxX13"
        Me.TextBoxX13.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX13.TabIndex = 178
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(43, 285)
        Me.LabelX16.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(170, 28)
        Me.LabelX16.TabIndex = 177
        Me.LabelX16.Text = "KEWARGANEGARAAN"
        '
        'TextBoxX12
        '
        '
        '
        '
        Me.TextBoxX12.Border.Class = "TextBoxBorder"
        Me.TextBoxX12.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX12.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX12.Location = New System.Drawing.Point(221, 249)
        Me.TextBoxX12.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX12.Name = "TextBoxX12"
        Me.TextBoxX12.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX12.TabIndex = 176
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(43, 249)
        Me.LabelX15.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(152, 28)
        Me.LabelX15.TabIndex = 175
        Me.LabelX15.Text = "AGAMA"
        '
        'TextBoxX11
        '
        '
        '
        '
        Me.TextBoxX11.Border.Class = "TextBoxBorder"
        Me.TextBoxX11.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX11.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX11.Location = New System.Drawing.Point(221, 213)
        Me.TextBoxX11.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX11.Name = "TextBoxX11"
        Me.TextBoxX11.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX11.TabIndex = 174
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(43, 213)
        Me.LabelX14.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(152, 28)
        Me.LabelX14.TabIndex = 173
        Me.LabelX14.Text = "NAMA LENGKAP"
        '
        'TextBoxX10
        '
        '
        '
        '
        Me.TextBoxX10.Border.Class = "TextBoxBorder"
        Me.TextBoxX10.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX10.Location = New System.Drawing.Point(221, 177)
        Me.TextBoxX10.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX10.Name = "TextBoxX10"
        Me.TextBoxX10.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX10.TabIndex = 172
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(43, 177)
        Me.LabelX13.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(152, 28)
        Me.LabelX13.TabIndex = 171
        Me.LabelX13.Text = "TEMPAT LAHIR"
        '
        'TextBoxX9
        '
        '
        '
        '
        Me.TextBoxX9.Border.Class = "TextBoxBorder"
        Me.TextBoxX9.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX9.Location = New System.Drawing.Point(221, 141)
        Me.TextBoxX9.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX9.Name = "TextBoxX9"
        Me.TextBoxX9.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX9.TabIndex = 170
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(43, 141)
        Me.LabelX12.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(152, 28)
        Me.LabelX12.TabIndex = 169
        Me.LabelX12.Text = "NAMA PANGGIL"
        '
        'TextBoxX8
        '
        '
        '
        '
        Me.TextBoxX8.Border.Class = "TextBoxBorder"
        Me.TextBoxX8.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX8.Location = New System.Drawing.Point(221, 105)
        Me.TextBoxX8.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX8.Name = "TextBoxX8"
        Me.TextBoxX8.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX8.TabIndex = 168
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(43, 105)
        Me.LabelX11.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(152, 28)
        Me.LabelX11.TabIndex = 167
        Me.LabelX11.Text = "NAMA LENGKAP"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Blue
        Me.Panel2.Controls.Add(Me.LabelX10)
        Me.Panel2.Location = New System.Drawing.Point(692, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(668, 51)
        Me.Panel2.TabIndex = 166
        '
        'LabelX10
        '
        Me.LabelX10.AutoSize = True
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX10.ForeColor = System.Drawing.Color.White
        Me.LabelX10.Location = New System.Drawing.Point(221, 6)
        Me.LabelX10.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(265, 41)
        Me.LabelX10.TabIndex = 168
        Me.LabelX10.Text = "TELEPON DAN EMAIL"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.Controls.Add(Me.LabelX9)
        Me.Panel1.Location = New System.Drawing.Point(18, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(668, 51)
        Me.Panel1.TabIndex = 165
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX9.ForeColor = System.Drawing.Color.White
        Me.LabelX9.Location = New System.Drawing.Point(250, 8)
        Me.LabelX9.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(152, 28)
        Me.LabelX9.TabIndex = 167
        Me.LabelX9.Text = "PERSONAL"
        '
        'TextBoxX7
        '
        '
        '
        '
        Me.TextBoxX7.Border.Class = "TextBoxBorder"
        Me.TextBoxX7.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX7.Location = New System.Drawing.Point(221, 69)
        Me.TextBoxX7.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX7.Name = "TextBoxX7"
        Me.TextBoxX7.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX7.TabIndex = 164
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(43, 69)
        Me.LabelX8.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(152, 28)
        Me.LabelX8.TabIndex = 163
        Me.LabelX8.Text = "NO KTP"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TextBoxX27)
        Me.TabPage2.Controls.Add(Me.LabelX31)
        Me.TabPage2.Controls.Add(Me.TextBoxX26)
        Me.TabPage2.Controls.Add(Me.LabelX30)
        Me.TabPage2.Controls.Add(Me.TextBoxX25)
        Me.TabPage2.Controls.Add(Me.LabelX29)
        Me.TabPage2.Controls.Add(Me.TextBoxX24)
        Me.TabPage2.Controls.Add(Me.LabelX28)
        Me.TabPage2.Controls.Add(Me.TextBoxX23)
        Me.TabPage2.Controls.Add(Me.LabelX27)
        Me.TabPage2.Controls.Add(Me.TextBoxX22)
        Me.TabPage2.Controls.Add(Me.LabelX26)
        Me.TabPage2.Controls.Add(Me.TextBoxX21)
        Me.TabPage2.Controls.Add(Me.LabelX25)
        Me.TabPage2.Controls.Add(Me.TextBoxX20)
        Me.TabPage2.Controls.Add(Me.LabelX24)
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 30)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1385, 397)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "ALAMAT"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TextBoxX27
        '
        '
        '
        '
        Me.TextBoxX27.Border.Class = "TextBoxBorder"
        Me.TextBoxX27.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX27.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX27.Location = New System.Drawing.Point(882, 244)
        Me.TextBoxX27.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX27.Name = "TextBoxX27"
        Me.TextBoxX27.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX27.TabIndex = 182
        '
        'LabelX31
        '
        Me.LabelX31.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX31.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX31.ForeColor = System.Drawing.Color.Black
        Me.LabelX31.Location = New System.Drawing.Point(704, 244)
        Me.LabelX31.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX31.Name = "LabelX31"
        Me.LabelX31.Size = New System.Drawing.Size(152, 28)
        Me.LabelX31.TabIndex = 181
        Me.LabelX31.Text = "KODE POS"
        '
        'TextBoxX26
        '
        '
        '
        '
        Me.TextBoxX26.Border.Class = "TextBoxBorder"
        Me.TextBoxX26.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX26.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX26.Location = New System.Drawing.Point(882, 208)
        Me.TextBoxX26.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX26.Name = "TextBoxX26"
        Me.TextBoxX26.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX26.TabIndex = 180
        '
        'LabelX30
        '
        Me.LabelX30.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX30.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX30.ForeColor = System.Drawing.Color.Black
        Me.LabelX30.Location = New System.Drawing.Point(704, 208)
        Me.LabelX30.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX30.Name = "LabelX30"
        Me.LabelX30.Size = New System.Drawing.Size(152, 28)
        Me.LabelX30.TabIndex = 179
        Me.LabelX30.Text = "KELURAHAN"
        '
        'TextBoxX25
        '
        '
        '
        '
        Me.TextBoxX25.Border.Class = "TextBoxBorder"
        Me.TextBoxX25.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX25.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX25.Location = New System.Drawing.Point(884, 172)
        Me.TextBoxX25.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX25.Name = "TextBoxX25"
        Me.TextBoxX25.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX25.TabIndex = 178
        '
        'LabelX29
        '
        Me.LabelX29.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX29.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX29.ForeColor = System.Drawing.Color.Black
        Me.LabelX29.Location = New System.Drawing.Point(706, 172)
        Me.LabelX29.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX29.Name = "LabelX29"
        Me.LabelX29.Size = New System.Drawing.Size(152, 28)
        Me.LabelX29.TabIndex = 177
        Me.LabelX29.Text = "KECAMATAN"
        '
        'TextBoxX24
        '
        '
        '
        '
        Me.TextBoxX24.Border.Class = "TextBoxBorder"
        Me.TextBoxX24.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX24.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX24.Location = New System.Drawing.Point(884, 136)
        Me.TextBoxX24.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX24.Name = "TextBoxX24"
        Me.TextBoxX24.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX24.TabIndex = 176
        '
        'LabelX28
        '
        Me.LabelX28.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX28.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX28.ForeColor = System.Drawing.Color.Black
        Me.LabelX28.Location = New System.Drawing.Point(706, 136)
        Me.LabelX28.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX28.Name = "LabelX28"
        Me.LabelX28.Size = New System.Drawing.Size(152, 28)
        Me.LabelX28.TabIndex = 175
        Me.LabelX28.Text = "KOTA / KABUPATEN"
        '
        'TextBoxX23
        '
        '
        '
        '
        Me.TextBoxX23.Border.Class = "TextBoxBorder"
        Me.TextBoxX23.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX23.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX23.Location = New System.Drawing.Point(882, 95)
        Me.TextBoxX23.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX23.Name = "TextBoxX23"
        Me.TextBoxX23.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX23.TabIndex = 174
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(704, 95)
        Me.LabelX27.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(152, 28)
        Me.LabelX27.TabIndex = 173
        Me.LabelX27.Text = "PROVINSI"
        '
        'TextBoxX22
        '
        '
        '
        '
        Me.TextBoxX22.Border.Class = "TextBoxBorder"
        Me.TextBoxX22.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX22.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX22.Location = New System.Drawing.Point(224, 244)
        Me.TextBoxX22.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX22.Name = "TextBoxX22"
        Me.TextBoxX22.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX22.TabIndex = 172
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(46, 244)
        Me.LabelX26.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(152, 28)
        Me.LabelX26.TabIndex = 171
        Me.LabelX26.Text = "DETAIL LOKASI"
        '
        'TextBoxX21
        '
        '
        '
        '
        Me.TextBoxX21.Border.Class = "TextBoxBorder"
        Me.TextBoxX21.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX21.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX21.Location = New System.Drawing.Point(226, 208)
        Me.TextBoxX21.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX21.Name = "TextBoxX21"
        Me.TextBoxX21.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX21.TabIndex = 170
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(48, 208)
        Me.LabelX25.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(152, 28)
        Me.LabelX25.TabIndex = 169
        Me.LabelX25.Text = "STATUS RUMAH"
        '
        'TextBoxX20
        '
        '
        '
        '
        Me.TextBoxX20.Border.Class = "TextBoxBorder"
        Me.TextBoxX20.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX20.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX20.Location = New System.Drawing.Point(224, 95)
        Me.TextBoxX20.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX20.Multiline = True
        Me.TextBoxX20.Name = "TextBoxX20"
        Me.TextBoxX20.Size = New System.Drawing.Size(435, 105)
        Me.TextBoxX20.TabIndex = 168
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(48, 95)
        Me.LabelX24.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(152, 28)
        Me.LabelX24.TabIndex = 167
        Me.LabelX24.Text = "ALAMAT RUMAH"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Red
        Me.Panel3.Controls.Add(Me.LabelX23)
        Me.Panel3.Location = New System.Drawing.Point(17, 6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1351, 51)
        Me.Panel3.TabIndex = 166
        '
        'LabelX23
        '
        Me.LabelX23.AutoSize = True
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.White
        Me.LabelX23.Location = New System.Drawing.Point(653, 4)
        Me.LabelX23.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(111, 41)
        Me.LabelX23.TabIndex = 167
        Me.LabelX23.Text = "ALAMAT"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DateTimePicker2)
        Me.TabPage3.Controls.Add(Me.TextBoxX32)
        Me.TabPage3.Controls.Add(Me.LabelX37)
        Me.TabPage3.Controls.Add(Me.TextBoxX31)
        Me.TabPage3.Controls.Add(Me.LabelX36)
        Me.TabPage3.Controls.Add(Me.TextBoxX30)
        Me.TabPage3.Controls.Add(Me.LabelX35)
        Me.TabPage3.Controls.Add(Me.LabelX34)
        Me.TabPage3.Controls.Add(Me.TextBoxX28)
        Me.TabPage3.Controls.Add(Me.LabelX33)
        Me.TabPage3.Controls.Add(Me.Panel4)
        Me.TabPage3.Location = New System.Drawing.Point(4, 30)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1385, 397)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "KELUARGA"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(243, 120)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(200, 28)
        Me.DateTimePicker2.TabIndex = 185
        '
        'TextBoxX32
        '
        '
        '
        '
        Me.TextBoxX32.Border.Class = "TextBoxBorder"
        Me.TextBoxX32.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX32.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX32.Location = New System.Drawing.Point(243, 228)
        Me.TextBoxX32.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX32.Name = "TextBoxX32"
        Me.TextBoxX32.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX32.TabIndex = 184
        '
        'LabelX37
        '
        Me.LabelX37.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX37.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX37.ForeColor = System.Drawing.Color.Black
        Me.LabelX37.Location = New System.Drawing.Point(53, 226)
        Me.LabelX37.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX37.Name = "LabelX37"
        Me.LabelX37.Size = New System.Drawing.Size(170, 28)
        Me.LabelX37.TabIndex = 183
        Me.LabelX37.Text = "NAMA IBU KANDUNG"
        '
        'TextBoxX31
        '
        '
        '
        '
        Me.TextBoxX31.Border.Class = "TextBoxBorder"
        Me.TextBoxX31.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX31.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX31.Location = New System.Drawing.Point(243, 192)
        Me.TextBoxX31.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX31.Name = "TextBoxX31"
        Me.TextBoxX31.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX31.TabIndex = 182
        '
        'LabelX36
        '
        Me.LabelX36.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX36.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX36.ForeColor = System.Drawing.Color.Black
        Me.LabelX36.Location = New System.Drawing.Point(53, 190)
        Me.LabelX36.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX36.Name = "LabelX36"
        Me.LabelX36.Size = New System.Drawing.Size(170, 28)
        Me.LabelX36.TabIndex = 181
        Me.LabelX36.Text = "ANAK NOMOR / KE"
        '
        'TextBoxX30
        '
        '
        '
        '
        Me.TextBoxX30.Border.Class = "TextBoxBorder"
        Me.TextBoxX30.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX30.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX30.Location = New System.Drawing.Point(243, 156)
        Me.TextBoxX30.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX30.Name = "TextBoxX30"
        Me.TextBoxX30.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX30.TabIndex = 180
        '
        'LabelX35
        '
        Me.LabelX35.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX35.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX35.ForeColor = System.Drawing.Color.Black
        Me.LabelX35.Location = New System.Drawing.Point(53, 154)
        Me.LabelX35.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX35.Name = "LabelX35"
        Me.LabelX35.Size = New System.Drawing.Size(170, 28)
        Me.LabelX35.TabIndex = 179
        Me.LabelX35.Text = "JUMLAH TANGGUNGAN"
        '
        'LabelX34
        '
        Me.LabelX34.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX34.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX34.ForeColor = System.Drawing.Color.Black
        Me.LabelX34.Location = New System.Drawing.Point(53, 118)
        Me.LabelX34.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX34.Name = "LabelX34"
        Me.LabelX34.Size = New System.Drawing.Size(170, 28)
        Me.LabelX34.TabIndex = 177
        Me.LabelX34.Text = "TANGGAL MENIKAH"
        '
        'TextBoxX28
        '
        '
        '
        '
        Me.TextBoxX28.Border.Class = "TextBoxBorder"
        Me.TextBoxX28.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX28.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX28.Location = New System.Drawing.Point(243, 84)
        Me.TextBoxX28.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX28.Name = "TextBoxX28"
        Me.TextBoxX28.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX28.TabIndex = 176
        '
        'LabelX33
        '
        Me.LabelX33.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX33.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX33.ForeColor = System.Drawing.Color.Black
        Me.LabelX33.Location = New System.Drawing.Point(53, 82)
        Me.LabelX33.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX33.Name = "LabelX33"
        Me.LabelX33.Size = New System.Drawing.Size(170, 28)
        Me.LabelX33.TabIndex = 175
        Me.LabelX33.Text = "STATUS PERKAWINAN"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Red
        Me.Panel4.Controls.Add(Me.LabelX32)
        Me.Panel4.Location = New System.Drawing.Point(17, 6)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1351, 51)
        Me.Panel4.TabIndex = 167
        '
        'LabelX32
        '
        Me.LabelX32.AutoSize = True
        Me.LabelX32.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX32.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX32.ForeColor = System.Drawing.Color.White
        Me.LabelX32.Location = New System.Drawing.Point(653, 3)
        Me.LabelX32.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX32.Name = "LabelX32"
        Me.LabelX32.Size = New System.Drawing.Size(138, 41)
        Me.LabelX32.TabIndex = 167
        Me.LabelX32.Text = "KELUARGA"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.DateTimePicker3)
        Me.TabPage4.Controls.Add(Me.TextBoxX29)
        Me.TabPage4.Controls.Add(Me.LabelX39)
        Me.TabPage4.Controls.Add(Me.TextBoxX33)
        Me.TabPage4.Controls.Add(Me.LabelX40)
        Me.TabPage4.Controls.Add(Me.TextBoxX34)
        Me.TabPage4.Controls.Add(Me.LabelX41)
        Me.TabPage4.Controls.Add(Me.LabelX42)
        Me.TabPage4.Controls.Add(Me.TextBoxX35)
        Me.TabPage4.Controls.Add(Me.LabelX43)
        Me.TabPage4.Controls.Add(Me.Panel5)
        Me.TabPage4.Location = New System.Drawing.Point(4, 30)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1385, 397)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "PEKERJAAN"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Location = New System.Drawing.Point(250, 224)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(200, 28)
        Me.DateTimePicker3.TabIndex = 195
        '
        'TextBoxX29
        '
        '
        '
        '
        Me.TextBoxX29.Border.Class = "TextBoxBorder"
        Me.TextBoxX29.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX29.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX29.Location = New System.Drawing.Point(250, 117)
        Me.TextBoxX29.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX29.Name = "TextBoxX29"
        Me.TextBoxX29.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX29.TabIndex = 194
        '
        'LabelX39
        '
        Me.LabelX39.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX39.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX39.ForeColor = System.Drawing.Color.Black
        Me.LabelX39.Location = New System.Drawing.Point(60, 223)
        Me.LabelX39.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX39.Name = "LabelX39"
        Me.LabelX39.Size = New System.Drawing.Size(170, 28)
        Me.LabelX39.TabIndex = 193
        Me.LabelX39.Text = "TANGGAL BUAT"
        '
        'TextBoxX33
        '
        '
        '
        '
        Me.TextBoxX33.Border.Class = "TextBoxBorder"
        Me.TextBoxX33.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX33.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX33.Location = New System.Drawing.Point(250, 189)
        Me.TextBoxX33.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX33.Name = "TextBoxX33"
        Me.TextBoxX33.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX33.TabIndex = 192
        '
        'LabelX40
        '
        Me.LabelX40.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX40.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX40.ForeColor = System.Drawing.Color.Black
        Me.LabelX40.Location = New System.Drawing.Point(60, 187)
        Me.LabelX40.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX40.Name = "LabelX40"
        Me.LabelX40.Size = New System.Drawing.Size(170, 28)
        Me.LabelX40.TabIndex = 191
        Me.LabelX40.Text = "ALAMAT"
        '
        'TextBoxX34
        '
        '
        '
        '
        Me.TextBoxX34.Border.Class = "TextBoxBorder"
        Me.TextBoxX34.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX34.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX34.Location = New System.Drawing.Point(250, 153)
        Me.TextBoxX34.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX34.Name = "TextBoxX34"
        Me.TextBoxX34.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX34.TabIndex = 190
        '
        'LabelX41
        '
        Me.LabelX41.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX41.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX41.ForeColor = System.Drawing.Color.Black
        Me.LabelX41.Location = New System.Drawing.Point(60, 151)
        Me.LabelX41.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX41.Name = "LabelX41"
        Me.LabelX41.Size = New System.Drawing.Size(170, 28)
        Me.LabelX41.TabIndex = 189
        Me.LabelX41.Text = "NAMA PERUSAHAAN"
        '
        'LabelX42
        '
        Me.LabelX42.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX42.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX42.ForeColor = System.Drawing.Color.Black
        Me.LabelX42.Location = New System.Drawing.Point(60, 115)
        Me.LabelX42.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX42.Name = "LabelX42"
        Me.LabelX42.Size = New System.Drawing.Size(170, 28)
        Me.LabelX42.TabIndex = 188
        Me.LabelX42.Text = "PROFESI"
        '
        'TextBoxX35
        '
        '
        '
        '
        Me.TextBoxX35.Border.Class = "TextBoxBorder"
        Me.TextBoxX35.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX35.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX35.Location = New System.Drawing.Point(250, 81)
        Me.TextBoxX35.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX35.Name = "TextBoxX35"
        Me.TextBoxX35.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX35.TabIndex = 187
        '
        'LabelX43
        '
        Me.LabelX43.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX43.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX43.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX43.ForeColor = System.Drawing.Color.Black
        Me.LabelX43.Location = New System.Drawing.Point(60, 79)
        Me.LabelX43.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX43.Name = "LabelX43"
        Me.LabelX43.Size = New System.Drawing.Size(170, 28)
        Me.LabelX43.TabIndex = 186
        Me.LabelX43.Text = "BIDANG USAHA"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Red
        Me.Panel5.Controls.Add(Me.LabelX38)
        Me.Panel5.Location = New System.Drawing.Point(17, 5)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1351, 51)
        Me.Panel5.TabIndex = 168
        '
        'LabelX38
        '
        Me.LabelX38.AutoSize = True
        Me.LabelX38.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX38.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX38.ForeColor = System.Drawing.Color.White
        Me.LabelX38.Location = New System.Drawing.Point(653, 3)
        Me.LabelX38.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX38.Name = "LabelX38"
        Me.LabelX38.Size = New System.Drawing.Size(147, 41)
        Me.LabelX38.TabIndex = 167
        Me.LabelX38.Text = "PEKERJAAN"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.TextBoxX41)
        Me.TabPage5.Controls.Add(Me.TextBoxX40)
        Me.TabPage5.Controls.Add(Me.LabelX50)
        Me.TabPage5.Controls.Add(Me.TextBoxX36)
        Me.TabPage5.Controls.Add(Me.LabelX45)
        Me.TabPage5.Controls.Add(Me.TextBoxX37)
        Me.TabPage5.Controls.Add(Me.LabelX46)
        Me.TabPage5.Controls.Add(Me.TextBoxX38)
        Me.TabPage5.Controls.Add(Me.LabelX47)
        Me.TabPage5.Controls.Add(Me.LabelX48)
        Me.TabPage5.Controls.Add(Me.TextBoxX39)
        Me.TabPage5.Controls.Add(Me.LabelX49)
        Me.TabPage5.Controls.Add(Me.Panel6)
        Me.TabPage5.Location = New System.Drawing.Point(4, 30)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1385, 397)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "PENDIDIKAN"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TextBoxX41
        '
        '
        '
        '
        Me.TextBoxX41.Border.Class = "TextBoxBorder"
        Me.TextBoxX41.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX41.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX41.Location = New System.Drawing.Point(244, 261)
        Me.TextBoxX41.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX41.Multiline = True
        Me.TextBoxX41.Name = "TextBoxX41"
        Me.TextBoxX41.Size = New System.Drawing.Size(433, 95)
        Me.TextBoxX41.TabIndex = 197
        '
        'TextBoxX40
        '
        '
        '
        '
        Me.TextBoxX40.Border.Class = "TextBoxBorder"
        Me.TextBoxX40.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX40.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX40.Location = New System.Drawing.Point(244, 225)
        Me.TextBoxX40.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX40.Name = "TextBoxX40"
        Me.TextBoxX40.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX40.TabIndex = 196
        '
        'LabelX50
        '
        Me.LabelX50.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX50.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX50.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX50.ForeColor = System.Drawing.Color.Black
        Me.LabelX50.Location = New System.Drawing.Point(54, 223)
        Me.LabelX50.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX50.Name = "LabelX50"
        Me.LabelX50.Size = New System.Drawing.Size(170, 28)
        Me.LabelX50.TabIndex = 195
        Me.LabelX50.Text = "JURUSAN STUDI"
        '
        'TextBoxX36
        '
        '
        '
        '
        Me.TextBoxX36.Border.Class = "TextBoxBorder"
        Me.TextBoxX36.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX36.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX36.Location = New System.Drawing.Point(244, 187)
        Me.TextBoxX36.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX36.Name = "TextBoxX36"
        Me.TextBoxX36.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX36.TabIndex = 194
        '
        'LabelX45
        '
        Me.LabelX45.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX45.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX45.ForeColor = System.Drawing.Color.Black
        Me.LabelX45.Location = New System.Drawing.Point(54, 187)
        Me.LabelX45.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX45.Name = "LabelX45"
        Me.LabelX45.Size = New System.Drawing.Size(170, 28)
        Me.LabelX45.TabIndex = 193
        Me.LabelX45.Text = "TAHUN IJAZAH"
        '
        'TextBoxX37
        '
        '
        '
        '
        Me.TextBoxX37.Border.Class = "TextBoxBorder"
        Me.TextBoxX37.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX37.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX37.Location = New System.Drawing.Point(244, 151)
        Me.TextBoxX37.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX37.Name = "TextBoxX37"
        Me.TextBoxX37.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX37.TabIndex = 192
        '
        'LabelX46
        '
        Me.LabelX46.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX46.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX46.ForeColor = System.Drawing.Color.Black
        Me.LabelX46.Location = New System.Drawing.Point(54, 259)
        Me.LabelX46.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX46.Name = "LabelX46"
        Me.LabelX46.Size = New System.Drawing.Size(170, 28)
        Me.LabelX46.TabIndex = 191
        Me.LabelX46.Text = "ALAMAT SEKOLAH"
        '
        'TextBoxX38
        '
        '
        '
        '
        Me.TextBoxX38.Border.Class = "TextBoxBorder"
        Me.TextBoxX38.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX38.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX38.Location = New System.Drawing.Point(244, 115)
        Me.TextBoxX38.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX38.Name = "TextBoxX38"
        Me.TextBoxX38.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX38.TabIndex = 190
        '
        'LabelX47
        '
        Me.LabelX47.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX47.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX47.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX47.ForeColor = System.Drawing.Color.Black
        Me.LabelX47.Location = New System.Drawing.Point(54, 149)
        Me.LabelX47.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX47.Name = "LabelX47"
        Me.LabelX47.Size = New System.Drawing.Size(170, 28)
        Me.LabelX47.TabIndex = 189
        Me.LabelX47.Text = "NAMA SEKOLAH"
        '
        'LabelX48
        '
        Me.LabelX48.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX48.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX48.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX48.ForeColor = System.Drawing.Color.Black
        Me.LabelX48.Location = New System.Drawing.Point(54, 113)
        Me.LabelX48.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX48.Name = "LabelX48"
        Me.LabelX48.Size = New System.Drawing.Size(170, 28)
        Me.LabelX48.TabIndex = 188
        Me.LabelX48.Text = "TINGKAT PENDIDIKAN"
        '
        'TextBoxX39
        '
        '
        '
        '
        Me.TextBoxX39.Border.Class = "TextBoxBorder"
        Me.TextBoxX39.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX39.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX39.Location = New System.Drawing.Point(244, 79)
        Me.TextBoxX39.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX39.Name = "TextBoxX39"
        Me.TextBoxX39.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX39.TabIndex = 187
        '
        'LabelX49
        '
        Me.LabelX49.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX49.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX49.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX49.ForeColor = System.Drawing.Color.Black
        Me.LabelX49.Location = New System.Drawing.Point(54, 77)
        Me.LabelX49.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX49.Name = "LabelX49"
        Me.LabelX49.Size = New System.Drawing.Size(170, 28)
        Me.LabelX49.TabIndex = 186
        Me.LabelX49.Text = "STATUS PENDIDIKAN"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Red
        Me.Panel6.Controls.Add(Me.LabelX44)
        Me.Panel6.Location = New System.Drawing.Point(17, 6)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1351, 51)
        Me.Panel6.TabIndex = 169
        '
        'LabelX44
        '
        Me.LabelX44.AutoSize = True
        Me.LabelX44.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX44.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX44.ForeColor = System.Drawing.Color.White
        Me.LabelX44.Location = New System.Drawing.Point(653, 2)
        Me.LabelX44.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX44.Name = "LabelX44"
        Me.LabelX44.Size = New System.Drawing.Size(161, 41)
        Me.LabelX44.TabIndex = 167
        Me.LabelX44.Text = "PENDIDIKAN"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.PictureBox3)
        Me.TabPage6.Controls.Add(Me.LabelX60)
        Me.TabPage6.Controls.Add(Me.PictureBox2)
        Me.TabPage6.Controls.Add(Me.LabelX59)
        Me.TabPage6.Controls.Add(Me.TextBoxX45)
        Me.TabPage6.Controls.Add(Me.TextBoxX46)
        Me.TabPage6.Controls.Add(Me.LabelX56)
        Me.TabPage6.Controls.Add(Me.LabelX57)
        Me.TabPage6.Controls.Add(Me.TextBoxX47)
        Me.TabPage6.Controls.Add(Me.LabelX58)
        Me.TabPage6.Controls.Add(Me.TextBoxX42)
        Me.TabPage6.Controls.Add(Me.TextBoxX43)
        Me.TabPage6.Controls.Add(Me.LabelX53)
        Me.TabPage6.Controls.Add(Me.LabelX54)
        Me.TabPage6.Controls.Add(Me.TextBoxX44)
        Me.TabPage6.Controls.Add(Me.LabelX55)
        Me.TabPage6.Controls.Add(Me.Panel7)
        Me.TabPage6.Controls.Add(Me.Panel8)
        Me.TabPage6.Location = New System.Drawing.Point(4, 30)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1385, 397)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "FILE DAN DOKUMEN"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(906, 225)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 159)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 208
        Me.PictureBox3.TabStop = False
        '
        'LabelX60
        '
        Me.LabelX60.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX60.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX60.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX60.ForeColor = System.Drawing.Color.Black
        Me.LabelX60.Location = New System.Drawing.Point(716, 225)
        Me.LabelX60.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX60.Name = "LabelX60"
        Me.LabelX60.Size = New System.Drawing.Size(170, 28)
        Me.LabelX60.TabIndex = 207
        Me.LabelX60.Text = "PHOTO KTP"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(230, 225)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(161, 159)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 206
        Me.PictureBox2.TabStop = False
        '
        'LabelX59
        '
        Me.LabelX59.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX59.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX59.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX59.ForeColor = System.Drawing.Color.Black
        Me.LabelX59.Location = New System.Drawing.Point(40, 225)
        Me.LabelX59.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX59.Name = "LabelX59"
        Me.LabelX59.Size = New System.Drawing.Size(170, 28)
        Me.LabelX59.TabIndex = 205
        Me.LabelX59.Text = "PHOTO PROFILE"
        '
        'TextBoxX45
        '
        '
        '
        '
        Me.TextBoxX45.Border.Class = "TextBoxBorder"
        Me.TextBoxX45.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX45.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX45.Location = New System.Drawing.Point(906, 161)
        Me.TextBoxX45.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX45.Name = "TextBoxX45"
        Me.TextBoxX45.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX45.TabIndex = 204
        '
        'TextBoxX46
        '
        '
        '
        '
        Me.TextBoxX46.Border.Class = "TextBoxBorder"
        Me.TextBoxX46.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX46.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX46.Location = New System.Drawing.Point(906, 125)
        Me.TextBoxX46.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX46.Name = "TextBoxX46"
        Me.TextBoxX46.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX46.TabIndex = 203
        '
        'LabelX56
        '
        Me.LabelX56.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX56.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX56.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX56.ForeColor = System.Drawing.Color.Black
        Me.LabelX56.Location = New System.Drawing.Point(716, 159)
        Me.LabelX56.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX56.Name = "LabelX56"
        Me.LabelX56.Size = New System.Drawing.Size(170, 28)
        Me.LabelX56.TabIndex = 202
        Me.LabelX56.Text = "NAMA SEKOLAH"
        '
        'LabelX57
        '
        Me.LabelX57.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX57.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX57.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX57.ForeColor = System.Drawing.Color.Black
        Me.LabelX57.Location = New System.Drawing.Point(716, 123)
        Me.LabelX57.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX57.Name = "LabelX57"
        Me.LabelX57.Size = New System.Drawing.Size(170, 28)
        Me.LabelX57.TabIndex = 201
        Me.LabelX57.Text = "NOMOR KARTU KREDIT"
        '
        'TextBoxX47
        '
        '
        '
        '
        Me.TextBoxX47.Border.Class = "TextBoxBorder"
        Me.TextBoxX47.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX47.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX47.Location = New System.Drawing.Point(906, 89)
        Me.TextBoxX47.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX47.Name = "TextBoxX47"
        Me.TextBoxX47.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX47.TabIndex = 200
        '
        'LabelX58
        '
        Me.LabelX58.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX58.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX58.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX58.ForeColor = System.Drawing.Color.Black
        Me.LabelX58.Location = New System.Drawing.Point(716, 87)
        Me.LabelX58.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX58.Name = "LabelX58"
        Me.LabelX58.Size = New System.Drawing.Size(170, 28)
        Me.LabelX58.TabIndex = 199
        Me.LabelX58.Text = "NOMOR NPWP"
        '
        'TextBoxX42
        '
        '
        '
        '
        Me.TextBoxX42.Border.Class = "TextBoxBorder"
        Me.TextBoxX42.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX42.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX42.Location = New System.Drawing.Point(230, 161)
        Me.TextBoxX42.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX42.Name = "TextBoxX42"
        Me.TextBoxX42.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX42.TabIndex = 198
        '
        'TextBoxX43
        '
        '
        '
        '
        Me.TextBoxX43.Border.Class = "TextBoxBorder"
        Me.TextBoxX43.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX43.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX43.Location = New System.Drawing.Point(230, 125)
        Me.TextBoxX43.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX43.Name = "TextBoxX43"
        Me.TextBoxX43.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX43.TabIndex = 197
        '
        'LabelX53
        '
        Me.LabelX53.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX53.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX53.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX53.ForeColor = System.Drawing.Color.Black
        Me.LabelX53.Location = New System.Drawing.Point(40, 159)
        Me.LabelX53.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX53.Name = "LabelX53"
        Me.LabelX53.Size = New System.Drawing.Size(170, 28)
        Me.LabelX53.TabIndex = 196
        Me.LabelX53.Text = "NAMA BANK"
        '
        'LabelX54
        '
        Me.LabelX54.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX54.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX54.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX54.ForeColor = System.Drawing.Color.Black
        Me.LabelX54.Location = New System.Drawing.Point(40, 123)
        Me.LabelX54.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX54.Name = "LabelX54"
        Me.LabelX54.Size = New System.Drawing.Size(170, 28)
        Me.LabelX54.TabIndex = 195
        Me.LabelX54.Text = "REK. ATAS NAMA"
        '
        'TextBoxX44
        '
        '
        '
        '
        Me.TextBoxX44.Border.Class = "TextBoxBorder"
        Me.TextBoxX44.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX44.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX44.Location = New System.Drawing.Point(230, 89)
        Me.TextBoxX44.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxX44.Name = "TextBoxX44"
        Me.TextBoxX44.Size = New System.Drawing.Size(433, 28)
        Me.TextBoxX44.TabIndex = 194
        '
        'LabelX55
        '
        Me.LabelX55.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX55.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX55.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX55.ForeColor = System.Drawing.Color.Black
        Me.LabelX55.Location = New System.Drawing.Point(40, 87)
        Me.LabelX55.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX55.Name = "LabelX55"
        Me.LabelX55.Size = New System.Drawing.Size(170, 28)
        Me.LabelX55.TabIndex = 193
        Me.LabelX55.Text = "NO. REKENING"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Blue
        Me.Panel7.Controls.Add(Me.LabelX51)
        Me.Panel7.Location = New System.Drawing.Point(691, 7)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(668, 51)
        Me.Panel7.TabIndex = 168
        '
        'LabelX51
        '
        Me.LabelX51.AutoSize = True
        Me.LabelX51.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX51.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX51.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX51.ForeColor = System.Drawing.Color.White
        Me.LabelX51.Location = New System.Drawing.Point(221, 6)
        Me.LabelX51.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX51.Name = "LabelX51"
        Me.LabelX51.Size = New System.Drawing.Size(257, 41)
        Me.LabelX51.TabIndex = 168
        Me.LabelX51.Text = "DOKUMEN LAINNYA"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Red
        Me.Panel8.Controls.Add(Me.LabelX52)
        Me.Panel8.Location = New System.Drawing.Point(17, 7)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(668, 51)
        Me.Panel8.TabIndex = 167
        '
        'LabelX52
        '
        Me.LabelX52.AutoSize = True
        Me.LabelX52.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX52.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX52.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX52.ForeColor = System.Drawing.Color.White
        Me.LabelX52.Location = New System.Drawing.Point(207, 6)
        Me.LabelX52.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX52.Name = "LabelX52"
        Me.LabelX52.Size = New System.Drawing.Size(258, 41)
        Me.LabelX52.TabIndex = 167
        Me.LabelX52.Text = "FILE DAN DOKUMEN"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(1415, 698)
        Me.BtnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(93, 89)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 10
        Me.BtnClose.Text = "CLOSE"
        '
        'frmInformasiKaryawan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1518, 803)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmInformasiKaryawan"
        Me.Text = "frmInformasiKaryawan"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TextBoxX6 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX5 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX4 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX3 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX1 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TextBoxX19 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX18 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX17 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX16 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX15 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX14 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX13 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX12 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX11 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX10 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX9 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX8 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX7 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TextBoxX27 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX31 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX26 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX30 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX25 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX29 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX24 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX28 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX23 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX22 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX21 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX20 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBoxX32 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX37 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX31 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX36 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX30 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX35 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX34 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX28 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX33 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents LabelX32 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBoxX29 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX39 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX33 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX40 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX34 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX41 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX42 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX35 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX43 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents LabelX38 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TextBoxX41 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TextBoxX40 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX50 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX36 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX45 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX37 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX46 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX38 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX47 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX48 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX39 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX49 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents LabelX44 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelX60 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelX59 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX45 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TextBoxX46 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX56 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX57 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX47 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX58 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX42 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TextBoxX43 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX53 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX54 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX44 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX55 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents LabelX51 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents LabelX52 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
End Class
