﻿Public Class frmDaftarKaryawan
    Dim FlNm As String
    Dim kk As Integer
    Sub ShowDataGrid()

        SQL = ""
        'SQL = " select a.kode,a.nama,a.type_perkiraan,a.kas_bank,b.nama_perkiraan_group,a.tanggal_buat,case a.status when 1 then 'AKTIF' else 'NON AKTIF' end as status"
        'SQL = SQL & " from acc_perkiraan_nama a left join acc_perkiraan_group b on a.acc_perkiraan_group_id=b.id where 0=0"

        SQL = "select a.id, b.kd_karyawan, a.nama_lengkap, c.nama_unitbisnis, a.no_ktp, a.telepon_1, a.email_1 from personal a "
        SQL = SQL & "inner join karyawan b on a.id = b.personal_id "
        SQL = SQL & "inner join unitbisnis_nama c on c.id = b.unitbisnis_id "
        SQL = SQL & "WHERE c.kd_unitbisnis = '" & myKdBisnis & "' "



        If txtSearch.Text <> "" Then

            SQL = SQL & " and (a.nama_lengkap like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If

        SQL = SQL & " and len(b.kd_karyawan) > 0 "
        SQL = SQL & " order by c.kd_unitbisnis"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 150
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 80
        DGView.Columns(3).Width = 150
        DGView.Columns(4).Width = 250
        DGView.Columns(5).Width = 200
        DGView.Columns(6).Width = 80

        DGView.Columns(0).HeaderText = "KODE KARYAWAN"
        DGView.Columns(1).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(2).HeaderText = "UNIT BISNIS"
        DGView.Columns(3).HeaderText = "KTP"
        DGView.Columns(4).HeaderText = "TELEPON"
        DGView.Columns(5).HeaderText = "EMAIL"
        DGView.Columns(6).HeaderText = "ID"
        DGView.Columns(6).Visible = False

        ColorChange()

    End Sub
    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next
    End Sub
    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
            End With
        End If
    End Sub

    Private Sub frmDaftarKaryawan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ShowDataGrid()
        Call ColorChange()
    End Sub

    Private Sub frmDaftarKaryawan_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 680
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Private Sub DGView_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellDoubleClick
        TakeDGView()
        frmInformasiKaryawan.ShowDialog()
    End Sub
End Class