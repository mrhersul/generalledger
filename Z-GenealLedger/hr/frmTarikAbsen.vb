﻿Imports System.IO
Imports System.Data.OleDb
Public Class frmTarikAbsen
    Dim kon As OleDbConnection
    Dim cmmd As OleDbCommand
    Dim adaptor As OleDbDataAdapter
    Dim baca As OleDbDataReader
    Dim dst As DataSet
    Dim str As String
    Dim IdBisnis As Integer
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'Dim oReader As StreamReader
        'OpenFileDialog1.FileName = ""
        'OpenFileDialog1.Filter = "All Files (*.*) | *.*"
        'If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    oReader = New StreamReader(OpenFileDialog1.FileName, True)
        '    txtFile.Text = oReader.Read
        'End If

        ' Call ShowDialog.
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()

        ' Test result.
        If result = Windows.Forms.DialogResult.OK Then

            ' Get the file name.
            Dim path As String = OpenFileDialog1.FileName
            Try
                ' Read in text.
                Dim text As String = File.ReadAllText(path)

                ' For debugging.
                txtFile.Text = path.ToString
            Catch ex As Exception

                ' Report an error.
                txtFile.Text = "Error"

            End Try
        End If
    End Sub
    Sub buka()
        kon = New OleDbConnection("provider=microsoft.jet.oledb.4.0;data source=" & txtFile.Text & " ")
        kon.Open()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim IsConError As Boolean
        On Error GoTo err

        'unitbisnisID
        'Cari ID unitbisnis
        SQL = ""
        SQL = "select * from unitbisnis_nama where kd_unitbisnis = '" & myKdBisnis & "' "
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read
                IdBisnis = dr.Item("id")
            End While
        End If

        Call SimpanUserInfo()
        Call SimpanAbsensi()

        ' Masukan ke data Absensi
        dr.Close()
        SQL = ""
        SQL = "select distinct karyawan_nama_id, tanggal, jam from ("
        SQL = SQL & " select a.badgenumber as karyawan_nama_id,convert(varchar,b.checktime,23) as tanggal, convert(varchar,b.checktime,8) as jam"
        SQL = SQL & " from temp_mstmachine a"
        SQL = SQL & " inner join temp_checkinout b on a.unitbisnis_id = b.unitbisnis_id and a.userid = b.userid) x "
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader
        If dr.HasRows Then
            'dr.Close()
            SQL = ""
            SQL = SQL & "DELETE FROM karyawan_absensi WHERE unitbisnis_id = " & IdBisnis & " "
            While dr.Read
                Dim kId As Integer
                Dim tgl As Date
                Dim jam As String
                kId = dr.Item("karyawan_nama_id")
                tgl = dr.Item("tanggal")
                jam = dr.Item("jam").ToString

                SQL = SQL & vbCrLf
                SQL = SQL & "exec SP_HR_INSABSEN " & IdBisnis & ", " & kId & ", '" & jam & "', '" & Format(tgl, "yyyy-MM-dd") & "'"
            End While
            dr.Close()
            Konek.IUDQuery(SQL)
        End If
        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        'Call frmBrowseIzin .segar()
        DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
        Exit Sub
err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
    Sub SimpanUserInfo()
        Dim Userid As Integer
        Dim Badgenum As String
        'Buka MDB
        buka()
        cmmd = New OleDbCommand("Select * From USERINFO", kon)
        baca = cmmd.ExecuteReader()
        'baca.Read()
        If Not baca.HasRows Then
            MsgBox("Tidak ada data!")
            Button1.Focus()
        Else
            dr.Close()
            SQL = ""
            While baca.Read
                Userid = baca.Item("USERID")
                Badgenum = baca.Item("Badgenumber")

                SQL = SQL & vbCrLf
                SQL = SQL & "DELETE FROM temp_mstmachine WHERE unitbisnis_id = " & IdBisnis & " AND userid = " & Userid & " "
                SQL = SQL & vbCrLf
                SQL = SQL & "INSERT INTO temp_mstmachine(unitbisnis_id,userid,badgenumber) VALUES(" & IdBisnis & "," & Userid & ",'" & Badgenum & "') "
            End While
            Konek.IUDQuery(SQL)
        End If
    End Sub

    Sub SimpanAbsensi()
        Dim Userid As Integer
        Dim Checktime As String

        'Buka MDB
        buka()
        cmmd = New OleDbCommand("SELECT * FROM CHECKINOUT", kon)
        baca = cmmd.ExecuteReader
        'baca.Read()
        If Not baca.HasRows Then
            MsgBox("Tidak Ada Data")
            Button1.Focus()
        Else
            dr.Close()
            SQL = ""
            SQL = SQL & "DELETE FROM temp_checkinout WHERE unitbisnis_id = " & IdBisnis & " "
            While baca.Read
                Dim oTime As DateTime
                Userid = baca.Item("USERID")
                Checktime = baca.Item("CHECKTIME").ToString
                oTime = Convert.ToDateTime(Checktime)
                SQL = SQL & vbCrLf
                SQL = SQL & "INSERT INTO temp_checkinout(unitbisnis_id,userid,checktime) VALUES(" & IdBisnis & ", " & Userid & ", "
                SQL = SQL & " '" & Format(oTime, "yyyy-MM-dd HH:mm:ss") & "')"
            End While
            Konek.IUDQuery(SQL)
        End If
    End Sub
End Class