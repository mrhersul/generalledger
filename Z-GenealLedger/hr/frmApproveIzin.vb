﻿Public Class frmApproveIzin
    Dim Idnya As Integer
    Public Sub Pengaturan(ByVal Kode As Integer)
        Idnya = Kode
    End Sub

    Private Sub frmApproveIzin_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        SQL = ""
        SQL = "select b.kd_karyawan, c.nama_lengkap, a.tgl_awal_cuti, a.tgl_akhir_cuti, a.keterangan from karyawan_cuti a"
        SQL = SQL & " inner join karyawan b on a.karyawan_nama_id = b.personal_id"
        SQL = SQL & " inner join personal c on c.id = a.karyawan_nama_id"
        SQL = SQL & " where a.id = " & Idnya & " "

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read
                txtNmKaryawan.Text = Trim(dr.Item("nama_lengkap").ToString())
                txtKdKaryawan.Text = Trim(dr.Item("kd_karyawan").ToString())
                txtDateAwal.Value = dr.Item("tgl_awal_cuti")
                txtDateAkhir.Value = dr.Item("tgl_akhir_cuti")
                txtKet.Text = dr.Item("keterangan").ToString
            End While
        End If
    End Sub

    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs)
        Dim IsConError As Boolean
        On Error GoTo err
        dr.Close()
        SQL = ""
        SQL = "update karyawan_cuti set status_permohonan = '0', tanggal_ubah = '" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', user_ubah = '" & myID & "' "
        SQL = SQL & " where id = " & Idnya & " "
        Konek.IUDQuery(SQL)

        MsgBox("Data sudah terupdate...", MsgBoxStyle.Information)
        IsConError = False
        DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

    Private Sub BtnSave_Click(sender As System.Object, e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err
        dr.Close()
        SQL = ""
        SQL = "update karyawan_cuti set tgl_awal_cuti = '" & Format(txtDateAwal.Value, "yyyy-MM-dd") & "', tgl_akhir_cuti = '" & Format(txtDateAkhir.Value, "yyyy-MM-dd") & "', "
        SQL = SQL & " keterangan = '" & txtKet.Text & "', tanggal_ubah = '" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', user_ubah = '" & myID & "' "
        SQL = SQL & " where id = " & Idnya & " "
        Konek.IUDQuery(SQL)

        MsgBox("Data sudah terupdate...", MsgBoxStyle.Information)
        IsConError = False
        DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
End Class