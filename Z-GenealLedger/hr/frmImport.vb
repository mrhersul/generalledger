﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OleDb
Public Class frmImport

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim fBrowse As New OpenFileDialog
        Dim fName As String

        Try
            With fBrowse
                .Filter = "Excel files(*.xlsx)|*.xlsx|All files (*.*)|*.*"
                .FilterIndex = 1
                .Title = "Import data from Excel file"
            End With
            If fBrowse.ShowDialog() = Windows.Forms.DialogResult.OK Then
                fName = fBrowse.FileName
                MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0; Data Source='" & fName & " '; " & "Extended Properties=Excel 8.0;")
                MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
                MyCommand.TableMappings.Add("Table", "Test")
                DtSet = New System.Data.DataSet
                MyCommand.Fill(DtSet)
                
                For Each Drr As DataRow In DtSet.Tables(0).Rows
                    dr.Close()
                    SQL = ""
                    SQL = "DELETE FROM karyawan_payroll where tanggal = '" & Format(txtDateAwal.Value, "yyyy-MM-dd") & "' AND karyawan_nama_id = '" & Drr(0).ToString & "' "
                    SQL = SQL & " INSERT INTO karyawan_payroll(Tanggal, karyawan_nama_id, gaji_pokok,t_jabatan,t_khusus,t_pulsa,t_service,u_makan,u_transport,u_lembur,bpjs_kesehatan, "
                    SQL = SQL & " bpjs_tenagakerja,iuran_koperasi,pinjaman_koperasi,pinjaman_karyawan,ots,potongan_lainnya,keterlambatan,tgl_buat,user_buat) "
                    SQL = SQL & " VALUES ('" & Format(txtDateAwal.Value, "yyyy-MM-dd") & "', '" & Drr(0).ToString & "','" & Drr(2).ToString & "',"
                    SQL = SQL & " '" & Drr(3).ToString & "','" & Drr(4).ToString & "','" & Drr(5).ToString & "','" & Drr(6).ToString & "','" & Drr(7).ToString & "',"
                    SQL = SQL & " '" & Drr(8).ToString & "','" & Drr(9).ToString & "','" & Drr(10).ToString & "', '" & Drr(11).ToString & "','" & Drr(12).ToString & "',"
                    SQL = SQL & " '" & Drr(13).ToString & "','" & Drr(14).ToString & "','" & Drr(15).ToString & "','" & Drr(16).ToString & "','" & Drr(17).ToString & "',"
                    SQL = SQL & " '" & Format(Now, "yyyy-MM-dd") & "', '" & myID & "') "
                    Konek.IUDQuery(SQL)
                Next
                MsgBox("Success")
                MyConnection.Close()

                DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
End Class