﻿Public Class frmResign
    Dim Idnya As Integer
    Dim kd, nm As String
    Public Sub Pengaturan(ByVal kode As String, ByVal nama As String, ByVal Sid As Integer)
        kd = kode
        nm = nama
        Idnya = Sid
    End Sub

    Private Sub frmResign_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        txtKdKaryawan.Text = kd
        txtNmKaryawan.Text = nm
        txtDateAwal.Text = ""
        txtKet.Text = ""
    End Sub

    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnSave_Click(sender As System.Object, e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err
        dr.Close()
        SQL = ""
        SQL = "UPDATE karyawan_mutasi_organisasi SET tanggal_resign = '" & Format(txtDateAwal.Value, "yyyy-MM-dd") & "', status_karyawan_mutasi = '0',"
        SQL = SQL & " tipe_mutasi_id = '0', tanggal_ubah = '" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', user_ubah = '" & myID & "', "
        SQL = SQL & " keterangan_resign = '" & txtKet.Text & "' WHERE personal_id = '" & Idnya & "' "
        Konek.IUDQuery(SQL)

        MsgBox("Data sudah terupdate...", MsgBoxStyle.Information)
        IsConError = False
        Me.Close()
err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
End Class