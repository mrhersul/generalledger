﻿Public Class frmDaftarAbsensivb
    Dim kk As Integer
    Sub ShowDataGrid()

        SQL = ""
        SQL = "SELECT a.tanggal, b.nama_lengkap, a.cekin, a.cekout, a.id FROM karyawan_absensi a"
        SQL = SQL & " inner join personal b on a.karyawan_nama_id = b.id"
        SQL = SQL & " inner join karyawan c on a.karyawan_nama_id = c.personal_id"
        SQL = SQL & " inner join unitbisnis_nama d on d.id = c.unitbisnis_id"
        SQL = SQL & " where d.kd_unitbisnis = '" & myKdBisnis & "' "

        If txtSearch.Text <> "" Then

            SQL = SQL & " and (b.nama_lengkap like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If

        SQL = SQL & " and len(c.kd_karyawan) > 0 "
        SQL = SQL & " order by b.nama_lengkap"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 150
        DGView.Columns(1).Width = 250
        DGView.Columns(2).Width = 150
        DGView.Columns(3).Width = 150
        DGView.Columns(4).Width = 80

        DGView.Columns(0).HeaderText = "TANGGAL"
        DGView.Columns(1).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(2).HeaderText = "ABSEN MASUK"
        DGView.Columns(3).HeaderText = "ABSEN KELUAR"
        DGView.Columns(4).HeaderText = "ID"
        DGView.Columns(4).Visible = False

    End Sub

    Private Sub frmDaftarAbsensivb_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmDaftarAbsensivb_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ShowDataGrid()
    End Sub
    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Private Sub frmDaftarAbsensivb_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 800
        GroupPanel2.Top = 2
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
            End With
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As System.Object, e As System.EventArgs) Handles ButtonX1.Click
        attlogs.ShowDialog()
        If attlogs.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGrid()
        End If
    End Sub
End Class