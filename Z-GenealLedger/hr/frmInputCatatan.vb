﻿Imports System.IO
Public Class frmInputCatatan
    Dim kk As Integer
    Dim Idnya As Integer
    Dim kd, nm, nm_unitbisnis, nm_departemen, kd_unitbisnis, departemen_id As String
    Dim myImage As Image
    Dim imgMemoryStream As IO.MemoryStream = New IO.MemoryStream()
    Dim imgByteArray As Byte() = Nothing
    Public Sub Karyawan(ByVal kode As String, ByVal nama As String, ByVal sid As Integer, ByVal ub As String, ByVal dp As String, ByVal ubid As Integer, ByVal dpid As Integer)
        kd = kode
        nm = nama
        Idnya = sid
        nm_unitbisnis = ub
        nm_departemen = dp
        kd_unitbisnis = ubid
        departemen_id = dpid

        txtKdKaryawan.Text = kd
        txtNmKaryawan.Text = nm
        txtUnitBisnis.Text = nm_unitbisnis
        txtDepartemen.Text = nm_departemen
    End Sub
    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()

        fd.Title = "Select your Image."
        fd.InitialDirectory = "C:\"
        fd.Filter = "All Files|*.*|Bitmaps|*.bmp|GIFs|*.gif|JPEGs|*.jpg|PNGs|*.png"
        fd.RestoreDirectory = False

        If fd.ShowDialog() = DialogResult.OK Then
            PictureBox1.ImageLocation = fd.FileName
            txtPicture.Text = fd.FileName.ToString
        ElseIf fd.FileName.Contains("jpeg") Or fd.FileName.Contains("jpg") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg)
            imgByteArray = imgMemoryStream.GetBuffer()

        ElseIf fd.FileName.Contains("png") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Png)
            imgByteArray = imgMemoryStream.GetBuffer()

        ElseIf fd.FileName.Contains("gif") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Gif)
            imgByteArray = imgMemoryStream.GetBuffer()

        ElseIf fd.FileName.Contains("bmp") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Bmp)
            imgByteArray = imgMemoryStream.GetBuffer()

        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        frmKaryawan.Text = "CATATAN"
        frmKaryawan.ShowDialog()
    End Sub

    Private Sub BtnSave_Click(sender As System.Object, e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        Dim ms As MemoryStream = Nothing
        Dim Genid As String
        'GENERATE ID
        'SQL = ""
        'SQL = "SP_GENERATE_ID"
        'Cmd = New SqlClient.SqlCommand(SQL, Cn)
        'dr = Cmd.ExecuteReader()
        'dr.Read()
        'Genid = dr.Item(0).ToString

        Using fs As FileStream = New FileStream(txtPicture.Text, FileMode.Open, FileAccess.Read)
            Dim bytes(fs.Length) As Byte
            fs.Read(bytes, 0, fs.Length)
            ms = New MemoryStream(bytes)
        End Using

        '-- PILIH CATATAN --
        'PRESTASI()
        'KOMPETENSI()
        'KASUS(Karyawan)
        Dim Jc As Integer
        Select Case cbJenis.Text
            Case "PRESTASI"
                Jc = "1"
            Case "KOMPETENSI"
                Jc = "2"
            Case "KASUS(Karyawan)"
                Jc = "3"
        End Select

        dr.Close()
        SQL = ""
        SQL = "INSERT INTO karyawan_catatan (tanggal, jenis_catatan, karyawan_nama_id, keterangan, upload_doc, judul, tgl_buat, user_buat) "
        SQL = SQL & " VALUES ('" & Format(txtDate.Value, "yyyy-MM-dd") & "', '" & Jc & "', '" & Idnya & "','" & txtKet.Text & "', "
        SQL = SQL & " '" & txtPicture.Text & "', '" & txtJudul.Text & "','" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', '" & myID & "') "
        Konek.IUDQuery(SQL)

        IsConError = False
        MessageBox.Show("Data sudah tersimpan...")
        DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
End Class