﻿Imports System.IO
Public Class frmInputTugasDinas
    Dim Idnya As Integer
    Dim iddt As Integer
    Dim kd, nm As String
    Dim unitbisnis_id As Integer
    Dim kd_unitbisnis, nm_unitbisnis As String
    Dim departemen_id As Integer
    Dim kd_departemen, nm_departemen As String

    Dim myImage As Image
    Dim imgMemoryStream As IO.MemoryStream = New IO.MemoryStream()
    Dim imgByteArray As Byte() = Nothing
    Public Sub Karyawan(ByVal kode As String, ByVal nama As String, ByVal sid As Integer, ByVal ub As String, ByVal dp As String, ByVal ubid As Integer, ByVal dpid As Integer)
        kd = kode
        nm = nama
        Idnya = sid
        nm_unitbisnis = ub
        nm_departemen = dp
        kd_unitbisnis = ubid
        departemen_id = dpid

        txtKdKaryawan.Text = kd
        txtNmKaryawan.Text = nm
        txtUnitBisnis.Text = nm_unitbisnis
        txtDepartemen.Text = nm_departemen
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        frmKaryawan.Text = "TUGAS DINAS"
        frmKaryawan.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub frmInputTugasDinas_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If Len(txtnosj.Text) > 0 Then
            Call ShowDataGrid()
        End If
    End Sub
    Sub ShowDataGrid()
        SQL = ""
        SQL = "select b.nama_lengkap,d.nama_unitbisnis, e.nama as departemen, a.tgl_berangkat, a.tgl_pulang, a.nilai, b.id, c.unitbisnis_mutasi_id, c.departemen_id   "
        SQL = SQL & " from karyawan_tugas_dt a   "
        SQL = SQL & " inner join personal b on a.karyawan_nama_id = b.id  "
        SQL = SQL & " left join karyawan_mutasi_organisasi c on a.karyawan_nama_id = c.personal_id"
        SQL = SQL & " left join unitbisnis_nama d on c.unitbisnis_mutasi_id = d.id"
        SQL = SQL & " left join karyawan_struktur_organisasi e on c.departemen_id = e.id"
        SQL = SQL & " where a.no_sj = '" & txtnosj.Text & "'"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 250
        DGView.Columns(1).Width = 100
        DGView.Columns(2).Width = 200
        DGView.Columns(3).Width = 150
        DGView.Columns(4).Width = 150
        DGView.Columns(5).Width = 250
        DGView.Columns(6).Width = 80
        DGView.Columns(7).Width = 80
        DGView.Columns(8).Width = 80

        DGView.Columns(0).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(1).HeaderText = "UNIT BISNIS"
        DGView.Columns(2).HeaderText = "DEPARTEMEN"
        DGView.Columns(3).HeaderText = "BERANGKAT"
        DGView.Columns(4).HeaderText = "PULANG"
        DGView.Columns(5).HeaderText = "NILAI"
        DGView.Columns(6).HeaderText = "ID"
        DGView.Columns(7).HeaderText = "ID UNITBISNIS"
        DGView.Columns(8).HeaderText = "ID DEPARTEMEN"
        DGView.Columns(6).Visible = False
        DGView.Columns(7).Visible = False
        DGView.Columns(8).Visible = False

    End Sub

    Private Sub btnUnitBisnis_Click(sender As System.Object, e As System.EventArgs)
        frmUnitBisnis.Text = "TUGAS DINAS"
        frmUnitBisnis.ShowDialog()
    End Sub

    Private Sub btnDepartemen_Click(sender As System.Object, e As System.EventArgs)
        frmDepartemen.Text = "TUGAS DINAS"
        frmDepartemen.ShowDialog()
    End Sub

    Private Sub BtnSave_Click(sender As System.Object, e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        Dim ms As MemoryStream = Nothing

        Using fs As FileStream = New FileStream(txtPicture.Text, FileMode.Open, FileAccess.Read)
            Dim bytes(fs.Length) As Byte
            fs.Read(bytes, 0, fs.Length)
            ms = New MemoryStream(bytes)
        End Using

        'Dim Genid As String
        'SQL = ""
        'SQL = "SP_GENERATE_ID"
        'Cmd = New SqlClient.SqlCommand(SQL, Cn)
        'dr = Cmd.ExecuteReader()
        'dr.Read()
        'Genid = dr.Item(0).ToString

        dr.Close()
        SQL = ""
        SQL = "INSERT INTO karyawan_tugas_hd (no_sj,tanggal, tgl_buat, user_buat,gambar,unitbisnis_id) "
        SQL = SQL & " VALUES ('" & txtnosj.Text & "', '" & Format(dtTanggal.Value, "yyyy-MM-dd") & "', "
        SQL = SQL & " '" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', '" & myID & "', '" & txtPicture.Text & "','" & myKdBisnis & "') "
        Konek.IUDQuery(SQL)

        'dr.Close()
        'SQL = ""
        'SQL = "UPDATE karyawan_tugas_dinas_dt SET karyawan_tugas_dinas_id = '" & Genid & "' WHERE no_sj = '" & txtnosj.Text & "' "
        'Konek.IUDQuery(SQL)

        IsConError = False
        ShowDataGrid()
        txtPicture.Text = ""
        txtnosj.Text = ""
        dtTanggal.Value = ""
        txtKdKaryawan.Text = ""
        txtNmKaryawan.Text = ""
        txtDepartemen.Text = ""
        txtUnitBisnis.Text = ""
        txtNilai.Text = ""

        MsgBox("Data sudah tersimpan.")
        DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim IsConError As Boolean
        On Error GoTo err
        If Len(txtKdKaryawan.Text) > 0 And Len(txtNilai.Text) > 0 Then
            'Dim Genid As String
            'SQL = ""
            'SQL = "SP_GENERATE_ID"
            'Cmd = New SqlClient.SqlCommand(SQL, Cn)
            'dr = Cmd.ExecuteReader()
            'dr.Read()
            'Genid = dr.Item(0).ToString

            dr.Close()
            SQL = ""
            SQL = "INSERT INTO karyawan_tugas_dt (no_sj,karyawan_nama_id, tgl_berangkat, tgl_pulang, nilai) "
            SQL = SQL & " VALUES ('" & txtnosj.Text & "', " & Idnya & ", '" & Format(dtBerangkat.Value, "yyyy-MM-dd") & "', '" & Format(dtPulang.Value, "yyyy-MM-dd") & "',"
            SQL = SQL & " '" & txtNilai.Text & "') "
            Konek.IUDQuery(SQL)

            IsConError = False
            ShowDataGrid()
            txtKdKaryawan.Text = ""
            txtNmKaryawan.Text = ""
            txtDepartemen.Text = ""
            txtUnitBisnis.Text = ""
            txtNilai.Text = ""
        Else
            MessageBox.Show("KARYAWAN BELUM DIPILIH...!")
            Return
        End If

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select

    End Sub

    Private Sub DGView_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellContentClick
        TakeDGView()
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                iddt = .Item("id", i).Value
                'txtKdKaryawan.Text = .Item("kd_karyawan", i).Value
                'txtNmKaryawan.Text = .Item("nama_lengkap", i).Value
            End With
        End If
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        TakeDGView()
        Dim IsConError As Boolean
        On Error GoTo err

        dr.Close()
        SQL = ""
        SQL = "DELETE FROM karyawan_tugas_dt WHERE id = '" & iddt & "' "
        Konek.IUDQuery(SQL)

        IsConError = False
        ShowDataGrid()
        txtKdKaryawan.Text = ""
        txtNmKaryawan.Text = ""
        txtDepartemen.Text = ""
        txtUnitBisnis.Text = ""
        txtNilai.Text = ""
        Call ShowDataGrid()

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        'Dim oReader As StreamReader
        'OpenFileDialog1.FileName = ""
        'OpenFileDialog1.Filter = "Text Files (*.jpg)|*.jpg|All Files (*.*)|*.*"

        'If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    oReader = New StreamReader(OpenFileDialog1.FileName, True)
        '    txtPicture.Text = oReader.ReadToEnd
        'End If

        ' Call ShowDialog.
        'Dim result As DialogResult = OpenFileDialog1.ShowDialog()

        '' Test result.
        'If result = Windows.Forms.DialogResult.OK Then

        '    ' Get the file name.
        '    Dim path As String = OpenFileDialog1.FileName
        '    Try
        '        ' Read in text.
        '        Dim text As String = File.ReadAllText(path)

        '        ' For debugging.
        '        'txtPicture.Text = text.Length.ToString
        '        'txtPicture.Text = text.ToString
        '        txtPicture.Text = path.ToString
        '    Catch ex As Exception

        '        ' Report an error.
        '        txtPicture.Text = "Error"

        '    End Try
        'End If

        Dim fd As OpenFileDialog = New OpenFileDialog()

        fd.Title = "Select your Image."
        fd.InitialDirectory = "C:\"
        fd.Filter = "All Files|*.*|Bitmaps|*.bmp|GIFs|*.gif|JPEGs|*.jpg|PNGs|*.png"
        fd.RestoreDirectory = False

        If fd.ShowDialog() = DialogResult.OK Then
            PictureBox1.ImageLocation = fd.FileName
            txtPicture.Text = fd.FileName.ToString
        ElseIf fd.FileName.Contains("jpeg") Or fd.FileName.Contains("jpg") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg)
            imgByteArray = imgMemoryStream.GetBuffer()

        ElseIf fd.FileName.Contains("png") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Png)
            imgByteArray = imgMemoryStream.GetBuffer()

        ElseIf fd.FileName.Contains("gif") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Gif)
            imgByteArray = imgMemoryStream.GetBuffer()

        ElseIf fd.FileName.Contains("bmp") Then

            myImage.Save(imgMemoryStream, System.Drawing.Imaging.ImageFormat.Bmp)
            imgByteArray = imgMemoryStream.GetBuffer()

        End If
    End Sub
End Class