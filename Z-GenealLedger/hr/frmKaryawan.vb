﻿Public Class frmKaryawan
    Dim kk, ubid, dpid, djab As Integer
    Dim kd, nm As String
    Dim ub As String
    Dim dp As String
    Dim jab As String
    Sub ShowDataGrid()

        SQL = ""
        'SQL = " select a.kode,a.nama,a.type_perkiraan,a.kas_bank,b.nama_perkiraan_group,a.tanggal_buat,case a.status when 1 then 'AKTIF' else 'NON AKTIF' end as status"
        'SQL = SQL & " from acc_perkiraan_nama a left join acc_perkiraan_group b on a.acc_perkiraan_group_id=b.id where 0=0"

        SQL = "select b.kd_karyawan, a.nama_lengkap, a.id from personal a "
        SQL = SQL & "inner join karyawan b on a.id = b.personal_id "
        SQL = SQL & "inner join unitbisnis_nama c on c.id = b.unitbisnis_id "
        SQL = SQL & "WHERE c.kd_unitbisnis = '" & myKdBisnis & "' "

        If txtSearch.Text <> "" Then

            SQL = SQL & " and (a.nama_lengkap like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If

        SQL = SQL & " and len(b.kd_karyawan) > 0 "
        SQL = SQL & " order by a.nama_lengkap"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 150
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 80

        DGView.Columns(0).HeaderText = "KODE KARYAWAN"
        DGView.Columns(1).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(2).HeaderText = "ID"
        DGView.Columns(2).Visible = False

        ColorChange()

    End Sub
    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next

    End Sub

    Private Sub frmKaryawan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txtSearch.Text = ""
        ShowDataGrid()
    End Sub

    Private Sub DGView_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellContentDoubleClick
        TakeDGView()
        Select Case Me.Text
            Case "CUTI"
                Call frmCuti.Karyawan(kd, nm, kk)
            Case "IZIN"
                Call frmIzin.Karyawan(kd, nm, kk)
            Case "TUGAS DINAS"
                SQL = ""
                SQL = "select b.kd_karyawan, a.nama_lengkap, c.nama_unitbisnis,d.nama as departemen, a.id, b.unitbisnis_mutasi_id, b.departemen_id from personal a "
                SQL = SQL & " inner join karyawan_mutasi_organisasi b on a.id = b.personal_id "
                SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_mutasi_id "
                SQL = SQL & " inner join karyawan_struktur_organisasi d on d.id = b.departemen_id"
                SQL = SQL & " WHERE a.id = '" & kk & "' "

                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()
                'dr.Read()
                While dr.Read()
                    ub = dr.Item(2)
                    dp = dr.Item(3)
                End While
                Call frmInputTugasDinas.Karyawan(kd, nm, kk, ub, dp, ubid, dpid)
            Case "PAYROLL"
                SQL = ""
                SQL = "select b.kd_karyawan, a.nama_lengkap, c.nama_unitbisnis, e.nama as departemen, f.nama as jabatan, a.id, d.unitbisnis_mutasi_id, d.departemen_id,"
                SQL = SQL & " d.jabatan_id  from personal a"
                SQL = SQL & " inner join karyawan b on b.personal_id = a.id"
                SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_id"
                SQL = SQL & " inner join karyawan_mutasi_organisasi d on a.id = d.personal_id"
                SQL = SQL & " inner join karyawan_struktur_organisasi e on e.id = d.departemen_id"
                SQL = SQL & " inner join karyawan_struktur_organisasi f on f.id = d.jabatan_id"
                SQL = SQL & " WHERE a.id = '" & kk & "' "
                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()
                'dr.Read()
                While dr.Read()
                    ub = dr.Item(2)
                    dp = dr.Item(3)
                    jab = dr.Item(4)
                End While
                Call frmInputPayroll.Karyawan(kd, nm, kk, ub, dp, jab, ubid, dpid, djab)
            Case "CATATAN"
                SQL = ""
                SQL = "select b.kd_karyawan, a.nama_lengkap, c.nama_unitbisnis,d.nama as departemen, a.id, b.unitbisnis_mutasi_id, b.departemen_id from personal a "
                SQL = SQL & " inner join karyawan_mutasi_organisasi b on a.id = b.personal_id "
                SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_mutasi_id "
                SQL = SQL & " inner join karyawan_struktur_organisasi d on d.id = b.departemen_id"
                SQL = SQL & " WHERE a.id = '" & kk & "' "

                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()
                'dr.Read()
                While dr.Read()
                    ub = dr.Item(2)
                    dp = dr.Item(3)
                End While
                Call frmInputCatatan.Karyawan(kd, nm, kk, ub, dp, ubid, dpid)
        End Select
        Me.Close()
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
                kd = .Item("kd_karyawan", i).Value
                nm = .Item("nama_lengkap", i).Value
            End With
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Call ShowDataGrid()
    End Sub
End Class