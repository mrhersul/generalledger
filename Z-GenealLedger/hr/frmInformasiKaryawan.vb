﻿Public Class frmInformasiKaryawan
    Dim Idnya As Integer
    Public Sub Pengaturan(ByVal Sid As Integer)
        Idnya = Sid
    End Sub

    Private Sub frmInformasiKaryawan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        SQL = ""
        SQL = "SP_HR_InfoKaryawan " & Idnya & " "


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read
                txtNama.Text = Trim(dr.Item("nama_lengkap").ToString())
                txtKdKaryawan.Text = Trim(dr.Item("kd_karyawan").ToString())
                txtDate.Value = dr.Item("tgl_join").ToString()
                txtUnitBisnis.Text = Trim(dr.Item("nama_unitbisnis").ToString())
                txtDepartemen.Text = Trim(dr.Item("departemen").ToString())
                txtJabatan.Text = Trim(dr.Item("jabatan").ToString())
                Select Case dr.Item("status_karyawan_mutasi").ToString()
                    Case "0"
                        txtStatus.Text = "RESIGN"
                    Case "1"
                        txtStatus.Text = "AKTIF"
                    Case "2"
                        txtStatus.Text = "MUTASI"
                End Select
                txtKelamin.Text = Trim(dr.Item("jenis_kelamin").ToString())
                txtKtp.Text = Trim(dr.Item("no_ktp").ToString())
                txtNamaLengkap.Text = Trim(dr.Item("nama_lengkap").ToString())
                txtPanggilan.Text = Trim(dr.Item("nama_panggilan").ToString())
                txtTempatLahir.Text = Trim(dr.Item("tempat_lahir").ToString())
                dtLahir.Value = dr.Item("tgl_lahir").ToString()
                txtAgama.Text = dr.Item("agama_nama").ToString
                txtWarga.Text = dr.Item("kewarganegaraan_nama").ToString
                txtSuku.Text = dr.Item("suku").ToString
                txtGolDarah.Text = dr.Item("goldar_nama").ToString
                txtTelp1.Text = Trim(dr.Item("telepon_1").ToString())
                txtTelp2.Text = Trim(dr.Item("telepon_2").ToString())
                txtEmail.Text = Trim(dr.Item("email_1").ToString())
                txtEmail2.Text = Trim(dr.Item("email_2").ToString())
                txAlamat.Text = Trim(dr.Item("alamat").ToString())
                txtStatusRumah.Text = dr.Item("status_rumah").ToString
                txtDetailLokasi.Text = Trim(dr.Item("detail_lokasi").ToString())
                txtProvinsi.Text = Trim(dr.Item("nama_provinsi").ToString())
                txtKota.Text = Trim(dr.Item("nama_kota").ToString())
                txtKecamatan.Text = Trim(dr.Item("nama_kecamatan").ToString())
                txtKelurahan.Text = Trim(dr.Item("nama_kelurahan").ToString())
                txtKodepos.Text = ""
                txtStatusPerkawinan.Text = dr.Item("status_perkawinan").ToString
                dtMenikah.Value = dr.Item("tgl_menikah").ToString()
                txtJumlahTanggungan.Text = Trim(dr.Item("tanggungan_keluarga").ToString())
                txtAnakKe.Text = Trim(dr.Item("anak_nomor").ToString())
                txtNamaIbu.Text = Trim(dr.Item("nama_ibu_kandung").ToString())

                txtStatusPendidikan.Text = dr.Item("status_masih_pendidikan").ToString()
                txtTingkat.Text = dr.Item("tingkat_pendidikan").ToString()
                txtNamaSekolah.Text = Trim(dr.Item("nama_sekolah").ToString())
                txtTahunIjazah.Text = Trim(dr.Item("tahun_ijazah").ToString())
                txtJurusan.Text = Trim(dr.Item("jurusan_studi").ToString())
                txAlamatSekolah.Text = Trim(dr.Item("alamat_sekolah").ToString())
                txtNoRek.Text = Trim(dr.Item("rek_no").ToString())
                txtRekAn.Text = Trim(dr.Item("rek_atas_nama").ToString())
                txtNamaBank.Text = Trim(dr.Item("rek_nama_bank").ToString())
                txtNpwp.Text = Trim(dr.Item("no_npwp").ToString())
                txtCC.Text = Trim(dr.Item("no_cc").ToString())
            End While
        End If
        ShowDataGridPrestasi()
        ShowDataGridPromosi()
    End Sub
    Sub ShowDataGridPrestasi()
        SQL = ""
        SQL = "SP_HR_CATATAN_BY_NAMA_ID " & Idnya & " "

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt

        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 100
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 350
        DGView.Columns(3).Width = 250
        DGView.Columns(4).Width = 150
        DGView.Columns(5).Width = 200
        DGView.Columns(6).Width = 150
        DGView.Columns(7).Width = 300
        DGView.Columns(8).Width = 80


        DGView.Columns(0).HeaderText = "TANGGAL"
        DGView.Columns(1).HeaderText = "KODE KARYAWAN"
        DGView.Columns(2).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(3).HeaderText = "JUDUL"
        DGView.Columns(4).HeaderText = "JENIS"
        DGView.Columns(5).HeaderText = "DEPARTEMEN"
        DGView.Columns(6).HeaderText = "JABATAN"
        DGView.Columns(7).HeaderText = "KETERANGAN"
        DGView.Columns(8).HeaderText = "ID"
        DGView.Columns(8).Visible = False
    End Sub
    Sub ShowDataGridPromosi()

        SQL = ""
        SQL = "SP_HR_List_Mutasi " & Idnya & " "
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGViewMutasi.DataSource = dt


        DGViewMutasi.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGViewMutasi.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGViewMutasi.Columns(0).Width = 300
        DGViewMutasi.Columns(1).Width = 150
        DGViewMutasi.Columns(2).Width = 150
        DGViewMutasi.Columns(3).Width = 150
        DGViewMutasi.Columns(4).Width = 150
        DGViewMutasi.Columns(5).Width = 150

        DGViewMutasi.Columns(0).HeaderText = "KODE KARYAWAN"
        DGViewMutasi.Columns(1).HeaderText = "NAMA KARYAWAN"
        DGViewMutasi.Columns(2).HeaderText = "UNIT BISNIS"
        DGViewMutasi.Columns(3).HeaderText = "DEPARTEMEN"
        DGViewMutasi.Columns(4).HeaderText = "JABATAN"
        DGViewMutasi.Columns(5).HeaderText = "TIPE"
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Call frmResign.Pengaturan(txtKdKaryawan.Text, txtNamaLengkap.Text, Idnya)
        frmResign.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        frmMutasi.Pengaturan(Idnya)
        frmMutasi.ShowDialog()
    End Sub
End Class