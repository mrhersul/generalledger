﻿Public Class frmCatatan
    Dim Idnya As Integer
    Public Sub Pengaturan(ByVal Sid As Integer)
        Idnya = Sid
    End Sub

    Sub ShowDataGridPrestasi()
        SQL = ""
        SQL = "SP_HR_CATATAN_BY_NAMA_ID " & Idnya & " "
        'SQL = "select a.tanggal, b.nama_lengkap, a.judul, d.nama as departemen, e.nama as jabatan, a.keterangan from karyawan_catatan a"
        'SQL = SQL & " inner join personal b on b.id = a.karyawan_nama_id"
        'SQL = SQL & " inner join karyawan_mutasi_organisasi c on c.personal_id = a.karyawan_nama_id"
        'SQL = SQL & " inner join karyawan_struktur_organisasi d on d.id = c.departemen_id"
        'SQL = SQL & " inner join karyawan_struktur_organisasi e on e.id = c.jabatan_id"
        'SQL = SQL & " where a.jenis_catatan = '1' AND a.karyawan_nama_id = " & Idnya & " "

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt

        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 100
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 350
        DGView.Columns(3).Width = 250
        DGView.Columns(4).Width = 150
        DGView.Columns(5).Width = 200
        DGView.Columns(6).Width = 150
        DGView.Columns(7).Width = 300
        DGView.Columns(8).Width = 80


        DGView.Columns(0).HeaderText = "TANGGAL"
        DGView.Columns(1).HeaderText = "KODE KARYAWAN"
        DGView.Columns(2).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(3).HeaderText = "JUDUL"
        DGView.Columns(4).HeaderText = "JENIS"
        DGView.Columns(5).HeaderText = "DEPARTEMEN"
        DGView.Columns(6).HeaderText = "JABATAN"
        DGView.Columns(7).HeaderText = "KETERANGAN"
        DGView.Columns(8).HeaderText = "ID"
        DGView.Columns(8).Visible = False
    End Sub
    
    Private Sub frmCatatan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ShowDataGridPrestasi()
    End Sub

    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub DGViewPrestasi_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellContentDoubleClick
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewKompetensi_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewKasus_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewPrestasi_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewKompetensi_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewKasus_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewKasus_DoubleClick(sender As System.Object, e As System.EventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewKompetensi_DoubleClick(sender As System.Object, e As System.EventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub

    Private Sub DGViewPrestasi_DoubleClick(sender As System.Object, e As System.EventArgs)
        frmDetailCatatan.ShowDialog()
    End Sub
End Class