﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInputCatatan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInputCatatan))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtPicture = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.txtUnitBisnis = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.txtJudul = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cbJenis = New System.Windows.Forms.ComboBox()
        Me.txtDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.txtDate = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtNmKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.PictureBox1)
        Me.GroupPanel1.Controls.Add(Me.txtPicture)
        Me.GroupPanel1.Controls.Add(Me.Button2)
        Me.GroupPanel1.Controls.Add(Me.LabelX10)
        Me.GroupPanel1.Controls.Add(Me.txtUnitBisnis)
        Me.GroupPanel1.Controls.Add(Me.LabelX8)
        Me.GroupPanel1.Controls.Add(Me.txtJudul)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.Button1)
        Me.GroupPanel1.Controls.Add(Me.cbJenis)
        Me.GroupPanel1.Controls.Add(Me.txtDepartemen)
        Me.GroupPanel1.Controls.Add(Me.LabelX7)
        Me.GroupPanel1.Controls.Add(Me.txtKdKaryawan)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.LabelX4)
        Me.GroupPanel1.Controls.Add(Me.txtDate)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.txtNmKaryawan)
        Me.GroupPanel1.Controls.Add(Me.txtKet)
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(13, 13)
        Me.GroupPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1275, 447)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 11
        Me.GroupPanel1.Text = "FORM CATATAN KARYAWAN"
        '
        'txtPicture
        '
        '
        '
        '
        Me.txtPicture.Border.Class = "TextBoxBorder"
        Me.txtPicture.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPicture.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPicture.Location = New System.Drawing.Point(195, 134)
        Me.txtPicture.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPicture.Name = "txtPicture"
        Me.txtPicture.Size = New System.Drawing.Size(342, 26)
        Me.txtPicture.TabIndex = 154
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(544, 130)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 33)
        Me.Button2.TabIndex = 153
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(35, 132)
        Me.LabelX10.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(152, 28)
        Me.LabelX10.TabIndex = 152
        Me.LabelX10.Text = "UPLOAD DOK."
        '
        'txtUnitBisnis
        '
        '
        '
        '
        Me.txtUnitBisnis.Border.Class = "TextBoxBorder"
        Me.txtUnitBisnis.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUnitBisnis.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnitBisnis.Location = New System.Drawing.Point(812, 130)
        Me.txtUnitBisnis.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUnitBisnis.Name = "txtUnitBisnis"
        Me.txtUnitBisnis.Size = New System.Drawing.Size(433, 26)
        Me.txtUnitBisnis.TabIndex = 149
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(652, 130)
        Me.LabelX8.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(152, 28)
        Me.LabelX8.TabIndex = 148
        Me.LabelX8.Text = "UNIT BISNIS"
        '
        'txtJudul
        '
        '
        '
        '
        Me.txtJudul.Border.Class = "TextBoxBorder"
        Me.txtJudul.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJudul.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJudul.Location = New System.Drawing.Point(195, 96)
        Me.txtJudul.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJudul.Name = "txtJudul"
        Me.txtJudul.Size = New System.Drawing.Size(433, 26)
        Me.txtJudul.TabIndex = 147
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(35, 96)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(152, 28)
        Me.LabelX2.TabIndex = 146
        Me.LabelX2.Text = "TITLE"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1170, 24)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 28)
        Me.Button1.TabIndex = 145
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbJenis
        '
        Me.cbJenis.AutoCompleteCustomSource.AddRange(New String() {"PRESTASI", "KOMPETENSI"})
        Me.cbJenis.FormattingEnabled = True
        Me.cbJenis.Items.AddRange(New Object() {"-- PILIH CATATAN --", "PRESTASI", "KOMPETENSI", "KASUS KARYAWAN"})
        Me.cbJenis.Location = New System.Drawing.Point(195, 60)
        Me.cbJenis.Name = "cbJenis"
        Me.cbJenis.Size = New System.Drawing.Size(188, 28)
        Me.cbJenis.TabIndex = 144
        '
        'txtDepartemen
        '
        '
        '
        '
        Me.txtDepartemen.Border.Class = "TextBoxBorder"
        Me.txtDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDepartemen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartemen.Location = New System.Drawing.Point(812, 96)
        Me.txtDepartemen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDepartemen.Name = "txtDepartemen"
        Me.txtDepartemen.Size = New System.Drawing.Size(433, 26)
        Me.txtDepartemen.TabIndex = 143
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(652, 96)
        Me.LabelX7.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(152, 28)
        Me.LabelX7.TabIndex = 142
        Me.LabelX7.Text = "DEPARTEMEN"
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.Enabled = False
        Me.txtKdKaryawan.Location = New System.Drawing.Point(812, 62)
        Me.txtKdKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(433, 26)
        Me.txtKdKaryawan.TabIndex = 140
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(652, 60)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(152, 28)
        Me.LabelX6.TabIndex = 139
        Me.LabelX6.Text = "KODE KARYAWAN"
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(35, 59)
        Me.LabelX4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(152, 28)
        Me.LabelX4.TabIndex = 138
        Me.LabelX4.Text = "JENIS CATATAN"
        '
        'txtDate
        '
        '
        '
        '
        Me.txtDate.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate.ButtonDropDown.Visible = True
        Me.txtDate.IsPopupCalendarOpen = False
        Me.txtDate.Location = New System.Drawing.Point(195, 26)
        Me.txtDate.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.txtDate.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.TodayButtonVisible = True
        Me.txtDate.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(188, 26)
        Me.txtDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate.TabIndex = 133
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(35, 24)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(152, 28)
        Me.LabelX5.TabIndex = 35
        Me.LabelX5.Text = "TANGGAL"
        '
        'txtNmKaryawan
        '
        '
        '
        '
        Me.txtNmKaryawan.Border.Class = "TextBoxBorder"
        Me.txtNmKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmKaryawan.Enabled = False
        Me.txtNmKaryawan.Location = New System.Drawing.Point(812, 26)
        Me.txtNmKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNmKaryawan.Name = "txtNmKaryawan"
        Me.txtNmKaryawan.Size = New System.Drawing.Size(351, 26)
        Me.txtNmKaryawan.TabIndex = 29
        '
        'txtKet
        '
        '
        '
        '
        Me.txtKet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKet.Location = New System.Drawing.Point(195, 170)
        Me.txtKet.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKet.Multiline = True
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(1050, 107)
        Me.txtKet.TabIndex = 4
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(35, 170)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(122, 28)
        Me.LabelX3.TabIndex = 12
        Me.LabelX3.Text = "KETERANGAN"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(1152, 309)
        Me.BtnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(93, 89)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(1033, 309)
        Me.BtnSave.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(93, 89)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "SAVE"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(652, 24)
        Me.LabelX1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(152, 28)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "NAMA KARYAWAN"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(195, 289)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(143, 109)
        Me.PictureBox1.TabIndex = 155
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'frmInputCatatan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1312, 483)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmInputCatatan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInputCatatan"
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents txtPicture As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUnitBisnis As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtJudul As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cbJenis As System.Windows.Forms.ComboBox
    Friend WithEvents txtDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDate As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNmKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
End Class
