﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIzin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIzin))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtDateAkhir = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtDateAwal = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtNmKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.txtDateAkhir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDateAwal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.Button1)
        Me.GroupPanel1.Controls.Add(Me.txtKdKaryawan)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.txtDateAkhir)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.txtDateAwal)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.txtNmKaryawan)
        Me.GroupPanel1.Controls.Add(Me.txtKet)
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(13, 13)
        Me.GroupPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(682, 428)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 10
        Me.GroupPanel1.Text = "FORM INPUT IZIN"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(583, 20)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(45, 28)
        Me.Button1.TabIndex = 143
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.Enabled = False
        Me.txtKdKaryawan.Location = New System.Drawing.Point(195, 58)
        Me.txtKdKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(433, 26)
        Me.txtKdKaryawan.TabIndex = 140
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(35, 56)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(152, 28)
        Me.LabelX6.TabIndex = 139
        Me.LabelX6.Text = "KODE KARYAWAN"
        '
        'txtDateAkhir
        '
        '
        '
        '
        Me.txtDateAkhir.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDateAkhir.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAkhir.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDateAkhir.ButtonDropDown.Visible = True
        Me.txtDateAkhir.IsPopupCalendarOpen = False
        Me.txtDateAkhir.Location = New System.Drawing.Point(440, 104)
        Me.txtDateAkhir.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.txtDateAkhir.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDateAkhir.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAkhir.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDateAkhir.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDateAkhir.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAkhir.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDateAkhir.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDateAkhir.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDateAkhir.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDateAkhir.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDateAkhir.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDateAkhir.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAkhir.MonthCalendar.TodayButtonVisible = True
        Me.txtDateAkhir.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDateAkhir.Name = "txtDateAkhir"
        Me.txtDateAkhir.Size = New System.Drawing.Size(188, 26)
        Me.txtDateAkhir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDateAkhir.TabIndex = 137
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(402, 105)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(47, 28)
        Me.LabelX2.TabIndex = 136
        Me.LabelX2.Text = "S/D"
        '
        'txtDateAwal
        '
        '
        '
        '
        Me.txtDateAwal.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDateAwal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAwal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDateAwal.ButtonDropDown.Visible = True
        Me.txtDateAwal.IsPopupCalendarOpen = False
        Me.txtDateAwal.Location = New System.Drawing.Point(195, 104)
        Me.txtDateAwal.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.txtDateAwal.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDateAwal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAwal.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDateAwal.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDateAwal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAwal.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDateAwal.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDateAwal.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDateAwal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDateAwal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDateAwal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDateAwal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDateAwal.MonthCalendar.TodayButtonVisible = True
        Me.txtDateAwal.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDateAwal.Name = "txtDateAwal"
        Me.txtDateAwal.Size = New System.Drawing.Size(188, 26)
        Me.txtDateAwal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDateAwal.TabIndex = 133
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(35, 105)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(152, 28)
        Me.LabelX5.TabIndex = 35
        Me.LabelX5.Text = "TANGGAL IZIN"
        '
        'txtNmKaryawan
        '
        '
        '
        '
        Me.txtNmKaryawan.Border.Class = "TextBoxBorder"
        Me.txtNmKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmKaryawan.Enabled = False
        Me.txtNmKaryawan.Location = New System.Drawing.Point(195, 22)
        Me.txtNmKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNmKaryawan.Name = "txtNmKaryawan"
        Me.txtNmKaryawan.Size = New System.Drawing.Size(378, 26)
        Me.txtNmKaryawan.TabIndex = 29
        '
        'txtKet
        '
        '
        '
        '
        Me.txtKet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKet.Location = New System.Drawing.Point(194, 151)
        Me.txtKet.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKet.Multiline = True
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(434, 78)
        Me.txtKet.TabIndex = 4
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(35, 149)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(109, 28)
        Me.LabelX3.TabIndex = 12
        Me.LabelX3.Text = "KETERANGAN"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(535, 287)
        Me.BtnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(93, 89)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(431, 287)
        Me.BtnSave.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(93, 89)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "SAVE"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(35, 20)
        Me.LabelX1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(152, 28)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "NAMA KARYAWAN"
        '
        'frmIzin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(710, 457)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmIzin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmIzin"
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.txtDateAkhir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDateAwal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDateAkhir As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDateAwal As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNmKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
End Class
