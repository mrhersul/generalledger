﻿Public Class frmJabatan
    Dim kk As Integer
    Dim kd, nm As String
    Dim IDept As Integer
    Dim PNama As String
    Dim nSque As String
    Dim KdFix As String
    Public Sub Pengaturan(ByVal IdDept As Integer, ByVal Nama As String)
        IDept = IdDept
        PNama = Nama
    End Sub
    Sub ShowDataGrid()

        SQL = ""
        SQL = "select kode, nama, id from karyawan_struktur_organisasi where parent_id = '" & IDept & "' "
        SQL = SQL & " and nama like '%" & txtSearch.Text & "%' "
        SQL = SQL & " order by kode"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 150
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 80

        DGView.Columns(0).HeaderText = "KODE JABATAN"
        DGView.Columns(1).HeaderText = "NAMA JABATAN"
        DGView.Columns(2).HeaderText = "ID"
        DGView.Columns(2).Visible = False

        ColorChange()

    End Sub
    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next

    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
                kd = .Item("kode", i).Value
                nm = .Item("nama", i).Value
            End With
        End If
    End Sub

    Private Sub frmJabatan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ShowDataGrid()
    End Sub

    Private Sub btnCari_Click(sender As System.Object, e As System.EventArgs) Handles btnCari.Click
        Call ShowDataGrid()
    End Sub

    Private Sub DGView_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellContentDoubleClick
        Dim Nk As String
        Dim KdJab As String
        Dim KdSementara As String

        Nk = frmMutasi.txtNikKaryawan.Text
        KdJab = ""
        TakeDGView()
        Select Case Me.Text
            Case "FORM MUTASI"
                frmMutasi.txtIDJabatan.Text = kk
                frmMutasi.txtKdJabatan.Text = kd
                frmMutasi.txtNmJabatan.Text = nm

                Select Case nm
                    Case "Sales Force"
                        KdJab = "SF"
                    Case "Mitra Usaha"
                        KdJab = "MU"
                    Case "Pramuniaga"
                        KdJab = "PR"
                    Case Else
                        KdJab = "MJ"
                End Select
                KdSementara = Nk + KdJab + PNama

                SQL = ""
                SQL = "SELECT top(1) right(kd_karyawan,3) as kdkar FROM karyawan where kd_karyawan like '%" & KdSementara & "%' "
                SQL = SQL & " order by kd_karyawan desc"
                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()
                If dr.HasRows Then
                    While dr.Read
                        Dim Sque As Integer
                        Sque = Val(Microsoft.VisualBasic.Right(Trim(dr.Item("kd_karyawan").ToString()), 3)) + 1
                        Select Case Len(Sque)
                            Case 1
                                nSque = "00" + Sque.ToString
                            Case 2
                                nSque = "0" + Sque.ToString
                            Case 3
                                nSque = Sque.ToString
                        End Select
                        KdFix = KdSementara + nSque
                    End While
                Else
                    KdFix = KdSementara + "001"
                End If

                frmMutasi.txtIDJabatan.Text = kk
                frmMutasi.txtKdJabatan.Text = kd
                frmMutasi.txtNmJabatan.Text = nm
                frmMutasi.txtNikKaryawan.Text = KdFix
        End Select
        Me.Close()
    End Sub
End Class