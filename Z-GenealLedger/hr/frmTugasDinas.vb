﻿Public Class frmTugasDinas
    Dim kk As String
    Dim nosj As String
    Sub ShowDataGrid()
        Dim nYear As String = Year(Now)
        Dim nMont As String = Month(Now)

        If Len(nMont) = 1 Then
            nMont = "0" + nMont
        Else
            nMont = nMont
        End If

        Select Case cmbBulan.Text
            Case "JANUARI"
                nMont = "01"
            Case "FEBRUARI"
                nMont = "02"
            Case "MARET"
                nMont = "03"
            Case "APRIL"
                nMont = "04"
            Case "MEI"
                nMont = "05"
            Case "JUNI"
                nMont = "06"
            Case "JULI"
                nMont = "07"
            Case "AGUSTUS"
                nMont = "08"
            Case "SEPTEMBER"
                nMont = "09"
            Case "OKTOBER"
                nMont = "10"
            Case "NOVEMBER"
                nMont = "11"
            Case "DESEMBER"
                nMont = "12"
            Case Else
                nMont = nMont
        End Select

        If txtTahun.Text = "" Then
            nYear = nYear
        Else
            nYear = txtTahun.Text
        End If

        SQL = ""
        SQL = "SELECT a.no_sj, a.tanggal, b.nama_unitbisnis, a.id FROM karyawan_tugas_hd a"
        SQL = SQL & " inner join unitbisnis_nama b on b.kd_unitbisnis = a.unitbisnis_id "
        SQL = SQL & " where b.kd_unitbisnis = '" & myKdBisnis & "' "
        SQL = SQL & " AND month(a.tanggal) = '" & nMont & "' AND year(a.tanggal) = '" & nYear & "' "
        SQL = SQL & " collate SQL_Latin1_General_CP1_CI_AS "
        SQL = SQL & " order by a.id desc"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 250
        DGView.Columns(1).Width = 150
        DGView.Columns(2).Width = 250
        DGView.Columns(3).Width = 80

        DGView.Columns(0).HeaderText = "NO SURAT TUGAS"
        DGView.Columns(1).HeaderText = "TANGGAL"
        DGView.Columns(2).HeaderText = "UNIT BISNIS"
        DGView.Columns(3).HeaderText = "ID"
        DGView.Columns(3).Visible = False

    End Sub
    Sub ShowDetailGrid()
        TakeDGView()
        SQL = ""
        SQL = "select b.kd_karyawan, p.nama_lengkap, e.nama as departemen, f.nama as jabatan, a.tgl_berangkat, a.tgl_pulang, a.nilai, a.id "
        SQL = SQL & " from karyawan_tugas_dt a"
        SQL = SQL & " inner join personal p on p.id = a.karyawan_nama_id "
        SQL = SQL & " inner join karyawan b on a.karyawan_nama_id = b.personal_id  "
        SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_id "
        SQL = SQL & " inner join karyawan_mutasi_organisasi d on d.personal_id = a.karyawan_nama_id "
        SQL = SQL & " inner join karyawan_struktur_organisasi e on e.id = d.departemen_id "
        SQL = SQL & " inner join karyawan_struktur_organisasi f on f.id = d.jabatan_id "
        'SQL = SQL & " where a.karyawan_tugas_id = '" & kk & "' "
        SQL = SQL & " where a.no_sj = '" & nosj & "' "

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGViewDetail.DataSource = dt

        DGViewDetail.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGViewDetail.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGViewDetail.Columns(0).Width = 250
        DGViewDetail.Columns(1).Width = 150
        DGViewDetail.Columns(2).Width = 150
        DGViewDetail.Columns(3).Width = 100
        DGViewDetail.Columns(4).Width = 150
        DGViewDetail.Columns(5).Width = 150
        DGViewDetail.Columns(6).Width = 150
        DGViewDetail.Columns(7).Width = 80

        DGViewDetail.Columns(0).HeaderText = "KODE KARYAWAN"
        DGViewDetail.Columns(1).HeaderText = "NAMA KARYAWAN"
        DGViewDetail.Columns(2).HeaderText = "DEPARTEMEN"
        DGViewDetail.Columns(3).HeaderText = "JABATAN"
        DGViewDetail.Columns(4).HeaderText = "TANGGAL BERANGKAT"
        DGViewDetail.Columns(5).HeaderText = "TANGGAL PULANG"
        DGViewDetail.Columns(6).HeaderText = "NILAI"
        DGViewDetail.Columns(7).HeaderText = "ID"
        DGViewDetail.Columns(7).Visible = False
    End Sub
    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub frmTugasDinas_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmTugasDinas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ShowDataGrid()
    End Sub

    Private Sub frmTugasDinas_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        'DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        'DGView.Left = 10
        'DGView.Top = 40
        DGViewDetail.Width = Me.Width - 40
        GroupPanel2.Left = Me.Width - 1100
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BubbleButton6.Click
        frmClose()
    End Sub

    Private Sub BtnAdd_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BubbleButton1.Click
        frmInputTugasDinas.ShowDialog()
        If frmInputTugasDinas.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGrid()
        End If
    End Sub

    Private Sub DGView_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellContentClick
        Call ShowDetailGrid()
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
                nosj = .Item("no_sj", i).Value
            End With
        End If
    End Sub
End Class