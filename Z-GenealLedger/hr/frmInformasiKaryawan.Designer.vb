﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInformasiKaryawan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInformasiKaryawan))
        Me.TabControlAkun = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel10 = New DevComponents.DotNetBar.TabControlPanel()
        Me.DGViewMutasi = New System.Windows.Forms.DataGridView()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.LabelX40 = New DevComponents.DotNetBar.LabelX()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabItem9 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.LabelX39 = New DevComponents.DotNetBar.LabelX()
        Me.DGView = New System.Windows.Forms.DataGridView()
        Me.TabItem8 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtKelamin = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.txtDate = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtStatus = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtUnitBisnis = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtNama = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.aktiva = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.dtLahir = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtEmail2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.txtEmail = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.txtTelp2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.txtTelp1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.txtGolDarah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.txtSuku = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.txtWarga = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.txtAgama = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.txtTempatLahir = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.txtPanggilan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.txtNamaLengkap = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.txtKtp = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel()
        Me.txtKodepos = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX31 = New DevComponents.DotNetBar.LabelX()
        Me.txtKelurahan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX30 = New DevComponents.DotNetBar.LabelX()
        Me.txtKecamatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX29 = New DevComponents.DotNetBar.LabelX()
        Me.txtKota = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX28 = New DevComponents.DotNetBar.LabelX()
        Me.txtProvinsi = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.txtDetailLokasi = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtStatusRumah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.txAlamat = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX32 = New DevComponents.DotNetBar.LabelX()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel()
        Me.dtMenikah = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtNamaIbu = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX37 = New DevComponents.DotNetBar.LabelX()
        Me.txtAnakKe = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX36 = New DevComponents.DotNetBar.LabelX()
        Me.txtJumlahTanggungan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX35 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX34 = New DevComponents.DotNetBar.LabelX()
        Me.txtStatusPerkawinan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX33 = New DevComponents.DotNetBar.LabelX()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.LabelX38 = New DevComponents.DotNetBar.LabelX()
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel()
        Me.txAlamatSekolah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtJurusan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX50 = New DevComponents.DotNetBar.LabelX()
        Me.txtTahunIjazah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX45 = New DevComponents.DotNetBar.LabelX()
        Me.txtNamaSekolah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX46 = New DevComponents.DotNetBar.LabelX()
        Me.txtTingkat = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX47 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX48 = New DevComponents.DotNetBar.LabelX()
        Me.txtStatusPendidikan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX49 = New DevComponents.DotNetBar.LabelX()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.LabelX44 = New DevComponents.DotNetBar.LabelX()
        Me.TabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel()
        Me.LabelX60 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX59 = New DevComponents.DotNetBar.LabelX()
        Me.txtCC = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX57 = New DevComponents.DotNetBar.LabelX()
        Me.txtNpwp = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX58 = New DevComponents.DotNetBar.LabelX()
        Me.txtNamaBank = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtRekAn = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX53 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX54 = New DevComponents.DotNetBar.LabelX()
        Me.txtNoRek = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX55 = New DevComponents.DotNetBar.LabelX()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.LabelX51 = New DevComponents.DotNetBar.LabelX()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.LabelX52 = New DevComponents.DotNetBar.LabelX()
        Me.TabItem6 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.TabControlAkun, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlAkun.SuspendLayout()
        Me.TabControlPanel10.SuspendLayout()
        CType(Me.DGViewMutasi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        Me.TabControlPanel9.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.dtLahir, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        CType(Me.dtMenikah, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TabControlPanel7.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControlAkun
        '
        Me.TabControlAkun.CanReorderTabs = True
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel10)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel1)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel9)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel2)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel3)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel4)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel6)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel7)
        Me.TabControlAkun.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlAkun.Location = New System.Drawing.Point(0, 0)
        Me.TabControlAkun.Name = "TabControlAkun"
        Me.TabControlAkun.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControlAkun.SelectedTabIndex = 0
        Me.TabControlAkun.Size = New System.Drawing.Size(1041, 505)
        Me.TabControlAkun.TabIndex = 132
        Me.TabControlAkun.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControlAkun.Tabs.Add(Me.aktiva)
        Me.TabControlAkun.Tabs.Add(Me.TabItem1)
        Me.TabControlAkun.Tabs.Add(Me.TabItem2)
        Me.TabControlAkun.Tabs.Add(Me.TabItem3)
        Me.TabControlAkun.Tabs.Add(Me.TabItem5)
        Me.TabControlAkun.Tabs.Add(Me.TabItem6)
        Me.TabControlAkun.Tabs.Add(Me.TabItem8)
        Me.TabControlAkun.Tabs.Add(Me.TabItem9)
        Me.TabControlAkun.Text = "BIAYA"
        '
        'TabControlPanel10
        '
        Me.TabControlPanel10.Controls.Add(Me.DGViewMutasi)
        Me.TabControlPanel10.Controls.Add(Me.Panel9)
        Me.TabControlPanel10.Controls.Add(Me.Button3)
        Me.TabControlPanel10.Controls.Add(Me.Button2)
        Me.TabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel10.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel10.Name = "TabControlPanel10"
        Me.TabControlPanel10.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel10.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel10.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel10.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel10.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel10.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel10.Style.GradientAngle = 90
        Me.TabControlPanel10.TabIndex = 10
        Me.TabControlPanel10.TabItem = Me.TabItem9
        '
        'DGViewMutasi
        '
        Me.DGViewMutasi.AllowUserToAddRows = False
        Me.DGViewMutasi.AllowUserToDeleteRows = False
        Me.DGViewMutasi.AllowUserToOrderColumns = True
        Me.DGViewMutasi.AllowUserToResizeRows = False
        Me.DGViewMutasi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGViewMutasi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGViewMutasi.Location = New System.Drawing.Point(15, 51)
        Me.DGViewMutasi.Name = "DGViewMutasi"
        Me.DGViewMutasi.Size = New System.Drawing.Size(1014, 356)
        Me.DGViewMutasi.TabIndex = 192
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Red
        Me.Panel9.Controls.Add(Me.LabelX40)
        Me.Panel9.Location = New System.Drawing.Point(6, 3)
        Me.Panel9.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(1028, 41)
        Me.Panel9.TabIndex = 191
        '
        'LabelX40
        '
        Me.LabelX40.AutoSize = True
        Me.LabelX40.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX40.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX40.ForeColor = System.Drawing.Color.White
        Me.LabelX40.Location = New System.Drawing.Point(27, 3)
        Me.LabelX40.Name = "LabelX40"
        Me.LabelX40.Size = New System.Drawing.Size(156, 34)
        Me.LabelX40.TabIndex = 167
        Me.LabelX40.Text = "HISTORY KARIR"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Red
        Me.Button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.Location = New System.Drawing.Point(287, 421)
        Me.Button3.Margin = New System.Windows.Forms.Padding(2)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(266, 47)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "RESIGN"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(17, 421)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(266, 47)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "MUTASI"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TabItem9
        '
        Me.TabItem9.AttachedControl = Me.TabControlPanel10
        Me.TabItem9.Name = "TabItem9"
        Me.TabItem9.Text = "KARIR"
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.Panel5)
        Me.TabControlPanel9.Controls.Add(Me.DGView)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.Style.GradientAngle = 90
        Me.TabControlPanel9.TabIndex = 9
        Me.TabControlPanel9.TabItem = Me.TabItem8
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Red
        Me.Panel5.Controls.Add(Me.LabelX39)
        Me.Panel5.Location = New System.Drawing.Point(6, 3)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1028, 41)
        Me.Panel5.TabIndex = 187
        '
        'LabelX39
        '
        Me.LabelX39.AutoSize = True
        Me.LabelX39.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX39.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX39.ForeColor = System.Drawing.Color.White
        Me.LabelX39.Location = New System.Drawing.Point(423, 3)
        Me.LabelX39.Name = "LabelX39"
        Me.LabelX39.Size = New System.Drawing.Size(224, 34)
        Me.LabelX39.TabIndex = 167
        Me.LabelX39.Text = "CATATAN KARYAWAN"
        '
        'DGView
        '
        Me.DGView.AllowUserToAddRows = False
        Me.DGView.AllowUserToDeleteRows = False
        Me.DGView.AllowUserToOrderColumns = True
        Me.DGView.AllowUserToResizeRows = False
        Me.DGView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGView.Location = New System.Drawing.Point(18, 58)
        Me.DGView.Name = "DGView"
        Me.DGView.Size = New System.Drawing.Size(1002, 409)
        Me.DGView.TabIndex = 188
        '
        'TabItem8
        '
        Me.TabItem8.AttachedControl = Me.TabControlPanel9
        Me.TabItem8.Name = "TabItem8"
        Me.TabItem8.Text = "CATATAN KARYAWAN"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.aktiva
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.GroupBox1.Controls.Add(Me.txtKelamin)
        Me.GroupBox1.Controls.Add(Me.LabelX8)
        Me.GroupBox1.Controls.Add(Me.txtDate)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.txtStatus)
        Me.GroupBox1.Controls.Add(Me.LabelX7)
        Me.GroupBox1.Controls.Add(Me.txtJabatan)
        Me.GroupBox1.Controls.Add(Me.LabelX6)
        Me.GroupBox1.Controls.Add(Me.txtDepartemen)
        Me.GroupBox1.Controls.Add(Me.LabelX5)
        Me.GroupBox1.Controls.Add(Me.txtUnitBisnis)
        Me.GroupBox1.Controls.Add(Me.LabelX4)
        Me.GroupBox1.Controls.Add(Me.LabelX3)
        Me.GroupBox1.Controls.Add(Me.txtKdKaryawan)
        Me.GroupBox1.Controls.Add(Me.LabelX1)
        Me.GroupBox1.Controls.Add(Me.txtNama)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(160, 85)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(739, 290)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "INFORMASI KARYAWAN"
        '
        'txtKelamin
        '
        '
        '
        '
        Me.txtKelamin.Border.Class = "TextBoxBorder"
        Me.txtKelamin.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKelamin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKelamin.Location = New System.Drawing.Point(136, 227)
        Me.txtKelamin.Name = "txtKelamin"
        Me.txtKelamin.Size = New System.Drawing.Size(325, 25)
        Me.txtKelamin.TabIndex = 165
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(16, 225)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(114, 23)
        Me.LabelX8.TabIndex = 164
        Me.LabelX8.Text = "JENIS KELAMIN"
        '
        'txtDate
        '
        '
        '
        '
        Me.txtDate.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate.ButtonDropDown.Visible = True
        Me.txtDate.IsPopupCalendarOpen = False
        Me.txtDate.Location = New System.Drawing.Point(136, 104)
        '
        '
        '
        Me.txtDate.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.TodayButtonVisible = True
        Me.txtDate.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(141, 25)
        Me.txtDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate.TabIndex = 163
        '
        'txtStatus
        '
        '
        '
        '
        Me.txtStatus.Border.Class = "TextBoxBorder"
        Me.txtStatus.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatus.Location = New System.Drawing.Point(136, 260)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.Size = New System.Drawing.Size(143, 25)
        Me.txtStatus.TabIndex = 161
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(16, 262)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(114, 23)
        Me.LabelX7.TabIndex = 160
        Me.LabelX7.Text = "STATUS"
        '
        'txtJabatan
        '
        '
        '
        '
        Me.txtJabatan.Border.Class = "TextBoxBorder"
        Me.txtJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJabatan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJabatan.Location = New System.Drawing.Point(136, 196)
        Me.txtJabatan.Name = "txtJabatan"
        Me.txtJabatan.Size = New System.Drawing.Size(325, 25)
        Me.txtJabatan.TabIndex = 159
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(16, 193)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(114, 23)
        Me.LabelX6.TabIndex = 158
        Me.LabelX6.Text = "JABATAN"
        '
        'txtDepartemen
        '
        '
        '
        '
        Me.txtDepartemen.Border.Class = "TextBoxBorder"
        Me.txtDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDepartemen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartemen.Location = New System.Drawing.Point(136, 166)
        Me.txtDepartemen.Name = "txtDepartemen"
        Me.txtDepartemen.Size = New System.Drawing.Size(325, 25)
        Me.txtDepartemen.TabIndex = 157
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(16, 163)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(114, 23)
        Me.LabelX5.TabIndex = 156
        Me.LabelX5.Text = "DEPARTEMEN"
        '
        'txtUnitBisnis
        '
        '
        '
        '
        Me.txtUnitBisnis.Border.Class = "TextBoxBorder"
        Me.txtUnitBisnis.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUnitBisnis.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnitBisnis.Location = New System.Drawing.Point(136, 136)
        Me.txtUnitBisnis.Name = "txtUnitBisnis"
        Me.txtUnitBisnis.Size = New System.Drawing.Size(325, 25)
        Me.txtUnitBisnis.TabIndex = 155
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(16, 134)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(114, 23)
        Me.LabelX4.TabIndex = 154
        Me.LabelX4.Text = "UNIT BISNIS"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(16, 105)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(114, 23)
        Me.LabelX3.TabIndex = 152
        Me.LabelX3.Text = "TGL GABUNG"
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKdKaryawan.Location = New System.Drawing.Point(136, 68)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(325, 25)
        Me.txtKdKaryawan.TabIndex = 151
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(16, 68)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(114, 23)
        Me.LabelX1.TabIndex = 150
        Me.LabelX1.Text = "KODE KARYAWAN"
        '
        'txtNama
        '
        '
        '
        '
        Me.txtNama.Border.Class = "TextBoxBorder"
        Me.txtNama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNama.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNama.Location = New System.Drawing.Point(136, 38)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(325, 25)
        Me.txtNama.TabIndex = 149
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(16, 38)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(114, 23)
        Me.LabelX2.TabIndex = 148
        Me.LabelX2.Text = "NAMA KARYAWAN"
        '
        'aktiva
        '
        Me.aktiva.AttachedControl = Me.TabControlPanel1
        Me.aktiva.Name = "aktiva"
        Me.aktiva.Text = "INFORMASI"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.dtLahir)
        Me.TabControlPanel2.Controls.Add(Me.txtEmail2)
        Me.TabControlPanel2.Controls.Add(Me.LabelX22)
        Me.TabControlPanel2.Controls.Add(Me.txtEmail)
        Me.TabControlPanel2.Controls.Add(Me.LabelX21)
        Me.TabControlPanel2.Controls.Add(Me.txtTelp2)
        Me.TabControlPanel2.Controls.Add(Me.LabelX20)
        Me.TabControlPanel2.Controls.Add(Me.txtTelp1)
        Me.TabControlPanel2.Controls.Add(Me.LabelX19)
        Me.TabControlPanel2.Controls.Add(Me.txtGolDarah)
        Me.TabControlPanel2.Controls.Add(Me.LabelX18)
        Me.TabControlPanel2.Controls.Add(Me.txtSuku)
        Me.TabControlPanel2.Controls.Add(Me.LabelX17)
        Me.TabControlPanel2.Controls.Add(Me.txtWarga)
        Me.TabControlPanel2.Controls.Add(Me.LabelX16)
        Me.TabControlPanel2.Controls.Add(Me.txtAgama)
        Me.TabControlPanel2.Controls.Add(Me.LabelX15)
        Me.TabControlPanel2.Controls.Add(Me.LabelX14)
        Me.TabControlPanel2.Controls.Add(Me.txtTempatLahir)
        Me.TabControlPanel2.Controls.Add(Me.LabelX13)
        Me.TabControlPanel2.Controls.Add(Me.txtPanggilan)
        Me.TabControlPanel2.Controls.Add(Me.LabelX12)
        Me.TabControlPanel2.Controls.Add(Me.txtNamaLengkap)
        Me.TabControlPanel2.Controls.Add(Me.LabelX11)
        Me.TabControlPanel2.Controls.Add(Me.Panel2)
        Me.TabControlPanel2.Controls.Add(Me.Panel1)
        Me.TabControlPanel2.Controls.Add(Me.txtKtp)
        Me.TabControlPanel2.Controls.Add(Me.LabelX23)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.Style.GradientAngle = 90
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.TabItem1
        '
        'dtLahir
        '
        '
        '
        '
        Me.dtLahir.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtLahir.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtLahir.ButtonDropDown.Visible = True
        Me.dtLahir.IsPopupCalendarOpen = False
        Me.dtLahir.Location = New System.Drawing.Point(176, 192)
        '
        '
        '
        Me.dtLahir.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtLahir.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtLahir.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtLahir.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtLahir.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.MonthCalendar.TodayButtonVisible = True
        Me.dtLahir.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtLahir.Name = "dtLahir"
        Me.dtLahir.Size = New System.Drawing.Size(141, 20)
        Me.dtLahir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtLahir.TabIndex = 219
        '
        'txtEmail2
        '
        '
        '
        '
        Me.txtEmail2.Border.Class = "TextBoxBorder"
        Me.txtEmail2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEmail2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmail2.Location = New System.Drawing.Point(667, 164)
        Me.txtEmail2.Name = "txtEmail2"
        Me.txtEmail2.Size = New System.Drawing.Size(325, 20)
        Me.txtEmail2.TabIndex = 218
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(547, 164)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(114, 23)
        Me.LabelX22.TabIndex = 217
        Me.LabelX22.Text = "EMAIL 2"
        '
        'txtEmail
        '
        '
        '
        '
        Me.txtEmail.Border.Class = "TextBoxBorder"
        Me.txtEmail.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmail.Location = New System.Drawing.Point(667, 135)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(325, 20)
        Me.txtEmail.TabIndex = 216
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX21.ForeColor = System.Drawing.Color.Black
        Me.LabelX21.Location = New System.Drawing.Point(547, 135)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(114, 23)
        Me.LabelX21.TabIndex = 215
        Me.LabelX21.Text = "EMAIL"
        '
        'txtTelp2
        '
        '
        '
        '
        Me.txtTelp2.Border.Class = "TextBoxBorder"
        Me.txtTelp2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTelp2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelp2.Location = New System.Drawing.Point(667, 106)
        Me.txtTelp2.Name = "txtTelp2"
        Me.txtTelp2.Size = New System.Drawing.Size(325, 20)
        Me.txtTelp2.TabIndex = 214
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(547, 106)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(114, 23)
        Me.LabelX20.TabIndex = 213
        Me.LabelX20.Text = "NOMOR TELEPON 2"
        '
        'txtTelp1
        '
        '
        '
        '
        Me.txtTelp1.Border.Class = "TextBoxBorder"
        Me.txtTelp1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTelp1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelp1.Location = New System.Drawing.Point(667, 75)
        Me.txtTelp1.Name = "txtTelp1"
        Me.txtTelp1.Size = New System.Drawing.Size(325, 20)
        Me.txtTelp1.TabIndex = 212
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(547, 75)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(114, 23)
        Me.LabelX19.TabIndex = 211
        Me.LabelX19.Text = "NOMOR TELEPON"
        '
        'txtGolDarah
        '
        '
        '
        '
        Me.txtGolDarah.Border.Class = "TextBoxBorder"
        Me.txtGolDarah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGolDarah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGolDarah.Location = New System.Drawing.Point(176, 309)
        Me.txtGolDarah.Name = "txtGolDarah"
        Me.txtGolDarah.Size = New System.Drawing.Size(325, 20)
        Me.txtGolDarah.TabIndex = 210
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(42, 309)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(114, 23)
        Me.LabelX18.TabIndex = 209
        Me.LabelX18.Text = "GOLONGAN DARAH"
        '
        'txtSuku
        '
        '
        '
        '
        Me.txtSuku.Border.Class = "TextBoxBorder"
        Me.txtSuku.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSuku.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSuku.Location = New System.Drawing.Point(176, 280)
        Me.txtSuku.Name = "txtSuku"
        Me.txtSuku.Size = New System.Drawing.Size(325, 20)
        Me.txtSuku.TabIndex = 208
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(42, 280)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(128, 23)
        Me.LabelX17.TabIndex = 207
        Me.LabelX17.Text = "DAERAH ASAL / SUKU"
        '
        'txtWarga
        '
        '
        '
        '
        Me.txtWarga.Border.Class = "TextBoxBorder"
        Me.txtWarga.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWarga.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtWarga.Location = New System.Drawing.Point(176, 251)
        Me.txtWarga.Name = "txtWarga"
        Me.txtWarga.Size = New System.Drawing.Size(325, 20)
        Me.txtWarga.TabIndex = 206
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(42, 251)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(128, 23)
        Me.LabelX16.TabIndex = 205
        Me.LabelX16.Text = "KEWARGANEGARAAN"
        '
        'txtAgama
        '
        '
        '
        '
        Me.txtAgama.Border.Class = "TextBoxBorder"
        Me.txtAgama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAgama.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAgama.Location = New System.Drawing.Point(176, 221)
        Me.txtAgama.Name = "txtAgama"
        Me.txtAgama.Size = New System.Drawing.Size(325, 20)
        Me.txtAgama.TabIndex = 204
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(42, 221)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(114, 23)
        Me.LabelX15.TabIndex = 203
        Me.LabelX15.Text = "AGAMA"
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(42, 192)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(114, 23)
        Me.LabelX14.TabIndex = 202
        Me.LabelX14.Text = "TANGGAL LAHIR"
        '
        'txtTempatLahir
        '
        '
        '
        '
        Me.txtTempatLahir.Border.Class = "TextBoxBorder"
        Me.txtTempatLahir.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTempatLahir.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTempatLahir.Location = New System.Drawing.Point(176, 163)
        Me.txtTempatLahir.Name = "txtTempatLahir"
        Me.txtTempatLahir.Size = New System.Drawing.Size(325, 20)
        Me.txtTempatLahir.TabIndex = 201
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(42, 163)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(114, 23)
        Me.LabelX13.TabIndex = 200
        Me.LabelX13.Text = "TEMPAT LAHIR"
        '
        'txtPanggilan
        '
        '
        '
        '
        Me.txtPanggilan.Border.Class = "TextBoxBorder"
        Me.txtPanggilan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPanggilan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPanggilan.Location = New System.Drawing.Point(176, 134)
        Me.txtPanggilan.Name = "txtPanggilan"
        Me.txtPanggilan.Size = New System.Drawing.Size(325, 20)
        Me.txtPanggilan.TabIndex = 199
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(42, 134)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(114, 23)
        Me.LabelX12.TabIndex = 198
        Me.LabelX12.Text = "NAMA PANGGILAN"
        '
        'txtNamaLengkap
        '
        '
        '
        '
        Me.txtNamaLengkap.Border.Class = "TextBoxBorder"
        Me.txtNamaLengkap.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaLengkap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaLengkap.Location = New System.Drawing.Point(176, 104)
        Me.txtNamaLengkap.Name = "txtNamaLengkap"
        Me.txtNamaLengkap.Size = New System.Drawing.Size(325, 20)
        Me.txtNamaLengkap.TabIndex = 197
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(42, 104)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(114, 23)
        Me.LabelX11.TabIndex = 196
        Me.LabelX11.Text = "NAMA LENGKAP"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Blue
        Me.Panel2.Controls.Add(Me.LabelX10)
        Me.Panel2.Location = New System.Drawing.Point(529, 24)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(501, 41)
        Me.Panel2.TabIndex = 195
        '
        'LabelX10
        '
        Me.LabelX10.AutoSize = True
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX10.ForeColor = System.Drawing.Color.White
        Me.LabelX10.Location = New System.Drawing.Point(166, 5)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(213, 34)
        Me.LabelX10.TabIndex = 168
        Me.LabelX10.Text = "TELEPON DAN EMAIL"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.Controls.Add(Me.LabelX9)
        Me.Panel1.Location = New System.Drawing.Point(24, 24)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(501, 41)
        Me.Panel1.TabIndex = 194
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX9.ForeColor = System.Drawing.Color.White
        Me.LabelX9.Location = New System.Drawing.Point(188, 6)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(114, 23)
        Me.LabelX9.TabIndex = 167
        Me.LabelX9.Text = "PERSONAL"
        '
        'txtKtp
        '
        '
        '
        '
        Me.txtKtp.Border.Class = "TextBoxBorder"
        Me.txtKtp.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKtp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKtp.Location = New System.Drawing.Point(176, 75)
        Me.txtKtp.Name = "txtKtp"
        Me.txtKtp.Size = New System.Drawing.Size(325, 20)
        Me.txtKtp.TabIndex = 193
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(42, 75)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(114, 23)
        Me.LabelX23.TabIndex = 192
        Me.LabelX23.Text = "NO KTP"
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel2
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "PERSONAL"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.txtKodepos)
        Me.TabControlPanel3.Controls.Add(Me.LabelX31)
        Me.TabControlPanel3.Controls.Add(Me.txtKelurahan)
        Me.TabControlPanel3.Controls.Add(Me.LabelX30)
        Me.TabControlPanel3.Controls.Add(Me.txtKecamatan)
        Me.TabControlPanel3.Controls.Add(Me.LabelX29)
        Me.TabControlPanel3.Controls.Add(Me.txtKota)
        Me.TabControlPanel3.Controls.Add(Me.LabelX28)
        Me.TabControlPanel3.Controls.Add(Me.txtProvinsi)
        Me.TabControlPanel3.Controls.Add(Me.LabelX27)
        Me.TabControlPanel3.Controls.Add(Me.txtDetailLokasi)
        Me.TabControlPanel3.Controls.Add(Me.txtStatusRumah)
        Me.TabControlPanel3.Controls.Add(Me.LabelX26)
        Me.TabControlPanel3.Controls.Add(Me.LabelX25)
        Me.TabControlPanel3.Controls.Add(Me.txAlamat)
        Me.TabControlPanel3.Controls.Add(Me.LabelX32)
        Me.TabControlPanel3.Controls.Add(Me.Panel3)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.Style.GradientAngle = 90
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.TabItem2
        '
        'txtKodepos
        '
        '
        '
        '
        Me.txtKodepos.Border.Class = "TextBoxBorder"
        Me.txtKodepos.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKodepos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKodepos.Location = New System.Drawing.Point(666, 219)
        Me.txtKodepos.Name = "txtKodepos"
        Me.txtKodepos.Size = New System.Drawing.Size(325, 20)
        Me.txtKodepos.TabIndex = 199
        '
        'LabelX31
        '
        Me.LabelX31.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX31.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX31.ForeColor = System.Drawing.Color.Black
        Me.LabelX31.Location = New System.Drawing.Point(532, 219)
        Me.LabelX31.Name = "LabelX31"
        Me.LabelX31.Size = New System.Drawing.Size(114, 23)
        Me.LabelX31.TabIndex = 198
        Me.LabelX31.Text = "KODE POS"
        '
        'txtKelurahan
        '
        '
        '
        '
        Me.txtKelurahan.Border.Class = "TextBoxBorder"
        Me.txtKelurahan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKelurahan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKelurahan.Location = New System.Drawing.Point(666, 190)
        Me.txtKelurahan.Name = "txtKelurahan"
        Me.txtKelurahan.Size = New System.Drawing.Size(325, 20)
        Me.txtKelurahan.TabIndex = 197
        '
        'LabelX30
        '
        Me.LabelX30.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX30.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX30.ForeColor = System.Drawing.Color.Black
        Me.LabelX30.Location = New System.Drawing.Point(532, 190)
        Me.LabelX30.Name = "LabelX30"
        Me.LabelX30.Size = New System.Drawing.Size(114, 23)
        Me.LabelX30.TabIndex = 196
        Me.LabelX30.Text = "KELURAHAN"
        '
        'txtKecamatan
        '
        '
        '
        '
        Me.txtKecamatan.Border.Class = "TextBoxBorder"
        Me.txtKecamatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKecamatan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKecamatan.Location = New System.Drawing.Point(667, 161)
        Me.txtKecamatan.Name = "txtKecamatan"
        Me.txtKecamatan.Size = New System.Drawing.Size(325, 20)
        Me.txtKecamatan.TabIndex = 195
        '
        'LabelX29
        '
        Me.LabelX29.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX29.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX29.ForeColor = System.Drawing.Color.Black
        Me.LabelX29.Location = New System.Drawing.Point(534, 161)
        Me.LabelX29.Name = "LabelX29"
        Me.LabelX29.Size = New System.Drawing.Size(114, 23)
        Me.LabelX29.TabIndex = 194
        Me.LabelX29.Text = "KECAMATAN"
        '
        'txtKota
        '
        '
        '
        '
        Me.txtKota.Border.Class = "TextBoxBorder"
        Me.txtKota.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKota.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKota.Location = New System.Drawing.Point(667, 131)
        Me.txtKota.Name = "txtKota"
        Me.txtKota.Size = New System.Drawing.Size(325, 20)
        Me.txtKota.TabIndex = 193
        '
        'LabelX28
        '
        Me.LabelX28.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX28.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX28.ForeColor = System.Drawing.Color.Black
        Me.LabelX28.Location = New System.Drawing.Point(534, 131)
        Me.LabelX28.Name = "LabelX28"
        Me.LabelX28.Size = New System.Drawing.Size(114, 23)
        Me.LabelX28.TabIndex = 192
        Me.LabelX28.Text = "KOTA / KABUPATEN"
        '
        'txtProvinsi
        '
        '
        '
        '
        Me.txtProvinsi.Border.Class = "TextBoxBorder"
        Me.txtProvinsi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtProvinsi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProvinsi.Location = New System.Drawing.Point(666, 98)
        Me.txtProvinsi.Name = "txtProvinsi"
        Me.txtProvinsi.Size = New System.Drawing.Size(325, 20)
        Me.txtProvinsi.TabIndex = 191
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(532, 98)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(114, 23)
        Me.LabelX27.TabIndex = 190
        Me.LabelX27.Text = "PROVINSI"
        '
        'txtDetailLokasi
        '
        '
        '
        '
        Me.txtDetailLokasi.Border.Class = "TextBoxBorder"
        Me.txtDetailLokasi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDetailLokasi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetailLokasi.Location = New System.Drawing.Point(172, 219)
        Me.txtDetailLokasi.Name = "txtDetailLokasi"
        Me.txtDetailLokasi.Size = New System.Drawing.Size(325, 20)
        Me.txtDetailLokasi.TabIndex = 189
        '
        'txtStatusRumah
        '
        '
        '
        '
        Me.txtStatusRumah.Border.Class = "TextBoxBorder"
        Me.txtStatusRumah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatusRumah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatusRumah.Location = New System.Drawing.Point(174, 190)
        Me.txtStatusRumah.Name = "txtStatusRumah"
        Me.txtStatusRumah.Size = New System.Drawing.Size(325, 20)
        Me.txtStatusRumah.TabIndex = 187
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(38, 219)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(114, 23)
        Me.LabelX26.TabIndex = 188
        Me.LabelX26.Text = "DETAIL LOKASI"
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(40, 190)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(114, 23)
        Me.LabelX25.TabIndex = 186
        Me.LabelX25.Text = "STATUS RUMAH"
        '
        'txAlamat
        '
        '
        '
        '
        Me.txAlamat.Border.Class = "TextBoxBorder"
        Me.txAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txAlamat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txAlamat.Location = New System.Drawing.Point(172, 98)
        Me.txAlamat.Multiline = True
        Me.txAlamat.Name = "txAlamat"
        Me.txAlamat.Size = New System.Drawing.Size(326, 85)
        Me.txAlamat.TabIndex = 185
        '
        'LabelX32
        '
        Me.LabelX32.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX32.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX32.ForeColor = System.Drawing.Color.Black
        Me.LabelX32.Location = New System.Drawing.Point(40, 98)
        Me.LabelX32.Name = "LabelX32"
        Me.LabelX32.Size = New System.Drawing.Size(114, 23)
        Me.LabelX32.TabIndex = 184
        Me.LabelX32.Text = "ALAMAT RUMAH"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Red
        Me.Panel3.Controls.Add(Me.LabelX24)
        Me.Panel3.Location = New System.Drawing.Point(17, 26)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1013, 41)
        Me.Panel3.TabIndex = 183
        '
        'LabelX24
        '
        Me.LabelX24.AutoSize = True
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.White
        Me.LabelX24.Location = New System.Drawing.Point(490, 3)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(89, 34)
        Me.LabelX24.TabIndex = 167
        Me.LabelX24.Text = "ALAMAT"
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel3
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "ALAMAT"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.dtMenikah)
        Me.TabControlPanel4.Controls.Add(Me.txtNamaIbu)
        Me.TabControlPanel4.Controls.Add(Me.LabelX37)
        Me.TabControlPanel4.Controls.Add(Me.txtAnakKe)
        Me.TabControlPanel4.Controls.Add(Me.LabelX36)
        Me.TabControlPanel4.Controls.Add(Me.txtJumlahTanggungan)
        Me.TabControlPanel4.Controls.Add(Me.LabelX35)
        Me.TabControlPanel4.Controls.Add(Me.LabelX34)
        Me.TabControlPanel4.Controls.Add(Me.txtStatusPerkawinan)
        Me.TabControlPanel4.Controls.Add(Me.LabelX33)
        Me.TabControlPanel4.Controls.Add(Me.Panel4)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.Style.GradientAngle = 90
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.TabItem3
        '
        'dtMenikah
        '
        '
        '
        '
        Me.dtMenikah.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtMenikah.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtMenikah.ButtonDropDown.Visible = True
        Me.dtMenikah.IsPopupCalendarOpen = False
        Me.dtMenikah.Location = New System.Drawing.Point(430, 147)
        '
        '
        '
        Me.dtMenikah.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtMenikah.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtMenikah.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtMenikah.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtMenikah.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.MonthCalendar.TodayButtonVisible = True
        Me.dtMenikah.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtMenikah.Name = "dtMenikah"
        Me.dtMenikah.Size = New System.Drawing.Size(141, 20)
        Me.dtMenikah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtMenikah.TabIndex = 196
        '
        'txtNamaIbu
        '
        '
        '
        '
        Me.txtNamaIbu.Border.Class = "TextBoxBorder"
        Me.txtNamaIbu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaIbu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaIbu.Location = New System.Drawing.Point(430, 234)
        Me.txtNamaIbu.Name = "txtNamaIbu"
        Me.txtNamaIbu.Size = New System.Drawing.Size(325, 20)
        Me.txtNamaIbu.TabIndex = 195
        '
        'LabelX37
        '
        Me.LabelX37.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX37.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX37.ForeColor = System.Drawing.Color.Black
        Me.LabelX37.Location = New System.Drawing.Point(288, 233)
        Me.LabelX37.Name = "LabelX37"
        Me.LabelX37.Size = New System.Drawing.Size(128, 23)
        Me.LabelX37.TabIndex = 194
        Me.LabelX37.Text = "NAMA IBU KANDUNG"
        '
        'txtAnakKe
        '
        '
        '
        '
        Me.txtAnakKe.Border.Class = "TextBoxBorder"
        Me.txtAnakKe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAnakKe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAnakKe.Location = New System.Drawing.Point(430, 205)
        Me.txtAnakKe.Name = "txtAnakKe"
        Me.txtAnakKe.Size = New System.Drawing.Size(325, 20)
        Me.txtAnakKe.TabIndex = 193
        '
        'LabelX36
        '
        Me.LabelX36.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX36.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX36.ForeColor = System.Drawing.Color.Black
        Me.LabelX36.Location = New System.Drawing.Point(288, 203)
        Me.LabelX36.Name = "LabelX36"
        Me.LabelX36.Size = New System.Drawing.Size(128, 23)
        Me.LabelX36.TabIndex = 192
        Me.LabelX36.Text = "ANAK NOMOR / KE"
        '
        'txtJumlahTanggungan
        '
        '
        '
        '
        Me.txtJumlahTanggungan.Border.Class = "TextBoxBorder"
        Me.txtJumlahTanggungan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJumlahTanggungan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJumlahTanggungan.Location = New System.Drawing.Point(430, 176)
        Me.txtJumlahTanggungan.Name = "txtJumlahTanggungan"
        Me.txtJumlahTanggungan.Size = New System.Drawing.Size(325, 20)
        Me.txtJumlahTanggungan.TabIndex = 191
        '
        'LabelX35
        '
        Me.LabelX35.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX35.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX35.ForeColor = System.Drawing.Color.Black
        Me.LabelX35.Location = New System.Drawing.Point(288, 174)
        Me.LabelX35.Name = "LabelX35"
        Me.LabelX35.Size = New System.Drawing.Size(128, 23)
        Me.LabelX35.TabIndex = 190
        Me.LabelX35.Text = "JUMLAH TANGGUNGAN"
        '
        'LabelX34
        '
        Me.LabelX34.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX34.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX34.ForeColor = System.Drawing.Color.Black
        Me.LabelX34.Location = New System.Drawing.Point(288, 145)
        Me.LabelX34.Name = "LabelX34"
        Me.LabelX34.Size = New System.Drawing.Size(128, 23)
        Me.LabelX34.TabIndex = 189
        Me.LabelX34.Text = "TANGGAL MENIKAH"
        '
        'txtStatusPerkawinan
        '
        '
        '
        '
        Me.txtStatusPerkawinan.Border.Class = "TextBoxBorder"
        Me.txtStatusPerkawinan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatusPerkawinan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatusPerkawinan.Location = New System.Drawing.Point(430, 117)
        Me.txtStatusPerkawinan.Name = "txtStatusPerkawinan"
        Me.txtStatusPerkawinan.Size = New System.Drawing.Size(325, 20)
        Me.txtStatusPerkawinan.TabIndex = 188
        '
        'LabelX33
        '
        Me.LabelX33.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX33.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX33.ForeColor = System.Drawing.Color.Black
        Me.LabelX33.Location = New System.Drawing.Point(288, 116)
        Me.LabelX33.Name = "LabelX33"
        Me.LabelX33.Size = New System.Drawing.Size(128, 23)
        Me.LabelX33.TabIndex = 187
        Me.LabelX33.Text = "STATUS PERKAWINAN"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Red
        Me.Panel4.Controls.Add(Me.LabelX38)
        Me.Panel4.Location = New System.Drawing.Point(261, 54)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(524, 41)
        Me.Panel4.TabIndex = 186
        '
        'LabelX38
        '
        Me.LabelX38.AutoSize = True
        Me.LabelX38.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX38.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX38.ForeColor = System.Drawing.Color.White
        Me.LabelX38.Location = New System.Drawing.Point(199, 0)
        Me.LabelX38.Name = "LabelX38"
        Me.LabelX38.Size = New System.Drawing.Size(111, 34)
        Me.LabelX38.TabIndex = 167
        Me.LabelX38.Text = "KELUARGA"
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel4
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "KELUARGA"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.txAlamatSekolah)
        Me.TabControlPanel6.Controls.Add(Me.txtJurusan)
        Me.TabControlPanel6.Controls.Add(Me.LabelX50)
        Me.TabControlPanel6.Controls.Add(Me.txtTahunIjazah)
        Me.TabControlPanel6.Controls.Add(Me.LabelX45)
        Me.TabControlPanel6.Controls.Add(Me.txtNamaSekolah)
        Me.TabControlPanel6.Controls.Add(Me.LabelX46)
        Me.TabControlPanel6.Controls.Add(Me.txtTingkat)
        Me.TabControlPanel6.Controls.Add(Me.LabelX47)
        Me.TabControlPanel6.Controls.Add(Me.LabelX48)
        Me.TabControlPanel6.Controls.Add(Me.txtStatusPendidikan)
        Me.TabControlPanel6.Controls.Add(Me.LabelX49)
        Me.TabControlPanel6.Controls.Add(Me.Panel6)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.Style.GradientAngle = 90
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.TabItem5
        '
        'txAlamatSekolah
        '
        '
        '
        '
        Me.txAlamatSekolah.Border.Class = "TextBoxBorder"
        Me.txAlamatSekolah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txAlamatSekolah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txAlamatSekolah.Location = New System.Drawing.Point(406, 256)
        Me.txAlamatSekolah.Multiline = True
        Me.txAlamatSekolah.Name = "txAlamatSekolah"
        Me.txAlamatSekolah.Size = New System.Drawing.Size(325, 77)
        Me.txAlamatSekolah.TabIndex = 210
        '
        'txtJurusan
        '
        '
        '
        '
        Me.txtJurusan.Border.Class = "TextBoxBorder"
        Me.txtJurusan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJurusan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJurusan.Location = New System.Drawing.Point(406, 227)
        Me.txtJurusan.Name = "txtJurusan"
        Me.txtJurusan.Size = New System.Drawing.Size(325, 20)
        Me.txtJurusan.TabIndex = 209
        '
        'LabelX50
        '
        Me.LabelX50.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX50.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX50.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX50.ForeColor = System.Drawing.Color.Black
        Me.LabelX50.Location = New System.Drawing.Point(263, 225)
        Me.LabelX50.Name = "LabelX50"
        Me.LabelX50.Size = New System.Drawing.Size(128, 23)
        Me.LabelX50.TabIndex = 208
        Me.LabelX50.Text = "JURUSAN STUDI"
        '
        'txtTahunIjazah
        '
        '
        '
        '
        Me.txtTahunIjazah.Border.Class = "TextBoxBorder"
        Me.txtTahunIjazah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTahunIjazah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTahunIjazah.Location = New System.Drawing.Point(406, 196)
        Me.txtTahunIjazah.Name = "txtTahunIjazah"
        Me.txtTahunIjazah.Size = New System.Drawing.Size(325, 20)
        Me.txtTahunIjazah.TabIndex = 207
        '
        'LabelX45
        '
        Me.LabelX45.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX45.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX45.ForeColor = System.Drawing.Color.Black
        Me.LabelX45.Location = New System.Drawing.Point(263, 196)
        Me.LabelX45.Name = "LabelX45"
        Me.LabelX45.Size = New System.Drawing.Size(128, 23)
        Me.LabelX45.TabIndex = 206
        Me.LabelX45.Text = "TAHUN IJAZAH"
        '
        'txtNamaSekolah
        '
        '
        '
        '
        Me.txtNamaSekolah.Border.Class = "TextBoxBorder"
        Me.txtNamaSekolah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaSekolah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaSekolah.Location = New System.Drawing.Point(406, 167)
        Me.txtNamaSekolah.Name = "txtNamaSekolah"
        Me.txtNamaSekolah.Size = New System.Drawing.Size(325, 20)
        Me.txtNamaSekolah.TabIndex = 205
        '
        'LabelX46
        '
        Me.LabelX46.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX46.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX46.ForeColor = System.Drawing.Color.Black
        Me.LabelX46.Location = New System.Drawing.Point(263, 254)
        Me.LabelX46.Name = "LabelX46"
        Me.LabelX46.Size = New System.Drawing.Size(128, 23)
        Me.LabelX46.TabIndex = 204
        Me.LabelX46.Text = "ALAMAT SEKOLAH"
        '
        'txtTingkat
        '
        '
        '
        '
        Me.txtTingkat.Border.Class = "TextBoxBorder"
        Me.txtTingkat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTingkat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTingkat.Location = New System.Drawing.Point(406, 137)
        Me.txtTingkat.Name = "txtTingkat"
        Me.txtTingkat.Size = New System.Drawing.Size(325, 20)
        Me.txtTingkat.TabIndex = 203
        '
        'LabelX47
        '
        Me.LabelX47.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX47.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX47.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX47.ForeColor = System.Drawing.Color.Black
        Me.LabelX47.Location = New System.Drawing.Point(263, 165)
        Me.LabelX47.Name = "LabelX47"
        Me.LabelX47.Size = New System.Drawing.Size(128, 23)
        Me.LabelX47.TabIndex = 202
        Me.LabelX47.Text = "NAMA SEKOLAH"
        '
        'LabelX48
        '
        Me.LabelX48.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX48.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX48.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX48.ForeColor = System.Drawing.Color.Black
        Me.LabelX48.Location = New System.Drawing.Point(263, 136)
        Me.LabelX48.Name = "LabelX48"
        Me.LabelX48.Size = New System.Drawing.Size(128, 23)
        Me.LabelX48.TabIndex = 201
        Me.LabelX48.Text = "TINGKAT PENDIDIKAN"
        '
        'txtStatusPendidikan
        '
        '
        '
        '
        Me.txtStatusPendidikan.Border.Class = "TextBoxBorder"
        Me.txtStatusPendidikan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatusPendidikan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatusPendidikan.Location = New System.Drawing.Point(406, 108)
        Me.txtStatusPendidikan.Name = "txtStatusPendidikan"
        Me.txtStatusPendidikan.Size = New System.Drawing.Size(325, 20)
        Me.txtStatusPendidikan.TabIndex = 200
        '
        'LabelX49
        '
        Me.LabelX49.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX49.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX49.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX49.ForeColor = System.Drawing.Color.Black
        Me.LabelX49.Location = New System.Drawing.Point(263, 107)
        Me.LabelX49.Name = "LabelX49"
        Me.LabelX49.Size = New System.Drawing.Size(128, 23)
        Me.LabelX49.TabIndex = 199
        Me.LabelX49.Text = "STATUS PENDIDIKAN"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Red
        Me.Panel6.Controls.Add(Me.LabelX44)
        Me.Panel6.Location = New System.Drawing.Point(229, 49)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(532, 41)
        Me.Panel6.TabIndex = 198
        '
        'LabelX44
        '
        Me.LabelX44.AutoSize = True
        Me.LabelX44.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX44.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX44.ForeColor = System.Drawing.Color.White
        Me.LabelX44.Location = New System.Drawing.Point(214, 4)
        Me.LabelX44.Name = "LabelX44"
        Me.LabelX44.Size = New System.Drawing.Size(129, 34)
        Me.LabelX44.TabIndex = 167
        Me.LabelX44.Text = "PENDIDIKAN"
        '
        'TabItem5
        '
        Me.TabItem5.AttachedControl = Me.TabControlPanel6
        Me.TabItem5.Name = "TabItem5"
        Me.TabItem5.Text = "PENDIDIKAN"
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.PictureBox3)
        Me.TabControlPanel7.Controls.Add(Me.LabelX60)
        Me.TabControlPanel7.Controls.Add(Me.LabelX59)
        Me.TabControlPanel7.Controls.Add(Me.txtCC)
        Me.TabControlPanel7.Controls.Add(Me.LabelX57)
        Me.TabControlPanel7.Controls.Add(Me.txtNpwp)
        Me.TabControlPanel7.Controls.Add(Me.LabelX58)
        Me.TabControlPanel7.Controls.Add(Me.txtNamaBank)
        Me.TabControlPanel7.Controls.Add(Me.txtRekAn)
        Me.TabControlPanel7.Controls.Add(Me.LabelX53)
        Me.TabControlPanel7.Controls.Add(Me.LabelX54)
        Me.TabControlPanel7.Controls.Add(Me.txtNoRek)
        Me.TabControlPanel7.Controls.Add(Me.LabelX55)
        Me.TabControlPanel7.Controls.Add(Me.Panel7)
        Me.TabControlPanel7.Controls.Add(Me.Panel8)
        Me.TabControlPanel7.Controls.Add(Me.PictureBox2)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(1041, 479)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.Style.GradientAngle = 90
        Me.TabControlPanel7.TabIndex = 7
        Me.TabControlPanel7.TabItem = Me.TabItem6
        '
        'LabelX60
        '
        Me.LabelX60.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX60.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX60.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX60.ForeColor = System.Drawing.Color.Black
        Me.LabelX60.Location = New System.Drawing.Point(546, 196)
        Me.LabelX60.Name = "LabelX60"
        Me.LabelX60.Size = New System.Drawing.Size(128, 23)
        Me.LabelX60.TabIndex = 223
        Me.LabelX60.Text = "PHOTO KTP"
        '
        'LabelX59
        '
        Me.LabelX59.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX59.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX59.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX59.ForeColor = System.Drawing.Color.Black
        Me.LabelX59.Location = New System.Drawing.Point(39, 196)
        Me.LabelX59.Name = "LabelX59"
        Me.LabelX59.Size = New System.Drawing.Size(128, 23)
        Me.LabelX59.TabIndex = 221
        Me.LabelX59.Text = "PHOTO PROFILE"
        '
        'txtCC
        '
        '
        '
        '
        Me.txtCC.Border.Class = "TextBoxBorder"
        Me.txtCC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCC.Location = New System.Drawing.Point(713, 115)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(300, 20)
        Me.txtCC.TabIndex = 220
        '
        'LabelX57
        '
        Me.LabelX57.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX57.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX57.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX57.ForeColor = System.Drawing.Color.Black
        Me.LabelX57.Location = New System.Drawing.Point(546, 113)
        Me.LabelX57.Name = "LabelX57"
        Me.LabelX57.Size = New System.Drawing.Size(161, 23)
        Me.LabelX57.TabIndex = 219
        Me.LabelX57.Text = "NOMOR KARTU KREDIT"
        '
        'txtNpwp
        '
        '
        '
        '
        Me.txtNpwp.Border.Class = "TextBoxBorder"
        Me.txtNpwp.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNpwp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNpwp.Location = New System.Drawing.Point(713, 85)
        Me.txtNpwp.Name = "txtNpwp"
        Me.txtNpwp.Size = New System.Drawing.Size(300, 20)
        Me.txtNpwp.TabIndex = 218
        '
        'LabelX58
        '
        Me.LabelX58.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX58.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX58.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX58.ForeColor = System.Drawing.Color.Black
        Me.LabelX58.Location = New System.Drawing.Point(546, 84)
        Me.LabelX58.Name = "LabelX58"
        Me.LabelX58.Size = New System.Drawing.Size(128, 23)
        Me.LabelX58.TabIndex = 217
        Me.LabelX58.Text = "NOMOR NPWP"
        '
        'txtNamaBank
        '
        '
        '
        '
        Me.txtNamaBank.Border.Class = "TextBoxBorder"
        Me.txtNamaBank.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaBank.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaBank.Location = New System.Drawing.Point(181, 144)
        Me.txtNamaBank.Name = "txtNamaBank"
        Me.txtNamaBank.Size = New System.Drawing.Size(325, 20)
        Me.txtNamaBank.TabIndex = 216
        '
        'txtRekAn
        '
        '
        '
        '
        Me.txtRekAn.Border.Class = "TextBoxBorder"
        Me.txtRekAn.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtRekAn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRekAn.Location = New System.Drawing.Point(181, 115)
        Me.txtRekAn.Name = "txtRekAn"
        Me.txtRekAn.Size = New System.Drawing.Size(325, 20)
        Me.txtRekAn.TabIndex = 215
        '
        'LabelX53
        '
        Me.LabelX53.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX53.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX53.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX53.ForeColor = System.Drawing.Color.Black
        Me.LabelX53.Location = New System.Drawing.Point(39, 142)
        Me.LabelX53.Name = "LabelX53"
        Me.LabelX53.Size = New System.Drawing.Size(128, 23)
        Me.LabelX53.TabIndex = 214
        Me.LabelX53.Text = "NAMA BANK"
        '
        'LabelX54
        '
        Me.LabelX54.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX54.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX54.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX54.ForeColor = System.Drawing.Color.Black
        Me.LabelX54.Location = New System.Drawing.Point(39, 113)
        Me.LabelX54.Name = "LabelX54"
        Me.LabelX54.Size = New System.Drawing.Size(128, 23)
        Me.LabelX54.TabIndex = 213
        Me.LabelX54.Text = "REK. ATAS NAMA"
        '
        'txtNoRek
        '
        '
        '
        '
        Me.txtNoRek.Border.Class = "TextBoxBorder"
        Me.txtNoRek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNoRek.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoRek.Location = New System.Drawing.Point(181, 85)
        Me.txtNoRek.Name = "txtNoRek"
        Me.txtNoRek.Size = New System.Drawing.Size(325, 20)
        Me.txtNoRek.TabIndex = 212
        '
        'LabelX55
        '
        Me.LabelX55.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX55.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX55.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX55.ForeColor = System.Drawing.Color.Black
        Me.LabelX55.Location = New System.Drawing.Point(39, 84)
        Me.LabelX55.Name = "LabelX55"
        Me.LabelX55.Size = New System.Drawing.Size(128, 23)
        Me.LabelX55.TabIndex = 211
        Me.LabelX55.Text = "NO. REKENING"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Blue
        Me.Panel7.Controls.Add(Me.LabelX51)
        Me.Panel7.Location = New System.Drawing.Point(527, 19)
        Me.Panel7.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(501, 41)
        Me.Panel7.TabIndex = 210
        '
        'LabelX51
        '
        Me.LabelX51.AutoSize = True
        Me.LabelX51.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX51.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX51.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX51.ForeColor = System.Drawing.Color.White
        Me.LabelX51.Location = New System.Drawing.Point(166, 5)
        Me.LabelX51.Name = "LabelX51"
        Me.LabelX51.Size = New System.Drawing.Size(206, 34)
        Me.LabelX51.TabIndex = 168
        Me.LabelX51.Text = "DOKUMEN LAINNYA"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Red
        Me.Panel8.Controls.Add(Me.LabelX52)
        Me.Panel8.Location = New System.Drawing.Point(22, 19)
        Me.Panel8.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(501, 41)
        Me.Panel8.TabIndex = 209
        '
        'LabelX52
        '
        Me.LabelX52.AutoSize = True
        Me.LabelX52.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX52.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX52.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX52.ForeColor = System.Drawing.Color.White
        Me.LabelX52.Location = New System.Drawing.Point(155, 5)
        Me.LabelX52.Name = "LabelX52"
        Me.LabelX52.Size = New System.Drawing.Size(207, 34)
        Me.LabelX52.TabIndex = 167
        Me.LabelX52.Text = "FILE DAN DOKUMEN"
        '
        'TabItem6
        '
        Me.TabItem6.AttachedControl = Me.TabControlPanel7
        Me.TabItem6.Name = "TabItem6"
        Me.TabItem6.Text = "FILE DAN DOKUMEN"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(540, 31)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(154, 206)
        Me.PictureBox1.TabIndex = 162
        Me.PictureBox1.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(689, 196)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(121, 129)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 224
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(181, 196)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(121, 129)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 222
        Me.PictureBox2.TabStop = False
        '
        'frmInformasiKaryawan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1041, 505)
        Me.Controls.Add(Me.TabControlAkun)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmInformasiKaryawan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DATA INFORMASI KARYAWAN LENGKAP"
        CType(Me.TabControlAkun, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlAkun.ResumeLayout(False)
        Me.TabControlPanel10.ResumeLayout(False)
        CType(Me.DGViewMutasi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.TabControlPanel9.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel2.ResumeLayout(False)
        CType(Me.dtLahir, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.TabControlPanel4.ResumeLayout(False)
        CType(Me.dtMenikah, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TabControlPanel6.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TabControlPanel7.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControlAkun As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents aktiva As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem8 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem6 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel10 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem9 As DevComponents.DotNetBar.TabItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDate As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtStatus As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUnitBisnis As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNama As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKelamin As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtLahir As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents txtEmail2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtEmail As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTelp2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTelp1 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtGolDarah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSuku As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtWarga As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAgama As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTempatLahir As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPanggilan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaLengkap As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKtp As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKodepos As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX31 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKelurahan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX30 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKecamatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX29 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKota As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX28 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtProvinsi As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDetailLokasi As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtStatusRumah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txAlamat As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX32 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtMenikah As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents txtNamaIbu As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX37 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAnakKe As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX36 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtJumlahTanggungan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX35 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX34 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtStatusPerkawinan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX33 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents LabelX38 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txAlamatSekolah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtJurusan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX50 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTahunIjazah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX45 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaSekolah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX46 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTingkat As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX47 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX48 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtStatusPendidikan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX49 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents LabelX44 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelX60 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX59 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCC As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX57 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNpwp As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX58 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaBank As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtRekAn As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX53 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX54 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNoRek As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX55 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents LabelX51 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents LabelX52 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents LabelX39 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DGView As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DGViewMutasi As System.Windows.Forms.DataGridView
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents LabelX40 As DevComponents.DotNetBar.LabelX
End Class
