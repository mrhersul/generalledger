﻿Public Class frmBrowseCatatan
    Dim kk As Integer
    Public Sub ShowDataGrid()

        SQL = ""
        SQL = "SP_HR_LIST_CATATAN "

        If txtSearch.Text <> "" Then
            SQL = SQL & " '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%', "
        Else
            SQL = SQL & " '', "
        End If

        Select Case cboStatus.Text
            Case "PRESTASI"
                SQL = SQL & " '1' "
            Case "KOMPETENSI"
                SQL = SQL & " '2' "
            Case "KASUS(KARYAWAN)"
                SQL = SQL & " '3' "
            Case Else
                SQL = SQL & " '' "
        End Select

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 100
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 350
        DGView.Columns(3).Width = 250
        DGView.Columns(4).Width = 150
        DGView.Columns(5).Width = 200
        DGView.Columns(6).Width = 150
        DGView.Columns(7).Width = 300
        DGView.Columns(8).Width = 80


        DGView.Columns(0).HeaderText = "TANGGAL"
        DGView.Columns(1).HeaderText = "KODE KARYAWAN"
        DGView.Columns(2).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(3).HeaderText = "JUDUL"
        DGView.Columns(4).HeaderText = "JENIS"
        DGView.Columns(5).HeaderText = "DEPARTEMEN"
        DGView.Columns(6).HeaderText = "JABATAN"
        DGView.Columns(7).HeaderText = "KETERANGAN"
        DGView.Columns(8).HeaderText = "ID"
        DGView.Columns(8).Visible = False

    End Sub

    Private Sub frmBrowseCatatan_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmBrowseCatatan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ShowDataGrid()
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BubbleButton6.Click
        Call frmClose()
    End Sub
    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
            End With
        End If
    End Sub

    Private Sub frmBrowseCatatan_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 836
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnAdd_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BubbleButton1.Click
        frmInputCatatan.ShowDialog()
        If frmInputCatatan.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGrid()
        End If
    End Sub

    Public Sub BtnSearch_Click(sender As System.Object, e As System.EventArgs) Handles BtnSearch.Click
        Call ShowDataGrid()
    End Sub
End Class