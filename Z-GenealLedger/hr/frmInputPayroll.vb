﻿Public Class frmInputPayroll
    Dim Idnya, iUb, iDp, iJab As Integer
    Dim kd, nm, nUnit, nDep, nJab As String
    Dim TotalGaji, TotalPot, GrandTotal As Double
    Public Sub Karyawan(ByVal kode As String, ByVal nama As String, ByVal sid As Integer, ByVal Ub As String, ByVal dp As String, ByVal jab As String,
                        ByVal UbId As Integer, ByVal dpId As Integer, ByVal jabId As Integer)
        kd = kode
        nm = nama
        Idnya = sid
        iUb = UbId
        iDp = dpId
        iJab = jabId
        nUnit = Ub
        nDep = dp
        nJab = jab
        'txtKdKaryawan.Text = kd
        'txtNmKaryawan.Text = nm
        'txtUnitBisnis.Text = nUnit
        'txtDepartemen.Text = nDep
        'txtJabatan.Text = nJab
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        frmKaryawan.Text = "PAYROLL"
        frmKaryawan.ShowDialog()
    End Sub

    Private Sub frmInputPayroll_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        txtKdKaryawan.Text = kd
        txtNmKaryawan.Text = nm
        txtUnitBisnis.Text = nUnit
        txtDepartemen.Text = nDep
        txtJabatan.Text = nJab
    End Sub

    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub txtGapok_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtGapok.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub frmInputPayroll_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        TotalGaji = 0
        TotalPot = 0
        GrandTotal = 0
        txtGapok.Text = 0
        txtTJabatan.Text = 0
        txtTKhusus.Text = 0
        txtTService.Text = 0
        txtTPulsa.Text = 0
        txtUMakan.Text = 0
        txtUTransport.Text = 0
        txtULembur.Text = 0

        txtBKes.Text = 0
        txtBTnk.Text = 0
        txtPinjaman.Text = 0
        txtTerlambat.Text = 0
        txtOTS.Text = 0
        txtPotLain.Text = 0
        txtPKoperasi.Text = 0
        txtIKoperasi.Text = 0
        txtTPotongan.Text = 0
        txtGrandTotal.Text = 0
    End Sub

    Private Sub txtTJabatan_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTJabatan.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtTKhusus_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTKhusus.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtTPulsa_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTPulsa.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtTService_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTService.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtUMakan_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtUMakan.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtUTransport_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtUTransport.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtULembur_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtULembur.TextChanged
        TotalGaji = Val(txtGapok.Text) + Val(txtTJabatan.Text) + Val(txtTKhusus.Text) + Val(txtTPulsa.Text) + Val(txtTService.Text) + Val(txtUMakan.Text) + Val(txtUTransport.Text) + Val(txtULembur.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTGaji.Text = TotalGaji
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtBKes_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBKes.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtBTnk_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBTnk.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtPinjaman_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtPinjaman.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtTerlambat_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTerlambat.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtOTS_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtOTS.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtPotLain_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtPotLain.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtPKoperasi_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtPKoperasi.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Private Sub txtIKoperasi_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtIKoperasi.TextChanged
        TotalPot = Val(txtBKes.Text) + Val(txtBTnk.Text) + Val(txtPinjaman.Text) + Val(txtPKoperasi.Text) + Val(txtTerlambat.Text) + Val(txtOTS.Text) + Val(txtPotLain.Text) + Val(txtIKoperasi.Text)
        GrandTotal = TotalGaji - TotalPot

        txtTPotongan.Text = TotalPot
        txtGrandTotal.Text = GrandTotal
    End Sub

    Public Sub formatUang(ByVal Text As TextBox)
        If Len(Text.Text) > 0 Then
            Text.Text = FormatNumber(CDbl(Text.Text), 0)
            Dim x As Integer = Text.SelectionStart.ToString
            If x = 0 Then
                Text.SelectionStart = Len(Text.Text)
                Text.SelectionLength = 0
            Else
                Text.SelectionStart = x
                Text.SelectionLength = 0
            End If
        End If
    End Sub

    Private Sub BtnSave_Click(sender As System.Object, e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        Dim Genid As String
        SQL = ""
        SQL = "SP_GENERATE_ID"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()
        Genid = dr.Item(0).ToString

        dr.Close()
        SQL = ""
        SQL = "INSERT INTO karyawan_payroll (id,karyawan_nama_id,gaji_pokok,t_jabatan,t_khusus,t_pulsa,t_service,u_makan,u_transport,u_lembur,bpjs_kesehatan,"
        SQL = SQL & " bpjs_tenagakerja,iuran_koperasi,pinjaman_koperasi,pinjaman_karyawan,ots,potongan_lainnya,keterlambatan,tgl_buat,user_buat,tanggal) "
        SQL = SQL & " VALUES ('" & Genid & "'," & Idnya & ", '" & Val(txtGapok.Text) & "','" & Val(txtTJabatan.Text) & "','" & Val(txtTKhusus.Text) & "', "
        SQL = SQL & " '" & Val(txtTPulsa.Text) & "','" & Val(txtTService.Text) & "','" & Val(txtUMakan.Text) & "','" & Val(txtUTransport.Text) & "', "
        SQL = SQL & " '" & Val(txtULembur.Text) & "','" & Val(txtBKes.Text) & "','" & Val(txtBTnk.Text) & "','" & Val(txtIKoperasi.Text) & "', "
        SQL = SQL & " '" & Val(txtPKoperasi.Text) & "','" & Val(txtPinjaman.Text) & "','" & Val(txtOTS.Text) & "','" & Val(txtPotLain.Text) & "', "
        SQL = SQL & " '" & Val(txtTerlambat.Text) & "','" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', '" & myID & "', "
        SQL = SQL & " '" & Format(dtTanggal.Value, "yyyy-MM-dd") & "') "
        Konek.IUDQuery(SQL)

        
        IsConError = False
        MsgBox("Data sudah tersimpan.")
        Me.Close()

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
End Class