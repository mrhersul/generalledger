﻿'Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop

Public Class frmPayroll
    Dim kk As Integer

    Sub ShowDataGrid()
        Dim nYear As String = Year(Now)
        Dim nMont As String = Month(Now)

        If Len(nMont) = 1 Then
            nMont = "0" + nMont
        Else
            nMont = nMont
        End If

        Select Case cmbBulan.Text
            Case "JANUARI"
                nMont = "01"
            Case "FEBRUARI"
                nMont = "02"
            Case "MARET"
                nMont = "03"
            Case "APRIL"
                nMont = "04"
            Case "MEI"
                nMont = "05"
            Case "JUNI"
                nMont = "06"
            Case "JULI"
                nMont = "07"
            Case "AGUSTUS"
                nMont = "08"
            Case "SEPTEMBER"
                nMont = "09"
            Case "OKTOBER"
                nMont = "10"
            Case "NOVEMBER"
                nMont = "11"
            Case "DESEMBER"
                nMont = "12"
            Case Else
                nMont = nMont
        End Select

        If txtTahun.Text = "" Then
            nYear = nYear
        Else
            nYear = txtTahun.Text
        End If

        SQL = ""
        SQL = "select distinct g.nama_lengkap, a.tanggal, e.nama as departemen, f.nama as jabatan, a.gaji_pokok, a.t_jabatan, a.t_khusus, a.t_pulsa, a.t_service, "
        SQL = SQL & " a.u_makan, a.u_transport, a.u_lembur, a.bpjs_kesehatan, a.bpjs_tenagakerja, a.iuran_koperasi, a.pinjaman_koperasi,"
        SQL = SQL & " a.pinjaman_karyawan, a.ots, a.potongan_lainnya, a.keterlambatan,a.id from karyawan_payroll a "
        SQL = SQL & " inner join karyawan b on a.karyawan_nama_id = b.personal_id"
        SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_id"
        SQL = SQL & " inner join karyawan_mutasi_organisasi d on d.personal_id = a.karyawan_nama_id"
        SQL = SQL & " inner join karyawan_struktur_organisasi e on e.id = d.departemen_id"
        SQL = SQL & " inner join karyawan_struktur_organisasi f on f.id = d.jabatan_id"
        SQL = SQL & " inner join personal g on g.id = a.karyawan_nama_id"
        SQL = SQL & " WHERE c.kd_unitbisnis = '" & myKdBisnis & "' "

        If txtSearch.Text <> "" Then

            SQL = SQL & " and (b.nama_lengkap like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If
        SQL = SQL & " group by g.nama_lengkap,a.tanggal,e.nama,f.nama,a.gaji_pokok,a.t_jabatan,a.t_khusus,a.t_pulsa,a.t_service,a.u_makan,a.u_transport,a.u_lembur,"
        SQL = SQL & " a.bpjs_kesehatan,a.bpjs_tenagakerja,a.iuran_koperasi,a.pinjaman_koperasi,a.pinjaman_karyawan,a.ots,a.potongan_lainnya,a.keterlambatan,a.id"
        SQL = SQL & " order by g.nama_lengkap asc"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 350
        DGView.Columns(1).Width = 250
        DGView.Columns(2).Width = 250
        DGView.Columns(3).Width = 250
        DGView.Columns(4).Width = 150
        DGView.Columns(5).Width = 150
        DGView.Columns(6).Width = 150
        DGView.Columns(7).Width = 150
        DGView.Columns(8).Width = 150
        DGView.Columns(9).Width = 150
        DGView.Columns(10).Width = 150
        DGView.Columns(11).Width = 150
        DGView.Columns(12).Width = 150
        DGView.Columns(13).Width = 150
        DGView.Columns(14).Width = 150
        DGView.Columns(15).Width = 150
        DGView.Columns(16).Width = 150
        DGView.Columns(17).Width = 150
        DGView.Columns(18).Width = 150
        DGView.Columns(19).Width = 150
        DGView.Columns(20).Width = 80

        DGView.Columns(0).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(1).HeaderText = "TANGGAL"
        DGView.Columns(2).HeaderText = "DEPARTEMEN"
        DGView.Columns(3).HeaderText = "JABATAN"
        DGView.Columns(4).HeaderText = "GAJI POKOK"
        DGView.Columns(5).HeaderText = "T.JABATAN"
        DGView.Columns(6).HeaderText = "T.KHUSUS"
        DGView.Columns(7).HeaderText = "T.PULSA"
        DGView.Columns(8).HeaderText = "T.SERVICE"
        DGView.Columns(9).HeaderText = "U.MAKAN"
        DGView.Columns(10).HeaderText = "U.TRANSPORT"
        DGView.Columns(11).HeaderText = "U.LEMBUR"
        DGView.Columns(12).HeaderText = "BPJS KESEHATAN"
        DGView.Columns(13).HeaderText = "BPJS TENAGAKERJA"
        DGView.Columns(14).HeaderText = "IURAN KOPERASI"
        DGView.Columns(15).HeaderText = "PINJAMAN KOPERASI"
        DGView.Columns(16).HeaderText = "PINJAMAN KARYAWAN"
        DGView.Columns(17).HeaderText = "OTS"
        DGView.Columns(18).HeaderText = "POT.LAINNYA"
        DGView.Columns(19).HeaderText = "POT.TERLAMBAT"
        DGView.Columns(20).HeaderText = "ID"
        DGView.Columns(20).Visible = False
    End Sub
    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub frmPayroll_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmPayroll_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ShowDataGrid()
    End Sub

    Private Sub frmPayroll_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 1120
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Private Sub BtnAdd_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnAdd.Click
        frmInputPayroll.ShowDialog()
    End Sub

    Private Sub BtnShow_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnShow.Click
        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook
        Dim shXL As Excel.Worksheet
        Dim raXL As Excel.Range
        ' Start Excel and get Application object.
        appXL = CreateObject("Excel.Application")
        appXL.Visible = True
        ' Add a new workbook.
        wbXl = appXL.Workbooks.Add
        shXL = wbXl.ActiveSheet
        ' Add table headers going cell by cell.
        shXL.Cells(1, 1).Value = "karyawan_nama_id"
        shXL.Cells(1, 2).value = "nama_karyawan"
        shXL.Cells(1, 3).Value = "gaji_pokok"
        shXL.Cells(1, 4).Value = "t_jabatan"
        shXL.Cells(1, 5).Value = "t_khusus"
        shXL.Cells(1, 6).Value = "t_pulsa"
        shXL.Cells(1, 7).Value = "t_service"
        shXL.Cells(1, 8).Value = "u_makan"
        shXL.Cells(1, 9).Value = "u_transport"
        shXL.Cells(1, 10).Value = "u_lembur"
        shXL.Cells(1, 11).Value = "bpjs_kesehatan"
        shXL.Cells(1, 12).Value = "bpjs_tenagakerja"
        shXL.Cells(1, 13).Value = "iuran_koperasi"
        shXL.Cells(1, 14).Value = "pinjaman_koperasi"
        shXL.Cells(1, 15).Value = "pinjaman_karyawan"
        shXL.Cells(1, 16).Value = "ots"
        shXL.Cells(1, 17).Value = "potongan_lainnya"
        shXL.Cells(1, 18).Value = "keterlambatan"

        '' Format A1:D1 as bold, vertical alignment = center.
        'With shXL.Range("A1", "D1")
        '    .Font.Bold = True
        '    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
        'End With
        '' Create an array to set multiple values at once.
        'Dim students(5, 2) As String
        'students(0, 0) = "Zara"
        'students(0, 1) = "Ali"
        'students(1, 0) = "Nuha"
        'students(1, 1) = "Ali"
        'students(2, 0) = "Arilia"
        'students(2, 1) = "RamKumar"
        'students(3, 0) = "Rita"
        'students(3, 1) = "Jones"
        'students(4, 0) = "Umme"
        'students(4, 1) = "Ayman"
        '' Fill A2:B6 with an array of values (First and Last Names).
        'shXL.Range("A2", "B6").Value = students
        '' Fill C2:C6 with a relative formula (=A2 & " " & B2).
        'raXL = shXL.Range("C2", "C6")
        'raXL.Formula = "=A2 & "" "" & B2"
        '' Fill D2:D6 values.

        SQL = ""
        SQL = "select a.id, a.nama_lengkap from personal a "
        SQL = SQL & " inner join karyawan b on a.id = b.personal_id "
        SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_id "
        SQL = SQL & " WHERE c.kd_unitbisnis = '" & myKdBisnis & "' "
        SQL = SQL & " and len(b.kd_karyawan) > 0 "
        SQL = SQL & " order by a.nama_lengkap"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim n As Integer = 1
        While dr.Read()
            n = n + 1
            shXL.Cells(n, 1).value = dr.Item("id").ToString
            shXL.Cells(n, 2).value = dr.Item("nama_lengkap").ToString
        End While
        'With shXL
        '    .Cells(2, 1).Value = "Biology"
        '    .Cells(3, 1).Value = "Mathmematics"
        '    .Cells(4, 1).Value = "Physics"
        '    .Cells(5, 1).Value = "Mathmematics"
        '    .Cells(6, 1).Value = "Arabic"
        'End With
        ' AutoFit columns A:D.
        raXL = shXL.Range("A1", "D1")
        raXL.EntireColumn.AutoFit()
        ' Make sure Excel is visible and give the user control
        ' of Excel's lifetime.
        appXL.Visible = True
        appXL.UserControl = True
        ' Release object references.
        raXL = Nothing
        shXL = Nothing
        wbXl = Nothing
        appXL.Quit()
        appXL = Nothing
        Exit Sub
Err_Handler:
        MsgBox(Err.Description, vbCritical, "Error: " & Err.Number)

    End Sub

    Private Sub BtnExcel_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnExcel.Click
        frmImport.ShowDialog()
        If frmImport.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGrid()
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Call ShowDataGrid()
    End Sub
End Class