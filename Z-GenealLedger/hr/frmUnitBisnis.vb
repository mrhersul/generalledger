﻿Public Class frmUnitBisnis
    Dim kk As Integer
    Dim kd, nm As String
    Sub ShowDataGrid()

        SQL = ""
        SQL = "select top(100) kd_unitbisnis, nama_unitbisnis, id from unitbisnis_nama "
        SQL = SQL & "WHERE kd_unitbisnis = '" & myKdBisnis & "' "

        'If txtSearch.Text <> "" Then

        '    SQL = SQL & " and (a.nama_lengkap like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        'End If

        'SQL = SQL & " and len(b.kd_karyawan) > 0 "
        SQL = SQL & " order by kd_unitbisnis"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 150
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 80

        DGView.Columns(0).HeaderText = "KODE UNITBISNIS"
        DGView.Columns(1).HeaderText = "NAMA UNITBISNIS"
        DGView.Columns(2).HeaderText = "ID"
        DGView.Columns(2).Visible = False

        ColorChange()

    End Sub
    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next

    End Sub

    Private Sub frmUnitBisnis_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ShowDataGrid()
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
                kd = .Item("kd_unitbisnis", i).Value
                nm = .Item("nama_unitbisnis", i).Value
            End With
        End If
    End Sub

    Private Sub DGView_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellDoubleClick
        TakeDGView()
        'Select Case Me.Text
        '    Case "TUGAS DINAS"
        '        frmInputTugasDinas.UnitBisnis(kd, nm, kk)
        'End Select
        Me.Close()
    End Sub
End Class