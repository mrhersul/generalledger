﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInputPayroll
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInputPayroll))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.txtGrandTotal = New DevComponents.DotNetBar.LabelX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtTPotongan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.txtIKoperasi = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.txtPKoperasi = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.txtPotLain = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.txtOTS = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.txtTerlambat = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.txtPinjaman = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.txtBTnk = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LableTnk = New DevComponents.DotNetBar.LabelX()
        Me.txtBKes = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.txtTGaji = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.txtULembur = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.txtUTransport = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.txtUMakan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.txtTService = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.txtTPulsa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.txtTKhusus = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.txtTJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtGapok = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.txtDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUnitBisnis = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.dtTanggal = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtNmKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.GroupPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dtTanggal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.txtJabatan)
        Me.GroupPanel1.Controls.Add(Me.LabelX26)
        Me.GroupPanel1.Controls.Add(Me.txtGrandTotal)
        Me.GroupPanel1.Controls.Add(Me.LabelX24)
        Me.GroupPanel1.Controls.Add(Me.GroupBox1)
        Me.GroupPanel1.Controls.Add(Me.txtDepartemen)
        Me.GroupPanel1.Controls.Add(Me.txtUnitBisnis)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.Button1)
        Me.GroupPanel1.Controls.Add(Me.txtKdKaryawan)
        Me.GroupPanel1.Controls.Add(Me.dtTanggal)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.txtNmKaryawan)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(13, 13)
        Me.GroupPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1057, 751)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 12
        Me.GroupPanel1.Text = "FORM INPUT PAYROLL "
        '
        'txtJabatan
        '
        '
        '
        '
        Me.txtJabatan.Border.Class = "TextBoxBorder"
        Me.txtJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJabatan.Enabled = False
        Me.txtJabatan.Location = New System.Drawing.Point(231, 203)
        Me.txtJabatan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJabatan.Name = "txtJabatan"
        Me.txtJabatan.Size = New System.Drawing.Size(433, 26)
        Me.txtJabatan.TabIndex = 188
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(72, 131)
        Me.LabelX26.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(109, 28)
        Me.LabelX26.TabIndex = 187
        Me.LabelX26.Text = "UNIT BISNIS"
        '
        'txtGrandTotal
        '
        Me.txtGrandTotal.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.txtGrandTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGrandTotal.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrandTotal.ForeColor = System.Drawing.Color.Black
        Me.txtGrandTotal.Location = New System.Drawing.Point(532, 692)
        Me.txtGrandTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtGrandTotal.Name = "txtGrandTotal"
        Me.txtGrandTotal.Size = New System.Drawing.Size(145, 28)
        Me.txtGrandTotal.TabIndex = 186
        Me.txtGrandTotal.Text = "RP.  0"
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(363, 692)
        Me.LabelX24.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(145, 28)
        Me.LabelX24.TabIndex = 185
        Me.LabelX24.Text = "TOTAL TERIMA"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtTPotongan)
        Me.GroupBox1.Controls.Add(Me.LabelX23)
        Me.GroupBox1.Controls.Add(Me.txtIKoperasi)
        Me.GroupBox1.Controls.Add(Me.LabelX22)
        Me.GroupBox1.Controls.Add(Me.txtPKoperasi)
        Me.GroupBox1.Controls.Add(Me.LabelX21)
        Me.GroupBox1.Controls.Add(Me.txtPotLain)
        Me.GroupBox1.Controls.Add(Me.LabelX20)
        Me.GroupBox1.Controls.Add(Me.txtOTS)
        Me.GroupBox1.Controls.Add(Me.LabelX19)
        Me.GroupBox1.Controls.Add(Me.txtTerlambat)
        Me.GroupBox1.Controls.Add(Me.LabelX18)
        Me.GroupBox1.Controls.Add(Me.txtPinjaman)
        Me.GroupBox1.Controls.Add(Me.LabelX17)
        Me.GroupBox1.Controls.Add(Me.txtBTnk)
        Me.GroupBox1.Controls.Add(Me.LableTnk)
        Me.GroupBox1.Controls.Add(Me.txtBKes)
        Me.GroupBox1.Controls.Add(Me.LabelX15)
        Me.GroupBox1.Controls.Add(Me.txtTGaji)
        Me.GroupBox1.Controls.Add(Me.LabelX14)
        Me.GroupBox1.Controls.Add(Me.txtULembur)
        Me.GroupBox1.Controls.Add(Me.LabelX13)
        Me.GroupBox1.Controls.Add(Me.txtUTransport)
        Me.GroupBox1.Controls.Add(Me.LabelX12)
        Me.GroupBox1.Controls.Add(Me.txtUMakan)
        Me.GroupBox1.Controls.Add(Me.LabelX11)
        Me.GroupBox1.Controls.Add(Me.txtTService)
        Me.GroupBox1.Controls.Add(Me.LabelX10)
        Me.GroupBox1.Controls.Add(Me.txtTPulsa)
        Me.GroupBox1.Controls.Add(Me.LabelX9)
        Me.GroupBox1.Controls.Add(Me.txtTKhusus)
        Me.GroupBox1.Controls.Add(Me.LabelX8)
        Me.GroupBox1.Controls.Add(Me.txtTJabatan)
        Me.GroupBox1.Controls.Add(Me.LabelX7)
        Me.GroupBox1.Controls.Add(Me.txtGapok)
        Me.GroupBox1.Controls.Add(Me.LabelX4)
        Me.GroupBox1.Location = New System.Drawing.Point(62, 260)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(949, 411)
        Me.GroupBox1.TabIndex = 151
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PAYROLL"
        '
        'txtTPotongan
        '
        '
        '
        '
        Me.txtTPotongan.Border.Class = "TextBoxBorder"
        Me.txtTPotongan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTPotongan.Enabled = False
        Me.txtTPotongan.Location = New System.Drawing.Point(705, 358)
        Me.txtTPotongan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTPotongan.Name = "txtTPotongan"
        Me.txtTPotongan.Size = New System.Drawing.Size(204, 26)
        Me.txtTPotongan.TabIndex = 184
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(521, 356)
        Me.LabelX23.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(145, 28)
        Me.LabelX23.TabIndex = 183
        Me.LabelX23.Text = "TOTAL POTONGAN"
        '
        'txtIKoperasi
        '
        '
        '
        '
        Me.txtIKoperasi.Border.Class = "TextBoxBorder"
        Me.txtIKoperasi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIKoperasi.Location = New System.Drawing.Point(705, 304)
        Me.txtIKoperasi.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIKoperasi.Name = "txtIKoperasi"
        Me.txtIKoperasi.Size = New System.Drawing.Size(204, 26)
        Me.txtIKoperasi.TabIndex = 182
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(521, 158)
        Me.LabelX22.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(176, 28)
        Me.LabelX22.TabIndex = 181
        Me.LabelX22.Text = "POTONGAN TERLAMBAT"
        '
        'txtPKoperasi
        '
        '
        '
        '
        Me.txtPKoperasi.Border.Class = "TextBoxBorder"
        Me.txtPKoperasi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPKoperasi.Location = New System.Drawing.Point(705, 268)
        Me.txtPKoperasi.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPKoperasi.Name = "txtPKoperasi"
        Me.txtPKoperasi.Size = New System.Drawing.Size(204, 26)
        Me.txtPKoperasi.TabIndex = 180
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.ForeColor = System.Drawing.Color.Black
        Me.LabelX21.Location = New System.Drawing.Point(521, 228)
        Me.LabelX21.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(145, 28)
        Me.LabelX21.TabIndex = 179
        Me.LabelX21.Text = "POTONGAN LAINNYA"
        '
        'txtPotLain
        '
        '
        '
        '
        Me.txtPotLain.Border.Class = "TextBoxBorder"
        Me.txtPotLain.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPotLain.Location = New System.Drawing.Point(705, 230)
        Me.txtPotLain.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPotLain.Name = "txtPotLain"
        Me.txtPotLain.Size = New System.Drawing.Size(204, 26)
        Me.txtPotLain.TabIndex = 178
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(521, 196)
        Me.LabelX20.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(145, 28)
        Me.LabelX20.TabIndex = 177
        Me.LabelX20.Text = "OTS"
        '
        'txtOTS
        '
        '
        '
        '
        Me.txtOTS.Border.Class = "TextBoxBorder"
        Me.txtOTS.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOTS.Location = New System.Drawing.Point(705, 196)
        Me.txtOTS.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOTS.Name = "txtOTS"
        Me.txtOTS.Size = New System.Drawing.Size(204, 26)
        Me.txtOTS.TabIndex = 176
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(521, 124)
        Me.LabelX19.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(156, 28)
        Me.LabelX19.TabIndex = 175
        Me.LabelX19.Text = "PINJAMAN KARYAWAN"
        '
        'txtTerlambat
        '
        '
        '
        '
        Me.txtTerlambat.Border.Class = "TextBoxBorder"
        Me.txtTerlambat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTerlambat.Location = New System.Drawing.Point(705, 160)
        Me.txtTerlambat.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTerlambat.Name = "txtTerlambat"
        Me.txtTerlambat.Size = New System.Drawing.Size(204, 26)
        Me.txtTerlambat.TabIndex = 174
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(521, 266)
        Me.LabelX18.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(145, 28)
        Me.LabelX18.TabIndex = 173
        Me.LabelX18.Text = "PINJAMAN KOPERASI"
        '
        'txtPinjaman
        '
        '
        '
        '
        Me.txtPinjaman.Border.Class = "TextBoxBorder"
        Me.txtPinjaman.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPinjaman.Location = New System.Drawing.Point(705, 124)
        Me.txtPinjaman.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPinjaman.Name = "txtPinjaman"
        Me.txtPinjaman.Size = New System.Drawing.Size(204, 26)
        Me.txtPinjaman.TabIndex = 172
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(521, 302)
        Me.LabelX17.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(145, 28)
        Me.LabelX17.TabIndex = 171
        Me.LabelX17.Text = "IURAN KOPERASI"
        '
        'txtBTnk
        '
        '
        '
        '
        Me.txtBTnk.Border.Class = "TextBoxBorder"
        Me.txtBTnk.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBTnk.Location = New System.Drawing.Point(705, 88)
        Me.txtBTnk.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBTnk.Name = "txtBTnk"
        Me.txtBTnk.Size = New System.Drawing.Size(204, 26)
        Me.txtBTnk.TabIndex = 170
        '
        'LableTnk
        '
        Me.LableTnk.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LableTnk.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LableTnk.ForeColor = System.Drawing.Color.Black
        Me.LableTnk.Location = New System.Drawing.Point(521, 86)
        Me.LableTnk.Margin = New System.Windows.Forms.Padding(4)
        Me.LableTnk.Name = "LableTnk"
        Me.LableTnk.Size = New System.Drawing.Size(145, 28)
        Me.LableTnk.TabIndex = 169
        Me.LableTnk.Text = "BPJS TENAGAKERJA"
        '
        'txtBKes
        '
        '
        '
        '
        Me.txtBKes.Border.Class = "TextBoxBorder"
        Me.txtBKes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBKes.Location = New System.Drawing.Point(705, 54)
        Me.txtBKes.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBKes.Name = "txtBKes"
        Me.txtBKes.Size = New System.Drawing.Size(204, 26)
        Me.txtBKes.TabIndex = 168
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(521, 52)
        Me.LabelX15.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(145, 28)
        Me.LabelX15.TabIndex = 167
        Me.LabelX15.Text = "BPJS KESEHATAN"
        '
        'txtTGaji
        '
        '
        '
        '
        Me.txtTGaji.Border.Class = "TextBoxBorder"
        Me.txtTGaji.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTGaji.Enabled = False
        Me.txtTGaji.Location = New System.Drawing.Point(190, 356)
        Me.txtTGaji.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTGaji.Name = "txtTGaji"
        Me.txtTGaji.Size = New System.Drawing.Size(204, 26)
        Me.txtTGaji.TabIndex = 166
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(37, 354)
        Me.LabelX14.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(145, 28)
        Me.LabelX14.TabIndex = 165
        Me.LabelX14.Text = "TOTAL GAJI"
        '
        'txtULembur
        '
        '
        '
        '
        Me.txtULembur.Border.Class = "TextBoxBorder"
        Me.txtULembur.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtULembur.Location = New System.Drawing.Point(190, 304)
        Me.txtULembur.Margin = New System.Windows.Forms.Padding(4)
        Me.txtULembur.Name = "txtULembur"
        Me.txtULembur.Size = New System.Drawing.Size(204, 26)
        Me.txtULembur.TabIndex = 164
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(37, 302)
        Me.LabelX13.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(145, 28)
        Me.LabelX13.TabIndex = 163
        Me.LabelX13.Text = "UANG LEMBUR"
        '
        'txtUTransport
        '
        '
        '
        '
        Me.txtUTransport.Border.Class = "TextBoxBorder"
        Me.txtUTransport.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUTransport.Location = New System.Drawing.Point(190, 268)
        Me.txtUTransport.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUTransport.Name = "txtUTransport"
        Me.txtUTransport.Size = New System.Drawing.Size(204, 26)
        Me.txtUTransport.TabIndex = 162
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(37, 266)
        Me.LabelX12.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(145, 28)
        Me.LabelX12.TabIndex = 161
        Me.LabelX12.Text = "UANG TRANSPORT"
        '
        'txtUMakan
        '
        '
        '
        '
        Me.txtUMakan.Border.Class = "TextBoxBorder"
        Me.txtUMakan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUMakan.Location = New System.Drawing.Point(190, 232)
        Me.txtUMakan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUMakan.Name = "txtUMakan"
        Me.txtUMakan.Size = New System.Drawing.Size(204, 26)
        Me.txtUMakan.TabIndex = 160
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(37, 230)
        Me.LabelX11.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(109, 28)
        Me.LabelX11.TabIndex = 159
        Me.LabelX11.Text = "UANG MAKAN"
        '
        'txtTService
        '
        '
        '
        '
        Me.txtTService.Border.Class = "TextBoxBorder"
        Me.txtTService.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTService.Location = New System.Drawing.Point(190, 196)
        Me.txtTService.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTService.Name = "txtTService"
        Me.txtTService.Size = New System.Drawing.Size(204, 26)
        Me.txtTService.TabIndex = 158
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(37, 194)
        Me.LabelX10.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(109, 28)
        Me.LabelX10.TabIndex = 157
        Me.LabelX10.Text = "T. SERVICE"
        '
        'txtTPulsa
        '
        '
        '
        '
        Me.txtTPulsa.Border.Class = "TextBoxBorder"
        Me.txtTPulsa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTPulsa.Location = New System.Drawing.Point(190, 160)
        Me.txtTPulsa.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTPulsa.Name = "txtTPulsa"
        Me.txtTPulsa.Size = New System.Drawing.Size(204, 26)
        Me.txtTPulsa.TabIndex = 156
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.ForeColor = System.Drawing.Color.Black
        Me.LabelX9.Location = New System.Drawing.Point(37, 158)
        Me.LabelX9.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(109, 28)
        Me.LabelX9.TabIndex = 155
        Me.LabelX9.Text = "T. PULSA"
        '
        'txtTKhusus
        '
        '
        '
        '
        Me.txtTKhusus.Border.Class = "TextBoxBorder"
        Me.txtTKhusus.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTKhusus.Location = New System.Drawing.Point(190, 124)
        Me.txtTKhusus.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTKhusus.Name = "txtTKhusus"
        Me.txtTKhusus.Size = New System.Drawing.Size(204, 26)
        Me.txtTKhusus.TabIndex = 154
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(37, 122)
        Me.LabelX8.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(109, 28)
        Me.LabelX8.TabIndex = 153
        Me.LabelX8.Text = "T. KHUSUS"
        '
        'txtTJabatan
        '
        '
        '
        '
        Me.txtTJabatan.Border.Class = "TextBoxBorder"
        Me.txtTJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTJabatan.Location = New System.Drawing.Point(190, 88)
        Me.txtTJabatan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTJabatan.Name = "txtTJabatan"
        Me.txtTJabatan.Size = New System.Drawing.Size(204, 26)
        Me.txtTJabatan.TabIndex = 152
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(37, 86)
        Me.LabelX7.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(109, 28)
        Me.LabelX7.TabIndex = 151
        Me.LabelX7.Text = "T. JABATAN"
        '
        'txtGapok
        '
        '
        '
        '
        Me.txtGapok.Border.Class = "TextBoxBorder"
        Me.txtGapok.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGapok.Location = New System.Drawing.Point(190, 54)
        Me.txtGapok.Margin = New System.Windows.Forms.Padding(4)
        Me.txtGapok.Name = "txtGapok"
        Me.txtGapok.Size = New System.Drawing.Size(204, 26)
        Me.txtGapok.TabIndex = 150
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(37, 52)
        Me.LabelX4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(109, 28)
        Me.LabelX4.TabIndex = 147
        Me.LabelX4.Text = "GAJI POKOK"
        '
        'txtDepartemen
        '
        '
        '
        '
        Me.txtDepartemen.Border.Class = "TextBoxBorder"
        Me.txtDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDepartemen.Enabled = False
        Me.txtDepartemen.Location = New System.Drawing.Point(231, 167)
        Me.txtDepartemen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDepartemen.Name = "txtDepartemen"
        Me.txtDepartemen.Size = New System.Drawing.Size(433, 26)
        Me.txtDepartemen.TabIndex = 149
        '
        'txtUnitBisnis
        '
        '
        '
        '
        Me.txtUnitBisnis.Border.Class = "TextBoxBorder"
        Me.txtUnitBisnis.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUnitBisnis.Enabled = False
        Me.txtUnitBisnis.Location = New System.Drawing.Point(231, 131)
        Me.txtUnitBisnis.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUnitBisnis.Name = "txtUnitBisnis"
        Me.txtUnitBisnis.Size = New System.Drawing.Size(433, 26)
        Me.txtUnitBisnis.TabIndex = 147
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(72, 201)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(109, 28)
        Me.LabelX2.TabIndex = 146
        Me.LabelX2.Text = "JABATAN"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(619, 59)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(45, 28)
        Me.Button1.TabIndex = 148
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.Enabled = False
        Me.txtKdKaryawan.Location = New System.Drawing.Point(231, 97)
        Me.txtKdKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(433, 26)
        Me.txtKdKaryawan.TabIndex = 147
        '
        'dtTanggal
        '
        '
        '
        '
        Me.dtTanggal.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtTanggal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtTanggal.ButtonDropDown.Visible = True
        Me.dtTanggal.IsPopupCalendarOpen = False
        Me.dtTanggal.Location = New System.Drawing.Point(231, 27)
        Me.dtTanggal.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.dtTanggal.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtTanggal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtTanggal.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtTanggal.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtTanggal.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.MonthCalendar.TodayButtonVisible = True
        Me.dtTanggal.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtTanggal.Name = "dtTanggal"
        Me.dtTanggal.Size = New System.Drawing.Size(188, 26)
        Me.dtTanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtTanggal.TabIndex = 133
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(71, 95)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(152, 28)
        Me.LabelX6.TabIndex = 146
        Me.LabelX6.Text = "KODE KARYAWAN"
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(72, 25)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(152, 28)
        Me.LabelX5.TabIndex = 35
        Me.LabelX5.Text = "TANGGAL"
        '
        'txtNmKaryawan
        '
        '
        '
        '
        Me.txtNmKaryawan.Border.Class = "TextBoxBorder"
        Me.txtNmKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmKaryawan.Enabled = False
        Me.txtNmKaryawan.Location = New System.Drawing.Point(231, 61)
        Me.txtNmKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNmKaryawan.Name = "txtNmKaryawan"
        Me.txtNmKaryawan.Size = New System.Drawing.Size(378, 26)
        Me.txtNmKaryawan.TabIndex = 145
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(71, 59)
        Me.LabelX1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(152, 28)
        Me.LabelX1.TabIndex = 144
        Me.LabelX1.Text = "NAMA KARYAWAN"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(72, 165)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(109, 28)
        Me.LabelX3.TabIndex = 12
        Me.LabelX3.Text = "DEPARTEMEN"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(918, 128)
        Me.BtnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(93, 89)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(814, 128)
        Me.BtnSave.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(93, 89)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "SAVE"
        '
        'frmInputPayroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1094, 781)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmInputPayroll"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInputPayroll"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dtTanggal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents txtDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUnitBisnis As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents dtTanggal As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNmKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTPotongan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtIKoperasi As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPKoperasi As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPotLain As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtOTS As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTerlambat As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPinjaman As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtBTnk As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LableTnk As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtBKes As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTGaji As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtULembur As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUTransport As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUMakan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTService As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTPulsa As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTKhusus As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtGapok As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtGrandTotal As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
End Class
