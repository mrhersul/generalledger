﻿Public Class frmKarirvb
    Dim Idnya As Integer
    Public Sub Pengaturan(ByVal Sid As Integer)
        Idnya = Sid
    End Sub
    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub DGViewPromosi_DoubleClick(sender As System.Object, e As System.EventArgs)
        frmDetailKarirvb.ShowDialog()
    End Sub

    Private Sub DGViewDegradasi_DoubleClick(sender As System.Object, e As System.EventArgs)
        frmDetailKarirvb.ShowDialog()
    End Sub

    Private Sub DGViewMutasi_DoubleClick(sender As System.Object, e As System.EventArgs)
        frmDetailKarirvb.ShowDialog()
    End Sub

    Sub ShowDataGridPromosi()

        SQL = ""
        SQL = "SP_HR_List_Mutasi " & Idnya & " "
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGViewMutasi.DataSource = dt


        DGViewMutasi.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGViewMutasi.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGViewMutasi.Columns(0).Width = 300
        DGViewMutasi.Columns(1).Width = 150
        DGViewMutasi.Columns(2).Width = 150
        DGViewMutasi.Columns(3).Width = 150
        DGViewMutasi.Columns(4).Width = 150
        DGViewMutasi.Columns(5).Width = 150

        DGViewMutasi.Columns(0).HeaderText = "KODE KARYAWAN"
        DGViewMutasi.Columns(1).HeaderText = "NAMA KARYAWAN"
        DGViewMutasi.Columns(2).HeaderText = "UNIT BISNIS"
        DGViewMutasi.Columns(3).HeaderText = "DEPARTEMEN"
        DGViewMutasi.Columns(4).HeaderText = "JABATAN"
        DGViewMutasi.Columns(5).HeaderText = "TIPE"

        ColorChange()

    End Sub
    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGViewMutasi.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next

    End Sub

    Private Sub frmKarirvb_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ShowDataGridPromosi()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        frmPindahKaryawan.ShowDialog()
        If frmPindahKaryawan.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGridPromosi()
        End If
    End Sub
End Class