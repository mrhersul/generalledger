﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInfoKaryawan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInfoKaryawan))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDate = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txtStatus = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtUnitBisnis = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtNama = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPersonal = New System.Windows.Forms.TabPage()
        Me.dtLahir = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtEmail2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.txtEmail = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.txtTelp2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.txtTelp1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.txtGolDarah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.txtSuku = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.txtWarga = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.txtAgama = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.txtTempatLahir = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.txtPanggilan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.txtNamaLengkap = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.txtKtp = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.TabAlamat = New System.Windows.Forms.TabPage()
        Me.txtKodepos = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX31 = New DevComponents.DotNetBar.LabelX()
        Me.txtKelurahan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX30 = New DevComponents.DotNetBar.LabelX()
        Me.txtKecamatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX29 = New DevComponents.DotNetBar.LabelX()
        Me.txtKota = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX28 = New DevComponents.DotNetBar.LabelX()
        Me.txtProvinsi = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.txtDetailLokasi = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.txtStatusRumah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.txAlamat = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.TabKeluarga = New System.Windows.Forms.TabPage()
        Me.dtMenikah = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtNamaIbu = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX37 = New DevComponents.DotNetBar.LabelX()
        Me.txtAnakKe = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX36 = New DevComponents.DotNetBar.LabelX()
        Me.txtJumlahTanggungan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX35 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX34 = New DevComponents.DotNetBar.LabelX()
        Me.txtStatusPerkawinan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX33 = New DevComponents.DotNetBar.LabelX()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.LabelX32 = New DevComponents.DotNetBar.LabelX()
        Me.TabPendidikan = New System.Windows.Forms.TabPage()
        Me.txAlamatSekolah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtJurusan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX50 = New DevComponents.DotNetBar.LabelX()
        Me.txtTahunIjazah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX45 = New DevComponents.DotNetBar.LabelX()
        Me.txtNamaSekolah = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX46 = New DevComponents.DotNetBar.LabelX()
        Me.txtTingkat = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX47 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX48 = New DevComponents.DotNetBar.LabelX()
        Me.txtStatusPendidikan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX49 = New DevComponents.DotNetBar.LabelX()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.LabelX44 = New DevComponents.DotNetBar.LabelX()
        Me.TabFile = New System.Windows.Forms.TabPage()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.LabelX60 = New DevComponents.DotNetBar.LabelX()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.LabelX59 = New DevComponents.DotNetBar.LabelX()
        Me.txtCC = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX57 = New DevComponents.DotNetBar.LabelX()
        Me.txtNpwp = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX58 = New DevComponents.DotNetBar.LabelX()
        Me.txtNamaBank = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtRekAn = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX53 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX54 = New DevComponents.DotNetBar.LabelX()
        Me.txtNoRek = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX55 = New DevComponents.DotNetBar.LabelX()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.LabelX51 = New DevComponents.DotNetBar.LabelX()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.LabelX52 = New DevComponents.DotNetBar.LabelX()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPersonal.SuspendLayout()
        CType(Me.dtLahir, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabAlamat.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TabKeluarga.SuspendLayout()
        CType(Me.dtMenikah, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.TabPendidikan.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TabFile.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.GroupBox1.Controls.Add(Me.txtDate)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.txtStatus)
        Me.GroupBox1.Controls.Add(Me.LabelX7)
        Me.GroupBox1.Controls.Add(Me.txtJabatan)
        Me.GroupBox1.Controls.Add(Me.LabelX6)
        Me.GroupBox1.Controls.Add(Me.txtDepartemen)
        Me.GroupBox1.Controls.Add(Me.LabelX5)
        Me.GroupBox1.Controls.Add(Me.txtUnitBisnis)
        Me.GroupBox1.Controls.Add(Me.LabelX4)
        Me.GroupBox1.Controls.Add(Me.LabelX3)
        Me.GroupBox1.Controls.Add(Me.txtKdKaryawan)
        Me.GroupBox1.Controls.Add(Me.LabelX1)
        Me.GroupBox1.Controls.Add(Me.txtNama)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(985, 328)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "INFORMASI KARYAWAN"
        '
        'txtDate
        '
        '
        '
        '
        Me.txtDate.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate.ButtonDropDown.Visible = True
        Me.txtDate.IsPopupCalendarOpen = False
        Me.txtDate.Location = New System.Drawing.Point(181, 128)
        Me.txtDate.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.txtDate.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.TodayButtonVisible = True
        Me.txtDate.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(188, 29)
        Me.txtDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate.TabIndex = 163
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(720, 38)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(206, 254)
        Me.PictureBox1.TabIndex = 162
        Me.PictureBox1.TabStop = False
        '
        'txtStatus
        '
        '
        '
        '
        Me.txtStatus.Border.Class = "TextBoxBorder"
        Me.txtStatus.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatus.Location = New System.Drawing.Point(181, 278)
        Me.txtStatus.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.Size = New System.Drawing.Size(191, 29)
        Me.txtStatus.TabIndex = 161
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(21, 273)
        Me.LabelX7.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(152, 28)
        Me.LabelX7.TabIndex = 160
        Me.LabelX7.Text = "STATUS"
        '
        'txtJabatan
        '
        '
        '
        '
        Me.txtJabatan.Border.Class = "TextBoxBorder"
        Me.txtJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJabatan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJabatan.Location = New System.Drawing.Point(182, 241)
        Me.txtJabatan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJabatan.Name = "txtJabatan"
        Me.txtJabatan.Size = New System.Drawing.Size(433, 29)
        Me.txtJabatan.TabIndex = 159
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(22, 237)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(152, 28)
        Me.LabelX6.TabIndex = 158
        Me.LabelX6.Text = "JABATAN"
        '
        'txtDepartemen
        '
        '
        '
        '
        Me.txtDepartemen.Border.Class = "TextBoxBorder"
        Me.txtDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDepartemen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartemen.Location = New System.Drawing.Point(181, 204)
        Me.txtDepartemen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDepartemen.Name = "txtDepartemen"
        Me.txtDepartemen.Size = New System.Drawing.Size(433, 29)
        Me.txtDepartemen.TabIndex = 157
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(22, 201)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(152, 28)
        Me.LabelX5.TabIndex = 156
        Me.LabelX5.Text = "DEPARTEMEN"
        '
        'txtUnitBisnis
        '
        '
        '
        '
        Me.txtUnitBisnis.Border.Class = "TextBoxBorder"
        Me.txtUnitBisnis.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUnitBisnis.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnitBisnis.Location = New System.Drawing.Point(181, 167)
        Me.txtUnitBisnis.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUnitBisnis.Name = "txtUnitBisnis"
        Me.txtUnitBisnis.Size = New System.Drawing.Size(433, 29)
        Me.txtUnitBisnis.TabIndex = 155
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(21, 165)
        Me.LabelX4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(152, 28)
        Me.LabelX4.TabIndex = 154
        Me.LabelX4.Text = "UNIT BISNIS"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(22, 129)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(152, 28)
        Me.LabelX3.TabIndex = 152
        Me.LabelX3.Text = "TGL GABUNG"
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKdKaryawan.Location = New System.Drawing.Point(181, 84)
        Me.txtKdKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(433, 29)
        Me.txtKdKaryawan.TabIndex = 151
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(21, 84)
        Me.LabelX1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(152, 28)
        Me.LabelX1.TabIndex = 150
        Me.LabelX1.Text = "KODE KARYAWAN"
        '
        'txtNama
        '
        '
        '
        '
        Me.txtNama.Border.Class = "TextBoxBorder"
        Me.txtNama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNama.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNama.Location = New System.Drawing.Point(181, 47)
        Me.txtNama.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(433, 29)
        Me.txtNama.TabIndex = 149
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(21, 47)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(152, 28)
        Me.LabelX2.TabIndex = 148
        Me.LabelX2.Text = "NAMA KARYAWAN"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(1003, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(405, 328)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ACTION"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Red
        Me.Button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.Location = New System.Drawing.Point(25, 178)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(354, 58)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "RESIGN"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(25, 106)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(354, 58)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "KARIR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button1.Location = New System.Drawing.Point(25, 35)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(354, 58)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "CATATAN KARYAWAN"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPersonal)
        Me.TabControl1.Controls.Add(Me.TabAlamat)
        Me.TabControl1.Controls.Add(Me.TabKeluarga)
        Me.TabControl1.Controls.Add(Me.TabPendidikan)
        Me.TabControl1.Controls.Add(Me.TabFile)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 357)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1393, 431)
        Me.TabControl1.TabIndex = 3
        '
        'TabPersonal
        '
        Me.TabPersonal.BackColor = System.Drawing.Color.White
        Me.TabPersonal.Controls.Add(Me.dtLahir)
        Me.TabPersonal.Controls.Add(Me.txtEmail2)
        Me.TabPersonal.Controls.Add(Me.LabelX22)
        Me.TabPersonal.Controls.Add(Me.txtEmail)
        Me.TabPersonal.Controls.Add(Me.LabelX21)
        Me.TabPersonal.Controls.Add(Me.txtTelp2)
        Me.TabPersonal.Controls.Add(Me.LabelX20)
        Me.TabPersonal.Controls.Add(Me.txtTelp1)
        Me.TabPersonal.Controls.Add(Me.LabelX19)
        Me.TabPersonal.Controls.Add(Me.txtGolDarah)
        Me.TabPersonal.Controls.Add(Me.LabelX18)
        Me.TabPersonal.Controls.Add(Me.txtSuku)
        Me.TabPersonal.Controls.Add(Me.LabelX17)
        Me.TabPersonal.Controls.Add(Me.txtWarga)
        Me.TabPersonal.Controls.Add(Me.LabelX16)
        Me.TabPersonal.Controls.Add(Me.txtAgama)
        Me.TabPersonal.Controls.Add(Me.LabelX15)
        Me.TabPersonal.Controls.Add(Me.LabelX14)
        Me.TabPersonal.Controls.Add(Me.txtTempatLahir)
        Me.TabPersonal.Controls.Add(Me.LabelX13)
        Me.TabPersonal.Controls.Add(Me.txtPanggilan)
        Me.TabPersonal.Controls.Add(Me.LabelX12)
        Me.TabPersonal.Controls.Add(Me.txtNamaLengkap)
        Me.TabPersonal.Controls.Add(Me.LabelX11)
        Me.TabPersonal.Controls.Add(Me.Panel2)
        Me.TabPersonal.Controls.Add(Me.Panel1)
        Me.TabPersonal.Controls.Add(Me.txtKtp)
        Me.TabPersonal.Controls.Add(Me.LabelX8)
        Me.TabPersonal.Location = New System.Drawing.Point(4, 30)
        Me.TabPersonal.Name = "TabPersonal"
        Me.TabPersonal.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPersonal.Size = New System.Drawing.Size(1385, 397)
        Me.TabPersonal.TabIndex = 0
        Me.TabPersonal.Text = "PERSONAL"
        '
        'dtLahir
        '
        '
        '
        '
        Me.dtLahir.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtLahir.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtLahir.ButtonDropDown.Visible = True
        Me.dtLahir.IsPopupCalendarOpen = False
        Me.dtLahir.Location = New System.Drawing.Point(221, 213)
        Me.dtLahir.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.dtLahir.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtLahir.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtLahir.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtLahir.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtLahir.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtLahir.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtLahir.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtLahir.MonthCalendar.TodayButtonVisible = True
        Me.dtLahir.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtLahir.Name = "dtLahir"
        Me.dtLahir.Size = New System.Drawing.Size(188, 28)
        Me.dtLahir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtLahir.TabIndex = 191
        '
        'txtEmail2
        '
        '
        '
        '
        Me.txtEmail2.Border.Class = "TextBoxBorder"
        Me.txtEmail2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEmail2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmail2.Location = New System.Drawing.Point(876, 179)
        Me.txtEmail2.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEmail2.Name = "txtEmail2"
        Me.txtEmail2.Size = New System.Drawing.Size(433, 28)
        Me.txtEmail2.TabIndex = 190
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(716, 179)
        Me.LabelX22.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(152, 28)
        Me.LabelX22.TabIndex = 189
        Me.LabelX22.Text = "EMAIL 2"
        '
        'txtEmail
        '
        '
        '
        '
        Me.txtEmail.Border.Class = "TextBoxBorder"
        Me.txtEmail.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmail.Location = New System.Drawing.Point(876, 143)
        Me.txtEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(433, 28)
        Me.txtEmail.TabIndex = 188
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX21.ForeColor = System.Drawing.Color.Black
        Me.LabelX21.Location = New System.Drawing.Point(716, 143)
        Me.LabelX21.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(152, 28)
        Me.LabelX21.TabIndex = 187
        Me.LabelX21.Text = "EMAIL"
        '
        'txtTelp2
        '
        '
        '
        '
        Me.txtTelp2.Border.Class = "TextBoxBorder"
        Me.txtTelp2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTelp2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelp2.Location = New System.Drawing.Point(876, 107)
        Me.txtTelp2.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTelp2.Name = "txtTelp2"
        Me.txtTelp2.Size = New System.Drawing.Size(433, 28)
        Me.txtTelp2.TabIndex = 186
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(716, 107)
        Me.LabelX20.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(152, 28)
        Me.LabelX20.TabIndex = 185
        Me.LabelX20.Text = "NOMOR TELEPON 2"
        '
        'txtTelp1
        '
        '
        '
        '
        Me.txtTelp1.Border.Class = "TextBoxBorder"
        Me.txtTelp1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTelp1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelp1.Location = New System.Drawing.Point(876, 69)
        Me.txtTelp1.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTelp1.Name = "txtTelp1"
        Me.txtTelp1.Size = New System.Drawing.Size(433, 28)
        Me.txtTelp1.TabIndex = 184
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(716, 69)
        Me.LabelX19.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(152, 28)
        Me.LabelX19.TabIndex = 183
        Me.LabelX19.Text = "NOMOR TELEPON"
        '
        'txtGolDarah
        '
        '
        '
        '
        Me.txtGolDarah.Border.Class = "TextBoxBorder"
        Me.txtGolDarah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGolDarah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGolDarah.Location = New System.Drawing.Point(221, 357)
        Me.txtGolDarah.Margin = New System.Windows.Forms.Padding(4)
        Me.txtGolDarah.Name = "txtGolDarah"
        Me.txtGolDarah.Size = New System.Drawing.Size(433, 28)
        Me.txtGolDarah.TabIndex = 182
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(43, 357)
        Me.LabelX18.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(152, 28)
        Me.LabelX18.TabIndex = 181
        Me.LabelX18.Text = "GOLONGAN DARAH"
        '
        'txtSuku
        '
        '
        '
        '
        Me.txtSuku.Border.Class = "TextBoxBorder"
        Me.txtSuku.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSuku.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSuku.Location = New System.Drawing.Point(221, 321)
        Me.txtSuku.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSuku.Name = "txtSuku"
        Me.txtSuku.Size = New System.Drawing.Size(433, 28)
        Me.txtSuku.TabIndex = 180
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(43, 321)
        Me.LabelX17.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(170, 28)
        Me.LabelX17.TabIndex = 179
        Me.LabelX17.Text = "DAERAH ASAL / SUKU"
        '
        'txtWarga
        '
        '
        '
        '
        Me.txtWarga.Border.Class = "TextBoxBorder"
        Me.txtWarga.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWarga.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtWarga.Location = New System.Drawing.Point(221, 285)
        Me.txtWarga.Margin = New System.Windows.Forms.Padding(4)
        Me.txtWarga.Name = "txtWarga"
        Me.txtWarga.Size = New System.Drawing.Size(433, 28)
        Me.txtWarga.TabIndex = 178
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(43, 285)
        Me.LabelX16.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(170, 28)
        Me.LabelX16.TabIndex = 177
        Me.LabelX16.Text = "KEWARGANEGARAAN"
        '
        'txtAgama
        '
        '
        '
        '
        Me.txtAgama.Border.Class = "TextBoxBorder"
        Me.txtAgama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAgama.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAgama.Location = New System.Drawing.Point(221, 249)
        Me.txtAgama.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAgama.Name = "txtAgama"
        Me.txtAgama.Size = New System.Drawing.Size(433, 28)
        Me.txtAgama.TabIndex = 176
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(43, 249)
        Me.LabelX15.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(152, 28)
        Me.LabelX15.TabIndex = 175
        Me.LabelX15.Text = "AGAMA"
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(43, 213)
        Me.LabelX14.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(152, 28)
        Me.LabelX14.TabIndex = 173
        Me.LabelX14.Text = "TANGGAL LAHIR"
        '
        'txtTempatLahir
        '
        '
        '
        '
        Me.txtTempatLahir.Border.Class = "TextBoxBorder"
        Me.txtTempatLahir.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTempatLahir.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTempatLahir.Location = New System.Drawing.Point(221, 177)
        Me.txtTempatLahir.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTempatLahir.Name = "txtTempatLahir"
        Me.txtTempatLahir.Size = New System.Drawing.Size(433, 28)
        Me.txtTempatLahir.TabIndex = 172
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(43, 177)
        Me.LabelX13.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(152, 28)
        Me.LabelX13.TabIndex = 171
        Me.LabelX13.Text = "TEMPAT LAHIR"
        '
        'txtPanggilan
        '
        '
        '
        '
        Me.txtPanggilan.Border.Class = "TextBoxBorder"
        Me.txtPanggilan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPanggilan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPanggilan.Location = New System.Drawing.Point(221, 141)
        Me.txtPanggilan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPanggilan.Name = "txtPanggilan"
        Me.txtPanggilan.Size = New System.Drawing.Size(433, 28)
        Me.txtPanggilan.TabIndex = 170
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(43, 141)
        Me.LabelX12.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(152, 28)
        Me.LabelX12.TabIndex = 169
        Me.LabelX12.Text = "NAMA PANGGILAN"
        '
        'txtNamaLengkap
        '
        '
        '
        '
        Me.txtNamaLengkap.Border.Class = "TextBoxBorder"
        Me.txtNamaLengkap.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaLengkap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaLengkap.Location = New System.Drawing.Point(221, 105)
        Me.txtNamaLengkap.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNamaLengkap.Name = "txtNamaLengkap"
        Me.txtNamaLengkap.Size = New System.Drawing.Size(433, 28)
        Me.txtNamaLengkap.TabIndex = 168
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(43, 105)
        Me.LabelX11.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(152, 28)
        Me.LabelX11.TabIndex = 167
        Me.LabelX11.Text = "NAMA LENGKAP"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Blue
        Me.Panel2.Controls.Add(Me.LabelX10)
        Me.Panel2.Location = New System.Drawing.Point(692, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(668, 51)
        Me.Panel2.TabIndex = 166
        '
        'LabelX10
        '
        Me.LabelX10.AutoSize = True
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX10.ForeColor = System.Drawing.Color.White
        Me.LabelX10.Location = New System.Drawing.Point(221, 6)
        Me.LabelX10.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(265, 41)
        Me.LabelX10.TabIndex = 168
        Me.LabelX10.Text = "TELEPON DAN EMAIL"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.Controls.Add(Me.LabelX9)
        Me.Panel1.Location = New System.Drawing.Point(18, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(668, 51)
        Me.Panel1.TabIndex = 165
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX9.ForeColor = System.Drawing.Color.White
        Me.LabelX9.Location = New System.Drawing.Point(250, 8)
        Me.LabelX9.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(152, 28)
        Me.LabelX9.TabIndex = 167
        Me.LabelX9.Text = "PERSONAL"
        '
        'txtKtp
        '
        '
        '
        '
        Me.txtKtp.Border.Class = "TextBoxBorder"
        Me.txtKtp.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKtp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKtp.Location = New System.Drawing.Point(221, 69)
        Me.txtKtp.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKtp.Name = "txtKtp"
        Me.txtKtp.Size = New System.Drawing.Size(433, 28)
        Me.txtKtp.TabIndex = 164
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(43, 69)
        Me.LabelX8.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(152, 28)
        Me.LabelX8.TabIndex = 163
        Me.LabelX8.Text = "NO KTP"
        '
        'TabAlamat
        '
        Me.TabAlamat.Controls.Add(Me.txtKodepos)
        Me.TabAlamat.Controls.Add(Me.LabelX31)
        Me.TabAlamat.Controls.Add(Me.txtKelurahan)
        Me.TabAlamat.Controls.Add(Me.LabelX30)
        Me.TabAlamat.Controls.Add(Me.txtKecamatan)
        Me.TabAlamat.Controls.Add(Me.LabelX29)
        Me.TabAlamat.Controls.Add(Me.txtKota)
        Me.TabAlamat.Controls.Add(Me.LabelX28)
        Me.TabAlamat.Controls.Add(Me.txtProvinsi)
        Me.TabAlamat.Controls.Add(Me.LabelX27)
        Me.TabAlamat.Controls.Add(Me.txtDetailLokasi)
        Me.TabAlamat.Controls.Add(Me.LabelX26)
        Me.TabAlamat.Controls.Add(Me.txtStatusRumah)
        Me.TabAlamat.Controls.Add(Me.LabelX25)
        Me.TabAlamat.Controls.Add(Me.txAlamat)
        Me.TabAlamat.Controls.Add(Me.LabelX24)
        Me.TabAlamat.Controls.Add(Me.Panel3)
        Me.TabAlamat.Location = New System.Drawing.Point(4, 30)
        Me.TabAlamat.Name = "TabAlamat"
        Me.TabAlamat.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAlamat.Size = New System.Drawing.Size(1385, 397)
        Me.TabAlamat.TabIndex = 1
        Me.TabAlamat.Text = "ALAMAT"
        Me.TabAlamat.UseVisualStyleBackColor = True
        '
        'txtKodepos
        '
        '
        '
        '
        Me.txtKodepos.Border.Class = "TextBoxBorder"
        Me.txtKodepos.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKodepos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKodepos.Location = New System.Drawing.Point(882, 244)
        Me.txtKodepos.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKodepos.Name = "txtKodepos"
        Me.txtKodepos.Size = New System.Drawing.Size(433, 28)
        Me.txtKodepos.TabIndex = 182
        '
        'LabelX31
        '
        Me.LabelX31.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX31.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX31.ForeColor = System.Drawing.Color.Black
        Me.LabelX31.Location = New System.Drawing.Point(704, 244)
        Me.LabelX31.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX31.Name = "LabelX31"
        Me.LabelX31.Size = New System.Drawing.Size(152, 28)
        Me.LabelX31.TabIndex = 181
        Me.LabelX31.Text = "KODE POS"
        '
        'txtKelurahan
        '
        '
        '
        '
        Me.txtKelurahan.Border.Class = "TextBoxBorder"
        Me.txtKelurahan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKelurahan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKelurahan.Location = New System.Drawing.Point(882, 208)
        Me.txtKelurahan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKelurahan.Name = "txtKelurahan"
        Me.txtKelurahan.Size = New System.Drawing.Size(433, 28)
        Me.txtKelurahan.TabIndex = 180
        '
        'LabelX30
        '
        Me.LabelX30.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX30.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX30.ForeColor = System.Drawing.Color.Black
        Me.LabelX30.Location = New System.Drawing.Point(704, 208)
        Me.LabelX30.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX30.Name = "LabelX30"
        Me.LabelX30.Size = New System.Drawing.Size(152, 28)
        Me.LabelX30.TabIndex = 179
        Me.LabelX30.Text = "KELURAHAN"
        '
        'txtKecamatan
        '
        '
        '
        '
        Me.txtKecamatan.Border.Class = "TextBoxBorder"
        Me.txtKecamatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKecamatan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKecamatan.Location = New System.Drawing.Point(884, 172)
        Me.txtKecamatan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKecamatan.Name = "txtKecamatan"
        Me.txtKecamatan.Size = New System.Drawing.Size(433, 28)
        Me.txtKecamatan.TabIndex = 178
        '
        'LabelX29
        '
        Me.LabelX29.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX29.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX29.ForeColor = System.Drawing.Color.Black
        Me.LabelX29.Location = New System.Drawing.Point(706, 172)
        Me.LabelX29.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX29.Name = "LabelX29"
        Me.LabelX29.Size = New System.Drawing.Size(152, 28)
        Me.LabelX29.TabIndex = 177
        Me.LabelX29.Text = "KECAMATAN"
        '
        'txtKota
        '
        '
        '
        '
        Me.txtKota.Border.Class = "TextBoxBorder"
        Me.txtKota.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKota.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKota.Location = New System.Drawing.Point(884, 136)
        Me.txtKota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKota.Name = "txtKota"
        Me.txtKota.Size = New System.Drawing.Size(433, 28)
        Me.txtKota.TabIndex = 176
        '
        'LabelX28
        '
        Me.LabelX28.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX28.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX28.ForeColor = System.Drawing.Color.Black
        Me.LabelX28.Location = New System.Drawing.Point(706, 136)
        Me.LabelX28.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX28.Name = "LabelX28"
        Me.LabelX28.Size = New System.Drawing.Size(152, 28)
        Me.LabelX28.TabIndex = 175
        Me.LabelX28.Text = "KOTA / KABUPATEN"
        '
        'txtProvinsi
        '
        '
        '
        '
        Me.txtProvinsi.Border.Class = "TextBoxBorder"
        Me.txtProvinsi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtProvinsi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProvinsi.Location = New System.Drawing.Point(882, 95)
        Me.txtProvinsi.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProvinsi.Name = "txtProvinsi"
        Me.txtProvinsi.Size = New System.Drawing.Size(433, 28)
        Me.txtProvinsi.TabIndex = 174
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(704, 95)
        Me.LabelX27.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(152, 28)
        Me.LabelX27.TabIndex = 173
        Me.LabelX27.Text = "PROVINSI"
        '
        'txtDetailLokasi
        '
        '
        '
        '
        Me.txtDetailLokasi.Border.Class = "TextBoxBorder"
        Me.txtDetailLokasi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDetailLokasi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetailLokasi.Location = New System.Drawing.Point(224, 244)
        Me.txtDetailLokasi.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDetailLokasi.Name = "txtDetailLokasi"
        Me.txtDetailLokasi.Size = New System.Drawing.Size(433, 28)
        Me.txtDetailLokasi.TabIndex = 172
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(46, 244)
        Me.LabelX26.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(152, 28)
        Me.LabelX26.TabIndex = 171
        Me.LabelX26.Text = "DETAIL LOKASI"
        '
        'txtStatusRumah
        '
        '
        '
        '
        Me.txtStatusRumah.Border.Class = "TextBoxBorder"
        Me.txtStatusRumah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatusRumah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatusRumah.Location = New System.Drawing.Point(226, 208)
        Me.txtStatusRumah.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStatusRumah.Name = "txtStatusRumah"
        Me.txtStatusRumah.Size = New System.Drawing.Size(433, 28)
        Me.txtStatusRumah.TabIndex = 170
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(48, 208)
        Me.LabelX25.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(152, 28)
        Me.LabelX25.TabIndex = 169
        Me.LabelX25.Text = "STATUS RUMAH"
        '
        'txAlamat
        '
        '
        '
        '
        Me.txAlamat.Border.Class = "TextBoxBorder"
        Me.txAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txAlamat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txAlamat.Location = New System.Drawing.Point(224, 95)
        Me.txAlamat.Margin = New System.Windows.Forms.Padding(4)
        Me.txAlamat.Multiline = True
        Me.txAlamat.Name = "txAlamat"
        Me.txAlamat.Size = New System.Drawing.Size(435, 105)
        Me.txAlamat.TabIndex = 168
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(48, 95)
        Me.LabelX24.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(152, 28)
        Me.LabelX24.TabIndex = 167
        Me.LabelX24.Text = "ALAMAT RUMAH"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Red
        Me.Panel3.Controls.Add(Me.LabelX23)
        Me.Panel3.Location = New System.Drawing.Point(17, 6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1351, 51)
        Me.Panel3.TabIndex = 166
        '
        'LabelX23
        '
        Me.LabelX23.AutoSize = True
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.White
        Me.LabelX23.Location = New System.Drawing.Point(653, 4)
        Me.LabelX23.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(111, 41)
        Me.LabelX23.TabIndex = 167
        Me.LabelX23.Text = "ALAMAT"
        '
        'TabKeluarga
        '
        Me.TabKeluarga.Controls.Add(Me.dtMenikah)
        Me.TabKeluarga.Controls.Add(Me.txtNamaIbu)
        Me.TabKeluarga.Controls.Add(Me.LabelX37)
        Me.TabKeluarga.Controls.Add(Me.txtAnakKe)
        Me.TabKeluarga.Controls.Add(Me.LabelX36)
        Me.TabKeluarga.Controls.Add(Me.txtJumlahTanggungan)
        Me.TabKeluarga.Controls.Add(Me.LabelX35)
        Me.TabKeluarga.Controls.Add(Me.LabelX34)
        Me.TabKeluarga.Controls.Add(Me.txtStatusPerkawinan)
        Me.TabKeluarga.Controls.Add(Me.LabelX33)
        Me.TabKeluarga.Controls.Add(Me.Panel4)
        Me.TabKeluarga.Location = New System.Drawing.Point(4, 30)
        Me.TabKeluarga.Name = "TabKeluarga"
        Me.TabKeluarga.Size = New System.Drawing.Size(1385, 397)
        Me.TabKeluarga.TabIndex = 2
        Me.TabKeluarga.Text = "KELUARGA"
        Me.TabKeluarga.UseVisualStyleBackColor = True
        '
        'dtMenikah
        '
        '
        '
        '
        Me.dtMenikah.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtMenikah.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtMenikah.ButtonDropDown.Visible = True
        Me.dtMenikah.IsPopupCalendarOpen = False
        Me.dtMenikah.Location = New System.Drawing.Point(243, 120)
        Me.dtMenikah.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.dtMenikah.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtMenikah.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtMenikah.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtMenikah.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtMenikah.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtMenikah.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtMenikah.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtMenikah.MonthCalendar.TodayButtonVisible = True
        Me.dtMenikah.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtMenikah.Name = "dtMenikah"
        Me.dtMenikah.Size = New System.Drawing.Size(188, 28)
        Me.dtMenikah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtMenikah.TabIndex = 185
        '
        'txtNamaIbu
        '
        '
        '
        '
        Me.txtNamaIbu.Border.Class = "TextBoxBorder"
        Me.txtNamaIbu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaIbu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaIbu.Location = New System.Drawing.Point(243, 228)
        Me.txtNamaIbu.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNamaIbu.Name = "txtNamaIbu"
        Me.txtNamaIbu.Size = New System.Drawing.Size(433, 28)
        Me.txtNamaIbu.TabIndex = 184
        '
        'LabelX37
        '
        Me.LabelX37.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX37.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX37.ForeColor = System.Drawing.Color.Black
        Me.LabelX37.Location = New System.Drawing.Point(53, 226)
        Me.LabelX37.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX37.Name = "LabelX37"
        Me.LabelX37.Size = New System.Drawing.Size(170, 28)
        Me.LabelX37.TabIndex = 183
        Me.LabelX37.Text = "NAMA IBU KANDUNG"
        '
        'txtAnakKe
        '
        '
        '
        '
        Me.txtAnakKe.Border.Class = "TextBoxBorder"
        Me.txtAnakKe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAnakKe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAnakKe.Location = New System.Drawing.Point(243, 192)
        Me.txtAnakKe.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAnakKe.Name = "txtAnakKe"
        Me.txtAnakKe.Size = New System.Drawing.Size(433, 28)
        Me.txtAnakKe.TabIndex = 182
        '
        'LabelX36
        '
        Me.LabelX36.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX36.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX36.ForeColor = System.Drawing.Color.Black
        Me.LabelX36.Location = New System.Drawing.Point(53, 190)
        Me.LabelX36.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX36.Name = "LabelX36"
        Me.LabelX36.Size = New System.Drawing.Size(170, 28)
        Me.LabelX36.TabIndex = 181
        Me.LabelX36.Text = "ANAK NOMOR / KE"
        '
        'txtJumlahTanggungan
        '
        '
        '
        '
        Me.txtJumlahTanggungan.Border.Class = "TextBoxBorder"
        Me.txtJumlahTanggungan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJumlahTanggungan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJumlahTanggungan.Location = New System.Drawing.Point(243, 156)
        Me.txtJumlahTanggungan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJumlahTanggungan.Name = "txtJumlahTanggungan"
        Me.txtJumlahTanggungan.Size = New System.Drawing.Size(433, 28)
        Me.txtJumlahTanggungan.TabIndex = 180
        '
        'LabelX35
        '
        Me.LabelX35.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX35.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX35.ForeColor = System.Drawing.Color.Black
        Me.LabelX35.Location = New System.Drawing.Point(53, 154)
        Me.LabelX35.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX35.Name = "LabelX35"
        Me.LabelX35.Size = New System.Drawing.Size(170, 28)
        Me.LabelX35.TabIndex = 179
        Me.LabelX35.Text = "JUMLAH TANGGUNGAN"
        '
        'LabelX34
        '
        Me.LabelX34.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX34.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX34.ForeColor = System.Drawing.Color.Black
        Me.LabelX34.Location = New System.Drawing.Point(53, 118)
        Me.LabelX34.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX34.Name = "LabelX34"
        Me.LabelX34.Size = New System.Drawing.Size(170, 28)
        Me.LabelX34.TabIndex = 177
        Me.LabelX34.Text = "TANGGAL MENIKAH"
        '
        'txtStatusPerkawinan
        '
        '
        '
        '
        Me.txtStatusPerkawinan.Border.Class = "TextBoxBorder"
        Me.txtStatusPerkawinan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatusPerkawinan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatusPerkawinan.Location = New System.Drawing.Point(243, 84)
        Me.txtStatusPerkawinan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStatusPerkawinan.Name = "txtStatusPerkawinan"
        Me.txtStatusPerkawinan.Size = New System.Drawing.Size(433, 28)
        Me.txtStatusPerkawinan.TabIndex = 176
        '
        'LabelX33
        '
        Me.LabelX33.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX33.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX33.ForeColor = System.Drawing.Color.Black
        Me.LabelX33.Location = New System.Drawing.Point(53, 82)
        Me.LabelX33.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX33.Name = "LabelX33"
        Me.LabelX33.Size = New System.Drawing.Size(170, 28)
        Me.LabelX33.TabIndex = 175
        Me.LabelX33.Text = "STATUS PERKAWINAN"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Red
        Me.Panel4.Controls.Add(Me.LabelX32)
        Me.Panel4.Location = New System.Drawing.Point(17, 6)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1351, 51)
        Me.Panel4.TabIndex = 167
        '
        'LabelX32
        '
        Me.LabelX32.AutoSize = True
        Me.LabelX32.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX32.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX32.ForeColor = System.Drawing.Color.White
        Me.LabelX32.Location = New System.Drawing.Point(653, 3)
        Me.LabelX32.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX32.Name = "LabelX32"
        Me.LabelX32.Size = New System.Drawing.Size(138, 41)
        Me.LabelX32.TabIndex = 167
        Me.LabelX32.Text = "KELUARGA"
        '
        'TabPendidikan
        '
        Me.TabPendidikan.Controls.Add(Me.txAlamatSekolah)
        Me.TabPendidikan.Controls.Add(Me.txtJurusan)
        Me.TabPendidikan.Controls.Add(Me.LabelX50)
        Me.TabPendidikan.Controls.Add(Me.txtTahunIjazah)
        Me.TabPendidikan.Controls.Add(Me.LabelX45)
        Me.TabPendidikan.Controls.Add(Me.txtNamaSekolah)
        Me.TabPendidikan.Controls.Add(Me.LabelX46)
        Me.TabPendidikan.Controls.Add(Me.txtTingkat)
        Me.TabPendidikan.Controls.Add(Me.LabelX47)
        Me.TabPendidikan.Controls.Add(Me.LabelX48)
        Me.TabPendidikan.Controls.Add(Me.txtStatusPendidikan)
        Me.TabPendidikan.Controls.Add(Me.LabelX49)
        Me.TabPendidikan.Controls.Add(Me.Panel6)
        Me.TabPendidikan.Location = New System.Drawing.Point(4, 30)
        Me.TabPendidikan.Name = "TabPendidikan"
        Me.TabPendidikan.Size = New System.Drawing.Size(1385, 397)
        Me.TabPendidikan.TabIndex = 4
        Me.TabPendidikan.Text = "PENDIDIKAN"
        Me.TabPendidikan.UseVisualStyleBackColor = True
        '
        'txAlamatSekolah
        '
        '
        '
        '
        Me.txAlamatSekolah.Border.Class = "TextBoxBorder"
        Me.txAlamatSekolah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txAlamatSekolah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txAlamatSekolah.Location = New System.Drawing.Point(244, 261)
        Me.txAlamatSekolah.Margin = New System.Windows.Forms.Padding(4)
        Me.txAlamatSekolah.Multiline = True
        Me.txAlamatSekolah.Name = "txAlamatSekolah"
        Me.txAlamatSekolah.Size = New System.Drawing.Size(433, 95)
        Me.txAlamatSekolah.TabIndex = 197
        '
        'txtJurusan
        '
        '
        '
        '
        Me.txtJurusan.Border.Class = "TextBoxBorder"
        Me.txtJurusan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJurusan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJurusan.Location = New System.Drawing.Point(244, 225)
        Me.txtJurusan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJurusan.Name = "txtJurusan"
        Me.txtJurusan.Size = New System.Drawing.Size(433, 28)
        Me.txtJurusan.TabIndex = 196
        '
        'LabelX50
        '
        Me.LabelX50.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX50.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX50.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX50.ForeColor = System.Drawing.Color.Black
        Me.LabelX50.Location = New System.Drawing.Point(54, 223)
        Me.LabelX50.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX50.Name = "LabelX50"
        Me.LabelX50.Size = New System.Drawing.Size(170, 28)
        Me.LabelX50.TabIndex = 195
        Me.LabelX50.Text = "JURUSAN STUDI"
        '
        'txtTahunIjazah
        '
        '
        '
        '
        Me.txtTahunIjazah.Border.Class = "TextBoxBorder"
        Me.txtTahunIjazah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTahunIjazah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTahunIjazah.Location = New System.Drawing.Point(244, 187)
        Me.txtTahunIjazah.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTahunIjazah.Name = "txtTahunIjazah"
        Me.txtTahunIjazah.Size = New System.Drawing.Size(433, 28)
        Me.txtTahunIjazah.TabIndex = 194
        '
        'LabelX45
        '
        Me.LabelX45.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX45.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX45.ForeColor = System.Drawing.Color.Black
        Me.LabelX45.Location = New System.Drawing.Point(54, 187)
        Me.LabelX45.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX45.Name = "LabelX45"
        Me.LabelX45.Size = New System.Drawing.Size(170, 28)
        Me.LabelX45.TabIndex = 193
        Me.LabelX45.Text = "TAHUN IJAZAH"
        '
        'txtNamaSekolah
        '
        '
        '
        '
        Me.txtNamaSekolah.Border.Class = "TextBoxBorder"
        Me.txtNamaSekolah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaSekolah.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaSekolah.Location = New System.Drawing.Point(244, 151)
        Me.txtNamaSekolah.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNamaSekolah.Name = "txtNamaSekolah"
        Me.txtNamaSekolah.Size = New System.Drawing.Size(433, 28)
        Me.txtNamaSekolah.TabIndex = 192
        '
        'LabelX46
        '
        Me.LabelX46.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX46.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX46.ForeColor = System.Drawing.Color.Black
        Me.LabelX46.Location = New System.Drawing.Point(54, 259)
        Me.LabelX46.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX46.Name = "LabelX46"
        Me.LabelX46.Size = New System.Drawing.Size(170, 28)
        Me.LabelX46.TabIndex = 191
        Me.LabelX46.Text = "ALAMAT SEKOLAH"
        '
        'txtTingkat
        '
        '
        '
        '
        Me.txtTingkat.Border.Class = "TextBoxBorder"
        Me.txtTingkat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTingkat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTingkat.Location = New System.Drawing.Point(244, 115)
        Me.txtTingkat.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTingkat.Name = "txtTingkat"
        Me.txtTingkat.Size = New System.Drawing.Size(433, 28)
        Me.txtTingkat.TabIndex = 190
        '
        'LabelX47
        '
        Me.LabelX47.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX47.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX47.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX47.ForeColor = System.Drawing.Color.Black
        Me.LabelX47.Location = New System.Drawing.Point(54, 149)
        Me.LabelX47.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX47.Name = "LabelX47"
        Me.LabelX47.Size = New System.Drawing.Size(170, 28)
        Me.LabelX47.TabIndex = 189
        Me.LabelX47.Text = "NAMA SEKOLAH"
        '
        'LabelX48
        '
        Me.LabelX48.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX48.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX48.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX48.ForeColor = System.Drawing.Color.Black
        Me.LabelX48.Location = New System.Drawing.Point(54, 113)
        Me.LabelX48.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX48.Name = "LabelX48"
        Me.LabelX48.Size = New System.Drawing.Size(170, 28)
        Me.LabelX48.TabIndex = 188
        Me.LabelX48.Text = "TINGKAT PENDIDIKAN"
        '
        'txtStatusPendidikan
        '
        '
        '
        '
        Me.txtStatusPendidikan.Border.Class = "TextBoxBorder"
        Me.txtStatusPendidikan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStatusPendidikan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStatusPendidikan.Location = New System.Drawing.Point(244, 79)
        Me.txtStatusPendidikan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStatusPendidikan.Name = "txtStatusPendidikan"
        Me.txtStatusPendidikan.Size = New System.Drawing.Size(433, 28)
        Me.txtStatusPendidikan.TabIndex = 187
        '
        'LabelX49
        '
        Me.LabelX49.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX49.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX49.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX49.ForeColor = System.Drawing.Color.Black
        Me.LabelX49.Location = New System.Drawing.Point(54, 77)
        Me.LabelX49.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX49.Name = "LabelX49"
        Me.LabelX49.Size = New System.Drawing.Size(170, 28)
        Me.LabelX49.TabIndex = 186
        Me.LabelX49.Text = "STATUS PENDIDIKAN"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Red
        Me.Panel6.Controls.Add(Me.LabelX44)
        Me.Panel6.Location = New System.Drawing.Point(17, 6)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1351, 51)
        Me.Panel6.TabIndex = 169
        '
        'LabelX44
        '
        Me.LabelX44.AutoSize = True
        Me.LabelX44.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX44.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX44.ForeColor = System.Drawing.Color.White
        Me.LabelX44.Location = New System.Drawing.Point(653, 2)
        Me.LabelX44.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX44.Name = "LabelX44"
        Me.LabelX44.Size = New System.Drawing.Size(161, 41)
        Me.LabelX44.TabIndex = 167
        Me.LabelX44.Text = "PENDIDIKAN"
        '
        'TabFile
        '
        Me.TabFile.Controls.Add(Me.PictureBox3)
        Me.TabFile.Controls.Add(Me.LabelX60)
        Me.TabFile.Controls.Add(Me.PictureBox2)
        Me.TabFile.Controls.Add(Me.LabelX59)
        Me.TabFile.Controls.Add(Me.txtCC)
        Me.TabFile.Controls.Add(Me.LabelX57)
        Me.TabFile.Controls.Add(Me.txtNpwp)
        Me.TabFile.Controls.Add(Me.LabelX58)
        Me.TabFile.Controls.Add(Me.txtNamaBank)
        Me.TabFile.Controls.Add(Me.txtRekAn)
        Me.TabFile.Controls.Add(Me.LabelX53)
        Me.TabFile.Controls.Add(Me.LabelX54)
        Me.TabFile.Controls.Add(Me.txtNoRek)
        Me.TabFile.Controls.Add(Me.LabelX55)
        Me.TabFile.Controls.Add(Me.Panel7)
        Me.TabFile.Controls.Add(Me.Panel8)
        Me.TabFile.Location = New System.Drawing.Point(4, 30)
        Me.TabFile.Name = "TabFile"
        Me.TabFile.Size = New System.Drawing.Size(1385, 397)
        Me.TabFile.TabIndex = 5
        Me.TabFile.Text = "FILE DAN DOKUMEN"
        Me.TabFile.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(906, 225)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 159)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 208
        Me.PictureBox3.TabStop = False
        '
        'LabelX60
        '
        Me.LabelX60.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX60.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX60.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX60.ForeColor = System.Drawing.Color.Black
        Me.LabelX60.Location = New System.Drawing.Point(716, 225)
        Me.LabelX60.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX60.Name = "LabelX60"
        Me.LabelX60.Size = New System.Drawing.Size(170, 28)
        Me.LabelX60.TabIndex = 207
        Me.LabelX60.Text = "PHOTO KTP"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(230, 225)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(161, 159)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 206
        Me.PictureBox2.TabStop = False
        '
        'LabelX59
        '
        Me.LabelX59.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX59.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX59.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX59.ForeColor = System.Drawing.Color.Black
        Me.LabelX59.Location = New System.Drawing.Point(40, 225)
        Me.LabelX59.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX59.Name = "LabelX59"
        Me.LabelX59.Size = New System.Drawing.Size(170, 28)
        Me.LabelX59.TabIndex = 205
        Me.LabelX59.Text = "PHOTO PROFILE"
        '
        'txtCC
        '
        '
        '
        '
        Me.txtCC.Border.Class = "TextBoxBorder"
        Me.txtCC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCC.Location = New System.Drawing.Point(939, 125)
        Me.txtCC.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(400, 28)
        Me.txtCC.TabIndex = 203
        '
        'LabelX57
        '
        Me.LabelX57.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX57.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX57.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX57.ForeColor = System.Drawing.Color.Black
        Me.LabelX57.Location = New System.Drawing.Point(716, 123)
        Me.LabelX57.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX57.Name = "LabelX57"
        Me.LabelX57.Size = New System.Drawing.Size(215, 28)
        Me.LabelX57.TabIndex = 201
        Me.LabelX57.Text = "NOMOR KARTU KREDIT"
        '
        'txtNpwp
        '
        '
        '
        '
        Me.txtNpwp.Border.Class = "TextBoxBorder"
        Me.txtNpwp.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNpwp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNpwp.Location = New System.Drawing.Point(939, 89)
        Me.txtNpwp.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNpwp.Name = "txtNpwp"
        Me.txtNpwp.Size = New System.Drawing.Size(400, 28)
        Me.txtNpwp.TabIndex = 200
        '
        'LabelX58
        '
        Me.LabelX58.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX58.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX58.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX58.ForeColor = System.Drawing.Color.Black
        Me.LabelX58.Location = New System.Drawing.Point(716, 87)
        Me.LabelX58.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX58.Name = "LabelX58"
        Me.LabelX58.Size = New System.Drawing.Size(170, 28)
        Me.LabelX58.TabIndex = 199
        Me.LabelX58.Text = "NOMOR NPWP"
        '
        'txtNamaBank
        '
        '
        '
        '
        Me.txtNamaBank.Border.Class = "TextBoxBorder"
        Me.txtNamaBank.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaBank.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNamaBank.Location = New System.Drawing.Point(230, 161)
        Me.txtNamaBank.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNamaBank.Name = "txtNamaBank"
        Me.txtNamaBank.Size = New System.Drawing.Size(433, 28)
        Me.txtNamaBank.TabIndex = 198
        '
        'txtRekAn
        '
        '
        '
        '
        Me.txtRekAn.Border.Class = "TextBoxBorder"
        Me.txtRekAn.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtRekAn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRekAn.Location = New System.Drawing.Point(230, 125)
        Me.txtRekAn.Margin = New System.Windows.Forms.Padding(4)
        Me.txtRekAn.Name = "txtRekAn"
        Me.txtRekAn.Size = New System.Drawing.Size(433, 28)
        Me.txtRekAn.TabIndex = 197
        '
        'LabelX53
        '
        Me.LabelX53.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX53.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX53.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX53.ForeColor = System.Drawing.Color.Black
        Me.LabelX53.Location = New System.Drawing.Point(40, 159)
        Me.LabelX53.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX53.Name = "LabelX53"
        Me.LabelX53.Size = New System.Drawing.Size(170, 28)
        Me.LabelX53.TabIndex = 196
        Me.LabelX53.Text = "NAMA BANK"
        '
        'LabelX54
        '
        Me.LabelX54.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX54.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX54.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX54.ForeColor = System.Drawing.Color.Black
        Me.LabelX54.Location = New System.Drawing.Point(40, 123)
        Me.LabelX54.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX54.Name = "LabelX54"
        Me.LabelX54.Size = New System.Drawing.Size(170, 28)
        Me.LabelX54.TabIndex = 195
        Me.LabelX54.Text = "REK. ATAS NAMA"
        '
        'txtNoRek
        '
        '
        '
        '
        Me.txtNoRek.Border.Class = "TextBoxBorder"
        Me.txtNoRek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNoRek.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoRek.Location = New System.Drawing.Point(230, 89)
        Me.txtNoRek.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNoRek.Name = "txtNoRek"
        Me.txtNoRek.Size = New System.Drawing.Size(433, 28)
        Me.txtNoRek.TabIndex = 194
        '
        'LabelX55
        '
        Me.LabelX55.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX55.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX55.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX55.ForeColor = System.Drawing.Color.Black
        Me.LabelX55.Location = New System.Drawing.Point(40, 87)
        Me.LabelX55.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX55.Name = "LabelX55"
        Me.LabelX55.Size = New System.Drawing.Size(170, 28)
        Me.LabelX55.TabIndex = 193
        Me.LabelX55.Text = "NO. REKENING"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Blue
        Me.Panel7.Controls.Add(Me.LabelX51)
        Me.Panel7.Location = New System.Drawing.Point(691, 7)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(668, 51)
        Me.Panel7.TabIndex = 168
        '
        'LabelX51
        '
        Me.LabelX51.AutoSize = True
        Me.LabelX51.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX51.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX51.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX51.ForeColor = System.Drawing.Color.White
        Me.LabelX51.Location = New System.Drawing.Point(221, 6)
        Me.LabelX51.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX51.Name = "LabelX51"
        Me.LabelX51.Size = New System.Drawing.Size(257, 41)
        Me.LabelX51.TabIndex = 168
        Me.LabelX51.Text = "DOKUMEN LAINNYA"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Red
        Me.Panel8.Controls.Add(Me.LabelX52)
        Me.Panel8.Location = New System.Drawing.Point(17, 7)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(668, 51)
        Me.Panel8.TabIndex = 167
        '
        'LabelX52
        '
        Me.LabelX52.AutoSize = True
        Me.LabelX52.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX52.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX52.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX52.ForeColor = System.Drawing.Color.White
        Me.LabelX52.Location = New System.Drawing.Point(207, 6)
        Me.LabelX52.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX52.Name = "LabelX52"
        Me.LabelX52.Size = New System.Drawing.Size(258, 41)
        Me.LabelX52.TabIndex = 167
        Me.LabelX52.Text = "FILE DAN DOKUMEN"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Red
        Me.Button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button4.Location = New System.Drawing.Point(25, 249)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(354, 58)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "CLOSE"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'frmInfoKaryawan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1412, 800)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmInfoKaryawan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInfoKaryawan"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPersonal.ResumeLayout(False)
        CType(Me.dtLahir, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.TabAlamat.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.TabKeluarga.ResumeLayout(False)
        CType(Me.dtMenikah, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TabPendidikan.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TabFile.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtStatus As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUnitBisnis As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNama As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPersonal As System.Windows.Forms.TabPage
    Friend WithEvents txtEmail2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtEmail As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTelp2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTelp1 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtGolDarah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSuku As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtWarga As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAgama As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTempatLahir As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPanggilan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaLengkap As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKtp As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabAlamat As System.Windows.Forms.TabPage
    Friend WithEvents txtKodepos As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX31 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKelurahan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX30 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKecamatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX29 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKota As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX28 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtProvinsi As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDetailLokasi As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtStatusRumah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txAlamat As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabKeluarga As System.Windows.Forms.TabPage
    Friend WithEvents txtNamaIbu As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX37 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAnakKe As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX36 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtJumlahTanggungan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX35 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX34 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtStatusPerkawinan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX33 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents LabelX32 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabPendidikan As System.Windows.Forms.TabPage
    Friend WithEvents txAlamatSekolah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtJurusan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX50 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTahunIjazah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX45 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaSekolah As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX46 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTingkat As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX47 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX48 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtStatusPendidikan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX49 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents LabelX44 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabFile As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelX60 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelX59 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCC As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX57 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNpwp As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX58 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaBank As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtRekAn As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX53 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX54 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNoRek As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX55 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents LabelX51 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents LabelX52 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDate As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents dtLahir As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents dtMenikah As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class
