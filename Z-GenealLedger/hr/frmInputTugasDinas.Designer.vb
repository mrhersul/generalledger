﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInputTugasDinas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInputTugasDinas))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtNilai = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.dtPulang = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.dtBerangkat = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.DGView = New System.Windows.Forms.DataGridView()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtNmKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtnosj = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.dtTanggal = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.txtDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUnitBisnis = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txtPicture = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dtPulang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtBerangkat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtTanggal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.PictureBox1)
        Me.GroupPanel1.Controls.Add(Me.txtPicture)
        Me.GroupPanel1.Controls.Add(Me.Button4)
        Me.GroupPanel1.Controls.Add(Me.LabelX10)
        Me.GroupPanel1.Controls.Add(Me.GroupBox1)
        Me.GroupPanel1.Controls.Add(Me.txtnosj)
        Me.GroupPanel1.Controls.Add(Me.LabelX4)
        Me.GroupPanel1.Controls.Add(Me.dtTanggal)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(25, 13)
        Me.GroupPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1057, 751)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 11
        Me.GroupPanel1.Text = "FORM TUGAS DINAS"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtDepartemen)
        Me.GroupBox1.Controls.Add(Me.txtUnitBisnis)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.Controls.Add(Me.LabelX3)
        Me.GroupBox1.Controls.Add(Me.txtNilai)
        Me.GroupBox1.Controls.Add(Me.LabelX9)
        Me.GroupBox1.Controls.Add(Me.dtPulang)
        Me.GroupBox1.Controls.Add(Me.LabelX8)
        Me.GroupBox1.Controls.Add(Me.dtBerangkat)
        Me.GroupBox1.Controls.Add(Me.LabelX7)
        Me.GroupBox1.Controls.Add(Me.DGView)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.txtKdKaryawan)
        Me.GroupBox1.Controls.Add(Me.LabelX6)
        Me.GroupBox1.Controls.Add(Me.txtNmKaryawan)
        Me.GroupBox1.Controls.Add(Me.LabelX1)
        Me.GroupBox1.Location = New System.Drawing.Point(61, 166)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(949, 407)
        Me.GroupBox1.TabIndex = 151
        Me.GroupBox1.TabStop = False
        '
        'txtNilai
        '
        '
        '
        '
        Me.txtNilai.Border.Class = "TextBoxBorder"
        Me.txtNilai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNilai.Location = New System.Drawing.Point(171, 215)
        Me.txtNilai.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNilai.Name = "txtNilai"
        Me.txtNilai.Size = New System.Drawing.Size(295, 26)
        Me.txtNilai.TabIndex = 157
        Me.txtNilai.Text = "0"
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.ForeColor = System.Drawing.Color.Black
        Me.LabelX9.Location = New System.Drawing.Point(13, 215)
        Me.LabelX9.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(152, 28)
        Me.LabelX9.TabIndex = 156
        Me.LabelX9.Text = "NILAI"
        '
        'dtPulang
        '
        '
        '
        '
        Me.dtPulang.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtPulang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtPulang.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtPulang.ButtonDropDown.Visible = True
        Me.dtPulang.IsPopupCalendarOpen = False
        Me.dtPulang.Location = New System.Drawing.Point(599, 181)
        Me.dtPulang.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.dtPulang.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtPulang.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtPulang.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtPulang.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtPulang.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtPulang.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtPulang.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtPulang.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtPulang.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtPulang.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtPulang.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtPulang.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtPulang.MonthCalendar.TodayButtonVisible = True
        Me.dtPulang.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtPulang.Name = "dtPulang"
        Me.dtPulang.Size = New System.Drawing.Size(188, 26)
        Me.dtPulang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtPulang.TabIndex = 155
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(441, 179)
        Me.LabelX8.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(152, 28)
        Me.LabelX8.TabIndex = 154
        Me.LabelX8.Text = "TGL KEPULANGAN"
        '
        'dtBerangkat
        '
        '
        '
        '
        Me.dtBerangkat.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtBerangkat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtBerangkat.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtBerangkat.ButtonDropDown.Visible = True
        Me.dtBerangkat.IsPopupCalendarOpen = False
        Me.dtBerangkat.Location = New System.Drawing.Point(171, 181)
        Me.dtBerangkat.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.dtBerangkat.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtBerangkat.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtBerangkat.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtBerangkat.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtBerangkat.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtBerangkat.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtBerangkat.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtBerangkat.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtBerangkat.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtBerangkat.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtBerangkat.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtBerangkat.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtBerangkat.MonthCalendar.TodayButtonVisible = True
        Me.dtBerangkat.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtBerangkat.Name = "dtBerangkat"
        Me.dtBerangkat.Size = New System.Drawing.Size(188, 26)
        Me.dtBerangkat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtBerangkat.TabIndex = 153
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(13, 179)
        Me.LabelX7.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(152, 28)
        Me.LabelX7.TabIndex = 152
        Me.LabelX7.Text = "TGL BERANGKAT"
        '
        'DGView
        '
        Me.DGView.AllowUserToAddRows = False
        Me.DGView.AllowUserToDeleteRows = False
        Me.DGView.AllowUserToOrderColumns = True
        Me.DGView.AllowUserToResizeRows = False
        Me.DGView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGView.Location = New System.Drawing.Point(7, 254)
        Me.DGView.Margin = New System.Windows.Forms.Padding(4)
        Me.DGView.Name = "DGView"
        Me.DGView.Size = New System.Drawing.Size(935, 146)
        Me.DGView.TabIndex = 151
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.Location = New System.Drawing.Point(737, 36)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(98, 67)
        Me.Button3.TabIndex = 150
        Me.Button3.Text = "HAPUS"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(633, 36)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(98, 67)
        Me.Button2.TabIndex = 149
        Me.Button2.Text = "TAMBAH"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(559, 36)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(45, 28)
        Me.Button1.TabIndex = 148
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.Enabled = False
        Me.txtKdKaryawan.Location = New System.Drawing.Point(171, 74)
        Me.txtKdKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(433, 26)
        Me.txtKdKaryawan.TabIndex = 147
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(11, 72)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(152, 28)
        Me.LabelX6.TabIndex = 146
        Me.LabelX6.Text = "KODE PESERTA"
        '
        'txtNmKaryawan
        '
        '
        '
        '
        Me.txtNmKaryawan.Border.Class = "TextBoxBorder"
        Me.txtNmKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmKaryawan.Enabled = False
        Me.txtNmKaryawan.Location = New System.Drawing.Point(171, 38)
        Me.txtNmKaryawan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNmKaryawan.Name = "txtNmKaryawan"
        Me.txtNmKaryawan.Size = New System.Drawing.Size(378, 26)
        Me.txtNmKaryawan.TabIndex = 145
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(11, 36)
        Me.LabelX1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(152, 28)
        Me.LabelX1.TabIndex = 144
        Me.LabelX1.Text = "NAMA PESERTA"
        '
        'txtnosj
        '
        '
        '
        '
        Me.txtnosj.Border.Class = "TextBoxBorder"
        Me.txtnosj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtnosj.Location = New System.Drawing.Point(231, 54)
        Me.txtnosj.Margin = New System.Windows.Forms.Padding(4)
        Me.txtnosj.Name = "txtnosj"
        Me.txtnosj.Size = New System.Drawing.Size(433, 26)
        Me.txtnosj.TabIndex = 145
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(72, 52)
        Me.LabelX4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(152, 28)
        Me.LabelX4.TabIndex = 144
        Me.LabelX4.Text = "NO SURAT TUGAS"
        '
        'dtTanggal
        '
        '
        '
        '
        Me.dtTanggal.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtTanggal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtTanggal.ButtonDropDown.Visible = True
        Me.dtTanggal.IsPopupCalendarOpen = False
        Me.dtTanggal.Location = New System.Drawing.Point(231, 90)
        Me.dtTanggal.Margin = New System.Windows.Forms.Padding(4)
        '
        '
        '
        Me.dtTanggal.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtTanggal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtTanggal.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtTanggal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtTanggal.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtTanggal.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtTanggal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTanggal.MonthCalendar.TodayButtonVisible = True
        Me.dtTanggal.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtTanggal.Name = "dtTanggal"
        Me.dtTanggal.Size = New System.Drawing.Size(188, 26)
        Me.dtTanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtTanggal.TabIndex = 133
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(72, 88)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(152, 28)
        Me.LabelX5.TabIndex = 35
        Me.LabelX5.Text = "TANGGAL"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(917, 580)
        Me.BtnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(93, 89)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(813, 580)
        Me.BtnSave.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(93, 89)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "SAVE"
        '
        'txtDepartemen
        '
        '
        '
        '
        Me.txtDepartemen.Border.Class = "TextBoxBorder"
        Me.txtDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDepartemen.Enabled = False
        Me.txtDepartemen.Location = New System.Drawing.Point(170, 146)
        Me.txtDepartemen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDepartemen.Name = "txtDepartemen"
        Me.txtDepartemen.Size = New System.Drawing.Size(433, 26)
        Me.txtDepartemen.TabIndex = 161
        '
        'txtUnitBisnis
        '
        '
        '
        '
        Me.txtUnitBisnis.Border.Class = "TextBoxBorder"
        Me.txtUnitBisnis.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUnitBisnis.Enabled = False
        Me.txtUnitBisnis.Location = New System.Drawing.Point(170, 110)
        Me.txtUnitBisnis.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUnitBisnis.Name = "txtUnitBisnis"
        Me.txtUnitBisnis.Size = New System.Drawing.Size(434, 26)
        Me.txtUnitBisnis.TabIndex = 160
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(11, 144)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(109, 28)
        Me.LabelX2.TabIndex = 159
        Me.LabelX2.Text = "DEPARTEMEN"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(11, 108)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(109, 28)
        Me.LabelX3.TabIndex = 158
        Me.LabelX3.Text = "UNIT BISNIS"
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(74, 124)
        Me.LabelX10.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(152, 28)
        Me.LabelX10.TabIndex = 152
        Me.LabelX10.Text = "DOKUMEN"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(616, 124)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(45, 28)
        Me.Button4.TabIndex = 162
        Me.Button4.Text = "..."
        Me.Button4.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txtPicture
        '
        '
        '
        '
        Me.txtPicture.Border.Class = "TextBoxBorder"
        Me.txtPicture.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPicture.Location = New System.Drawing.Point(228, 126)
        Me.txtPicture.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPicture.Name = "txtPicture"
        Me.txtPicture.Size = New System.Drawing.Size(382, 26)
        Me.txtPicture.TabIndex = 163
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(877, 40)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(126, 120)
        Me.PictureBox1.TabIndex = 164
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'frmInputTugasDinas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1108, 786)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmInputTugasDinas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInputTugasDinas"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dtPulang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtBerangkat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtTanggal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNmKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtnosj As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtTanggal As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtNilai As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtPulang As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtBerangkat As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DGView As System.Windows.Forms.DataGridView
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUnitBisnis As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtPicture As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
