﻿Public Class frmMutasi
    Dim Idnya As Integer
    Public Sub Pengaturan(ByVal Sid As Integer)
        Idnya = Sid
    End Sub
    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub frmMutasi_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        SQL = ""
        SQL = "SP_HR_InfoKaryawan " & Idnya & " "


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read
                txtNama.Text = Trim(dr.Item("nama_lengkap").ToString())
                txtKdKaryawan.Text = Trim(dr.Item("kd_karyawan").ToString())
                txtTglJoin.Value = dr.Item("tgl_join").ToString()
                txtUnitBisnis.Text = Trim(dr.Item("nama_unitbisnis").ToString())
                txtDepartemen.Text = Trim(dr.Item("departemen").ToString())
                txtJabatan.Text = Trim(dr.Item("jabatan").ToString())
                txtPanggilan.Text = Trim(dr.Item("nama_panggilan").ToString())
            End While
        End If
    End Sub

    Private Sub btnUnit_Click(sender As System.Object, e As System.EventArgs) Handles btnUnit.Click
        blnMutasi = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub btnDept_Click(sender As System.Object, e As System.EventArgs) Handles btnDept.Click
        frmDepartemen.Text = "FORM MUTASI"
        frmDepartemen.ShowDialog()
    End Sub

    Private Sub btnJabatan_Click(sender As System.Object, e As System.EventArgs) Handles btnJabatan.Click
        Dim PNm As String
        PNm = Microsoft.VisualBasic.Left(txtNama.Text, 3)
        If txtIdDepartemen.Text = "" Then
            MsgBox("Isi Nama Departemen Terlebih dahulu..!")
        Else
            frmJabatan.Text = "FORM MUTASI"
            frmJabatan.Pengaturan(txtIdDepartemen.Text, PNm)
            frmJabatan.ShowDialog()
        End If
    End Sub
End Class