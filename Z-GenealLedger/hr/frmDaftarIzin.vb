﻿Public Class frmDaftarIzin
    Dim kk As Integer
    Dim posting As Integer
    Sub ShowDataGrid()
        Dim nYear As String = Year(Now)
        Dim nMont As String = Month(Now)

        If Len(nMont) = 1 Then
            nMont = "0" + nMont
        Else
            nMont = nMont
        End If

        Select Case cmbBulan.Text
            Case "JANUARI"
                nMont = "01"
            Case "FEBRUARI"
                nMont = "02"
            Case "MARET"
                nMont = "03"
            Case "APRIL"
                nMont = "04"
            Case "MEI"
                nMont = "05"
            Case "JUNI"
                nMont = "06"
            Case "JULI"
                nMont = "07"
            Case "AGUSTUS"
                nMont = "08"
            Case "SEPTEMBER"
                nMont = "09"
            Case "OKTOBER"
                nMont = "10"
            Case "NOVEMBER"
                nMont = "11"
            Case "DESEMBER"
                nMont = "12"
            Case Else
                nMont = nMont
        End Select

        If txtTahun.Text = "" Then
            nYear = nYear
        Else
            nYear = txtTahun.Text
        End If

        SQL = ""
        SQL = "SELECT b.nama_lengkap, a.tgl_permohonan, a.tgl_awal_cuti, a.tgl_akhir_cuti, case a.status_permohonan"
        SQL = SQL & " when '1' then 'PENDING' "
        SQL = SQL & " when '2' then 'APPROVE' "
        SQL = SQL & " when '0' then 'CANCEL' "
        SQL = SQL & " end as status_permohonan, a.keterangan, a.id, coalesce(0,a.posting) as posting FROM karyawan_cuti a"
        SQL = SQL & " inner join personal b on a.karyawan_nama_id = b.id"
        SQL = SQL & " inner join karyawan c on a.karyawan_nama_id = c.personal_id"
        SQL = SQL & " inner join unitbisnis_nama d on d.id = c.unitbisnis_id"
        SQL = SQL & " where year(a.tgl_permohonan) = '" & nYear & "' and month(a.tgl_permohonan) = '" & nMont & "' and jenis = '1'"
        SQL = SQL & " AND d.kd_unitbisnis = '" & myKdBisnis & "' "

        If txtSearch.Text <> "" Then

            SQL = SQL & " and (b.nama_lengkap like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If

        SQL = SQL & " and len(c.kd_karyawan) > 0 "
        SQL = SQL & " order by b.nama_lengkap"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 250
        DGView.Columns(1).Width = 150
        DGView.Columns(2).Width = 150
        DGView.Columns(3).Width = 150
        DGView.Columns(4).Width = 250
        DGView.Columns(5).Width = 350
        DGView.Columns(6).Width = 80
        DGView.Columns(7).Width = 80

        DGView.Columns(0).HeaderText = "NAMA KARYAWAN"
        DGView.Columns(1).HeaderText = "TGL PERMOHONAN"
        DGView.Columns(2).HeaderText = "TGL AWAL"
        DGView.Columns(3).HeaderText = "TGL AKHIR"
        DGView.Columns(4).HeaderText = "STATUS"
        DGView.Columns(5).HeaderText = "KETERANGAN"
        DGView.Columns(6).HeaderText = "ID"
        DGView.Columns(6).Visible = False
        DGView.Columns(7).HeaderText = "POSTING"
        DGView.Columns(7).Visible = False

    End Sub
    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub frmDaftarIzin_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 1420
        GroupPanel2.Top = 2
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Call ShowDataGrid()
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
                posting = .Item("posting", i).Value
            End With
        End If
    End Sub

    Private Sub BtnAdd_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnAdd.Click
        frmIzin.ShowDialog()
        If frmIzin.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGrid()
        End If
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        Call frmClose()
    End Sub

    Private Sub BtnEdit_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnEdit.Click
        TakeDGView()
        frmApproveIzin.Pengaturan(kk)
        frmApproveIzin.ShowDialog()
        If frmApproveIzin.DialogResult = Windows.Forms.DialogResult.OK Then
            Call ShowDataGrid()
        End If
    End Sub

    Private Sub BtnHapus_Click(sender As Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnHapus.Click
        TakeDGView()
        If posting = "1" Then
            MessageBox.Show("Data yang sudah di approve, tidak bisa di hapus!")
        Else
            dr.Close()
            SQL = ""
            SQL = "DELETE FROM karyawan_cuti where id = " & kk & " "
            Konek.IUDQuery(SQL)

            MsgBox("Data sudah terhapus...", MsgBoxStyle.Information)
            Call ShowDataGrid()
        End If
    End Sub
End Class