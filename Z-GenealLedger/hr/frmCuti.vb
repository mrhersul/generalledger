﻿Public Class frmCuti
    Dim Idnya As Integer
    Dim kd, nm As String
    Public Sub Karyawan(ByVal kode As String, ByVal nama As String, ByVal sid As Integer)
        kd = kode
        nm = nama
        Idnya = sid
    End Sub

    Private Sub frmCuti_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        txtKdKaryawan.Text = kd
        txtNmKaryawan.Text = nm
    End Sub

    Private Sub frmCuti_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        kd = ""
        nm = ""
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        frmKaryawan.Text = "CUTI"
        frmKaryawan.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(sender As System.Object, e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnSave_Click(sender As System.Object, e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        SQL = "INSERT INTO karyawan_cuti (karyawan_nama_id, tgl_permohonan, status_permohonan, tgl_awal_cuti, tgl_akhir_cuti, keterangan,"
        SQL = SQL & " tgl_input, user_buat,jenis) VALUES (" & Idnya & ",'" & Format(Now(), "yyyy-MM-dd HH:mm:ss") & "', '1', "
        SQL = SQL & " '" & Format(txtDateAwal.Value, "yyyy-MM-dd") & "','" & Format(txtDateAkhir.Value, "yyyy-MM-dd") & "','" & txtKet.Text & "',"
        SQL = SQL & " '" & Format(Now(), "yyyy-MM-dd HH:mm:ss") & "','" & myID & "',0) "
        Konek.IUDQuery(SQL)
        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        Call frmInputCuti.segar()
        DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
        Exit Sub

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
End Class