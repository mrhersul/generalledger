﻿Public Class frmInfoKaryawan
    Dim Idnya As Integer
    Public Sub Pengaturan(ByVal Sid As Integer)
        Idnya = Sid
    End Sub

    Private Sub frmInfoKaryawan_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'SQL = "select a.id, a.nama_lengkap, b.kd_karyawan, a.tanggal_buat as tgl_join, c.nama_unitbisnis, e.nama as departemen, f.nama as jabatan,"
        'SQL = SQL & " a.no_ktp, a.nama_panggilan, a.tempat_lahir, a.tgl_lahir, a.agama_id, a.kewarganegaraan_id, a.daerahasal_suku, a.golongan_darah_id, "
        'SQL = SQL & " a.telepon_1, a.telepon_2, a.email_1, a.email_2, g.alamat, g.status_rumah_id, g.detail_lokasi, h.nama_provinsi, i.nama_kota, "
        'SQL = SQL & " j.nama_kecamatan, k.nama_kelurahan, l.status_perkawinan_id, coalesce(l.tgl_menikah,'') as tgl_menikah, l.tanggungan_keluarga, l.anak_nomor,l.nama_ibu_kandung,"
        'SQL = SQL & " n.status_masih_pendidikan, n.informasi_tingkat_pendidikan_id, n.nama_sekolah, n.tahun_ijazah, n.jurusan_studi, n.alamat as alamat_sekolah,"
        'SQL = SQL & " o.rek_no, o.rek_atas_nama, o.rek_nama_bank, o.no_npwp, o.no_cc, d.status_karyawan_mutasi"
        'SQL = SQL & " from personal a "
        'SQL = SQL & " inner join karyawan b on a.id = b.personal_id "
        'SQL = SQL & " inner join unitbisnis_nama c on c.id = b.unitbisnis_id"
        'SQL = SQL & " inner join karyawan_mutasi_organisasi d on d.personal_id = a.id"
        'SQL = SQL & " inner join karyawan_struktur_organisasi e on e.id = d.departemen_id"
        'SQL = SQL & " inner join karyawan_struktur_organisasi f on f.id = d.jabatan_id"
        'SQL = SQL & " inner join personal_alamat g on a.id = g.personal_id"
        'SQL = SQL & " inner join regional_provinsi h on g.provinsi_id = h.id"
        'SQL = SQL & " inner join regional_kota i on g.kota_id = i.id"
        'SQL = SQL & " inner join regional_kecamatan j on g.kecamatan_id = j.id"
        'SQL = SQL & " inner join regional_kelurahan k on g.kelurahan_id = k.id"
        'SQL = SQL & " inner join personal_keluarga l on a.id = l.personal_id"
        'SQL = SQL & " inner join personal_pekerjaan m on a.id = m.personal_id"
        'SQL = SQL & " inner join personal_pendidikan n on a.id = n.personal_id"
        'SQL = SQL & " inner join personal_file_dokumen o on a.id = o.personal_id"
        'SQL = SQL & " where a.id = " & Idnya & " "
        SQL = ""
        SQL = "SP_HR_InfoKaryawan " & Idnya & " "


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read
                txtNama.Text = Trim(dr.Item("nama_lengkap").ToString())
                txtKdKaryawan.Text = Trim(dr.Item("kd_karyawan").ToString())
                txtDate.Value = dr.Item("tgl_join").ToString()
                txtUnitBisnis.Text = Trim(dr.Item("nama_unitbisnis").ToString())
                txtDepartemen.Text = Trim(dr.Item("departemen").ToString())
                txtJabatan.Text = Trim(dr.Item("jabatan").ToString())
                Select Case dr.Item("status_karyawan_mutasi").ToString()
                    Case "0"
                        txtStatus.Text = "RESIGN"
                    Case "1"
                        txtStatus.Text = "AKTIF"
                    Case "2"
                        txtStatus.Text = "MUTASI"
                End Select
                txtKtp.Text = Trim(dr.Item("no_ktp").ToString())
                txtNamaLengkap.Text = Trim(dr.Item("nama_lengkap").ToString())
                txtPanggilan.Text = Trim(dr.Item("nama_panggilan").ToString())
                txtTempatLahir.Text = Trim(dr.Item("tempat_lahir").ToString())
                dtLahir.Value = dr.Item("tgl_lahir").ToString()
                txtAgama.Text = dr.Item("agama_nama").ToString
                txtWarga.Text = dr.Item("kewarganegaraan_nama").ToString
                txtSuku.Text = dr.Item("suku").ToString
                txtGolDarah.Text = dr.Item("goldar_nama").ToString
                txtTelp1.Text = Trim(dr.Item("telepon_1").ToString())
                txtTelp2.Text = Trim(dr.Item("telepon_2").ToString())
                txtEmail.Text = Trim(dr.Item("email_1").ToString())
                txtEmail2.Text = Trim(dr.Item("email_2").ToString())
                txAlamat.Text = Trim(dr.Item("alamat").ToString())
                txtStatusRumah.Text = dr.Item("status_rumah").ToString
                txtDetailLokasi.Text = Trim(dr.Item("detail_lokasi").ToString())
                txtProvinsi.Text = Trim(dr.Item("nama_provinsi").ToString())
                txtKota.Text = Trim(dr.Item("nama_kota").ToString())
                txtKecamatan.Text = Trim(dr.Item("nama_kecamatan").ToString())
                txtKelurahan.Text = Trim(dr.Item("nama_kelurahan").ToString())
                txtKodepos.Text = ""
                txtStatusPerkawinan.Text = dr.Item("status_perkawinan").ToString
                dtMenikah.Value = dr.Item("tgl_menikah").ToString()
                txtJumlahTanggungan.Text = Trim(dr.Item("tanggungan_keluarga").ToString())
                txtAnakKe.Text = Trim(dr.Item("anak_nomor").ToString())
                txtNamaIbu.Text = Trim(dr.Item("nama_ibu_kandung").ToString())
                'txtBidangUsaha.Text = ""
                'txtProfesi.Text = ""
                'txtNamaPerusahaan.Text = ""
                'txtAlamatPerusahaan.Text = ""
                'dtTglBuat.Value = dr.Item("
                txtStatusPendidikan.Text = dr.Item("status_masih_pendidikan").ToString()
                txtTingkat.Text = dr.Item("tingkat_pendidikan").ToString()
                txtNamaSekolah.Text = Trim(dr.Item("nama_sekolah").ToString())
                txtTahunIjazah.Text = Trim(dr.Item("tahun_ijazah").ToString())
                txtJurusan.Text = Trim(dr.Item("jurusan_studi").ToString())
                txAlamatSekolah.Text = Trim(dr.Item("alamat_sekolah").ToString())
                txtNoRek.Text = Trim(dr.Item("rek_no").ToString())
                txtRekAn.Text = Trim(dr.Item("rek_atas_nama").ToString())
                txtNamaBank.Text = Trim(dr.Item("rek_nama_bank").ToString())
                txtNpwp.Text = Trim(dr.Item("no_npwp").ToString())
                txtCC.Text = Trim(dr.Item("no_cc").ToString())
            End While
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Call frmCatatan.Pengaturan(Idnya)
        frmCatatan.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Call frmKarirvb.Pengaturan(Idnya)
        frmKarirvb.ShowDialog()
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Call frmResign.Pengaturan(txtKdKaryawan.Text, txtNamaLengkap.Text, Idnya)
        frmResign.ShowDialog()
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class