﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMutasi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMutasi))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtNmJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNmDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.txtNmUnitCabang = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.dtTglMutasi = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.cbKarir = New System.Windows.Forms.ComboBox()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.cbTipeKarir = New System.Windows.Forms.ComboBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKdKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUnitBisnis = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtTglJoin = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtNikKaryawan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNilai = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtKet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtNama = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.txtPanggilan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.txtKdUnitCabang = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKdDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKdJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIdDepartemen = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIDJabatan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnJabatan = New DevComponents.DotNetBar.ButtonX()
        Me.btnDept = New DevComponents.DotNetBar.ButtonX()
        Me.btnUnit = New DevComponents.DotNetBar.ButtonX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.dtTglMutasi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.txtTglJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.txtIDJabatan)
        Me.GroupPanel1.Controls.Add(Me.txtIdDepartemen)
        Me.GroupPanel1.Controls.Add(Me.txtIdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtKdDepartemen)
        Me.GroupPanel1.Controls.Add(Me.txtPanggilan)
        Me.GroupPanel1.Controls.Add(Me.LabelX17)
        Me.GroupPanel1.Controls.Add(Me.txtNama)
        Me.GroupPanel1.Controls.Add(Me.LabelX16)
        Me.GroupPanel1.Controls.Add(Me.txtNmJabatan)
        Me.GroupPanel1.Controls.Add(Me.btnJabatan)
        Me.GroupPanel1.Controls.Add(Me.txtNmDepartemen)
        Me.GroupPanel1.Controls.Add(Me.LabelX14)
        Me.GroupPanel1.Controls.Add(Me.LabelX13)
        Me.GroupPanel1.Controls.Add(Me.txtNmUnitCabang)
        Me.GroupPanel1.Controls.Add(Me.LabelX12)
        Me.GroupPanel1.Controls.Add(Me.dtTglMutasi)
        Me.GroupPanel1.Controls.Add(Me.LabelX11)
        Me.GroupPanel1.Controls.Add(Me.cbKarir)
        Me.GroupPanel1.Controls.Add(Me.LabelX8)
        Me.GroupPanel1.Controls.Add(Me.cbTipeKarir)
        Me.GroupPanel1.Controls.Add(Me.Panel3)
        Me.GroupPanel1.Controls.Add(Me.Panel2)
        Me.GroupPanel1.Controls.Add(Me.txtNikKaryawan)
        Me.GroupPanel1.Controls.Add(Me.txtNilai)
        Me.GroupPanel1.Controls.Add(Me.LabelX7)
        Me.GroupPanel1.Controls.Add(Me.btnDept)
        Me.GroupPanel1.Controls.Add(Me.btnUnit)
        Me.GroupPanel1.Controls.Add(Me.txtKet)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.LabelX10)
        Me.GroupPanel1.Controls.Add(Me.LabelX15)
        Me.GroupPanel1.Controls.Add(Me.txtKdUnitCabang)
        Me.GroupPanel1.Controls.Add(Me.txtKdJabatan)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 2)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(941, 445)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 7
        Me.GroupPanel1.Text = "FORM MUTASI"
        '
        'txtNmJabatan
        '
        '
        '
        '
        Me.txtNmJabatan.Border.Class = "TextBoxBorder"
        Me.txtNmJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmJabatan.Enabled = False
        Me.txtNmJabatan.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNmJabatan.Location = New System.Drawing.Point(538, 225)
        Me.txtNmJabatan.Name = "txtNmJabatan"
        Me.txtNmJabatan.Size = New System.Drawing.Size(302, 22)
        Me.txtNmJabatan.TabIndex = 182
        '
        'txtNmDepartemen
        '
        '
        '
        '
        Me.txtNmDepartemen.Border.Class = "TextBoxBorder"
        Me.txtNmDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmDepartemen.Enabled = False
        Me.txtNmDepartemen.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNmDepartemen.Location = New System.Drawing.Point(538, 198)
        Me.txtNmDepartemen.Name = "txtNmDepartemen"
        Me.txtNmDepartemen.Size = New System.Drawing.Size(302, 22)
        Me.txtNmDepartemen.TabIndex = 180
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(458, 226)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(109, 23)
        Me.LabelX14.TabIndex = 179
        Me.LabelX14.Text = "Jabatan"
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(458, 196)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(109, 23)
        Me.LabelX13.TabIndex = 177
        Me.LabelX13.Text = "Departemen"
        '
        'txtNmUnitCabang
        '
        '
        '
        '
        Me.txtNmUnitCabang.Border.Class = "TextBoxBorder"
        Me.txtNmUnitCabang.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNmUnitCabang.Enabled = False
        Me.txtNmUnitCabang.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNmUnitCabang.Location = New System.Drawing.Point(538, 170)
        Me.txtNmUnitCabang.Name = "txtNmUnitCabang"
        Me.txtNmUnitCabang.Size = New System.Drawing.Size(302, 22)
        Me.txtNmUnitCabang.TabIndex = 176
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(458, 169)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(109, 23)
        Me.LabelX12.TabIndex = 175
        Me.LabelX12.Text = "Unit Cabang"
        '
        'dtTglMutasi
        '
        '
        '
        '
        Me.dtTglMutasi.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtTglMutasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTglMutasi.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtTglMutasi.ButtonDropDown.Visible = True
        Me.dtTglMutasi.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTglMutasi.IsPopupCalendarOpen = False
        Me.dtTglMutasi.Location = New System.Drawing.Point(538, 141)
        '
        '
        '
        Me.dtTglMutasi.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtTglMutasi.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTglMutasi.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtTglMutasi.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtTglMutasi.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTglMutasi.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.dtTglMutasi.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtTglMutasi.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtTglMutasi.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtTglMutasi.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtTglMutasi.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtTglMutasi.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtTglMutasi.MonthCalendar.TodayButtonVisible = True
        Me.dtTglMutasi.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtTglMutasi.Name = "dtTglMutasi"
        Me.dtTglMutasi.Size = New System.Drawing.Size(141, 22)
        Me.dtTglMutasi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtTglMutasi.TabIndex = 174
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(458, 141)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(109, 23)
        Me.LabelX11.TabIndex = 173
        Me.LabelX11.Text = "Tanggal Mutasi"
        '
        'cbKarir
        '
        Me.cbKarir.FormattingEnabled = True
        Me.cbKarir.Items.AddRange(New Object() {"Promosi", "Degradasi", "Mutasi"})
        Me.cbKarir.Location = New System.Drawing.Point(538, 82)
        Me.cbKarir.Name = "cbKarir"
        Me.cbKarir.Size = New System.Drawing.Size(168, 24)
        Me.cbKarir.TabIndex = 171
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(458, 82)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(109, 23)
        Me.LabelX8.TabIndex = 170
        Me.LabelX8.Text = "Karir"
        '
        'cbTipeKarir
        '
        Me.cbTipeKarir.FormattingEnabled = True
        Me.cbTipeKarir.Items.AddRange(New Object() {"Internal", "Eksternal"})
        Me.cbTipeKarir.Location = New System.Drawing.Point(538, 52)
        Me.cbTipeKarir.Name = "cbTipeKarir"
        Me.cbTipeKarir.Size = New System.Drawing.Size(168, 24)
        Me.cbTipeKarir.TabIndex = 169
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Panel3.Controls.Add(Me.LabelX4)
        Me.Panel3.Location = New System.Drawing.Point(453, 14)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(446, 26)
        Me.Panel3.TabIndex = 168
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.White
        Me.LabelX4.Location = New System.Drawing.Point(12, 0)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(229, 23)
        Me.LabelX4.TabIndex = 167
        Me.LabelX4.Text = "MUTASI KE"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.txtDepartemen)
        Me.Panel2.Controls.Add(Me.txtKdKaryawan)
        Me.Panel2.Controls.Add(Me.txtUnitBisnis)
        Me.Panel2.Controls.Add(Me.txtTglJoin)
        Me.Panel2.Controls.Add(Me.txtJabatan)
        Me.Panel2.Controls.Add(Me.LabelX2)
        Me.Panel2.Controls.Add(Me.LabelX5)
        Me.Panel2.Controls.Add(Me.LabelX3)
        Me.Panel2.Controls.Add(Me.LabelX1)
        Me.Panel2.Controls.Add(Me.LabelX6)
        Me.Panel2.Location = New System.Drawing.Point(23, 82)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(395, 181)
        Me.Panel2.TabIndex = 167
        '
        'txtDepartemen
        '
        '
        '
        '
        Me.txtDepartemen.Border.Class = "TextBoxBorder"
        Me.txtDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDepartemen.Enabled = False
        Me.txtDepartemen.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartemen.Location = New System.Drawing.Point(94, 123)
        Me.txtDepartemen.Name = "txtDepartemen"
        Me.txtDepartemen.Size = New System.Drawing.Size(277, 20)
        Me.txtDepartemen.TabIndex = 145
        '
        'txtKdKaryawan
        '
        '
        '
        '
        Me.txtKdKaryawan.Border.Class = "TextBoxBorder"
        Me.txtKdKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdKaryawan.Enabled = False
        Me.txtKdKaryawan.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKdKaryawan.Location = New System.Drawing.Point(94, 49)
        Me.txtKdKaryawan.Name = "txtKdKaryawan"
        Me.txtKdKaryawan.Size = New System.Drawing.Size(277, 20)
        Me.txtKdKaryawan.TabIndex = 29
        '
        'txtUnitBisnis
        '
        '
        '
        '
        Me.txtUnitBisnis.Border.Class = "TextBoxBorder"
        Me.txtUnitBisnis.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUnitBisnis.Enabled = False
        Me.txtUnitBisnis.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnitBisnis.Location = New System.Drawing.Point(94, 97)
        Me.txtUnitBisnis.Name = "txtUnitBisnis"
        Me.txtUnitBisnis.Size = New System.Drawing.Size(277, 20)
        Me.txtUnitBisnis.TabIndex = 30
        '
        'txtTglJoin
        '
        '
        '
        '
        Me.txtTglJoin.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtTglJoin.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTglJoin.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtTglJoin.ButtonDropDown.Visible = True
        Me.txtTglJoin.Enabled = False
        Me.txtTglJoin.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTglJoin.IsPopupCalendarOpen = False
        Me.txtTglJoin.Location = New System.Drawing.Point(94, 73)
        '
        '
        '
        Me.txtTglJoin.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtTglJoin.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTglJoin.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtTglJoin.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtTglJoin.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTglJoin.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtTglJoin.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtTglJoin.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtTglJoin.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtTglJoin.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtTglJoin.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtTglJoin.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTglJoin.MonthCalendar.TodayButtonVisible = True
        Me.txtTglJoin.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtTglJoin.Name = "txtTglJoin"
        Me.txtTglJoin.Size = New System.Drawing.Size(141, 20)
        Me.txtTglJoin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtTglJoin.TabIndex = 133
        '
        'txtJabatan
        '
        '
        '
        '
        Me.txtJabatan.Border.Class = "TextBoxBorder"
        Me.txtJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtJabatan.Enabled = False
        Me.txtJabatan.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJabatan.Location = New System.Drawing.Point(94, 149)
        Me.txtJabatan.Name = "txtJabatan"
        Me.txtJabatan.Size = New System.Drawing.Size(277, 20)
        Me.txtJabatan.TabIndex = 138
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(25, 120)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(114, 23)
        Me.LabelX2.TabIndex = 1
        Me.LabelX2.Text = "Departemen"
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(25, 68)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(114, 23)
        Me.LabelX5.TabIndex = 35
        Me.LabelX5.Text = "Tgl Gabung"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(25, 146)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(82, 23)
        Me.LabelX3.TabIndex = 12
        Me.LabelX3.Text = "Jabatan"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(25, 46)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(54, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "NIK"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Arial Narrow", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(25, 94)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(73, 23)
        Me.LabelX6.TabIndex = 140
        Me.LabelX6.Text = "Unit Bisnis"
        '
        'txtNikKaryawan
        '
        '
        '
        '
        Me.txtNikKaryawan.Border.Class = "TextBoxBorder"
        Me.txtNikKaryawan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNikKaryawan.Enabled = False
        Me.txtNikKaryawan.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNikKaryawan.Location = New System.Drawing.Point(538, 113)
        Me.txtNikKaryawan.Name = "txtNikKaryawan"
        Me.txtNikKaryawan.Size = New System.Drawing.Size(353, 22)
        Me.txtNikKaryawan.TabIndex = 146
        '
        'txtNilai
        '
        '
        '
        '
        Me.txtNilai.Border.Class = "TextBoxBorder"
        Me.txtNilai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNilai.Location = New System.Drawing.Point(941, 384)
        Me.txtNilai.Name = "txtNilai"
        Me.txtNilai.Size = New System.Drawing.Size(149, 22)
        Me.txtNilai.TabIndex = 142
        Me.txtNilai.Text = "0"
        Me.txtNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(458, 52)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(109, 23)
        Me.LabelX7.TabIndex = 141
        Me.LabelX7.Text = "Tipe Karir"
        '
        'txtKet
        '
        '
        '
        '
        Me.txtKet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKet.Location = New System.Drawing.Point(538, 250)
        Me.txtKet.Multiline = True
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(353, 63)
        Me.txtKet.TabIndex = 4
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(458, 112)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(109, 23)
        Me.LabelX10.TabIndex = 172
        Me.LabelX10.Text = "NIK Karyawan"
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(458, 249)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(109, 23)
        Me.LabelX15.TabIndex = 183
        Me.LabelX15.Text = "Keterangan"
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX9.ForeColor = System.Drawing.Color.White
        Me.LabelX9.Location = New System.Drawing.Point(12, 0)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(229, 23)
        Me.LabelX9.TabIndex = 167
        Me.LabelX9.Text = "MUTASI DARI"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.Controls.Add(Me.LabelX9)
        Me.Panel1.Location = New System.Drawing.Point(16, 14)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(355, 26)
        Me.Panel1.TabIndex = 166
        '
        'txtNama
        '
        '
        '
        '
        Me.txtNama.Border.Class = "TextBoxBorder"
        Me.txtNama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNama.Enabled = False
        Me.txtNama.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNama.Location = New System.Drawing.Point(138, 18)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(280, 22)
        Me.txtNama.TabIndex = 184
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(23, 18)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(109, 23)
        Me.LabelX16.TabIndex = 185
        Me.LabelX16.Text = "NAMA KARYAWAN"
        '
        'txtPanggilan
        '
        '
        '
        '
        Me.txtPanggilan.Border.Class = "TextBoxBorder"
        Me.txtPanggilan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPanggilan.Enabled = False
        Me.txtPanggilan.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPanggilan.Location = New System.Drawing.Point(138, 47)
        Me.txtPanggilan.Name = "txtPanggilan"
        Me.txtPanggilan.Size = New System.Drawing.Size(280, 22)
        Me.txtPanggilan.TabIndex = 186
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(23, 47)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(109, 23)
        Me.LabelX17.TabIndex = 187
        Me.LabelX17.Text = "NAMA PANGGILAN"
        '
        'txtKdUnitCabang
        '
        '
        '
        '
        Me.txtKdUnitCabang.Border.Class = "TextBoxBorder"
        Me.txtKdUnitCabang.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdUnitCabang.Enabled = False
        Me.txtKdUnitCabang.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKdUnitCabang.Location = New System.Drawing.Point(538, 198)
        Me.txtKdUnitCabang.Name = "txtKdUnitCabang"
        Me.txtKdUnitCabang.Size = New System.Drawing.Size(302, 22)
        Me.txtKdUnitCabang.TabIndex = 188
        Me.txtKdUnitCabang.Visible = False
        '
        'txtKdDepartemen
        '
        '
        '
        '
        Me.txtKdDepartemen.Border.Class = "TextBoxBorder"
        Me.txtKdDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdDepartemen.Enabled = False
        Me.txtKdDepartemen.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKdDepartemen.Location = New System.Drawing.Point(538, 224)
        Me.txtKdDepartemen.Name = "txtKdDepartemen"
        Me.txtKdDepartemen.Size = New System.Drawing.Size(302, 22)
        Me.txtKdDepartemen.TabIndex = 189
        Me.txtKdDepartemen.Visible = False
        '
        'txtKdJabatan
        '
        '
        '
        '
        Me.txtKdJabatan.Border.Class = "TextBoxBorder"
        Me.txtKdJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKdJabatan.Enabled = False
        Me.txtKdJabatan.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKdJabatan.Location = New System.Drawing.Point(538, 250)
        Me.txtKdJabatan.Name = "txtKdJabatan"
        Me.txtKdJabatan.Size = New System.Drawing.Size(302, 22)
        Me.txtKdJabatan.TabIndex = 190
        Me.txtKdJabatan.Visible = False
        '
        'txtIdUnit
        '
        '
        '
        '
        Me.txtIdUnit.Border.Class = "TextBoxBorder"
        Me.txtIdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdUnit.Enabled = False
        Me.txtIdUnit.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdUnit.Location = New System.Drawing.Point(894, 170)
        Me.txtIdUnit.Name = "txtIdUnit"
        Me.txtIdUnit.Size = New System.Drawing.Size(38, 22)
        Me.txtIdUnit.TabIndex = 191
        Me.txtIdUnit.Visible = False
        '
        'txtIdDepartemen
        '
        '
        '
        '
        Me.txtIdDepartemen.Border.Class = "TextBoxBorder"
        Me.txtIdDepartemen.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdDepartemen.Enabled = False
        Me.txtIdDepartemen.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdDepartemen.Location = New System.Drawing.Point(894, 198)
        Me.txtIdDepartemen.Name = "txtIdDepartemen"
        Me.txtIdDepartemen.Size = New System.Drawing.Size(38, 22)
        Me.txtIdDepartemen.TabIndex = 192
        Me.txtIdDepartemen.Visible = False
        '
        'txtIDJabatan
        '
        '
        '
        '
        Me.txtIDJabatan.Border.Class = "TextBoxBorder"
        Me.txtIDJabatan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIDJabatan.Enabled = False
        Me.txtIDJabatan.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDJabatan.Location = New System.Drawing.Point(894, 223)
        Me.txtIDJabatan.Name = "txtIDJabatan"
        Me.txtIDJabatan.Size = New System.Drawing.Size(38, 22)
        Me.txtIDJabatan.TabIndex = 193
        Me.txtIDJabatan.Visible = False
        '
        'btnJabatan
        '
        Me.btnJabatan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnJabatan.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnJabatan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnJabatan.Image = CType(resources.GetObject("btnJabatan.Image"), System.Drawing.Image)
        Me.btnJabatan.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnJabatan.Location = New System.Drawing.Point(846, 223)
        Me.btnJabatan.Name = "btnJabatan"
        Me.btnJabatan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnJabatan.Size = New System.Drawing.Size(47, 22)
        Me.btnJabatan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnJabatan.TabIndex = 181
        '
        'btnDept
        '
        Me.btnDept.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDept.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnDept.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnDept.Image = CType(resources.GetObject("btnDept.Image"), System.Drawing.Image)
        Me.btnDept.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDept.Location = New System.Drawing.Point(846, 196)
        Me.btnDept.Name = "btnDept"
        Me.btnDept.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnDept.Size = New System.Drawing.Size(47, 22)
        Me.btnDept.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnDept.TabIndex = 139
        '
        'btnUnit
        '
        Me.btnUnit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUnit.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnUnit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnUnit.Image = CType(resources.GetObject("btnUnit.Image"), System.Drawing.Image)
        Me.btnUnit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUnit.Location = New System.Drawing.Point(846, 170)
        Me.btnUnit.Name = "btnUnit"
        Me.btnUnit.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnUnit.Size = New System.Drawing.Size(45, 22)
        Me.btnUnit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnUnit.TabIndex = 33
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(840, 334)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(70, 72)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(764, 334)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(70, 72)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "SAVE"
        '
        'frmMutasi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(962, 459)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMutasi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMutasi"
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.dtTglMutasi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.txtTglJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents txtNikKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNilai As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnDept As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtTglJoin As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnUnit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtUnitBisnis As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKdKaryawan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNmJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnJabatan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtNmDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNmUnitCabang As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtTglMutasi As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cbKarir As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cbTipeKarir As System.Windows.Forms.ComboBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPanggilan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNama As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKdDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKdUnitCabang As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKdJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIDJabatan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdDepartemen As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdUnit As DevComponents.DotNetBar.Controls.TextBoxX
End Class
