﻿Public Class frmDepartemen
    Dim kk As Integer
    Dim kd, nm As String
    Sub ShowDataGrid()

        SQL = ""
        SQL = "select kode, nama, id from karyawan_struktur_organisasi where parent_id = '0' "
        SQL = SQL & " and nama like '%" & txtSearch.Text & "%' "
        SQL = SQL & " order by kode"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 150
        DGView.Columns(1).Width = 350
        DGView.Columns(2).Width = 80

        DGView.Columns(0).HeaderText = "KODE DEPARTEMEN"
        DGView.Columns(1).HeaderText = "NAMA DEPARTEMEN"
        DGView.Columns(2).HeaderText = "ID"
        DGView.Columns(2).Visible = False

        ColorChange()

    End Sub
    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next

    End Sub

    Private Sub frmDepartemen_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ShowDataGrid()
    End Sub
    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                kk = .Item("id", i).Value
                kd = .Item("kode", i).Value
                nm = .Item("nama", i).Value
            End With
        End If
    End Sub

    Private Sub DGView_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellDoubleClick
        TakeDGView()
        Select Case Me.Text
            Case "FORM MUTASI"
                frmMutasi.txtIdDepartemen.Text = kk
                frmMutasi.txtKdDepartemen.Text = kd
                frmMutasi.txtNmDepartemen.Text = nm
        End Select
        Me.Close()
    End Sub

    Private Sub btnCari_Click(sender As System.Object, e As System.EventArgs) Handles btnCari.Click
        Call ShowDataGrid()
    End Sub
End Class