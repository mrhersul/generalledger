﻿Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmRptNeraca
    Dim xythn
    Dim xythnBef

    Private Sub frmRptNeraca_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        txtDate.Value = Now
    End Sub

    Private Sub frmRptNeraca_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim objRpt As New CR_Neraca


        xythn = Format(txtDate.Value, "yyyy-MM-dd")
        xythnBef = Format(DateAdd(DateInterval.Year, -1, txtDate.Value), "yyyy-MM-dd")


        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
        SQL = SQL & " T1.parent_id IS NOT NULL )"
        SQL = SQL & " select m.tahunnow,m.namaunitbisnis,m.nama_perkiraan_group,m.subnama,m.kode,m.nama,m.jan,m.feb,m.mar,"
        SQL = SQL & " m.apr,m.mei,m.jun,m.jul,m.agt,m.sep,m.okt,m.nov,m.d3s,m.thn from"
        SQL = SQL & " (select f.unitbisnis_id," & Year(xythn) & " as tahunnow,'" & txtNamaUnit.Text & "' as namaunitbisnis,f.nama_perkiraan_group,f.subnama,f.kode,f.nama,sum(f.jan) as jan,sum(f.feb) as feb,sum(f.mar) as mar,"
        SQL = SQL & " sum(f.apr) as apr,sum(f.mei) as mei,sum(f.jun) as jun,sum(f.jul) as jul,sum(f.agt) as agt,sum(f.sep) as sep,"
        SQL = SQL & " sum(f.okt) as okt,sum(f.nov) as nov,sum(f.d3s) as d3s,sum(f.thn) as thn"
        SQL = SQL & " from"
        SQL = SQL & " (select x.unitbisnis_id,x.nama_perkiraan_group,z.nama as subnama,x.kode,x.nama,x.jan,x.feb,x.mar,x.apr,x.mei,x.jun,"
        SQL = SQL & "  x.jul, x.agt, x.sep, x.okt, x.nov, x.d3s, x.thn"
        SQL = SQL & " from"
        SQL = SQL & " (select a.unitbisnis_id,c.nama_perkiraan_group,b.kode,b.nama,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =1 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as jan,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =2 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as feb,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =3 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as mar,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =4 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as apr,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =5 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as mei,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =6 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as jun,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =7 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as jul,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =8 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as agt,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =9 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as sep,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =10 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as okt,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =11 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as nov,"
        SQL = SQL & " case when DATEPART(month, a.tanggal) =12 then sum(a.nilai_debet - a.nilai_kredit) else 0 end as d3s,"
        SQL = SQL & " 0 as thn,b.parent_id"
        SQL = SQL & " from acc_jurnal a"
        SQL = SQL & " left join acc_perkiraan_nama b on a.acc_coa_id=b.id"
        SQL = SQL & " left join acc_perkiraan_group c on b.acc_perkiraan_group_id=c.id "
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.acc_coa_id=d.id"
        SQL = SQL & " where datediff(year,a.tanggal,'" & xythn & "')=0 and d.neraca=1 "
        SQL = SQL & " group by a.unitbisnis_id,b.kode,b.nama,c.nama_perkiraan_group,DATEPART(month, a.tanggal),b.parent_id) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('AKTIVA', 'HUTANG')"
        SQL = SQL & " union all"
        SQL = SQL & " select x.unitbisnis_id,x.nama_perkiraan_group,z.nama as subnama,x.kode,x.nama,x.jan,x.feb,x.mar,x.apr,x.mei,x.jun,"
        SQL = SQL & " x.jul, x.agt, x.sep, x.okt, x.nov, x.d3s, x.thn"
        SQL = SQL & " from"
        SQL = SQL & " (select a.unitbisnis_id,c.nama_perkiraan_group,b.kode,b.nama,"
        SQL = SQL & " 0 as jan,0 as feb,0 as mar,0 as apr,0 as mei,0 as jun,0 as jul,0 as agt,0 as sep,0 as okt,0 as nov,0 as d3s,"
        SQL = SQL & " sum(a.nilai_debet - a.nilai_kredit) as thn,b.parent_id"
        SQL = SQL & " from acc_jurnal a"
        SQL = SQL & " left join acc_perkiraan_nama b on a.acc_coa_id=b.id"
        SQL = SQL & " left join acc_perkiraan_group c on b.acc_perkiraan_group_id=c.id"
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.acc_coa_id=d.id"
        SQL = SQL & " where datediff(year,a.tanggal,'" & xythnBef & "')=0 and d.neraca=1 "
        SQL = SQL & " group by a.unitbisnis_id,b.kode,b.nama,c.nama_perkiraan_group,b.parent_id) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('AKTIVA', 'HUTANG')) f"
        SQL = SQL & " group by f.unitbisnis_id,f.nama_perkiraan_group,f.subnama,f.kode,f.nama) as m"
        SQL = SQL & " where m.unitbisnis_id in (SELECT id FROM ParentChildCTE)"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_neraca").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_neraca").ImportRow(dr)
        Next
        'txtTahun()

        objRpt.SetDataSource(ds1.Tables("ds_neraca"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt

        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default


    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitRptNeraca = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub
End Class