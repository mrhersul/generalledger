Namespace SuperGridDemo
	Partial Public Class DemoMasterDetail
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DemoMasterDetail))
            Dim GridColumn1 As DevComponents.DotNetBar.SuperGrid.GridColumn = New DevComponents.DotNetBar.SuperGrid.GridColumn()
            Dim GridColumn2 As DevComponents.DotNetBar.SuperGrid.GridColumn = New DevComponents.DotNetBar.SuperGrid.GridColumn()
            Dim GridColumn3 As DevComponents.DotNetBar.SuperGrid.GridColumn = New DevComponents.DotNetBar.SuperGrid.GridColumn()
            Dim GridColumn4 As DevComponents.DotNetBar.SuperGrid.GridColumn = New DevComponents.DotNetBar.SuperGrid.GridColumn()
            Me.styleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
            Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
            Me.BubbleBar1 = New DevComponents.DotNetBar.BubbleBar()
            Me.BubbleBarTab1 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
            Me.BtnShow = New DevComponents.DotNetBar.BubbleButton()
            Me.BtnAdd = New DevComponents.DotNetBar.BubbleButton()
            Me.BtnEdit = New DevComponents.DotNetBar.BubbleButton()
            Me.BtnExcel = New DevComponents.DotNetBar.BubbleButton()
            Me.BtnClose = New DevComponents.DotNetBar.BubbleButton()
            Me.superGridControl1 = New DevComponents.DotNetBar.SuperGrid.SuperGridControl()
            Me.GroupPanel1.SuspendLayout()
            CType(Me.BubbleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'styleManager1
            '
            Me.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue
            Me.styleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(26, Byte), Integer)))
            '
            'GroupPanel1
            '
            Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
            Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
            Me.GroupPanel1.Controls.Add(Me.superGridControl1)
            Me.GroupPanel1.Controls.Add(Me.BubbleBar1)
            Me.GroupPanel1.Location = New System.Drawing.Point(12, 12)
            Me.GroupPanel1.Name = "GroupPanel1"
            Me.GroupPanel1.Size = New System.Drawing.Size(811, 526)
            '
            '
            '
            Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
            Me.GroupPanel1.Style.BackColorGradientAngle = 90
            Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
            Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.GroupPanel1.Style.BorderBottomWidth = 1
            Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
            Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.GroupPanel1.Style.BorderLeftWidth = 1
            Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.GroupPanel1.Style.BorderRightWidth = 1
            Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.GroupPanel1.Style.BorderTopWidth = 1
            Me.GroupPanel1.Style.CornerDiameter = 4
            Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
            Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
            Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
            Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
            '
            '
            '
            Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
            '
            '
            '
            Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
            Me.GroupPanel1.TabIndex = 8
            '
            'BubbleBar1
            '
            Me.BubbleBar1.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom
            Me.BubbleBar1.AntiAlias = True
            '
            '
            '
            Me.BubbleBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
            '
            '
            '
            Me.BubbleBar1.ButtonBackAreaStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
            Me.BubbleBar1.ButtonBackAreaStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.BubbleBar1.ButtonBackAreaStyle.BorderBottomWidth = 1
            Me.BubbleBar1.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
            Me.BubbleBar1.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.BubbleBar1.ButtonBackAreaStyle.BorderLeftWidth = 1
            Me.BubbleBar1.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.BubbleBar1.ButtonBackAreaStyle.BorderRightWidth = 1
            Me.BubbleBar1.ButtonBackAreaStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
            Me.BubbleBar1.ButtonBackAreaStyle.BorderTopWidth = 1
            Me.BubbleBar1.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
            Me.BubbleBar1.ButtonBackAreaStyle.PaddingBottom = 3
            Me.BubbleBar1.ButtonBackAreaStyle.PaddingLeft = 3
            Me.BubbleBar1.ButtonBackAreaStyle.PaddingRight = 3
            Me.BubbleBar1.ButtonBackAreaStyle.PaddingTop = 3
            Me.BubbleBar1.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.BubbleBar1.ImageSizeNormal = New System.Drawing.Size(24, 24)
            Me.BubbleBar1.Location = New System.Drawing.Point(0, 483)
            Me.BubbleBar1.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight
            Me.BubbleBar1.Name = "BubbleBar1"
            Me.BubbleBar1.SelectedTab = Me.BubbleBarTab1
            Me.BubbleBar1.SelectedTabColors.BorderColor = System.Drawing.Color.Black
            Me.BubbleBar1.Size = New System.Drawing.Size(805, 37)
            Me.BubbleBar1.TabIndex = 1
            Me.BubbleBar1.Tabs.Add(Me.BubbleBarTab1)
            '
            'BubbleBarTab1
            '
            Me.BubbleBarTab1.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(247, Byte), Integer))
            Me.BubbleBarTab1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(228, Byte), Integer))
            Me.BubbleBarTab1.Buttons.AddRange(New DevComponents.DotNetBar.BubbleButton() {Me.BtnShow, Me.BtnAdd, Me.BtnEdit, Me.BtnExcel, Me.BtnClose})
            Me.BubbleBarTab1.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
            Me.BubbleBarTab1.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.BubbleBarTab1.Name = "BubbleBarTab1"
            Me.BubbleBarTab1.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue
            Me.BubbleBarTab1.Text = ""
            Me.BubbleBarTab1.TextColor = System.Drawing.Color.Black
            '
            'BtnShow
            '
            Me.BtnShow.Image = CType(resources.GetObject("BtnShow.Image"), System.Drawing.Image)
            Me.BtnShow.ImageLarge = CType(resources.GetObject("BtnShow.ImageLarge"), System.Drawing.Image)
            Me.BtnShow.Name = "BtnShow"
            Me.BtnShow.TooltipText = "Show"
            '
            'BtnAdd
            '
            Me.BtnAdd.Image = CType(resources.GetObject("BtnAdd.Image"), System.Drawing.Image)
            Me.BtnAdd.ImageLarge = CType(resources.GetObject("BtnAdd.ImageLarge"), System.Drawing.Image)
            Me.BtnAdd.Name = "BtnAdd"
            Me.BtnAdd.TooltipText = "Add"
            '
            'BtnEdit
            '
            Me.BtnEdit.Image = CType(resources.GetObject("BtnEdit.Image"), System.Drawing.Image)
            Me.BtnEdit.ImageLarge = CType(resources.GetObject("BtnEdit.ImageLarge"), System.Drawing.Image)
            Me.BtnEdit.Name = "BtnEdit"
            Me.BtnEdit.TooltipText = "Edit"
            '
            'BtnExcel
            '
            Me.BtnExcel.Image = CType(resources.GetObject("BtnExcel.Image"), System.Drawing.Image)
            Me.BtnExcel.ImageLarge = CType(resources.GetObject("BtnExcel.ImageLarge"), System.Drawing.Image)
            Me.BtnExcel.Name = "BtnExcel"
            Me.BtnExcel.TooltipText = "Export To Excel"
            '
            'BtnClose
            '
            Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
            Me.BtnClose.ImageLarge = CType(resources.GetObject("BtnClose.ImageLarge"), System.Drawing.Image)
            Me.BtnClose.Name = "BtnClose"
            Me.BtnClose.TooltipText = "Close"
            '
            'superGridControl1
            '
            Me.superGridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.superGridControl1.FilterExprColors.SysFunction = System.Drawing.Color.DarkRed
            Me.superGridControl1.Location = New System.Drawing.Point(3, 3)
            Me.superGridControl1.Name = "superGridControl1"
            GridColumn1.HeaderText = "ID"
            GridColumn1.Name = ""
            GridColumn1.Width = 50
            GridColumn2.HeaderText = "KODE PERKIRAAN GROUP"
            GridColumn2.Name = ""
            GridColumn2.Width = 200
            GridColumn3.HeaderText = "DESKRIPSI"
            GridColumn3.Name = ""
            GridColumn3.Width = 300
            GridColumn4.HeaderText = "STATUS"
            GridColumn4.Name = ""
            GridColumn4.Width = 200
            Me.superGridControl1.PrimaryGrid.Columns.Add(GridColumn1)
            Me.superGridControl1.PrimaryGrid.Columns.Add(GridColumn2)
            Me.superGridControl1.PrimaryGrid.Columns.Add(GridColumn3)
            Me.superGridControl1.PrimaryGrid.Columns.Add(GridColumn4)
            Me.superGridControl1.PrimaryGrid.GroupByRow.Visible = True
            Me.superGridControl1.PrimaryGrid.Title.RowHeaderVisibility = DevComponents.DotNetBar.SuperGrid.RowHeaderVisibility.PanelControlled
            Me.superGridControl1.Size = New System.Drawing.Size(799, 404)
            Me.superGridControl1.TabIndex = 129
            Me.superGridControl1.Text = "superGridControl1"
            '
            'DemoMasterDetail
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(832, 549)
            Me.Controls.Add(Me.GroupPanel1)
            Me.EnableGlass = False
            Me.Name = "DemoMasterDetail"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "SuperGrid Demo - Master / Detail Sample"
            Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
            Me.GroupPanel1.ResumeLayout(False)
            CType(Me.BubbleBar1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

		#End Region

        Private styleManager1 As DevComponents.DotNetBar.StyleManager
        Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
        Friend WithEvents BubbleBar1 As DevComponents.DotNetBar.BubbleBar
        Friend WithEvents BubbleBarTab1 As DevComponents.DotNetBar.BubbleBarTab
        Friend WithEvents BtnShow As DevComponents.DotNetBar.BubbleButton
        Friend WithEvents BtnAdd As DevComponents.DotNetBar.BubbleButton
        Friend WithEvents BtnEdit As DevComponents.DotNetBar.BubbleButton
        Friend WithEvents BtnExcel As DevComponents.DotNetBar.BubbleButton
        Friend WithEvents BtnClose As DevComponents.DotNetBar.BubbleButton
        Private WithEvents superGridControl1 As DevComponents.DotNetBar.SuperGrid.SuperGridControl
	End Class
End Namespace