﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJurnalUmumBrowse
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJurnalUmumBrowse))
        Dim GridEX_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Me.BtnValidation = New DevComponents.DotNetBar.BubbleButton()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.GridEX = New Janus.Windows.GridEX.GridEX()
        Me.GroupPanel2 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.BtnSearch = New DevComponents.DotNetBar.ButtonX()
        Me.cboTrans = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtDate1 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtDate2 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtSearch = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnTampil = New DevComponents.DotNetBar.BubbleBar()
        Me.BubbleBarTab1 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
        Me.btnAdd2 = New DevComponents.DotNetBar.BubbleButton()
        Me.btnEdit = New DevComponents.DotNetBar.BubbleButton()
        Me.btnPrint = New DevComponents.DotNetBar.BubbleButton()
        Me.BtnClose = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleBarTab2 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
        Me.BtnAdd = New DevComponents.DotNetBar.BubbleButton()
        Me.DataSetJurnal = New MyGL.DataSetJurnal()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.GridEX, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPanel2.SuspendLayout()
        CType(Me.txtDate1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnTampil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetJurnal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtnValidation
        '
        Me.BtnValidation.Image = CType(resources.GetObject("BtnValidation.Image"), System.Drawing.Image)
        Me.BtnValidation.ImageLarge = CType(resources.GetObject("BtnValidation.ImageLarge"), System.Drawing.Image)
        Me.BtnValidation.Name = "BtnValidation"
        Me.BtnValidation.TooltipText = "Validation"
        '
        'GroupPanel1
        '
        Me.GroupPanel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2000
        Me.GroupPanel1.Controls.Add(Me.GridEX)
        Me.GroupPanel1.Controls.Add(Me.GroupPanel2)
        Me.GroupPanel1.Controls.Add(Me.btnTampil)
        Me.GroupPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupPanel1.Location = New System.Drawing.Point(0, 0)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1103, 741)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 11
        '
        'GridEX
        '
        GridEX_DesignTimeLayout.LayoutString = resources.GetString("GridEX_DesignTimeLayout.LayoutString")
        Me.GridEX.DesignTimeLayout = GridEX_DesignTimeLayout
        Me.GridEX.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX.Hierarchical = True
        Me.GridEX.Location = New System.Drawing.Point(9, 52)
        Me.GridEX.Name = "GridEX"
        Me.GridEX.ScrollBarWidth = 17
        Me.GridEX.Size = New System.Drawing.Size(961, 385)
        Me.GridEX.TabIndex = 134
        Me.GridEX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'GroupPanel2
        '
        Me.GroupPanel2.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP
        Me.GroupPanel2.Controls.Add(Me.BtnSearch)
        Me.GroupPanel2.Controls.Add(Me.cboTrans)
        Me.GroupPanel2.Controls.Add(Me.LabelX4)
        Me.GroupPanel2.Controls.Add(Me.LabelX3)
        Me.GroupPanel2.Controls.Add(Me.txtDate1)
        Me.GroupPanel2.Controls.Add(Me.txtDate2)
        Me.GroupPanel2.Controls.Add(Me.LabelX2)
        Me.GroupPanel2.Controls.Add(Me.LabelX1)
        Me.GroupPanel2.Controls.Add(Me.txtSearch)
        Me.GroupPanel2.Location = New System.Drawing.Point(9, 3)
        Me.GroupPanel2.Name = "GroupPanel2"
        Me.GroupPanel2.Size = New System.Drawing.Size(1044, 34)
        '
        '
        '
        Me.GroupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel2.Style.BackColorGradientAngle = 90
        Me.GroupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderBottomWidth = 1
        Me.GroupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderLeftWidth = 1
        Me.GroupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderRightWidth = 1
        Me.GroupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderTopWidth = 1
        Me.GroupPanel2.Style.CornerDiameter = 4
        Me.GroupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel2.TabIndex = 128
        '
        'BtnSearch
        '
        Me.BtnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSearch.Location = New System.Drawing.Point(958, 2)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(75, 23)
        Me.BtnSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSearch.TabIndex = 139
        Me.BtnSearch.Text = "Search"
        '
        'cboTrans
        '
        Me.cboTrans.DisplayMember = "Text"
        Me.cboTrans.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboTrans.FormattingEnabled = True
        Me.cboTrans.ItemHeight = 14
        Me.cboTrans.Location = New System.Drawing.Point(457, 5)
        Me.cboTrans.Name = "cboTrans"
        Me.cboTrans.Size = New System.Drawing.Size(191, 20)
        Me.cboTrans.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cboTrans.TabIndex = 138
        '
        'LabelX4
        '
        Me.LabelX4.AutoSize = True
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.White
        Me.LabelX4.Location = New System.Drawing.Point(352, 6)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(99, 15)
        Me.LabelX4.TabIndex = 137
        Me.LabelX4.Text = "JENIS TRANSAKSI"
        '
        'LabelX3
        '
        Me.LabelX3.AutoSize = True
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.White
        Me.LabelX3.Location = New System.Drawing.Point(199, 6)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(21, 15)
        Me.LabelX3.TabIndex = 136
        Me.LabelX3.Text = "S/D"
        '
        'txtDate1
        '
        '
        '
        '
        Me.txtDate1.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate1.ButtonDropDown.Visible = True
        Me.txtDate1.IsPopupCalendarOpen = False
        Me.txtDate1.Location = New System.Drawing.Point(68, 5)
        '
        '
        '
        Me.txtDate1.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate1.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate1.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate1.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.MonthCalendar.TodayButtonVisible = True
        Me.txtDate1.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate1.Name = "txtDate1"
        Me.txtDate1.Size = New System.Drawing.Size(125, 20)
        Me.txtDate1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate1.TabIndex = 135
        '
        'txtDate2
        '
        '
        '
        '
        Me.txtDate2.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate2.ButtonDropDown.Visible = True
        Me.txtDate2.IsPopupCalendarOpen = False
        Me.txtDate2.Location = New System.Drawing.Point(226, 5)
        '
        '
        '
        Me.txtDate2.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate2.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate2.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate2.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate2.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.MonthCalendar.TodayButtonVisible = True
        Me.txtDate2.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate2.Name = "txtDate2"
        Me.txtDate2.Size = New System.Drawing.Size(120, 20)
        Me.txtDate2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate2.TabIndex = 132
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.White
        Me.LabelX2.Location = New System.Drawing.Point(6, 2)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(56, 23)
        Me.LabelX2.TabIndex = 131
        Me.LabelX2.Text = "PERIODE"
        '
        'LabelX1
        '
        Me.LabelX1.AutoSize = True
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.White
        Me.LabelX1.Location = New System.Drawing.Point(654, 6)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(84, 15)
        Me.LabelX1.TabIndex = 130
        Me.LabelX1.Text = "NO TRANSAKSI"
        '
        'txtSearch
        '
        '
        '
        '
        Me.txtSearch.Border.Class = "TextBoxBorder"
        Me.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSearch.Location = New System.Drawing.Point(744, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(208, 20)
        Me.txtSearch.TabIndex = 125
        '
        'btnTampil
        '
        Me.btnTampil.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom
        Me.btnTampil.AntiAlias = True
        '
        '
        '
        Me.btnTampil.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.btnTampil.ButtonBackAreaStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.btnTampil.ButtonBackAreaStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.btnTampil.ButtonBackAreaStyle.BorderBottomWidth = 1
        Me.btnTampil.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.btnTampil.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.btnTampil.ButtonBackAreaStyle.BorderLeftWidth = 1
        Me.btnTampil.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.btnTampil.ButtonBackAreaStyle.BorderRightWidth = 1
        Me.btnTampil.ButtonBackAreaStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.btnTampil.ButtonBackAreaStyle.BorderTopWidth = 1
        Me.btnTampil.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.btnTampil.ButtonBackAreaStyle.PaddingBottom = 3
        Me.btnTampil.ButtonBackAreaStyle.PaddingLeft = 3
        Me.btnTampil.ButtonBackAreaStyle.PaddingRight = 3
        Me.btnTampil.ButtonBackAreaStyle.PaddingTop = 3
        Me.btnTampil.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.btnTampil.ImageSizeNormal = New System.Drawing.Size(24, 24)
        Me.btnTampil.Location = New System.Drawing.Point(0, 698)
        Me.btnTampil.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight
        Me.btnTampil.Name = "btnTampil"
        Me.btnTampil.SelectedTab = Me.BubbleBarTab1
        Me.btnTampil.SelectedTabColors.BorderColor = System.Drawing.Color.Black
        Me.btnTampil.Size = New System.Drawing.Size(1097, 37)
        Me.btnTampil.TabIndex = 1
        Me.btnTampil.Tabs.Add(Me.BubbleBarTab1)
        Me.btnTampil.Tabs.Add(Me.BubbleBarTab2)
        '
        'BubbleBarTab1
        '
        Me.BubbleBarTab1.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BubbleBarTab1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.BubbleBarTab1.Buttons.AddRange(New DevComponents.DotNetBar.BubbleButton() {Me.btnAdd2, Me.btnEdit, Me.btnPrint, Me.BtnClose})
        Me.BubbleBarTab1.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab1.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BubbleBarTab1.Name = "BubbleBarTab1"
        Me.BubbleBarTab1.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue
        Me.BubbleBarTab1.Text = ""
        Me.BubbleBarTab1.TextColor = System.Drawing.Color.Black
        '
        'btnAdd2
        '
        Me.btnAdd2.Image = CType(resources.GetObject("btnAdd2.Image"), System.Drawing.Image)
        Me.btnAdd2.ImageLarge = CType(resources.GetObject("btnAdd2.ImageLarge"), System.Drawing.Image)
        Me.btnAdd2.Name = "btnAdd2"
        '
        'btnEdit
        '
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageLarge = CType(resources.GetObject("btnEdit.ImageLarge"), System.Drawing.Image)
        Me.btnEdit.Name = "btnEdit"
        '
        'btnPrint
        '
        Me.btnPrint.Image = CType(resources.GetObject("btnPrint.Image"), System.Drawing.Image)
        Me.btnPrint.ImageLarge = CType(resources.GetObject("btnPrint.ImageLarge"), System.Drawing.Image)
        Me.btnPrint.Name = "btnPrint"
        '
        'BtnClose
        '
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImageLarge = CType(resources.GetObject("BtnClose.ImageLarge"), System.Drawing.Image)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.TooltipText = "Close"
        '
        'BubbleBarTab2
        '
        Me.BubbleBarTab2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.BubbleBarTab2.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab2.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab2.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BubbleBarTab2.Name = "BubbleBarTab2"
        Me.BubbleBarTab2.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Yellow
        Me.BubbleBarTab2.Text = "BubbleBarTab2"
        Me.BubbleBarTab2.TextColor = System.Drawing.Color.Black
        '
        'BtnAdd
        '
        Me.BtnAdd.Image = CType(resources.GetObject("BtnAdd.Image"), System.Drawing.Image)
        Me.BtnAdd.ImageLarge = CType(resources.GetObject("BtnAdd.ImageLarge"), System.Drawing.Image)
        Me.BtnAdd.Name = "BtnAdd"
        Me.BtnAdd.TooltipText = "Add"
        '
        'DataSetJurnal
        '
        Me.DataSetJurnal.DataSetName = "DataSetJurnal"
        Me.DataSetJurnal.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmJurnalUmumBrowse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1103, 741)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmJurnalUmumBrowse"
        Me.Text = "Jurnal Umum - Browse"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.GridEX, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPanel2.ResumeLayout(False)
        Me.GroupPanel2.PerformLayout()
        CType(Me.txtDate1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnTampil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetJurnal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnValidation As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GridEX As Janus.Windows.GridEX.GridEX
    Friend WithEvents GroupPanel2 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents txtDate2 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSearch As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnTampil As DevComponents.DotNetBar.BubbleBar
    Friend WithEvents BubbleBarTab1 As DevComponents.DotNetBar.BubbleBarTab
    Friend WithEvents BtnClose As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents DataSetJurnal As MyGL.DataSetJurnal
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDate1 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents btnAdd2 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnEdit As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleBarTab2 As DevComponents.DotNetBar.BubbleBarTab
    Friend WithEvents BtnAdd As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnPrint As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cboTrans As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents BtnSearch As DevComponents.DotNetBar.ButtonX
End Class
