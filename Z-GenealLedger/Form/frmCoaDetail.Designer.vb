﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCoaDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCoaDetail))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtIdCoa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSep = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKodeK = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.RBD = New System.Windows.Forms.RadioButton()
        Me.RBH = New System.Windows.Forms.RadioButton()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtKet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtIdParent = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIdGroup = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnParent = New DevComponents.DotNetBar.ButtonX()
        Me.txtNamaParent = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtkdParent = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtnamagroup = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btngroup = New DevComponents.DotNetBar.ButtonX()
        Me.txtkdgroup = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaCoa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.ChkStatus = New System.Windows.Forms.CheckBox()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.txtNoCoa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.cboCategory = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.GroupPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.txtIdCoa)
        Me.GroupPanel1.Controls.Add(Me.txtSep)
        Me.GroupPanel1.Controls.Add(Me.txtKodeK)
        Me.GroupPanel1.Controls.Add(Me.LabelX10)
        Me.GroupPanel1.Controls.Add(Me.LabelX7)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.LabelX9)
        Me.GroupPanel1.Controls.Add(Me.RBD)
        Me.GroupPanel1.Controls.Add(Me.RBH)
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.txtKet)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.txtIdParent)
        Me.GroupPanel1.Controls.Add(Me.txtIdGroup)
        Me.GroupPanel1.Controls.Add(Me.btnParent)
        Me.GroupPanel1.Controls.Add(Me.txtNamaParent)
        Me.GroupPanel1.Controls.Add(Me.txtkdParent)
        Me.GroupPanel1.Controls.Add(Me.txtnamagroup)
        Me.GroupPanel1.Controls.Add(Me.btngroup)
        Me.GroupPanel1.Controls.Add(Me.txtkdgroup)
        Me.GroupPanel1.Controls.Add(Me.txtNamaCoa)
        Me.GroupPanel1.Controls.Add(Me.LabelX12)
        Me.GroupPanel1.Controls.Add(Me.ChkStatus)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.txtNoCoa)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 12)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(780, 345)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 2
        '
        'txtIdCoa
        '
        '
        '
        '
        Me.txtIdCoa.Border.Class = "TextBoxBorder"
        Me.txtIdCoa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdCoa.Location = New System.Drawing.Point(705, 198)
        Me.txtIdCoa.Name = "txtIdCoa"
        Me.txtIdCoa.Size = New System.Drawing.Size(37, 20)
        Me.txtIdCoa.TabIndex = 54
        Me.txtIdCoa.Visible = False
        '
        'txtSep
        '
        '
        '
        '
        Me.txtSep.Border.Class = "TextBoxBorder"
        Me.txtSep.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSep.Enabled = False
        Me.txtSep.Location = New System.Drawing.Point(127, 174)
        Me.txtSep.Name = "txtSep"
        Me.txtSep.Size = New System.Drawing.Size(51, 20)
        Me.txtSep.TabIndex = 53
        Me.txtSep.Text = "-"
        Me.txtSep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtKodeK
        '
        '
        '
        '
        Me.txtKodeK.Border.Class = "TextBoxBorder"
        Me.txtKodeK.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKodeK.Enabled = False
        Me.txtKodeK.Location = New System.Drawing.Point(38, 174)
        Me.txtKodeK.Name = "txtKodeK"
        Me.txtKodeK.Size = New System.Drawing.Size(78, 20)
        Me.txtKodeK.TabIndex = 52
        Me.txtKodeK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Location = New System.Drawing.Point(278, 139)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(105, 23)
        Me.LabelX10.TabIndex = 51
        Me.LabelX10.Text = "Nama"
        '
        'LabelX7
        '
        Me.LabelX7.AutoSize = True
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Location = New System.Drawing.Point(188, 143)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(28, 15)
        Me.LabelX7.TabIndex = 50
        Me.LabelX7.Text = "Kode"
        '
        'LabelX2
        '
        Me.LabelX2.AutoSize = True
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(122, 143)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(51, 15)
        Me.LabelX2.TabIndex = 49
        Me.LabelX2.Text = "Separator"
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Location = New System.Drawing.Point(35, 101)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(105, 23)
        Me.LabelX9.TabIndex = 48
        Me.LabelX9.Text = "PARENT"
        '
        'RBD
        '
        Me.RBD.AutoSize = True
        Me.RBD.BackColor = System.Drawing.Color.Transparent
        Me.RBD.Checked = True
        Me.RBD.Location = New System.Drawing.Point(295, 19)
        Me.RBD.Name = "RBD"
        Me.RBD.Size = New System.Drawing.Size(63, 17)
        Me.RBD.TabIndex = 45
        Me.RBD.TabStop = True
        Me.RBD.Text = "DETAIL"
        Me.RBD.UseVisualStyleBackColor = False
        '
        'RBH
        '
        Me.RBH.AutoSize = True
        Me.RBH.BackColor = System.Drawing.Color.Transparent
        Me.RBH.Location = New System.Drawing.Point(157, 19)
        Me.RBH.Name = "RBH"
        Me.RBH.Size = New System.Drawing.Size(70, 17)
        Me.RBH.TabIndex = 44
        Me.RBH.Text = "HEADER"
        Me.RBH.UseVisualStyleBackColor = False
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(32, 16)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(105, 23)
        Me.LabelX3.TabIndex = 43
        Me.LabelX3.Text = "TIPE PERKIRAAN"
        '
        'txtKet
        '
        '
        '
        '
        Me.txtKet.Border.Class = "TextBoxBorder"
        Me.txtKet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKet.Location = New System.Drawing.Point(158, 211)
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(516, 20)
        Me.txtKet.TabIndex = 6
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(33, 211)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(105, 23)
        Me.LabelX5.TabIndex = 42
        Me.LabelX5.Text = "KETERANGAN"
        '
        'txtIdParent
        '
        '
        '
        '
        Me.txtIdParent.Border.Class = "TextBoxBorder"
        Me.txtIdParent.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdParent.Location = New System.Drawing.Point(608, 104)
        Me.txtIdParent.Name = "txtIdParent"
        Me.txtIdParent.Size = New System.Drawing.Size(37, 20)
        Me.txtIdParent.TabIndex = 39
        Me.txtIdParent.Visible = False
        '
        'txtIdGroup
        '
        '
        '
        '
        Me.txtIdGroup.Border.Class = "TextBoxBorder"
        Me.txtIdGroup.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdGroup.Location = New System.Drawing.Point(487, 60)
        Me.txtIdGroup.Name = "txtIdGroup"
        Me.txtIdGroup.Size = New System.Drawing.Size(39, 20)
        Me.txtIdGroup.TabIndex = 38
        Me.txtIdGroup.Visible = False
        '
        'btnParent
        '
        Me.btnParent.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnParent.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnParent.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnParent.Image = CType(resources.GetObject("btnParent.Image"), System.Drawing.Image)
        Me.btnParent.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnParent.Location = New System.Drawing.Point(568, 104)
        Me.btnParent.Name = "btnParent"
        Me.btnParent.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnParent.Size = New System.Drawing.Size(34, 20)
        Me.btnParent.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnParent.TabIndex = 5
        '
        'txtNamaParent
        '
        '
        '
        '
        Me.txtNamaParent.Border.Class = "TextBoxBorder"
        Me.txtNamaParent.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaParent.Enabled = False
        Me.txtNamaParent.Location = New System.Drawing.Point(248, 104)
        Me.txtNamaParent.Name = "txtNamaParent"
        Me.txtNamaParent.Size = New System.Drawing.Size(324, 20)
        Me.txtNamaParent.TabIndex = 36
        '
        'txtkdParent
        '
        '
        '
        '
        Me.txtkdParent.Border.Class = "TextBoxBorder"
        Me.txtkdParent.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtkdParent.Enabled = False
        Me.txtkdParent.Location = New System.Drawing.Point(160, 104)
        Me.txtkdParent.Name = "txtkdParent"
        Me.txtkdParent.Size = New System.Drawing.Size(82, 20)
        Me.txtkdParent.TabIndex = 35
        '
        'txtnamagroup
        '
        '
        '
        '
        Me.txtnamagroup.Border.Class = "TextBoxBorder"
        Me.txtnamagroup.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtnamagroup.Enabled = False
        Me.txtnamagroup.Location = New System.Drawing.Point(248, 60)
        Me.txtnamagroup.Name = "txtnamagroup"
        Me.txtnamagroup.Size = New System.Drawing.Size(202, 20)
        Me.txtnamagroup.TabIndex = 34
        '
        'btngroup
        '
        Me.btngroup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btngroup.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btngroup.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btngroup.Image = CType(resources.GetObject("btngroup.Image"), System.Drawing.Image)
        Me.btngroup.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btngroup.Location = New System.Drawing.Point(447, 60)
        Me.btngroup.Name = "btngroup"
        Me.btngroup.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btngroup.Size = New System.Drawing.Size(34, 20)
        Me.btngroup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btngroup.TabIndex = 0
        '
        'txtkdgroup
        '
        '
        '
        '
        Me.txtkdgroup.Border.Class = "TextBoxBorder"
        Me.txtkdgroup.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtkdgroup.Enabled = False
        Me.txtkdgroup.Location = New System.Drawing.Point(160, 60)
        Me.txtkdgroup.Name = "txtkdgroup"
        Me.txtkdgroup.Size = New System.Drawing.Size(82, 20)
        Me.txtkdgroup.TabIndex = 29
        '
        'txtNamaCoa
        '
        '
        '
        '
        Me.txtNamaCoa.Border.Class = "TextBoxBorder"
        Me.txtNamaCoa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaCoa.Location = New System.Drawing.Point(278, 172)
        Me.txtNamaCoa.Name = "txtNamaCoa"
        Me.txtNamaCoa.Size = New System.Drawing.Size(472, 20)
        Me.txtNamaCoa.TabIndex = 3
        '
        'LabelX12
        '
        Me.LabelX12.AutoSize = True
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Location = New System.Drawing.Point(33, 143)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(38, 15)
        Me.LabelX12.TabIndex = 28
        Me.LabelX12.Text = "Kode K"
        '
        'ChkStatus
        '
        Me.ChkStatus.AutoSize = True
        Me.ChkStatus.BackColor = System.Drawing.Color.Transparent
        Me.ChkStatus.Location = New System.Drawing.Point(158, 254)
        Me.ChkStatus.Name = "ChkStatus"
        Me.ChkStatus.Size = New System.Drawing.Size(56, 17)
        Me.ChkStatus.TabIndex = 7
        Me.ChkStatus.Text = "Active"
        Me.ChkStatus.UseVisualStyleBackColor = False
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(33, 250)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(105, 23)
        Me.LabelX6.TabIndex = 14
        Me.LabelX6.Text = "STATUS"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(680, 254)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(70, 72)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 9
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(604, 254)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(70, 72)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 8
        Me.BtnSave.Text = "SAVE"
        '
        'txtNoCoa
        '
        '
        '
        '
        Me.txtNoCoa.Border.Class = "TextBoxBorder"
        Me.txtNoCoa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNoCoa.Location = New System.Drawing.Point(188, 172)
        Me.txtNoCoa.MaxLength = 4
        Me.txtNoCoa.Name = "txtNoCoa"
        Me.txtNoCoa.Size = New System.Drawing.Size(84, 20)
        Me.txtNoCoa.TabIndex = 2
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(35, 60)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(105, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "KELOMPOK AKUN"
        '
        'cboCategory
        '
        Me.cboCategory.DisplayMember = "Text"
        Me.cboCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.ItemHeight = 14
        Me.cboCategory.Location = New System.Drawing.Point(161, 23)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(199, 20)
        Me.cboCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cboCategory.TabIndex = 13
        '
        'frmCoaDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(806, 372)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmCoaDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCoaDetail"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ChkStatus As System.Windows.Forms.CheckBox
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtNoCoa As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNamaCoa As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cboCategory As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents txtkdgroup As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btngroup As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtnamagroup As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnParent As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtNamaParent As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtkdParent As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdGroup As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdParent As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents RBD As System.Windows.Forms.RadioButton
    Friend WithEvents RBH As System.Windows.Forms.RadioButton
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSep As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKodeK As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtIdCoa As DevComponents.DotNetBar.Controls.TextBoxX
End Class
