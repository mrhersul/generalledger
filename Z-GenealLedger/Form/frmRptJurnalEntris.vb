﻿Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmRptJurnalEntris
    Dim xytgl1
    Dim xytgl2
    Dim xygabtgl

    Private Sub frmRptJurnalEntris_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        callLogika
    End Sub

    Sub callLogika()
        blnRptKasMasuk = False
        blnRptKasKeluar = False
        blnRptBankMasuk = False
        blnRptBankKeluar = False
    End Sub

    Private Sub frmRptJurnalEntris_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        txtDate1.Value = Now
        txtDate2.Value = Now

        cboTipe2.Text = ""
        cboTipe2.Items.Clear()
        If blnRptKasMasuk Then
            cboTipe2.Items.Add("KAS MASUK CABANG")
            cboTipe2.Items.Add("KAS MASUK PERANTARA")
        ElseIf blnRptKasKeluar Then
            cboTipe2.Items.Add("KAS KELUAR CABANG")
            cboTipe2.Items.Add("KAS KELUAR PERANTARA")
        ElseIf blnRptBankMasuk Then
            cboTipe2.Items.Add("BANK MASUK")
        ElseIf blnRptBankKeluar Then
            cboTipe2.Items.Add("BANK KELUAR")
        End If
    End Sub

    Private Sub frmRptJurnalEntris_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim objRpt As New CR_JurnalEntris

        xytgl1 = Format(txtDate1.Value, "yyyy-MM-dd")
        xytgl2 = Format(txtDate2.Value, "yyyy-MM-dd")
        xygabtgl = Format(txtDate1.Value, "dd/MM/yyyy") & " S/D " & Format(txtDate2.Value, "dd/MM/yyyy")

        If blnRptKasMasuk Then
            SQL = ""
            SQL = " WITH ParentChildCTE AS ("
            SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
            SQL = SQL & " UNION ALL"
            SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
            SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
            SQL = SQL & " T1.parent_id IS NOT NULL )"
            SQL = SQL & " select '" & cboTipe2.Text & "' as tipe_trans,'" & xygabtgl & "' as periode,'" & txtNamaUnit.Text & "' as namaunit,x.kode,x.nama,sum(x.debet) as debet, sum(x.credit) as credit from"
            SQL = SQL & " (select a.unitbisnis_id,c.kode,c.nama,sum(b.debet) as debet, sum(b.credit) as credit"
            SQL = SQL & " from acc_jurnal_hd a inner join acc_jurnal_dt b on a.id=b.header_id"
            SQL = SQL & " left join acc_perkiraan_nama c on b.acc_kode=c.id"
            SQL = SQL & " where a.tipe_transaksi='KAS MASUK' and a.tipe_transaksi2='" & cboTipe2.Text & "' and tgl_transaksi between '" & xytgl1 & "' and '" & xytgl2 & "'"
            SQL = SQL & " group by a.unitbisnis_id,c.kode,c.nama) as x"
            SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"
            SQL = SQL & " group by x.kode,x.nama"

        ElseIf blnRptKasKeluar Then
            SQL = ""
            SQL = " WITH ParentChildCTE AS ("
            SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
            SQL = SQL & " UNION ALL"
            SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
            SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
            SQL = SQL & " T1.parent_id IS NOT NULL )"
            SQL = SQL & " select '" & cboTipe2.Text & "' as tipe_trans,'" & xygabtgl & "' as periode,'" & txtNamaUnit.Text & "' as namaunit,x.kode,x.nama,sum(x.debet) as debet, sum(x.credit) as credit from"
            SQL = SQL & " (select a.unitbisnis_id,c.kode,c.nama,sum(b.debet) as debet, sum(b.credit) as credit"
            SQL = SQL & " from acc_jurnal_hd a inner join acc_jurnal_dt b on a.id=b.header_id"
            SQL = SQL & " left join acc_perkiraan_nama c on b.acc_kode=c.id"
            SQL = SQL & " where a.tipe_transaksi='KAS KELUAR' and a.tipe_transaksi2='" & cboTipe2.Text & "' and tgl_transaksi between '" & xytgl1 & "' and '" & xytgl2 & "'"
            SQL = SQL & " group by a.unitbisnis_id,c.kode,c.nama) as x"
            SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"
            SQL = SQL & " group by x.kode,x.nama"


        ElseIf blnRptBankMasuk Then
            SQL = ""
            SQL = " WITH ParentChildCTE AS ("
            SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
            SQL = SQL & " UNION ALL"
            SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
            SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
            SQL = SQL & " T1.parent_id IS NOT NULL )"
            SQL = SQL & " select '" & cboTipe2.Text & "' as tipe_trans,'" & xygabtgl & "' as periode,'" & txtNamaUnit.Text & "' as namaunit,x.kode,x.nama,sum(x.debet) as debet, sum(x.credit) as credit from"
            SQL = SQL & " (select a.unitbisnis_id,c.kode,c.nama,sum(b.debet) as debet, sum(b.credit) as credit"
            SQL = SQL & " from acc_jurnal_hd a inner join acc_jurnal_dt b on a.id=b.header_id"
            SQL = SQL & " left join acc_perkiraan_nama c on b.acc_kode=c.id"
            SQL = SQL & " where a.tipe_transaksi='BANK MASUK' and a.tipe_transaksi2='" & cboTipe2.Text & "' and tgl_transaksi between '" & xytgl1 & "' and '" & xytgl2 & "'"
            SQL = SQL & " group by a.unitbisnis_id,c.kode,c.nama) as x"
            SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"
            SQL = SQL & " group by x.kode,x.nama"

        ElseIf blnRptBankKeluar Then
            SQL = ""
            SQL = " WITH ParentChildCTE AS ("
            SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
            SQL = SQL & " UNION ALL"
            SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
            SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
            SQL = SQL & " T1.parent_id IS NOT NULL )"
            SQL = SQL & " select '" & cboTipe2.Text & "' as tipe_trans,'" & xygabtgl & "' as periode,'" & txtNamaUnit.Text & "' as namaunit,x.kode,x.nama,sum(x.debet) as debet, sum(x.credit) as credit from"
            SQL = SQL & " (select a.unitbisnis_id,c.kode,c.nama,sum(b.debet) as debet, sum(b.credit) as credit"
            SQL = SQL & " from acc_jurnal_hd a inner join acc_jurnal_dt b on a.id=b.header_id"
            SQL = SQL & " left join acc_perkiraan_nama c on b.acc_kode=c.id"
            SQL = SQL & " where a.tipe_transaksi='BANK KELUAR' and a.tipe_transaksi2='" & cboTipe2.Text & "' and tgl_transaksi between '" & xytgl1 & "' and '" & xytgl2 & "'"
            SQL = SQL & " group by a.unitbisnis_id,c.kode,c.nama) as x"
            SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"
            SQL = SQL & " group by x.kode,x.nama"

        End If


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_jurnalentris").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_jurnalentris").ImportRow(dr)
        Next

        objRpt.SetDataSource(ds1.Tables("ds_jurnalentris"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt

        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default


    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitRptNeraca = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        callLogika()
        Close()
    End Sub

End Class