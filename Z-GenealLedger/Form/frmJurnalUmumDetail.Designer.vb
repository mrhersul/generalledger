﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJurnalUmumDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJurnalUmumDetail))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtIdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnUnit = New DevComponents.DotNetBar.ButtonX()
        Me.txtkdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNoTrans = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.DGView = New System.Windows.Forms.DataGridView()
        Me.no_perkiraan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.nama_perkiraan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dk = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.nominal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hapus = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.id_coa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBalance = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtCredit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtDebet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDate = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtKet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtgroup = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaCoa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.txtIdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNamaUnit)
        Me.GroupPanel1.Controls.Add(Me.btnUnit)
        Me.GroupPanel1.Controls.Add(Me.txtkdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNoTrans)
        Me.GroupPanel1.Controls.Add(Me.DGView)
        Me.GroupPanel1.Controls.Add(Me.txtBalance)
        Me.GroupPanel1.Controls.Add(Me.LabelX7)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.txtCredit)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.txtDebet)
        Me.GroupPanel1.Controls.Add(Me.txtDate)
        Me.GroupPanel1.Controls.Add(Me.txtKet)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 12)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1025, 586)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 5
        Me.GroupPanel1.Text = "JURNAL UMUM"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.White
        Me.LabelX3.Location = New System.Drawing.Point(14, 61)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(114, 23)
        Me.LabelX3.TabIndex = 160
        Me.LabelX3.Text = "NAMA UNIT BISNIS"
        '
        'txtIdUnit
        '
        '
        '
        '
        Me.txtIdUnit.Border.Class = "TextBoxBorder"
        Me.txtIdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdUnit.Location = New System.Drawing.Point(799, 60)
        Me.txtIdUnit.Name = "txtIdUnit"
        Me.txtIdUnit.Size = New System.Drawing.Size(37, 22)
        Me.txtIdUnit.TabIndex = 159
        Me.txtIdUnit.Visible = False
        '
        'txtNamaUnit
        '
        '
        '
        '
        Me.txtNamaUnit.Border.Class = "TextBoxBorder"
        Me.txtNamaUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaUnit.Enabled = False
        Me.txtNamaUnit.Location = New System.Drawing.Point(395, 60)
        Me.txtNamaUnit.Name = "txtNamaUnit"
        Me.txtNamaUnit.Size = New System.Drawing.Size(353, 22)
        Me.txtNamaUnit.TabIndex = 158
        '
        'btnUnit
        '
        Me.btnUnit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUnit.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnUnit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnUnit.Image = CType(resources.GetObject("btnUnit.Image"), System.Drawing.Image)
        Me.btnUnit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUnit.Location = New System.Drawing.Point(748, 60)
        Me.btnUnit.Name = "btnUnit"
        Me.btnUnit.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnUnit.Size = New System.Drawing.Size(45, 22)
        Me.btnUnit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnUnit.TabIndex = 156
        '
        'txtkdUnit
        '
        '
        '
        '
        Me.txtkdUnit.Border.Class = "TextBoxBorder"
        Me.txtkdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtkdUnit.Enabled = False
        Me.txtkdUnit.Location = New System.Drawing.Point(148, 60)
        Me.txtkdUnit.Name = "txtkdUnit"
        Me.txtkdUnit.Size = New System.Drawing.Size(241, 22)
        Me.txtkdUnit.TabIndex = 155
        '
        'txtNoTrans
        '
        '
        '
        '
        Me.txtNoTrans.Border.Class = "TextBoxBorder"
        Me.txtNoTrans.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNoTrans.Enabled = False
        Me.txtNoTrans.Location = New System.Drawing.Point(148, 4)
        Me.txtNoTrans.Name = "txtNoTrans"
        Me.txtNoTrans.Size = New System.Drawing.Size(439, 22)
        Me.txtNoTrans.TabIndex = 2
        '
        'DGView
        '
        Me.DGView.AllowUserToAddRows = False
        Me.DGView.AllowUserToDeleteRows = False
        Me.DGView.AllowUserToOrderColumns = True
        Me.DGView.AllowUserToResizeRows = False
        Me.DGView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.no_perkiraan, Me.btn, Me.nama_perkiraan, Me.dk, Me.nominal, Me.hapus, Me.id_coa})
        Me.DGView.Location = New System.Drawing.Point(14, 149)
        Me.DGView.Name = "DGView"
        Me.DGView.Size = New System.Drawing.Size(986, 278)
        Me.DGView.TabIndex = 4
        '
        'no_perkiraan
        '
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.no_perkiraan.DefaultCellStyle = DataGridViewCellStyle1
        Me.no_perkiraan.HeaderText = "NO PERKIRAAN"
        Me.no_perkiraan.Name = "no_perkiraan"
        Me.no_perkiraan.Width = 230
        '
        'btn
        '
        Me.btn.HeaderText = "FIND"
        Me.btn.Name = "btn"
        Me.btn.Text = "FIND"
        Me.btn.Width = 40
        '
        'nama_perkiraan
        '
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        Me.nama_perkiraan.DefaultCellStyle = DataGridViewCellStyle2
        Me.nama_perkiraan.HeaderText = "NAMA PERKIRAAN"
        Me.nama_perkiraan.Name = "nama_perkiraan"
        Me.nama_perkiraan.Width = 350
        '
        'dk
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dk.DefaultCellStyle = DataGridViewCellStyle3
        Me.dk.HeaderText = "D/K"
        Me.dk.Items.AddRange(New Object() {"D", "K"})
        Me.dk.Name = "dk"
        Me.dk.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dk.Width = 80
        '
        'nominal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.nominal.DefaultCellStyle = DataGridViewCellStyle4
        Me.nominal.HeaderText = "NOMINAL"
        Me.nominal.Name = "nominal"
        Me.nominal.Width = 130
        '
        'hapus
        '
        Me.hapus.HeaderText = "ACTION"
        Me.hapus.Name = "hapus"
        Me.hapus.Text = "HAPUS"
        Me.hapus.ToolTipText = "HAPUS"
        Me.hapus.UseColumnTextForButtonValue = True
        Me.hapus.Width = 70
        '
        'id_coa
        '
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        Me.id_coa.DefaultCellStyle = DataGridViewCellStyle5
        Me.id_coa.HeaderText = "ID COA"
        Me.id_coa.Name = "id_coa"
        Me.id_coa.Visible = False
        '
        'txtBalance
        '
        '
        '
        '
        Me.txtBalance.Border.Class = "TextBoxBorder"
        Me.txtBalance.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBalance.Location = New System.Drawing.Point(615, 463)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Size = New System.Drawing.Size(148, 22)
        Me.txtBalance.TabIndex = 146
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX7.ForeColor = System.Drawing.Color.White
        Me.LabelX7.Location = New System.Drawing.Point(535, 462)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(69, 23)
        Me.LabelX7.TabIndex = 145
        Me.LabelX7.Text = "BALANCE"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.White
        Me.LabelX6.Location = New System.Drawing.Point(535, 433)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(64, 23)
        Me.LabelX6.TabIndex = 144
        Me.LabelX6.Text = "TOTAL"
        '
        'txtCredit
        '
        '
        '
        '
        Me.txtCredit.Border.Class = "TextBoxBorder"
        Me.txtCredit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCredit.Location = New System.Drawing.Point(769, 433)
        Me.txtCredit.Name = "txtCredit"
        Me.txtCredit.Size = New System.Drawing.Size(148, 22)
        Me.txtCredit.TabIndex = 143
        Me.txtCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.White
        Me.LabelX5.Location = New System.Drawing.Point(14, 90)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(114, 23)
        Me.LabelX5.TabIndex = 142
        Me.LabelX5.Text = "KETERANGAN"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.White
        Me.LabelX2.Location = New System.Drawing.Point(14, 32)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(114, 23)
        Me.LabelX2.TabIndex = 137
        Me.LabelX2.Text = "TANGGAL"
        '
        'txtDebet
        '
        '
        '
        '
        Me.txtDebet.Border.Class = "TextBoxBorder"
        Me.txtDebet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDebet.Location = New System.Drawing.Point(615, 433)
        Me.txtDebet.Name = "txtDebet"
        Me.txtDebet.Size = New System.Drawing.Size(148, 22)
        Me.txtDebet.TabIndex = 136
        Me.txtDebet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDate
        '
        '
        '
        '
        Me.txtDate.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate.ButtonDropDown.Visible = True
        Me.txtDate.IsPopupCalendarOpen = False
        Me.txtDate.Location = New System.Drawing.Point(148, 32)
        '
        '
        '
        Me.txtDate.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.TodayButtonVisible = True
        Me.txtDate.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(141, 22)
        Me.txtDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate.TabIndex = 0
        '
        'txtKet
        '
        '
        '
        '
        Me.txtKet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKet.Location = New System.Drawing.Point(148, 88)
        Me.txtKet.Multiline = True
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(560, 45)
        Me.txtKet.TabIndex = 3
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(903, 479)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(70, 72)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 6
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(825, 479)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(70, 72)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 5
        Me.BtnSave.Text = "SAVE"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.White
        Me.LabelX1.Location = New System.Drawing.Point(14, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(114, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "NO. TRANSAKSI"
        '
        'txtgroup
        '
        '
        '
        '
        Me.txtgroup.Border.Class = "TextBoxBorder"
        Me.txtgroup.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgroup.Location = New System.Drawing.Point(161, 20)
        Me.txtgroup.Name = "txtgroup"
        Me.txtgroup.Size = New System.Drawing.Size(272, 22)
        Me.txtgroup.TabIndex = 29
        '
        'txtNamaCoa
        '
        '
        '
        '
        Me.txtNamaCoa.Border.Class = "TextBoxBorder"
        Me.txtNamaCoa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaCoa.Location = New System.Drawing.Point(161, 78)
        Me.txtNamaCoa.Multiline = True
        Me.txtNamaCoa.Name = "txtNamaCoa"
        Me.txtNamaCoa.Size = New System.Drawing.Size(400, 66)
        Me.txtNamaCoa.TabIndex = 4
        '
        'frmJurnalUmumDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1056, 619)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frmJurnalUmumDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmJurnalUmumDetail"
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDebet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDate As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents txtKet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtBalance As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCredit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents DGView As System.Windows.Forms.DataGridView
    Friend WithEvents txtgroup As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNamaCoa As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNoTrans As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNamaUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnUnit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtkdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents no_perkiraan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents nama_perkiraan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dk As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents nominal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hapus As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents id_coa As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
