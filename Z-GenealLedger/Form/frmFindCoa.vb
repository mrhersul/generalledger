﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO

Public Class frmFindCoa
    Dim blnsearch As Boolean
    Dim strcari As String
    Dim xyz
    Dim xyKodeK

    Sub ShowDataGrid()
        If blngroup = True Then
            dr.Close()
            SQL = "select id,kd_perkiraan_group, nama_perkiraan_group from acc_perkiraan_group where 0=0"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (kd_perkiraan_group like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or nama_perkiraan_group like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by id "


        ElseIf blnParent = True Then
            dr.Close()
            SQL = "select id,kode, nama from acc_perkiraan_nama where type_perkiraan='H' and acc_perkiraan_group_id='" & frmCoaDetail.txtIdGroup.Text & "'"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (kode like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by id "


        ElseIf blnUnitKasOut = True Or blnUnitKasIn = True Or blnUnitBankIn = True Or blnunitbisnis = True Or blnUnitBankOut = True Or blnUnitTransfer = True Or blnUnitJurnal = True Or blnUnitSesuai = True Or blnUnitRptNeraca = True Or blnUnitRptNeraca2 = True Or blnUnitRptRL = True Or blnUnitCLosing = True Or blnMutasi = True Then

            SQL = ""
            SQL = " WITH ParentChildCTE AS ("
            SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & myIDCabang & ""
            SQL = SQL & " UNION ALL"
            SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
            SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
            SQL = SQL & " T1.parent_id IS NOT NULL ) SELECT id, kd_unitbisnis,nama_unitbisnis FROM ParentChildCTE "

            'SQL = "select id,kd_unitbisnis,nama_unitbisnis from unitbisnis_nama where substring(kd_unitbisnis,1," & xyInt & ")='" & myIDSub & "'"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (kd_unitbisnis like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or nama_unitbisnis like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by id"

        ElseIf blnCoaKasIn = True Or blnCoaKasOut = True Then

            SQL = "select id,kode, nama from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=1 and left(kode,4)='1-11'"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (kode like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by id "


        ElseIf blnCoaBankIn = True Or blnCoaBankOut = True Then

            SQL = "select id,kode, nama from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=1 and left(kode,4)='1-12'"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (kode like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by id "


        ElseIf blnCoaGridKasIn = True Or blnCoaGridKasOut = True Or blnCoaGridBankIn = True Or blnCoaGridBankOut = True Or blnCoaGridJurnal = True Or blnCoaGridSesuai = True Or blnCoaSetting = True Then

            SQL = "select id,kode,nama,keterangan from acc_perkiraan_nama where type_perkiraan='D'"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (kode like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by id "


        ElseIf blnCoaGridJurnalPiutang = True Then

            SQL = ""
            SQL = " WITH ParentChildCTE AS ("
            SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id =" & myIDCabang & ""
            SQL = SQL & " UNION ALL"
            SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
            SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
            SQL = SQL & " T1.parent_id IS NOT NULL )"
            SQL = SQL & " select x.id,x.kode,x.nama,x.keterangan,x.karyawan_id,x.personal_id from"
            SQL = SQL & " (select a.id,b.kd_karyawan as kode,c.nama_lengkap as nama,a.keterangan,b.karyawan_id,b.personal_id,b.unitbisnis_mutasi_id from acc_perkiraan_nama a"
            SQL = SQL & " left join karyawan_mutasi_organisasi b on a.id=b.acc_piutang_usaha_id "
            SQL = SQL & " left join personal c on b.personal_id=c.id"
            SQL = SQL & " where a.type_perkiraan='D'"
            SQL = SQL & " and a.id in (40)) as x"
            SQL = SQL & " where x.unitbisnis_mutasi_id in (SELECT id FROM ParentChildCTE)"


            If txtSearch.Text <> "" Then
                SQL = SQL & " and (x.kode like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or x.nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by 1 "


        End If


            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            Dim dt = New DataTable
            dt.Load(dr)
            DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)

        If blnUnitKasOut = True Or blnUnitKasIn = True Or blnUnitBankIn = True Or blnunitbisnis = True Or blnUnitBankOut = True Or blnUnitTransfer = True Or blnUnitJurnal = True Or blnUnitSesuai = True Or blnUnitRptNeraca = True Or blnUnitRptNeraca2 = True Or blnUnitRptRL = True Or blnUnitCLosing = True Or blnMutasi = True Then

            DGView.Columns(0).Width = 50
            DGView.Columns(1).Width = 250
            DGView.Columns(2).Width = 250

        Else
            DGView.Columns(0).Width = 50
            DGView.Columns(1).Width = 200
            DGView.Columns(2).Width = 350

        End If

        DGView.Columns(0).HeaderText = "ID"
        DGView.Columns(1).HeaderText = "KODE"
        DGView.Columns(2).HeaderText = "NAMA"

        If blnCoaGridKasIn = True Or blnCoaGridKasOut = True Or blnCoaGridBankIn = True Or blnCoaGridBankOut = True Or blnCoaGridTransfer = True Or blnCoaGridJurnal = True Or blnCoaGridSesuai = True Or blnCoaSetting = True Then
            DGView.Columns(3).Visible = False
        End If

        If blnCoaGridJurnalPiutang = True Then
            DGView.Columns(3).Visible = False
            DGView.Columns(4).Visible = False
            DGView.Columns(5).Visible = False
        End If

    End Sub

    Private Sub frmFindCoa_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        myFalse()
    End Sub

    Private Sub frmFindCoa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSearch.Text = ""
        txtSearch.Focus()
        ShowDataGrid()
    End Sub

    Private Sub DGView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.DoubleClick
        TakeDGView()
    End Sub

    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index

                If blngroup = True Then
                    frmCoaDetail.txtIdGroup.Text = .Item("id", i).Value
                    frmCoaDetail.txtkdgroup.Text = .Item("kd_perkiraan_group", i).Value
                    frmCoaDetail.txtnamagroup.Text = .Item("nama_perkiraan_group", i).Value
                    xyKodeK = .Item("kd_perkiraan_group", i).Value
                    frmCoaDetail.txtKodeK.Text = xyKodeK.ToString.Substring(0, 1)

                ElseIf blnParent = True Then
                    frmCoaDetail.txtIdParent.Text = .Item("id", i).Value
                    frmCoaDetail.txtkdParent.Text = .Item("kode", i).Value
                    frmCoaDetail.txtNamaParent.Text = .Item("nama", i).Value

                ElseIf blnunitbisnis Then
                    frmCoaDetail.txtIdParent.Text = .Item("id", i).Value
                    frmCoaDetail.txtkdParent.Text = .Item("kd_unitbisnis", i).Value
                    frmCoaDetail.txtNamaParent.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitKasIn Then
                    frmKasInDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmKasInDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmKasInDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitKasOut Then
                    frmKasOutDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmKasOutDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmKasOutDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitBankIn Then
                    frmBankInDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmBankInDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmBankInDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitBankOut Then
                    frmBankOutDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmBankOutDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmBankOutDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitTransfer Then
                    frmKasBankTransferDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmKasBankTransferDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmKasBankTransferDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitJurnal Then
                    frmJurnalUmumDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmJurnalUmumDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmJurnalUmumDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitSesuai Then
                    frmJurnalSesuaiDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmJurnalSesuaiDetail.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmJurnalSesuaiDetail.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitRptNeraca Then
                    frmRptNeraca.txtIdUnit.Text = .Item("id", i).Value
                    frmRptNeraca.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmRptNeraca.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitCLosing Then
                    frmClosingBulanan.txtIdUnit.Text = .Item("id", i).Value
                    frmClosingBulanan.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmClosingBulanan.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitRptNeraca2 Then
                    frmRptTotalNeraca.txtIdUnit.Text = .Item("id", i).Value
                    frmRptTotalNeraca.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmRptTotalNeraca.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnUnitRptRL Then
                    frmRptLostProfit.txtIdUnit.Text = .Item("id", i).Value
                    frmRptLostProfit.txtkdUnit.Text = .Item("kd_unitbisnis", i).Value
                    frmRptLostProfit.txtNamaUnit.Text = .Item("nama_unitbisnis", i).Value
                ElseIf blnCoaKasIn Then
                    frmKasInDetail.txtIdAkun.Text = .Item("id", i).Value
                    frmKasInDetail.txtNoCoa.Text = .Item("kode", i).Value
                    frmKasInDetail.txtNamaCoa.Text = .Item("nama", i).Value
                ElseIf blnCoaKasOut Then
                    frmKasOutDetail.txtIdAkun.Text = .Item("id", i).Value
                    frmKasOutDetail.txtNoCoa.Text = .Item("kode", i).Value
                    frmKasOutDetail.txtNamaCoa.Text = .Item("nama", i).Value
                ElseIf blnCoaBankIn Then
                    frmBankInDetail.txtIdAkun.Text = .Item("id", i).Value
                    frmBankInDetail.txtNoCoa.Text = .Item("kode", i).Value
                    frmBankInDetail.txtNamaCoa.Text = .Item("nama", i).Value
                ElseIf blnCoaBankOut Then
                    frmBankOutDetail.txtIdAkun.Text = .Item("id", i).Value
                    frmBankOutDetail.txtNoCoa.Text = .Item("kode", i).Value
                    frmBankOutDetail.txtNamaCoa.Text = .Item("nama", i).Value
                ElseIf blnCoaTransfer Then
                    frmKasBankTransferDetail.txtIdAkun.Text = .Item("id", i).Value
                    frmKasBankTransferDetail.txtNoCoa.Text = .Item("kode", i).Value
                    frmKasBankTransferDetail.txtNamaCoa.Text = .Item("nama", i).Value
                ElseIf blnCoaGridKasIn Then
                    Dim i2 = frmKasInDetail.DGView.CurrentRow.Index
                    frmKasInDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmKasInDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmKasInDetail.DGView.Item(3, i2).Value = .Item("keterangan", i).Value
                    frmKasInDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmKasInDetail.DGView.CurrentCell = frmKasInDetail.DGView(3, i2)
                ElseIf blnCoaGridKasOut Then
                    Dim i2 = frmKasOutDetail.DGView.CurrentRow.Index
                    frmKasOutDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmKasOutDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmKasOutDetail.DGView.Item(3, i2).Value = .Item("keterangan", i).Value
                    frmKasOutDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmKasOutDetail.DGView.CurrentCell = frmKasOutDetail.DGView(3, i2)
                ElseIf blnCoaGridBankIn Then
                    Dim i2 = frmBankInDetail.DGView.CurrentRow.Index
                    frmBankInDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmBankInDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmBankInDetail.DGView.Item(3, i2).Value = .Item("keterangan", i).Value
                    frmBankInDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmBankInDetail.DGView.CurrentCell = frmBankInDetail.DGView(3, i2)
                ElseIf blnCoaGridBankOut Then
                    Dim i2 = frmBankOutDetail.DGView.CurrentRow.Index
                    frmBankOutDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmBankOutDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmBankOutDetail.DGView.Item(3, i2).Value = .Item("keterangan", i).Value
                    frmBankOutDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmBankOutDetail.DGView.CurrentCell = frmBankOutDetail.DGView(3, i2)


                ElseIf blnCoaGridJurnal Then
                    Dim i2 = frmJurnalUmumDetail.DGView.CurrentRow.Index
                    frmJurnalUmumDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmJurnalUmumDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmJurnalUmumDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmJurnalUmumDetail.DGView.CurrentCell = frmJurnalUmumDetail.DGView(3, i2)

                ElseIf blnCoaGridJurnalPiutang Then
                    Dim i2 = frmJurnalUmumDetail.DGView.CurrentRow.Index
                    frmJurnalUmumDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmJurnalUmumDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmJurnalUmumDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmJurnalUmumDetail.DGView.Item(7, i2).Value = .Item("karyawan_id", i).Value
                    frmJurnalUmumDetail.DGView.Item(8, i2).Value = .Item("personal_id", i).Value
                    frmJurnalUmumDetail.DGView.CurrentCell = frmJurnalUmumDetail.DGView(3, i2)

                ElseIf blnCoaGridSesuai Then
                    Dim i2 = frmJurnalSesuaiDetail.DGView.CurrentRow.Index
                    frmJurnalSesuaiDetail.DGView.Item(0, i2).Value = .Item("kode", i).Value
                    frmJurnalSesuaiDetail.DGView.Item(2, i2).Value = .Item("nama", i).Value
                    frmJurnalSesuaiDetail.DGView.Item(6, i2).Value = .Item("id", i).Value
                    frmJurnalSesuaiDetail.DGView.CurrentCell = frmJurnalSesuaiDetail.DGView(3, i2)

                ElseIf blnCoaSetting Then
                    frmSettingCoaDetail.txtIdUnit.Text = .Item("id", i).Value
                    frmSettingCoaDetail.txtkdUnit.Text = .Item("kode", i).Value
                    frmSettingCoaDetail.txtNamaUnit.Text = .Item("nama", i).Value

                ElseIf blnMutasi Then
                    frmMutasi.txtIdUnit.Text = .Item("id", i).Value
                    frmMutasi.txtKdUnitCabang.Text = .Item("kd_unitbisnis", i).Value
                    frmMutasi.txtNmUnitCabang.Text = .Item("nama_unitbisnis", i).Value
                    frmMutasi.txtNikKaryawan.Text = "KW" + .Item("kd_unitbisnis", i).Value
                End If
            End With

            myFalse()
            Close()
        End If
    End Sub

    Sub myFalse()
        blngroup = False
        blnunitbisnis = False
        blnUnitJurnal = False
        blnUnitKasOut = False
        blnUnitBankOut = False
        blnUnitKasIn = False
        blnUnitBankIn = False
        blnCoaBankIn = False
        blnCoaBankOut = False
        blnCoaKasIn = False
        blnCoaKasOut = False
        blnCoaGridKasIn = False
        blnCoaGridKasOut = False
        blnCoaGridBankIn = False
        blnCoaGridBankOut = False
        blnUnitTransfer = False
        blnCoaTransfer = False
        blnCoaGridTransfer = False
        blnCoaGridJurnal = False
        blnUnitSesuai = False
        blnCoaGridSesuai = False
        blnCoaSetting = False
        blnUnitRptNeraca = False
        blnUnitRptNeraca2 = False
        blnUnitRptRL = False
        blnParent = False
        blnCoaGridJurnalPiutang = False

    End Sub

    Private Sub BtnSearch_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        ShowDataGrid()

        txtSearch.Text = ""
        txtSearch.Focus()
    End Sub

End Class