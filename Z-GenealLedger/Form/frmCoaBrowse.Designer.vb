﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCoaBrowse
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridEX1_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCoaBrowse))
        Dim GridEX9_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX6_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX7_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX8_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX2_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX3_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX5_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim GridEX4_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.TabControlAkun = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.DataAkunAktiva = New MyGL.DataAkunAktiva()
        Me.aktiva = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX9 = New Janus.Windows.GridEX.GridEX()
        Me.DataAkunDua = New MyGL.DataAkunDua()
        Me.TabItem8 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX6 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX7 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem6 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel8 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX8 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem7 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX2 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX3 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX5 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel()
        Me.GridEX4 = New Janus.Windows.GridEX.GridEX()
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.BubbleBar1 = New DevComponents.DotNetBar.BubbleBar()
        Me.BubbleBarTab1 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
        Me.BtnShow = New DevComponents.DotNetBar.BubbleButton()
        Me.BtnAdd = New DevComponents.DotNetBar.BubbleButton()
        Me.BtnEdit = New DevComponents.DotNetBar.BubbleButton()
        Me.BtnClose = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton1 = New DevComponents.DotNetBar.BubbleButton()
        Me.DataAkun = New MyGL.DataAkun()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.TabControlAkun, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlAkun.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataAkunAktiva, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel9.SuspendLayout()
        CType(Me.GridEX9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataAkunDua, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel6.SuspendLayout()
        CType(Me.GridEX6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel7.SuspendLayout()
        CType(Me.GridEX7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel8.SuspendLayout()
        CType(Me.GridEX8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.GridEX2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel3.SuspendLayout()
        CType(Me.GridEX3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel5.SuspendLayout()
        CType(Me.GridEX5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel4.SuspendLayout()
        CType(Me.GridEX4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BubbleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataAkun, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.TabControlAkun)
        Me.GroupPanel1.Controls.Add(Me.BubbleBar1)
        Me.GroupPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupPanel1.Location = New System.Drawing.Point(0, 0)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(833, 556)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 6
        '
        'TabControlAkun
        '
        Me.TabControlAkun.CanReorderTabs = True
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel1)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel9)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel8)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel7)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel6)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel5)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel4)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel3)
        Me.TabControlAkun.Controls.Add(Me.TabControlPanel2)
        Me.TabControlAkun.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlAkun.Location = New System.Drawing.Point(0, 0)
        Me.TabControlAkun.Name = "TabControlAkun"
        Me.TabControlAkun.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControlAkun.SelectedTabIndex = 0
        Me.TabControlAkun.Size = New System.Drawing.Size(827, 513)
        Me.TabControlAkun.TabIndex = 131
        Me.TabControlAkun.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControlAkun.Tabs.Add(Me.aktiva)
        Me.TabControlAkun.Tabs.Add(Me.TabItem1)
        Me.TabControlAkun.Tabs.Add(Me.TabItem2)
        Me.TabControlAkun.Tabs.Add(Me.TabItem3)
        Me.TabControlAkun.Tabs.Add(Me.TabItem4)
        Me.TabControlAkun.Tabs.Add(Me.TabItem5)
        Me.TabControlAkun.Tabs.Add(Me.TabItem6)
        Me.TabControlAkun.Tabs.Add(Me.TabItem7)
        Me.TabControlAkun.Tabs.Add(Me.TabItem8)
        Me.TabControlAkun.Text = "BIAYA"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.GridEX1)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.aktiva
        '
        'GridEX1
        '
        Me.GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX1.DataSource = Me.DataAkunAktiva
        GridEX1_DesignTimeLayout.LayoutString = resources.GetString("GridEX1_DesignTimeLayout.LayoutString")
        Me.GridEX1.DesignTimeLayout = GridEX1_DesignTimeLayout
        Me.GridEX1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX1.Hierarchical = True
        Me.GridEX1.Location = New System.Drawing.Point(1, 1)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.ScrollBarWidth = 17
        Me.GridEX1.Size = New System.Drawing.Size(825, 485)
        Me.GridEX1.TabIndex = 133
        Me.GridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'DataAkunAktiva
        '
        Me.DataAkunAktiva.DataSetName = "DataAkunAktiva"
        Me.DataAkunAktiva.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'aktiva
        '
        Me.aktiva.AttachedControl = Me.TabControlPanel1
        Me.aktiva.Name = "aktiva"
        Me.aktiva.Text = "AKTIVA"
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.GridEX9)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.Style.GradientAngle = 90
        Me.TabControlPanel9.TabIndex = 9
        Me.TabControlPanel9.TabItem = Me.TabItem8
        '
        'GridEX9
        '
        Me.GridEX9.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX9.DataSource = Me.DataAkunDua
        GridEX9_DesignTimeLayout.LayoutString = resources.GetString("GridEX9_DesignTimeLayout.LayoutString")
        Me.GridEX9.DesignTimeLayout = GridEX9_DesignTimeLayout
        Me.GridEX9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX9.Hierarchical = True
        Me.GridEX9.Location = New System.Drawing.Point(1, 1)
        Me.GridEX9.Name = "GridEX9"
        Me.GridEX9.ScrollBarWidth = 17
        Me.GridEX9.Size = New System.Drawing.Size(825, 485)
        Me.GridEX9.TabIndex = 133
        Me.GridEX9.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'DataAkunDua
        '
        Me.DataAkunDua.DataSetName = "DataAkunDua"
        Me.DataAkunDua.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabItem8
        '
        Me.TabItem8.AttachedControl = Me.TabControlPanel9
        Me.TabItem8.Name = "TabItem8"
        Me.TabItem8.Text = "PAJAK"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.GridEX6)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.Style.GradientAngle = 90
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.TabItem5
        '
        'GridEX6
        '
        Me.GridEX6.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX6.DataSource = Me.DataAkunDua
        GridEX6_DesignTimeLayout.LayoutString = resources.GetString("GridEX6_DesignTimeLayout.LayoutString")
        Me.GridEX6.DesignTimeLayout = GridEX6_DesignTimeLayout
        Me.GridEX6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX6.Hierarchical = True
        Me.GridEX6.Location = New System.Drawing.Point(1, 1)
        Me.GridEX6.Name = "GridEX6"
        Me.GridEX6.ScrollBarWidth = 17
        Me.GridEX6.Size = New System.Drawing.Size(825, 485)
        Me.GridEX6.TabIndex = 133
        Me.GridEX6.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem5
        '
        Me.TabItem5.AttachedControl = Me.TabControlPanel6
        Me.TabItem5.Name = "TabItem5"
        Me.TabItem5.Text = "BIAYA"
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.GridEX7)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.Style.GradientAngle = 90
        Me.TabControlPanel7.TabIndex = 7
        Me.TabControlPanel7.TabItem = Me.TabItem6
        '
        'GridEX7
        '
        Me.GridEX7.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX7.DataSource = Me.DataAkunDua
        GridEX7_DesignTimeLayout.LayoutString = resources.GetString("GridEX7_DesignTimeLayout.LayoutString")
        Me.GridEX7.DesignTimeLayout = GridEX7_DesignTimeLayout
        Me.GridEX7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX7.Hierarchical = True
        Me.GridEX7.Location = New System.Drawing.Point(1, 1)
        Me.GridEX7.Name = "GridEX7"
        Me.GridEX7.ScrollBarWidth = 17
        Me.GridEX7.Size = New System.Drawing.Size(825, 485)
        Me.GridEX7.TabIndex = 133
        Me.GridEX7.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem6
        '
        Me.TabItem6.AttachedControl = Me.TabControlPanel7
        Me.TabItem6.Name = "TabItem6"
        Me.TabItem6.Text = "PENDAPATAN LAIN"
        '
        'TabControlPanel8
        '
        Me.TabControlPanel8.Controls.Add(Me.GridEX8)
        Me.TabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel8.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel8.Name = "TabControlPanel8"
        Me.TabControlPanel8.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel8.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel8.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel8.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel8.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel8.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel8.Style.GradientAngle = 90
        Me.TabControlPanel8.TabIndex = 8
        Me.TabControlPanel8.TabItem = Me.TabItem7
        '
        'GridEX8
        '
        Me.GridEX8.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX8.DataSource = Me.DataAkunDua
        GridEX8_DesignTimeLayout.LayoutString = resources.GetString("GridEX8_DesignTimeLayout.LayoutString")
        Me.GridEX8.DesignTimeLayout = GridEX8_DesignTimeLayout
        Me.GridEX8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX8.Hierarchical = True
        Me.GridEX8.Location = New System.Drawing.Point(1, 1)
        Me.GridEX8.Name = "GridEX8"
        Me.GridEX8.ScrollBarWidth = 17
        Me.GridEX8.Size = New System.Drawing.Size(825, 485)
        Me.GridEX8.TabIndex = 133
        Me.GridEX8.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem7
        '
        Me.TabItem7.AttachedControl = Me.TabControlPanel8
        Me.TabItem7.Name = "TabItem7"
        Me.TabItem7.Text = "BEBAN LAIN"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.GridEX2)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.Style.GradientAngle = 90
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.TabItem1
        '
        'GridEX2
        '
        Me.GridEX2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX2.DataSource = Me.DataAkunDua
        GridEX2_DesignTimeLayout.LayoutString = resources.GetString("GridEX2_DesignTimeLayout.LayoutString")
        Me.GridEX2.DesignTimeLayout = GridEX2_DesignTimeLayout
        Me.GridEX2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX2.Hierarchical = True
        Me.GridEX2.Location = New System.Drawing.Point(1, 1)
        Me.GridEX2.Name = "GridEX2"
        Me.GridEX2.ScrollBarWidth = 17
        Me.GridEX2.Size = New System.Drawing.Size(825, 485)
        Me.GridEX2.TabIndex = 132
        Me.GridEX2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel2
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "HUTANG"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.GridEX3)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.Style.GradientAngle = 90
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.TabItem2
        '
        'GridEX3
        '
        Me.GridEX3.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX3.DataSource = Me.DataAkunDua
        GridEX3_DesignTimeLayout.LayoutString = resources.GetString("GridEX3_DesignTimeLayout.LayoutString")
        Me.GridEX3.DesignTimeLayout = GridEX3_DesignTimeLayout
        Me.GridEX3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX3.Hierarchical = True
        Me.GridEX3.Location = New System.Drawing.Point(1, 1)
        Me.GridEX3.Name = "GridEX3"
        Me.GridEX3.ScrollBarWidth = 17
        Me.GridEX3.Size = New System.Drawing.Size(825, 485)
        Me.GridEX3.TabIndex = 133
        Me.GridEX3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel3
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "MODAL"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.GridEX5)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.Style.GradientAngle = 90
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.TabItem4
        '
        'GridEX5
        '
        Me.GridEX5.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX5.DataSource = Me.DataAkunDua
        GridEX5_DesignTimeLayout.LayoutString = resources.GetString("GridEX5_DesignTimeLayout.LayoutString")
        Me.GridEX5.DesignTimeLayout = GridEX5_DesignTimeLayout
        Me.GridEX5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX5.Hierarchical = True
        Me.GridEX5.Location = New System.Drawing.Point(1, 1)
        Me.GridEX5.Name = "GridEX5"
        Me.GridEX5.ScrollBarWidth = 17
        Me.GridEX5.Size = New System.Drawing.Size(825, 485)
        Me.GridEX5.TabIndex = 133
        Me.GridEX5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem4
        '
        Me.TabItem4.AttachedControl = Me.TabControlPanel5
        Me.TabItem4.Name = "TabItem4"
        Me.TabItem4.Text = "HARGA POKOK"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.GridEX4)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(827, 487)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.Style.GradientAngle = 90
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.TabItem3
        '
        'GridEX4
        '
        Me.GridEX4.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX4.DataSource = Me.DataAkunDua
        GridEX4_DesignTimeLayout.LayoutString = resources.GetString("GridEX4_DesignTimeLayout.LayoutString")
        Me.GridEX4.DesignTimeLayout = GridEX4_DesignTimeLayout
        Me.GridEX4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridEX4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX4.Hierarchical = True
        Me.GridEX4.Location = New System.Drawing.Point(1, 1)
        Me.GridEX4.Name = "GridEX4"
        Me.GridEX4.ScrollBarWidth = 17
        Me.GridEX4.Size = New System.Drawing.Size(825, 485)
        Me.GridEX4.TabIndex = 132
        Me.GridEX4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel4
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "PENGHASILAN"
        '
        'BubbleBar1
        '
        Me.BubbleBar1.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom
        Me.BubbleBar1.AntiAlias = True
        '
        '
        '
        Me.BubbleBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.BubbleBar1.ButtonBackAreaStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBar1.ButtonBackAreaStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar1.ButtonBackAreaStyle.BorderBottomWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.BubbleBar1.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar1.ButtonBackAreaStyle.BorderLeftWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar1.ButtonBackAreaStyle.BorderRightWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar1.ButtonBackAreaStyle.BorderTopWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingBottom = 3
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingLeft = 3
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingRight = 3
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingTop = 3
        Me.BubbleBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BubbleBar1.ImageSizeNormal = New System.Drawing.Size(24, 24)
        Me.BubbleBar1.Location = New System.Drawing.Point(0, 513)
        Me.BubbleBar1.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight
        Me.BubbleBar1.Name = "BubbleBar1"
        Me.BubbleBar1.SelectedTab = Me.BubbleBarTab1
        Me.BubbleBar1.SelectedTabColors.BorderColor = System.Drawing.Color.Black
        Me.BubbleBar1.Size = New System.Drawing.Size(827, 37)
        Me.BubbleBar1.TabIndex = 1
        Me.BubbleBar1.Tabs.Add(Me.BubbleBarTab1)
        '
        'BubbleBarTab1
        '
        Me.BubbleBarTab1.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BubbleBarTab1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.BubbleBarTab1.Buttons.AddRange(New DevComponents.DotNetBar.BubbleButton() {Me.BtnShow, Me.BtnAdd, Me.BtnEdit, Me.BtnClose})
        Me.BubbleBarTab1.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab1.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BubbleBarTab1.Name = "BubbleBarTab1"
        Me.BubbleBarTab1.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue
        Me.BubbleBarTab1.Text = ""
        Me.BubbleBarTab1.TextColor = System.Drawing.Color.Black
        '
        'BtnShow
        '
        Me.BtnShow.Image = CType(resources.GetObject("BtnShow.Image"), System.Drawing.Image)
        Me.BtnShow.ImageLarge = CType(resources.GetObject("BtnShow.ImageLarge"), System.Drawing.Image)
        Me.BtnShow.Name = "BtnShow"
        Me.BtnShow.TooltipText = "Show"
        '
        'BtnAdd
        '
        Me.BtnAdd.Image = CType(resources.GetObject("BtnAdd.Image"), System.Drawing.Image)
        Me.BtnAdd.ImageLarge = CType(resources.GetObject("BtnAdd.ImageLarge"), System.Drawing.Image)
        Me.BtnAdd.Name = "BtnAdd"
        Me.BtnAdd.TooltipText = "Add"
        '
        'BtnEdit
        '
        Me.BtnEdit.Image = CType(resources.GetObject("BtnEdit.Image"), System.Drawing.Image)
        Me.BtnEdit.ImageLarge = CType(resources.GetObject("BtnEdit.ImageLarge"), System.Drawing.Image)
        Me.BtnEdit.Name = "BtnEdit"
        Me.BtnEdit.TooltipText = "Edit"
        '
        'BtnClose
        '
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImageLarge = CType(resources.GetObject("BtnClose.ImageLarge"), System.Drawing.Image)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.TooltipText = "Close"
        '
        'BubbleButton1
        '
        Me.BubbleButton1.Image = CType(resources.GetObject("BubbleButton1.Image"), System.Drawing.Image)
        Me.BubbleButton1.ImageLarge = CType(resources.GetObject("BubbleButton1.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton1.Name = "BubbleButton1"
        Me.BubbleButton1.TooltipText = "Edit"
        '
        'DataAkun
        '
        Me.DataAkun.DataSetName = "DataAkun"
        Me.DataAkun.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmCoaBrowse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(833, 556)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmCoaBrowse"
        Me.Text = "Perkiraan"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.TabControlAkun, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlAkun.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataAkunAktiva, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel9.ResumeLayout(False)
        CType(Me.GridEX9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataAkunDua, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel6.ResumeLayout(False)
        CType(Me.GridEX6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel7.ResumeLayout(False)
        CType(Me.GridEX7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel8.ResumeLayout(False)
        CType(Me.GridEX8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel2.ResumeLayout(False)
        CType(Me.GridEX2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel3.ResumeLayout(False)
        CType(Me.GridEX3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel5.ResumeLayout(False)
        CType(Me.GridEX5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel4.ResumeLayout(False)
        CType(Me.GridEX4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BubbleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataAkun, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents BubbleBar1 As DevComponents.DotNetBar.BubbleBar
    Friend WithEvents BubbleBarTab1 As DevComponents.DotNetBar.BubbleBarTab
    Friend WithEvents BtnAdd As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BtnEdit As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BtnShow As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BtnClose As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents TabControlAkun As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents aktiva As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem6 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel8 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem7 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem8 As DevComponents.DotNetBar.TabItem
    Friend WithEvents DataAkun As MyGL.DataAkun
    Friend WithEvents GridEX4 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX2 As Janus.Windows.GridEX.GridEX
    Friend WithEvents DataAkunDua As MyGL.DataAkunDua
    Friend WithEvents BubbleButton1 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents DataAkunAktiva As MyGL.DataAkunAktiva
    Friend WithEvents GridEX5 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX3 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX6 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX9 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX8 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GridEX7 As Janus.Windows.GridEX.GridEX
End Class
