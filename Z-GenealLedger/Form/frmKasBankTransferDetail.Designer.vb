﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKasBankTransferDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmKasBankTransferDetail))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtDate = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtIDHdr = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIdAkun = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaCoa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNilai = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.btnAkun = New DevComponents.DotNetBar.ButtonX()
        Me.txtNoCoa = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.DGView = New System.Windows.Forms.DataGridView()
        Me.no_perkiraan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.nama_perkiraan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.keterangan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nominal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hapus = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.id_coa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.btnUnit = New DevComponents.DotNetBar.ButtonX()
        Me.txtkdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNoTrans = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKet = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005
        Me.GroupPanel1.Controls.Add(Me.txtDate)
        Me.GroupPanel1.Controls.Add(Me.txtIDHdr)
        Me.GroupPanel1.Controls.Add(Me.txtIdAkun)
        Me.GroupPanel1.Controls.Add(Me.txtIdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNamaCoa)
        Me.GroupPanel1.Controls.Add(Me.txtNamaUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNilai)
        Me.GroupPanel1.Controls.Add(Me.LabelX7)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.btnAkun)
        Me.GroupPanel1.Controls.Add(Me.txtNoCoa)
        Me.GroupPanel1.Controls.Add(Me.DGView)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.btnUnit)
        Me.GroupPanel1.Controls.Add(Me.txtkdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNoTrans)
        Me.GroupPanel1.Controls.Add(Me.txtKet)
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupPanel1.Location = New System.Drawing.Point(25, 12)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1209, 521)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 8
        Me.GroupPanel1.Text = "KAS & BANK TRANSFER - CASH & BANK TRANSFER"
        '
        'txtDate
        '
        '
        '
        '
        Me.txtDate.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate.ButtonDropDown.Visible = True
        Me.txtDate.ForeColor = System.Drawing.Color.Black
        Me.txtDate.IsPopupCalendarOpen = False
        Me.txtDate.Location = New System.Drawing.Point(146, 49)
        '
        '
        '
        Me.txtDate.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.TodayButtonVisible = True
        Me.txtDate.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(141, 22)
        Me.txtDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate.TabIndex = 151
        '
        'txtIDHdr
        '
        '
        '
        '
        Me.txtIDHdr.Border.Class = "TextBoxBorder"
        Me.txtIDHdr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIDHdr.Location = New System.Drawing.Point(477, 18)
        Me.txtIDHdr.Name = "txtIDHdr"
        Me.txtIDHdr.Size = New System.Drawing.Size(37, 22)
        Me.txtIDHdr.TabIndex = 150
        Me.txtIDHdr.Visible = False
        '
        'txtIdAkun
        '
        '
        '
        '
        Me.txtIdAkun.Border.Class = "TextBoxBorder"
        Me.txtIdAkun.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdAkun.Location = New System.Drawing.Point(797, 119)
        Me.txtIdAkun.Name = "txtIdAkun"
        Me.txtIdAkun.Size = New System.Drawing.Size(37, 22)
        Me.txtIdAkun.TabIndex = 149
        Me.txtIdAkun.Visible = False
        '
        'txtIdUnit
        '
        '
        '
        '
        Me.txtIdUnit.Border.Class = "TextBoxBorder"
        Me.txtIdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdUnit.Location = New System.Drawing.Point(797, 82)
        Me.txtIdUnit.Name = "txtIdUnit"
        Me.txtIdUnit.Size = New System.Drawing.Size(37, 22)
        Me.txtIdUnit.TabIndex = 147
        Me.txtIdUnit.Visible = False
        '
        'txtNamaCoa
        '
        '
        '
        '
        Me.txtNamaCoa.Border.Class = "TextBoxBorder"
        Me.txtNamaCoa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaCoa.Enabled = False
        Me.txtNamaCoa.Location = New System.Drawing.Point(393, 119)
        Me.txtNamaCoa.Name = "txtNamaCoa"
        Me.txtNamaCoa.Size = New System.Drawing.Size(353, 22)
        Me.txtNamaCoa.TabIndex = 146
        '
        'txtNamaUnit
        '
        '
        '
        '
        Me.txtNamaUnit.Border.Class = "TextBoxBorder"
        Me.txtNamaUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaUnit.Enabled = False
        Me.txtNamaUnit.Location = New System.Drawing.Point(393, 82)
        Me.txtNamaUnit.Name = "txtNamaUnit"
        Me.txtNamaUnit.Size = New System.Drawing.Size(353, 22)
        Me.txtNamaUnit.TabIndex = 145
        '
        'txtNilai
        '
        '
        '
        '
        Me.txtNilai.Border.Class = "TextBoxBorder"
        Me.txtNilai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNilai.Location = New System.Drawing.Point(941, 384)
        Me.txtNilai.Name = "txtNilai"
        Me.txtNilai.Size = New System.Drawing.Size(149, 22)
        Me.txtNilai.TabIndex = 142
        Me.txtNilai.Text = "0"
        Me.txtNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.White
        Me.LabelX7.Location = New System.Drawing.Point(838, 383)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(109, 23)
        Me.LabelX7.TabIndex = 141
        Me.LabelX7.Text = "JUMLAH NILAI"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.White
        Me.LabelX6.Location = New System.Drawing.Point(26, 84)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(114, 23)
        Me.LabelX6.TabIndex = 140
        Me.LabelX6.Text = "NAMA UNIT BISNIS"
        '
        'btnAkun
        '
        Me.btnAkun.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAkun.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnAkun.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnAkun.Image = CType(resources.GetObject("btnAkun.Image"), System.Drawing.Image)
        Me.btnAkun.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnAkun.Location = New System.Drawing.Point(744, 119)
        Me.btnAkun.Name = "btnAkun"
        Me.btnAkun.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnAkun.Size = New System.Drawing.Size(47, 22)
        Me.btnAkun.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAkun.TabIndex = 139
        '
        'txtNoCoa
        '
        '
        '
        '
        Me.txtNoCoa.Border.Class = "TextBoxBorder"
        Me.txtNoCoa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNoCoa.Enabled = False
        Me.txtNoCoa.Location = New System.Drawing.Point(147, 119)
        Me.txtNoCoa.Name = "txtNoCoa"
        Me.txtNoCoa.Size = New System.Drawing.Size(240, 22)
        Me.txtNoCoa.TabIndex = 138
        '
        'DGView
        '
        Me.DGView.AllowUserToAddRows = False
        Me.DGView.AllowUserToDeleteRows = False
        Me.DGView.AllowUserToOrderColumns = True
        Me.DGView.AllowUserToResizeRows = False
        Me.DGView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.no_perkiraan, Me.btn, Me.nama_perkiraan, Me.keterangan, Me.nominal, Me.hapus, Me.id_coa})
        Me.DGView.Location = New System.Drawing.Point(26, 158)
        Me.DGView.Name = "DGView"
        Me.DGView.Size = New System.Drawing.Size(1149, 210)
        Me.DGView.TabIndex = 135
        '
        'no_perkiraan
        '
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.no_perkiraan.DefaultCellStyle = DataGridViewCellStyle1
        Me.no_perkiraan.HeaderText = "NO PERKIRAAN"
        Me.no_perkiraan.Name = "no_perkiraan"
        Me.no_perkiraan.Width = 150
        '
        'btn
        '
        Me.btn.HeaderText = "FIND"
        Me.btn.Name = "btn"
        Me.btn.Text = "FIND"
        Me.btn.Width = 40
        '
        'nama_perkiraan
        '
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        Me.nama_perkiraan.DefaultCellStyle = DataGridViewCellStyle2
        Me.nama_perkiraan.HeaderText = "NAMA PERKIRAAN"
        Me.nama_perkiraan.Name = "nama_perkiraan"
        Me.nama_perkiraan.Width = 320
        '
        'keterangan
        '
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        Me.keterangan.DefaultCellStyle = DataGridViewCellStyle3
        Me.keterangan.HeaderText = "KETERANGAN"
        Me.keterangan.Name = "keterangan"
        Me.keterangan.Width = 370
        '
        'nominal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.nominal.DefaultCellStyle = DataGridViewCellStyle4
        Me.nominal.HeaderText = "NOMINAL"
        Me.nominal.Name = "nominal"
        Me.nominal.Width = 130
        '
        'hapus
        '
        Me.hapus.HeaderText = "ACTION"
        Me.hapus.Name = "hapus"
        Me.hapus.Text = "HAPUS"
        Me.hapus.ToolTipText = "HAPUS"
        Me.hapus.UseColumnTextForButtonValue = True
        Me.hapus.Width = 70
        '
        'id_coa
        '
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        Me.id_coa.DefaultCellStyle = DataGridViewCellStyle5
        Me.id_coa.HeaderText = "ID COA"
        Me.id_coa.Name = "id_coa"
        Me.id_coa.Visible = False
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.White
        Me.LabelX5.Location = New System.Drawing.Point(26, 49)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(114, 23)
        Me.LabelX5.TabIndex = 35
        Me.LabelX5.Text = "TANGGAL"
        '
        'btnUnit
        '
        Me.btnUnit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUnit.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnUnit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnUnit.Image = CType(resources.GetObject("btnUnit.Image"), System.Drawing.Image)
        Me.btnUnit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUnit.Location = New System.Drawing.Point(744, 82)
        Me.btnUnit.Name = "btnUnit"
        Me.btnUnit.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnUnit.Size = New System.Drawing.Size(45, 22)
        Me.btnUnit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnUnit.TabIndex = 33
        '
        'txtkdUnit
        '
        '
        '
        '
        Me.txtkdUnit.Border.Class = "TextBoxBorder"
        Me.txtkdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtkdUnit.Enabled = False
        Me.txtkdUnit.Location = New System.Drawing.Point(146, 82)
        Me.txtkdUnit.Name = "txtkdUnit"
        Me.txtkdUnit.Size = New System.Drawing.Size(241, 22)
        Me.txtkdUnit.TabIndex = 30
        '
        'txtNoTrans
        '
        '
        '
        '
        Me.txtNoTrans.Border.Class = "TextBoxBorder"
        Me.txtNoTrans.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNoTrans.Enabled = False
        Me.txtNoTrans.Location = New System.Drawing.Point(146, 18)
        Me.txtNoTrans.Name = "txtNoTrans"
        Me.txtNoTrans.Size = New System.Drawing.Size(325, 22)
        Me.txtNoTrans.TabIndex = 29
        '
        'txtKet
        '
        '
        '
        '
        Me.txtKet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKet.Location = New System.Drawing.Point(114, 386)
        Me.txtKet.Multiline = True
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(522, 63)
        Me.txtKet.TabIndex = 4
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.White
        Me.LabelX3.Location = New System.Drawing.Point(26, 400)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(82, 23)
        Me.LabelX3.TabIndex = 12
        Me.LabelX3.Text = "KETERANGAN"
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(1105, 423)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(70, 72)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(1020, 423)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(70, 72)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "SAVE"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.White
        Me.LabelX2.Location = New System.Drawing.Point(26, 119)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(136, 23)
        Me.LabelX2.TabIndex = 1
        Me.LabelX2.Text = "TRANSFER DARI AKUN"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.White
        Me.LabelX1.Location = New System.Drawing.Point(26, 20)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(114, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "NO. TRANSAKSI"
        '
        'frmKasBankTransferDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1253, 551)
        Me.Controls.Add(Me.GroupPanel1)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmKasBankTransferDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCashInDetail"
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents txtIDHdr As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdAkun As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNamaCoa As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNamaUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNilai As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnAkun As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtNoCoa As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents DGView As System.Windows.Forms.DataGridView
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnUnit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtkdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNoTrans As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKet As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDate As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents no_perkiraan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents nama_perkiraan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents keterangan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nominal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hapus As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents id_coa As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
