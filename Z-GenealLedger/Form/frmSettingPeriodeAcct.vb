﻿Public Class frmSettingPeriodeAcct
    Public curcol, currow As Integer
    Dim svID As Integer
    Dim svSaldo As Double
    Private Sub frmSettingPeriodeAcct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DGView.Rows.Clear()
        ShowData()
        SettingGrid()
        ProgressBarX1.Visible = False
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        Dim IsConError As Boolean
        On Error GoTo err

        SQL = ""
        SQL = "select * from acc_periode_setting where id_unitbisnis=" & myIDCabang & ""
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            IsConError = True
            dr.Close()
            SQL = "update acc_periode_setting set tgl_awal='" & Format(DT2.Value, "yyyy-MM-dd") & "',tgl_berjalan='" & Format(DT1.Value, "yyyy-MM-dd") & "',user_ubah='" & myName & "',tgl_ubah='" & Format(Now, "yyyy-MM-dd hh:mm:ss") & "'"
            SQL = SQL & " where id_unitbisnis=" & myIDCabang & " and datediff(month,tgl_awal,'" & Format(DT2.Value, "yyyy-MM-dd") & "')=0"
            Konek.IUDQuery(SQL)
        Else
            IsConError = True
            dr.Close()
            SQL = "Insert Into acc_periode_setting (id_unitbisnis,tgl_awal,tgl_berjalan,user_buat,tgl_buat)"
            SQL = SQL & " Values(" & myIDCabang & ",'" & Format(DT2.Value, "yyyy-MM-dd") & "','" & Format(DT1.Value, "yyyy-MM-dd") & "','" & myName & "','" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "')"
            Konek.IUDQuery(SQL)
        End If

        'Simpan Saldo Awal

        SQL = ""
        SQL = "select * from acc_saldo_awal where unitbisnis_id=" & myIDCabang & " and datediff(month,periode,'" & Format(DT2.Value, "yyyy-MM-dd") & "')=0"
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        ProgressBarX1.Visible = True
        ProgressBarX1.Value = 0

        If dr.HasRows Then
            For x As Integer = 0 To DGView.Rows.Count - 1
                If DGView.Rows(x).Cells(0).Value <> "" Then
                    svID = DGView.Rows(x).Cells(3).Value
                    svSaldo = DGView.Rows(x).Cells(2).Value

                    dr.Close()
                    SQL = ""
                    SQL = "update acc_saldo_awal set saldo=" & CDbl(svSaldo) & ",tanggal_buat = '" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',"
                    SQL = SQL & " user_buat=" & myID & " where unitbisnis_id=" & myIDCabang & " and datediff(month,periode,'" & Format(DT2.Value, "yyyy-MM-dd") & "')=0 and acc_coa_id=" & svID & ""
                    Konek.IUDQuery(SQL)

                    ProgressBarX1.Value = ProgressBarX1.Value + x
                End If
            Next

        Else
            For x As Integer = 0 To DGView.Rows.Count - 1
                If DGView.Rows(x).Cells(0).Value <> "" Then
                    svID = DGView.Rows(x).Cells(3).Value
                    svSaldo = DGView.Rows(x).Cells(2).Value

                    dr.Close()
                    SQL = ""
                    SQL = "Insert Into acc_saldo_awal (periode,unitbisnis_id,acc_coa_id,saldo,tanggal_buat,user_buat) "
                    SQL = SQL & " Values('" & Format(DT2.Value, "yyyy-MM-dd") & "'," & myIDCabang & "," & svID & "," & svSaldo & ",'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ")"
                    Konek.IUDQuery(SQL)

                    ProgressBarX1.Value = ProgressBarX1.Value + x
                End If
            Next

        End If

        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
                Exit Sub
            Case Else
        End Select
    End Sub

    Sub ShowData()

        SQL = ""
        SQL = " select * from acc_periode_setting where id_unitbisnis=" & myIDCabang & ""
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            dr.Read()
            DT1.Value = Format(dr.Item("tgl_berjalan"), "MMMM-yyyy")
            DT2.Value = Format(dr.Item("tgl_awal"), "MMMM-yyyy")

            SQL = ""
            SQL = " select b.periode,a.kode,a.nama,b.saldo,a.id from acc_perkiraan_nama a left join acc_saldo_awal b on a.id=b.acc_coa_id"
            SQL = SQL & " where a.type_perkiraan='D' and datediff(month,b.periode,'" & Format(DT2.Value, "yyyy-MM-dd") & "')=0 "
            SQL = SQL & " order by a.kode"

        Else
            SQL = ""
            SQL = " select b.periode,a.kode,a.nama,isnull(b.saldo,0) as saldo,a.id from acc_perkiraan_nama a left join acc_saldo_awal b on a.id=b.acc_coa_id"
            SQL = SQL & " where a.type_perkiraan='D'"
            SQL = SQL & " order by a.kode"

            MsgBox("Data Masih Kosong, Silakan Setting Bulan Awal dan Bulan Berjalan Akuntansi dan Input Saldo Awal", vbInformation)

        End If

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read

                DGView.Rows.Add(dr.Item("kode").ToString, dr.Item("nama").ToString, Format(dr.Item("saldo"), "N2"), dr.Item("id").ToString)

            End While
        End If

        DGView.Rows.Add()
        dr.Close()
    End Sub


    Private Sub DGView_CellFormatting(sender As Object, e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGView.CellFormatting
        If e.ColumnIndex = 3 Then 'AndAlso e.RowIndex <> DGView.NewRowIndex Then
            e.Value = CDec(e.Value).ToString("N2")
            e.FormattingApplied = True
        End If
    End Sub

    Private Sub DGView_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.CurrentCellChanged
        Try
            curcol = DGView.CurrentCell.ColumnIndex
            currow = DGView.CurrentCell.RowIndex
        Catch ex As Exception
            curcol = 0
            currow = 0
        End Try
    End Sub

    Private Sub DGView_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles DGView.EditingControlShowing
        RemoveHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress

        If DGView.CurrentCell.ColumnIndex = 3 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress
        Else
            RemoveHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress
        End If
    End Sub

    Private Sub TextBox_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

        If (Not Char.IsControl(e.KeyChar) _
                   AndAlso (Not Char.IsDigit(e.KeyChar) _
                   AndAlso (e.KeyChar <> Microsoft.VisualBasic.ChrW(46)))) Then
            e.Handled = True
        End If

    End Sub

    Private Sub DGView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGView.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                Try
                    If curcol = 2 Then
                        DGView.Item(2, currow).Value = Format(DGView.Item(2, currow).Value, "N2")
                        DGView.CurrentCell = DGView(2, currow + 1)
                    End If

                Catch ex As Exception
                    Exit Try
                End Try
        End Select
    End Sub

    Sub SettingGrid()
        DGView.Columns(0).ReadOnly = True
        DGView.Columns(1).ReadOnly = True
        DGView.Columns(2).ReadOnly = False
        DGView.Columns(3).ReadOnly = True
    End Sub
End Class