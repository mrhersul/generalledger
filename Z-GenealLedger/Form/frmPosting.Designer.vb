﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPosting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPosting))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.BtnShow = New DevComponents.DotNetBar.ButtonX()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.txtDate2 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtDate1 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.DGView = New System.Windows.Forms.DataGridView()
        Me.no_transaksi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jenis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kd_unitbisnis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nama_unitbisnis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.posting = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.txtDate2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.BtnShow)
        Me.GroupPanel1.Controls.Add(Me.ProgressBar1)
        Me.GroupPanel1.Controls.Add(Me.txtDate2)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.txtDate1)
        Me.GroupPanel1.Controls.Add(Me.DGView)
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 2)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(1090, 472)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 8
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.Location = New System.Drawing.Point(589, 427)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BtnClose.Size = New System.Drawing.Size(82, 23)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 146
        Me.BtnClose.Text = "Close"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.Location = New System.Drawing.Point(501, 427)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BtnSave.Size = New System.Drawing.Size(82, 23)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 145
        Me.BtnSave.Text = "Process"
        '
        'BtnShow
        '
        Me.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnShow.BackColor = System.Drawing.Color.Transparent
        Me.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnShow.Image = CType(resources.GetObject("BtnShow.Image"), System.Drawing.Image)
        Me.BtnShow.Location = New System.Drawing.Point(413, 427)
        Me.BtnShow.Name = "BtnShow"
        Me.BtnShow.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BtnShow.Size = New System.Drawing.Size(82, 23)
        Me.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnShow.TabIndex = 144
        Me.BtnShow.Text = "Show"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(0, 395)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(1065, 23)
        Me.ProgressBar1.TabIndex = 142
        '
        'txtDate2
        '
        '
        '
        '
        Me.txtDate2.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate2.ButtonDropDown.Visible = True
        Me.txtDate2.IsPopupCalendarOpen = False
        Me.txtDate2.Location = New System.Drawing.Point(266, 427)
        '
        '
        '
        Me.txtDate2.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate2.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate2.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate2.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate2.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate2.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate2.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate2.MonthCalendar.TodayButtonVisible = True
        Me.txtDate2.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate2.Name = "txtDate2"
        Me.txtDate2.Size = New System.Drawing.Size(141, 20)
        Me.txtDate2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate2.TabIndex = 141
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(231, 427)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(66, 23)
        Me.LabelX1.TabIndex = 140
        Me.LabelX1.Text = "S.D"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(3, 424)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(66, 23)
        Me.LabelX2.TabIndex = 139
        Me.LabelX2.Text = "PERIODE"
        '
        'txtDate1
        '
        '
        '
        '
        Me.txtDate1.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate1.ButtonDropDown.Visible = True
        Me.txtDate1.IsPopupCalendarOpen = False
        Me.txtDate1.Location = New System.Drawing.Point(75, 427)
        '
        '
        '
        Me.txtDate1.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate1.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate1.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate1.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate1.MonthCalendar.TodayButtonVisible = True
        Me.txtDate1.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate1.Name = "txtDate1"
        Me.txtDate1.Size = New System.Drawing.Size(141, 20)
        Me.txtDate1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate1.TabIndex = 138
        '
        'DGView
        '
        Me.DGView.AllowUserToAddRows = False
        Me.DGView.AllowUserToDeleteRows = False
        Me.DGView.AllowUserToOrderColumns = True
        Me.DGView.AllowUserToResizeRows = False
        Me.DGView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.no_transaksi, Me.jenis, Me.tanggal, Me.kd_unitbisnis, Me.nama_unitbisnis, Me.posting})
        Me.DGView.Location = New System.Drawing.Point(0, 7)
        Me.DGView.Name = "DGView"
        Me.DGView.ReadOnly = True
        Me.DGView.Size = New System.Drawing.Size(1065, 392)
        Me.DGView.TabIndex = 121
        '
        'no_transaksi
        '
        Me.no_transaksi.HeaderText = "NO TRANSAKSI"
        Me.no_transaksi.Name = "no_transaksi"
        Me.no_transaksi.ReadOnly = True
        Me.no_transaksi.Width = 250
        '
        'jenis
        '
        Me.jenis.HeaderText = "JENIS"
        Me.jenis.Name = "jenis"
        Me.jenis.ReadOnly = True
        Me.jenis.Width = 150
        '
        'tanggal
        '
        Me.tanggal.HeaderText = "TANGGAL"
        Me.tanggal.Name = "tanggal"
        Me.tanggal.ReadOnly = True
        Me.tanggal.Width = 150
        '
        'kd_unitbisnis
        '
        Me.kd_unitbisnis.HeaderText = "UNITBISNIS ID"
        Me.kd_unitbisnis.Name = "kd_unitbisnis"
        Me.kd_unitbisnis.ReadOnly = True
        Me.kd_unitbisnis.Width = 170
        '
        'nama_unitbisnis
        '
        Me.nama_unitbisnis.HeaderText = "NAMA UNITBISNIS"
        Me.nama_unitbisnis.Name = "nama_unitbisnis"
        Me.nama_unitbisnis.ReadOnly = True
        Me.nama_unitbisnis.Width = 220
        '
        'posting
        '
        Me.posting.HeaderText = "POSTING"
        Me.posting.Name = "posting"
        Me.posting.ReadOnly = True
        Me.posting.Width = 70
        '
        'frmPosting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1114, 481)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmPosting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Posting"
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.txtDate2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents DGView As System.Windows.Forms.DataGridView
    Friend WithEvents txtDate2 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDate1 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnShow As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents no_transaksi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jenis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tanggal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents kd_unitbisnis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nama_unitbisnis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents posting As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
