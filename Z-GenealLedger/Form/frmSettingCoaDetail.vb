﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmSettingCoaDetail
    Dim xytipe As String
    Dim savetipe As String

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        blnAdd = False
        blnEdit = False
        Me.Close()
    End Sub

    Private Sub btngroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngroup.Click
        blnCoaSetting = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub frmSettingCoaDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If blnAdd = True Then
            CboNamaTabel.Text = ""
            CboNamaField.Text = ""
            txtkdUnit.Text = ""
            txtNamaUnit.Text = ""
            txtIdUnit.Text = ""
            txtAccSetting.Text = ""
        Else
            ShowData()
        End If

        If blnAdd = True Or blnEdit = True Then
            BtnSave.Enabled = True
        Else
            BtnSave.Enabled = False
        End If

        ComboTabel()

    End Sub

    Sub ShowData()

        SQL = ""
        SQL = " select a.kd_akun_perkiraan,b.id,b.nama,a.tanggal_buat,a.nama_tabel,a.type_perkiraan,a.nama_field,a.acc_setting from acc_perkiraan_setting2 a"
        SQL = SQL & " left join acc_perkiraan_nama b on a.kd_akun_perkiraan=b.kode COLLATE SQL_Latin1_General_CP1_CI_AS where a.id_acc_perkiraan_setting = " & xyzID & ""

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read
                txtID.Text = xyzID
                CboNamaTabel.Text = Trim(dr.Item("nama_tabel").ToString())
                CboNamaField.Text = Trim(dr.Item("nama_field").ToString())
                txtkdUnit.Text = Trim(dr.Item("kd_akun_perkiraan").ToString())
                txtNamaUnit.Text = Trim(dr.Item("nama").ToString())
                txtIdUnit.Text = Trim(dr.Item("id").ToString())
                txtAccSetting.Text = Trim(dr.Item("acc_setting").ToString())
                xytipe = Trim(dr.Item("type_perkiraan").ToString())

                If xytipe = "D" Then
                    RBD.Checked = True
                Else
                    RBK.Checked = True
                End If

            End While
        End If

        dr.Close()

    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        If txtkdUnit.Text = "" Or CboNamaTabel.Text = "" Or CboNamaField.Text = "" Then
            MsgBox("Masih Ada Data Yang Kosong...", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If RBD.Checked = True Then
            savetipe = "D"
        Else
            savetipe = "K"
        End If

        IsConError = True

        If blnAdd = True Then
            dr.Close()
            SQL = "Select * From acc_perkiraan_setting2 where kd_akun_perkiraan='" & Trim(txtkdUnit.Text) & "' and nama_tabel='" & Konek.BuatKomaSatu(CboNamaTabel.Text) & "' and nama_field='" & Konek.BuatKomaSatu(CboNamaField.Text) & "' and acc_setting='" & Konek.BuatKomaSatu(txtAccSetting.Text) & "'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            dr.Read()

            If dr.HasRows Then
                MsgBox("Data tersebut sudah ada...", MsgBoxStyle.Critical)
                Exit Sub
            Else
                dr.Close()
                SQL = "Insert Into acc_perkiraan_setting2 (id_perkiraan_nama,id_unitbisnis_nama,tanggal_buat,user_buat,nama_tabel,nama_field,kd_akun_perkiraan,acc_setting,type_perkiraan) "
                SQL = SQL & " Values(" & txtIdUnit.Text & "," & myIDCabang & ",'" & Format(Now, "yyyy-MM-dd hh:mm:ss") & "'," & myID & ",'" & Konek.BuatKomaSatu(CboNamaTabel.Text) & "','" & Konek.BuatKomaSatu(CboNamaField.Text) & "','" & txtkdUnit.Text & "','" & Konek.BuatKomaSatu(txtAccSetting.Text) & "','" & savetipe & "')"
                Konek.IUDQuery(SQL)

            End If

        ElseIf blnEdit = True Then
            dr.Close()
            SQL = "update acc_perkiraan_setting2 set nama_tabel='" & Konek.BuatKomaSatu(CboNamaTabel.Text) & "',nama_field='" & Konek.BuatKomaSatu(CboNamaField.Text) & "',tanggal_ubah='" & Format(Now, "yyyy-MM-dd hh:mm:ss") & "',"
            SQL = SQL & " kd_akun_perkiraan='" & txtkdUnit.Text & "',id_perkiraan_nama=" & txtIdUnit.Text & ",user_ubah=" & myID & ",acc_setting='" & Konek.BuatKomaSatu(txtAccSetting.Text) & "',type_perkiraan='" & savetipe & "'"
            SQL = SQL & " where id_acc_perkiraan_setting=" & txtID.Text & ""

            Konek.IUDQuery(SQL)

        End If

        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        blnAdd = False
        blnEdit = False

        Me.Close()
        Exit Sub

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

    Sub ComboTabel()
        SQL = ""
        SQL = " SELECT name FROM sys.tables order by name"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            Do While dr.Read = True
                CboNamaTabel.Items.Add(dr.GetString(0))
            Loop
        End If

        dr.Close()
    End Sub

    Sub ComboField()
        SQL = ""
        SQL = " SELECT col.name as ColumnName FROM sys.columns col INNER JOIN sys.tables tbl ON col.object_id=tbl.object_id WHERE tbl.Name='" & Trim(CboNamaTabel.Text) & "'"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            Do While dr.Read = True
                CboNamaField.Items.Add(dr.GetString(0))
            Loop
        End If

        dr.Close()

    End Sub

    Private Sub CboNamaField_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboNamaField.Click
        ComboField()
    End Sub
End Class