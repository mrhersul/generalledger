﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits DevComponents.DotNetBar.Office2007RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.RibbonControl1 = New DevComponents.DotNetBar.RibbonControl()
        Me.RibbonPanel4 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar13 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnUnclosing = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar12 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnClosing = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar11 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnSaldoAwal = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar10 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnSettingPerAcct = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar7 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar9 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnPassword = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel2 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar21 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnJurnalUmum = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar16 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnJurnalSesuai = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar6 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnBankOut = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem50 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem51 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar5 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnKasIn = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem47 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem48 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel1 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar2 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnCoa = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnCoaGroup = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel5 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar29 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem43 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar28 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem42 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar27 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem41 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar26 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem40 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar25 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem39 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem44 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem45 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem46 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar24 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem38 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel3 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar23 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem33 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem34 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem35 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem36 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem37 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar22 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem24 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem25 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem26 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem27 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem28 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem29 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem30 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem31 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem32 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar20 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem23 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar19 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem18 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem19 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem20 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem21 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem22 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar17 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem15 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem16 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem17 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar14 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem8 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem9 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem10 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem11 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem12 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem13 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem14 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar18 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnDistToko = New DevComponents.DotNetBar.ButtonItem()
        Me.rb1 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnNeraca = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem6 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar8 = New DevComponents.DotNetBar.RibbonBar()
        Me.BtnRepBalance = New DevComponents.DotNetBar.ButtonItem()
        Me.ApplicationButton1 = New DevComponents.DotNetBar.ApplicationButton()
        Me.ItemContainer1 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer2 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer3 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemContainer4 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.RTMaster = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RTTransaction = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem1 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RTReport = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RTTools = New DevComponents.DotNetBar.RibbonTabItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.BtnThemes = New DevComponents.DotNetBar.ButtonItem()
        Me.Office2007Blue = New DevComponents.DotNetBar.LabelItem()
        Me.Office2007Black = New DevComponents.DotNetBar.LabelItem()
        Me.Office2007Silver = New DevComponents.DotNetBar.LabelItem()
        Me.Office2007VistaGlass = New DevComponents.DotNetBar.LabelItem()
        Me.Office2010Black = New DevComponents.DotNetBar.LabelItem()
        Me.Office2010Blue = New DevComponents.DotNetBar.LabelItem()
        Me.Office2010Silver = New DevComponents.DotNetBar.LabelItem()
        Me.Office2013 = New DevComponents.DotNetBar.LabelItem()
        Me.VisualStudio2010Blue = New DevComponents.DotNetBar.LabelItem()
        Me.VisualStudio2012Dark = New DevComponents.DotNetBar.LabelItem()
        Me.VisualStudio2012Light = New DevComponents.DotNetBar.LabelItem()
        Me.Windows7Blue = New DevComponents.DotNetBar.LabelItem()
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolDept = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolBulan = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txUser = New System.Windows.Forms.TextBox()
        Me.txPassword = New System.Windows.Forms.TextBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.BtnRepStock = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemContainer5 = New DevComponents.DotNetBar.ItemContainer()
        Me.RibbonControl1.SuspendLayout()
        Me.RibbonPanel4.SuspendLayout()
        Me.RibbonPanel2.SuspendLayout()
        Me.RibbonPanel1.SuspendLayout()
        Me.RibbonPanel5.SuspendLayout()
        Me.RibbonPanel3.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl1
        '
        Me.RibbonControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.RibbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonControl1.CaptionVisible = True
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel1)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel4)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel3)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel2)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel5)
        Me.RibbonControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RibbonControl1.ForeColor = System.Drawing.Color.Black
        Me.RibbonControl1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ApplicationButton1, Me.RTMaster, Me.RTTransaction, Me.RibbonTabItem1, Me.RTReport, Me.RTTools})
        Me.RibbonControl1.KeyTipsFont = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RibbonControl1.Location = New System.Drawing.Point(5, 1)
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 3)
        Me.RibbonControl1.QuickToolbarItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem1})
        Me.RibbonControl1.Size = New System.Drawing.Size(959, 154)
        Me.RibbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon"
        Me.RibbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon"
        Me.RibbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>"
        Me.RibbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar..."
        Me.RibbonControl1.SystemText.QatDialogAddButton = "&Add >>"
        Me.RibbonControl1.SystemText.QatDialogCancelButton = "Cancel"
        Me.RibbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:"
        Me.RibbonControl1.SystemText.QatDialogOkButton = "OK"
        Me.RibbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatDialogRemoveButton = "&Remove"
        Me.RibbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon"
        Me.RibbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar"
        Me.RibbonControl1.TabGroupHeight = 14
        Me.RibbonControl1.TabIndex = 1
        Me.RibbonControl1.Text = "RibbonControl1"
        '
        'RibbonPanel4
        '
        Me.RibbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel4.Controls.Add(Me.RibbonBar13)
        Me.RibbonPanel4.Controls.Add(Me.RibbonBar12)
        Me.RibbonPanel4.Controls.Add(Me.RibbonBar11)
        Me.RibbonPanel4.Controls.Add(Me.RibbonBar10)
        Me.RibbonPanel4.Controls.Add(Me.RibbonBar7)
        Me.RibbonPanel4.Controls.Add(Me.RibbonBar9)
        Me.RibbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel4.Location = New System.Drawing.Point(0, 51)
        Me.RibbonPanel4.Name = "RibbonPanel4"
        Me.RibbonPanel4.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel4.Size = New System.Drawing.Size(959, 100)
        '
        '
        '
        Me.RibbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel4.TabIndex = 4
        Me.RibbonPanel4.Visible = False
        '
        'RibbonBar13
        '
        Me.RibbonBar13.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar13.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar13.ContainerControlProcessDialogKey = True
        Me.RibbonBar13.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar13.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnUnclosing})
        Me.RibbonBar13.Location = New System.Drawing.Point(394, 0)
        Me.RibbonBar13.Name = "RibbonBar13"
        Me.RibbonBar13.Size = New System.Drawing.Size(71, 97)
        Me.RibbonBar13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar13.TabIndex = 6
        '
        '
        '
        Me.RibbonBar13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar13.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar13.Visible = False
        '
        'BtnUnclosing
        '
        Me.BtnUnclosing.Image = CType(resources.GetObject("BtnUnclosing.Image"), System.Drawing.Image)
        Me.BtnUnclosing.ImageAlt = CType(resources.GetObject("BtnUnclosing.ImageAlt"), System.Drawing.Image)
        Me.BtnUnclosing.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnUnclosing.Name = "BtnUnclosing"
        Me.BtnUnclosing.SubItemsExpandWidth = 14
        Me.BtnUnclosing.Text = "Un-Closing Bulanan"
        '
        'RibbonBar12
        '
        Me.RibbonBar12.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar12.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar12.ContainerControlProcessDialogKey = True
        Me.RibbonBar12.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar12.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnClosing})
        Me.RibbonBar12.Location = New System.Drawing.Point(329, 0)
        Me.RibbonBar12.Name = "RibbonBar12"
        Me.RibbonBar12.Size = New System.Drawing.Size(65, 97)
        Me.RibbonBar12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar12.TabIndex = 5
        '
        '
        '
        Me.RibbonBar12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar12.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnClosing
        '
        Me.BtnClosing.Image = CType(resources.GetObject("BtnClosing.Image"), System.Drawing.Image)
        Me.BtnClosing.ImageAlt = CType(resources.GetObject("BtnClosing.ImageAlt"), System.Drawing.Image)
        Me.BtnClosing.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClosing.Name = "BtnClosing"
        Me.BtnClosing.SubItemsExpandWidth = 14
        Me.BtnClosing.Text = "Closing Bulanan / Posting"
        '
        'RibbonBar11
        '
        Me.RibbonBar11.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar11.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar11.ContainerControlProcessDialogKey = True
        Me.RibbonBar11.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar11.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnSaldoAwal})
        Me.RibbonBar11.Location = New System.Drawing.Point(260, 0)
        Me.RibbonBar11.Name = "RibbonBar11"
        Me.RibbonBar11.Size = New System.Drawing.Size(69, 97)
        Me.RibbonBar11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar11.TabIndex = 4
        '
        '
        '
        Me.RibbonBar11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar11.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar11.Visible = False
        '
        'BtnSaldoAwal
        '
        Me.BtnSaldoAwal.Image = CType(resources.GetObject("BtnSaldoAwal.Image"), System.Drawing.Image)
        Me.BtnSaldoAwal.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSaldoAwal.Name = "BtnSaldoAwal"
        Me.BtnSaldoAwal.SubItemsExpandWidth = 14
        Me.BtnSaldoAwal.Text = "Saldo Awal Perkiraan"
        '
        'RibbonBar10
        '
        Me.RibbonBar10.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar10.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar10.ContainerControlProcessDialogKey = True
        Me.RibbonBar10.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar10.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnSettingPerAcct})
        Me.RibbonBar10.Location = New System.Drawing.Point(159, 0)
        Me.RibbonBar10.Name = "RibbonBar10"
        Me.RibbonBar10.Size = New System.Drawing.Size(101, 97)
        Me.RibbonBar10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar10.TabIndex = 3
        '
        '
        '
        Me.RibbonBar10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar10.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnSettingPerAcct
        '
        Me.BtnSettingPerAcct.Image = CType(resources.GetObject("BtnSettingPerAcct.Image"), System.Drawing.Image)
        Me.BtnSettingPerAcct.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSettingPerAcct.Name = "BtnSettingPerAcct"
        Me.BtnSettingPerAcct.SubItemsExpandWidth = 14
        Me.BtnSettingPerAcct.Text = "Setting Periode Akuntansi & Saldo Awal"
        '
        'RibbonBar7
        '
        Me.RibbonBar7.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar7.ContainerControlProcessDialogKey = True
        Me.RibbonBar7.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar7.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem4})
        Me.RibbonBar7.Location = New System.Drawing.Point(91, 0)
        Me.RibbonBar7.Name = "RibbonBar7"
        Me.RibbonBar7.Size = New System.Drawing.Size(68, 97)
        Me.RibbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar7.TabIndex = 2
        '
        '
        '
        Me.RibbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem4
        '
        Me.ButtonItem4.Image = CType(resources.GetObject("ButtonItem4.Image"), System.Drawing.Image)
        Me.ButtonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.SubItemsExpandWidth = 14
        Me.ButtonItem4.Text = "Setting Database"
        '
        'RibbonBar9
        '
        Me.RibbonBar9.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar9.ContainerControlProcessDialogKey = True
        Me.RibbonBar9.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar9.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnPassword})
        Me.RibbonBar9.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar9.Name = "RibbonBar9"
        Me.RibbonBar9.Size = New System.Drawing.Size(88, 97)
        Me.RibbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar9.TabIndex = 1
        '
        '
        '
        Me.RibbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnPassword
        '
        Me.BtnPassword.Image = CType(resources.GetObject("BtnPassword.Image"), System.Drawing.Image)
        Me.BtnPassword.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnPassword.Name = "BtnPassword"
        Me.BtnPassword.SubItemsExpandWidth = 14
        Me.BtnPassword.Text = "Setting Perkiraan Print"
        '
        'RibbonPanel2
        '
        Me.RibbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar21)
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar16)
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar6)
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar5)
        Me.RibbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel2.Location = New System.Drawing.Point(0, 51)
        Me.RibbonPanel2.Name = "RibbonPanel2"
        Me.RibbonPanel2.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel2.Size = New System.Drawing.Size(959, 100)
        '
        '
        '
        Me.RibbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel2.TabIndex = 2
        Me.RibbonPanel2.Visible = False
        '
        'RibbonBar21
        '
        Me.RibbonBar21.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar21.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar21.ContainerControlProcessDialogKey = True
        Me.RibbonBar21.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar21.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnJurnalUmum})
        Me.RibbonBar21.Location = New System.Drawing.Point(188, 0)
        Me.RibbonBar21.Name = "RibbonBar21"
        Me.RibbonBar21.Size = New System.Drawing.Size(50, 97)
        Me.RibbonBar21.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar21.TabIndex = 5
        '
        '
        '
        Me.RibbonBar21.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar21.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnJurnalUmum
        '
        Me.BtnJurnalUmum.Image = CType(resources.GetObject("BtnJurnalUmum.Image"), System.Drawing.Image)
        Me.BtnJurnalUmum.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnJurnalUmum.Name = "BtnJurnalUmum"
        Me.BtnJurnalUmum.SubItemsExpandWidth = 14
        Me.BtnJurnalUmum.Text = "Jurnal Umum"
        '
        'RibbonBar16
        '
        Me.RibbonBar16.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar16.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar16.ContainerControlProcessDialogKey = True
        Me.RibbonBar16.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar16.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnJurnalSesuai})
        Me.RibbonBar16.Location = New System.Drawing.Point(107, 0)
        Me.RibbonBar16.Name = "RibbonBar16"
        Me.RibbonBar16.Size = New System.Drawing.Size(81, 97)
        Me.RibbonBar16.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar16.TabIndex = 4
        '
        '
        '
        Me.RibbonBar16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar16.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnJurnalSesuai
        '
        Me.BtnJurnalSesuai.Image = CType(resources.GetObject("BtnJurnalSesuai.Image"), System.Drawing.Image)
        Me.BtnJurnalSesuai.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnJurnalSesuai.Name = "BtnJurnalSesuai"
        Me.BtnJurnalSesuai.SubItemsExpandWidth = 14
        Me.BtnJurnalSesuai.Text = "Jurnal Penyesuaian"
        '
        'RibbonBar6
        '
        Me.RibbonBar6.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar6.ContainerControlProcessDialogKey = True
        Me.RibbonBar6.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar6.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnBankOut})
        Me.RibbonBar6.Location = New System.Drawing.Point(55, 0)
        Me.RibbonBar6.Name = "RibbonBar6"
        Me.RibbonBar6.Size = New System.Drawing.Size(52, 97)
        Me.RibbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar6.TabIndex = 1
        '
        '
        '
        Me.RibbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnBankOut
        '
        Me.BtnBankOut.Image = CType(resources.GetObject("BtnBankOut.Image"), System.Drawing.Image)
        Me.BtnBankOut.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnBankOut.Name = "BtnBankOut"
        Me.BtnBankOut.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem50, Me.ButtonItem51})
        Me.BtnBankOut.SubItemsExpandWidth = 14
        Me.BtnBankOut.Text = "Bank"
        '
        'ButtonItem50
        '
        Me.ButtonItem50.Name = "ButtonItem50"
        Me.ButtonItem50.Text = "Bank Masuk"
        '
        'ButtonItem51
        '
        Me.ButtonItem51.Name = "ButtonItem51"
        Me.ButtonItem51.Text = "Bank Keluar"
        '
        'RibbonBar5
        '
        Me.RibbonBar5.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar5.ContainerControlProcessDialogKey = True
        Me.RibbonBar5.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar5.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnKasIn})
        Me.RibbonBar5.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar5.Name = "RibbonBar5"
        Me.RibbonBar5.Size = New System.Drawing.Size(52, 97)
        Me.RibbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar5.TabIndex = 0
        '
        '
        '
        Me.RibbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnKasIn
        '
        Me.BtnKasIn.Image = CType(resources.GetObject("BtnKasIn.Image"), System.Drawing.Image)
        Me.BtnKasIn.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnKasIn.Name = "BtnKasIn"
        Me.BtnKasIn.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem47, Me.ButtonItem48})
        Me.BtnKasIn.SubItemsExpandWidth = 14
        Me.BtnKasIn.Text = "Kas"
        '
        'ButtonItem47
        '
        Me.ButtonItem47.Name = "ButtonItem47"
        Me.ButtonItem47.Text = "Kas Masuk"
        '
        'ButtonItem48
        '
        Me.ButtonItem48.Name = "ButtonItem48"
        Me.ButtonItem48.Text = "Kas Keluar"
        '
        'RibbonPanel1
        '
        Me.RibbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel1.Controls.Add(Me.RibbonBar2)
        Me.RibbonPanel1.Controls.Add(Me.RibbonBar1)
        Me.RibbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel1.Location = New System.Drawing.Point(0, 51)
        Me.RibbonPanel1.Name = "RibbonPanel1"
        Me.RibbonPanel1.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel1.Size = New System.Drawing.Size(959, 100)
        '
        '
        '
        Me.RibbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel1.TabIndex = 1
        '
        'RibbonBar2
        '
        Me.RibbonBar2.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.ContainerControlProcessDialogKey = True
        Me.RibbonBar2.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnCoa})
        Me.RibbonBar2.Location = New System.Drawing.Point(69, 0)
        Me.RibbonBar2.Name = "RibbonBar2"
        Me.RibbonBar2.Size = New System.Drawing.Size(69, 97)
        Me.RibbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar2.TabIndex = 1
        '
        '
        '
        Me.RibbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnCoa
        '
        Me.BtnCoa.Image = CType(resources.GetObject("BtnCoa.Image"), System.Drawing.Image)
        Me.BtnCoa.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnCoa.Name = "BtnCoa"
        Me.BtnCoa.SubItemsExpandWidth = 14
        Me.BtnCoa.Text = "Perkiraan"
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnCoaGroup})
        Me.RibbonBar1.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(66, 97)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar1.TabIndex = 0
        '
        '
        '
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnCoaGroup
        '
        Me.BtnCoaGroup.Image = CType(resources.GetObject("BtnCoaGroup.Image"), System.Drawing.Image)
        Me.BtnCoaGroup.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnCoaGroup.Name = "BtnCoaGroup"
        Me.BtnCoaGroup.SubItemsExpandWidth = 14
        Me.BtnCoaGroup.Text = "Group Perkiraan"
        '
        'RibbonPanel5
        '
        Me.RibbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar29)
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar28)
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar27)
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar26)
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar25)
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar24)
        Me.RibbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel5.Location = New System.Drawing.Point(0, 51)
        Me.RibbonPanel5.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonPanel5.Name = "RibbonPanel5"
        Me.RibbonPanel5.Padding = New System.Windows.Forms.Padding(2, 0, 2, 2)
        Me.RibbonPanel5.Size = New System.Drawing.Size(959, 100)
        '
        '
        '
        Me.RibbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel5.TabIndex = 5
        Me.RibbonPanel5.Visible = False
        '
        'RibbonBar29
        '
        Me.RibbonBar29.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar29.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar29.ContainerControlProcessDialogKey = True
        Me.RibbonBar29.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar29.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RibbonBar29.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar29.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem43})
        Me.RibbonBar29.Location = New System.Drawing.Point(374, 0)
        Me.RibbonBar29.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonBar29.Name = "RibbonBar29"
        Me.RibbonBar29.Size = New System.Drawing.Size(75, 98)
        Me.RibbonBar29.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar29.TabIndex = 5
        Me.RibbonBar29.Text = "SETTING"
        '
        '
        '
        Me.RibbonBar29.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar29.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar29.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem43
        '
        Me.ButtonItem43.Image = CType(resources.GetObject("ButtonItem43.Image"), System.Drawing.Image)
        Me.ButtonItem43.Name = "ButtonItem43"
        Me.ButtonItem43.SubItemsExpandWidth = 14
        Me.ButtonItem43.Text = "Konfigurasi"
        '
        'RibbonBar28
        '
        Me.RibbonBar28.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar28.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar28.ContainerControlProcessDialogKey = True
        Me.RibbonBar28.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar28.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RibbonBar28.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar28.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem42})
        Me.RibbonBar28.Location = New System.Drawing.Point(299, 0)
        Me.RibbonBar28.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonBar28.Name = "RibbonBar28"
        Me.RibbonBar28.Size = New System.Drawing.Size(75, 98)
        Me.RibbonBar28.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar28.TabIndex = 4
        Me.RibbonBar28.Text = "PAYROLL"
        '
        '
        '
        Me.RibbonBar28.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar28.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar28.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem42
        '
        Me.ButtonItem42.Image = CType(resources.GetObject("ButtonItem42.Image"), System.Drawing.Image)
        Me.ButtonItem42.Name = "ButtonItem42"
        Me.ButtonItem42.SubItemsExpandWidth = 14
        Me.ButtonItem42.Text = "Payroll"
        '
        'RibbonBar27
        '
        Me.RibbonBar27.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar27.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar27.ContainerControlProcessDialogKey = True
        Me.RibbonBar27.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar27.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RibbonBar27.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar27.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem41})
        Me.RibbonBar27.Location = New System.Drawing.Point(224, 0)
        Me.RibbonBar27.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonBar27.Name = "RibbonBar27"
        Me.RibbonBar27.Size = New System.Drawing.Size(75, 98)
        Me.RibbonBar27.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar27.TabIndex = 3
        Me.RibbonBar27.Text = "ABSENSI"
        '
        '
        '
        Me.RibbonBar27.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar27.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar27.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem41
        '
        Me.ButtonItem41.Image = CType(resources.GetObject("ButtonItem41.Image"), System.Drawing.Image)
        Me.ButtonItem41.Name = "ButtonItem41"
        Me.ButtonItem41.SubItemsExpandWidth = 14
        Me.ButtonItem41.Text = "Absensi Karyawan"
        '
        'RibbonBar26
        '
        Me.RibbonBar26.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar26.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar26.ContainerControlProcessDialogKey = True
        Me.RibbonBar26.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar26.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RibbonBar26.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar26.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem40})
        Me.RibbonBar26.Location = New System.Drawing.Point(149, 0)
        Me.RibbonBar26.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonBar26.Name = "RibbonBar26"
        Me.RibbonBar26.Size = New System.Drawing.Size(75, 98)
        Me.RibbonBar26.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar26.TabIndex = 2
        Me.RibbonBar26.Text = "CATATAN"
        '
        '
        '
        Me.RibbonBar26.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar26.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar26.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem40
        '
        Me.ButtonItem40.Image = CType(resources.GetObject("ButtonItem40.Image"), System.Drawing.Image)
        Me.ButtonItem40.Name = "ButtonItem40"
        Me.ButtonItem40.SubItemsExpandWidth = 14
        Me.ButtonItem40.Text = "Catatan Karyawan"
        '
        'RibbonBar25
        '
        Me.RibbonBar25.AutoOverflowEnabled = True
        Me.RibbonBar25.BackColor = System.Drawing.SystemColors.ActiveBorder
        '
        '
        '
        Me.RibbonBar25.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar25.ContainerControlProcessDialogKey = True
        Me.RibbonBar25.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar25.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RibbonBar25.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar25.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem39})
        Me.RibbonBar25.Location = New System.Drawing.Point(74, 0)
        Me.RibbonBar25.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonBar25.Name = "RibbonBar25"
        Me.RibbonBar25.Size = New System.Drawing.Size(75, 98)
        Me.RibbonBar25.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar25.TabIndex = 1
        Me.RibbonBar25.Text = "LEAVING"
        '
        '
        '
        Me.RibbonBar25.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar25.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar25.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem39
        '
        Me.ButtonItem39.Image = CType(resources.GetObject("ButtonItem39.Image"), System.Drawing.Image)
        Me.ButtonItem39.Name = "ButtonItem39"
        Me.ButtonItem39.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem44, Me.ButtonItem45, Me.ButtonItem46})
        Me.ButtonItem39.SubItemsExpandWidth = 14
        Me.ButtonItem39.Text = "Manajemen Leaving"
        '
        'ButtonItem44
        '
        Me.ButtonItem44.Image = CType(resources.GetObject("ButtonItem44.Image"), System.Drawing.Image)
        Me.ButtonItem44.Name = "ButtonItem44"
        Me.ButtonItem44.Text = "INPUT CUTI"
        '
        'ButtonItem45
        '
        Me.ButtonItem45.Image = CType(resources.GetObject("ButtonItem45.Image"), System.Drawing.Image)
        Me.ButtonItem45.Name = "ButtonItem45"
        Me.ButtonItem45.Text = "INPUT IZIN"
        '
        'ButtonItem46
        '
        Me.ButtonItem46.Image = CType(resources.GetObject("ButtonItem46.Image"), System.Drawing.Image)
        Me.ButtonItem46.Name = "ButtonItem46"
        Me.ButtonItem46.Text = "INPUT TUGAS DINAS"
        '
        'RibbonBar24
        '
        Me.RibbonBar24.AutoOverflowEnabled = False
        '
        '
        '
        Me.RibbonBar24.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar24.ContainerControlProcessDialogKey = True
        Me.RibbonBar24.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar24.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RibbonBar24.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar24.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem38})
        Me.RibbonBar24.ItemSpacing = 2
        Me.RibbonBar24.Location = New System.Drawing.Point(2, 0)
        Me.RibbonBar24.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonBar24.MaximumOverflowTextLength = 50
        Me.RibbonBar24.Name = "RibbonBar24"
        Me.RibbonBar24.Size = New System.Drawing.Size(72, 98)
        Me.RibbonBar24.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar24.TabIndex = 0
        Me.RibbonBar24.Text = "INFO"
        '
        '
        '
        Me.RibbonBar24.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar24.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar24.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Bottom
        '
        'ButtonItem38
        '
        Me.ButtonItem38.Image = CType(resources.GetObject("ButtonItem38.Image"), System.Drawing.Image)
        Me.ButtonItem38.Name = "ButtonItem38"
        Me.ButtonItem38.SubItemsExpandWidth = 14
        Me.ButtonItem38.Text = "Informasi Karyawan"
        '
        'RibbonPanel3
        '
        Me.RibbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar23)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar22)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar20)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar19)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar17)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar14)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar18)
        Me.RibbonPanel3.Controls.Add(Me.rb1)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar8)
        Me.RibbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel3.Location = New System.Drawing.Point(0, 51)
        Me.RibbonPanel3.Name = "RibbonPanel3"
        Me.RibbonPanel3.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel3.Size = New System.Drawing.Size(959, 100)
        '
        '
        '
        Me.RibbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel3.TabIndex = 3
        Me.RibbonPanel3.Visible = False
        '
        'RibbonBar23
        '
        Me.RibbonBar23.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar23.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar23.ContainerControlProcessDialogKey = True
        Me.RibbonBar23.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar23.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem33})
        Me.RibbonBar23.Location = New System.Drawing.Point(478, 0)
        Me.RibbonBar23.Name = "RibbonBar23"
        Me.RibbonBar23.Size = New System.Drawing.Size(100, 97)
        Me.RibbonBar23.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar23.TabIndex = 10
        '
        '
        '
        Me.RibbonBar23.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar23.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem33
        '
        Me.ButtonItem33.Image = CType(resources.GetObject("ButtonItem33.Image"), System.Drawing.Image)
        Me.ButtonItem33.ImageAlt = CType(resources.GetObject("ButtonItem33.ImageAlt"), System.Drawing.Image)
        Me.ButtonItem33.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem33.Name = "ButtonItem33"
        Me.ButtonItem33.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem34, Me.ButtonItem35, Me.ButtonItem36, Me.ButtonItem37})
        Me.ButtonItem33.SubItemsExpandWidth = 14
        Me.ButtonItem33.Text = "Biaya"
        '
        'ButtonItem34
        '
        Me.ButtonItem34.Name = "ButtonItem34"
        Me.ButtonItem34.Text = "Per Cabang Level 5"
        '
        'ButtonItem35
        '
        Me.ButtonItem35.Name = "ButtonItem35"
        Me.ButtonItem35.Text = "Per Cabang Level 3"
        '
        'ButtonItem36
        '
        Me.ButtonItem36.Name = "ButtonItem36"
        Me.ButtonItem36.Text = "Per Outlet Level 5"
        '
        'ButtonItem37
        '
        Me.ButtonItem37.Name = "ButtonItem37"
        Me.ButtonItem37.Text = "Per Outlet Level 3"
        '
        'RibbonBar22
        '
        Me.RibbonBar22.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar22.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar22.ContainerControlProcessDialogKey = True
        Me.RibbonBar22.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar22.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem24})
        Me.RibbonBar22.Location = New System.Drawing.Point(428, 0)
        Me.RibbonBar22.Name = "RibbonBar22"
        Me.RibbonBar22.Size = New System.Drawing.Size(50, 97)
        Me.RibbonBar22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar22.TabIndex = 9
        '
        '
        '
        Me.RibbonBar22.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar22.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem24
        '
        Me.ButtonItem24.Image = CType(resources.GetObject("ButtonItem24.Image"), System.Drawing.Image)
        Me.ButtonItem24.ImageAlt = CType(resources.GetObject("ButtonItem24.ImageAlt"), System.Drawing.Image)
        Me.ButtonItem24.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem24.Name = "ButtonItem24"
        Me.ButtonItem24.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem25, Me.ButtonItem26, Me.ButtonItem27, Me.ButtonItem28, Me.ButtonItem29, Me.ButtonItem30, Me.ButtonItem31, Me.ButtonItem32})
        Me.ButtonItem24.SubItemsExpandWidth = 14
        Me.ButtonItem24.Text = "Jurnal Entris"
        '
        'ButtonItem25
        '
        Me.ButtonItem25.Name = "ButtonItem25"
        Me.ButtonItem25.Text = "Kas Masuk"
        '
        'ButtonItem26
        '
        Me.ButtonItem26.Name = "ButtonItem26"
        Me.ButtonItem26.Text = "Kas Keluar"
        '
        'ButtonItem27
        '
        Me.ButtonItem27.Name = "ButtonItem27"
        Me.ButtonItem27.Text = "Bank Masuk"
        '
        'ButtonItem28
        '
        Me.ButtonItem28.Name = "ButtonItem28"
        Me.ButtonItem28.Text = "Bank Keluar"
        '
        'ButtonItem29
        '
        Me.ButtonItem29.Name = "ButtonItem29"
        Me.ButtonItem29.Text = "Penerimaan Barang"
        '
        'ButtonItem30
        '
        Me.ButtonItem30.Name = "ButtonItem30"
        Me.ButtonItem30.Text = "Faktur Sewa Beli"
        '
        'ButtonItem31
        '
        Me.ButtonItem31.Name = "ButtonItem31"
        Me.ButtonItem31.Text = "Pengeluaran Barang"
        '
        'ButtonItem32
        '
        Me.ButtonItem32.Name = "ButtonItem32"
        Me.ButtonItem32.Text = "Memo Jurnal"
        '
        'RibbonBar20
        '
        Me.RibbonBar20.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar20.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar20.ContainerControlProcessDialogKey = True
        Me.RibbonBar20.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar20.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem23})
        Me.RibbonBar20.Location = New System.Drawing.Point(345, 0)
        Me.RibbonBar20.Name = "RibbonBar20"
        Me.RibbonBar20.Size = New System.Drawing.Size(83, 97)
        Me.RibbonBar20.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar20.TabIndex = 8
        '
        '
        '
        Me.RibbonBar20.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar20.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem23
        '
        Me.ButtonItem23.Image = CType(resources.GetObject("ButtonItem23.Image"), System.Drawing.Image)
        Me.ButtonItem23.ImageAlt = CType(resources.GetObject("ButtonItem23.ImageAlt"), System.Drawing.Image)
        Me.ButtonItem23.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem23.Name = "ButtonItem23"
        Me.ButtonItem23.SubItemsExpandWidth = 14
        Me.ButtonItem23.Text = "Pendapatan && Beban Lain"
        '
        'RibbonBar19
        '
        Me.RibbonBar19.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar19.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar19.ContainerControlProcessDialogKey = True
        Me.RibbonBar19.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar19.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem18})
        Me.RibbonBar19.Location = New System.Drawing.Point(297, 0)
        Me.RibbonBar19.Name = "RibbonBar19"
        Me.RibbonBar19.Size = New System.Drawing.Size(48, 97)
        Me.RibbonBar19.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar19.TabIndex = 7
        '
        '
        '
        Me.RibbonBar19.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar19.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem18
        '
        Me.ButtonItem18.Image = CType(resources.GetObject("ButtonItem18.Image"), System.Drawing.Image)
        Me.ButtonItem18.ImageAlt = CType(resources.GetObject("ButtonItem18.ImageAlt"), System.Drawing.Image)
        Me.ButtonItem18.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem18.Name = "ButtonItem18"
        Me.ButtonItem18.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem19, Me.ButtonItem20, Me.ButtonItem21, Me.ButtonItem22})
        Me.ButtonItem18.SubItemsExpandWidth = 14
        Me.ButtonItem18.Text = "Hutang"
        '
        'ButtonItem19
        '
        Me.ButtonItem19.Name = "ButtonItem19"
        Me.ButtonItem19.Text = "Daftar Hutang Dagang"
        '
        'ButtonItem20
        '
        Me.ButtonItem20.Name = "ButtonItem20"
        Me.ButtonItem20.Text = "Uang Muka Pelanggan"
        '
        'ButtonItem21
        '
        Me.ButtonItem21.Name = "ButtonItem21"
        Me.ButtonItem21.Text = "Jangka Panjang"
        '
        'ButtonItem22
        '
        Me.ButtonItem22.Name = "ButtonItem22"
        Me.ButtonItem22.Text = "Hutang Lain-lain"
        '
        'RibbonBar17
        '
        Me.RibbonBar17.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar17.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar17.ContainerControlProcessDialogKey = True
        Me.RibbonBar17.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar17.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem15})
        Me.RibbonBar17.Location = New System.Drawing.Point(223, 0)
        Me.RibbonBar17.Name = "RibbonBar17"
        Me.RibbonBar17.Size = New System.Drawing.Size(74, 97)
        Me.RibbonBar17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar17.TabIndex = 6
        '
        '
        '
        Me.RibbonBar17.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar17.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem15
        '
        Me.ButtonItem15.Image = CType(resources.GetObject("ButtonItem15.Image"), System.Drawing.Image)
        Me.ButtonItem15.ImageAlt = CType(resources.GetObject("ButtonItem15.ImageAlt"), System.Drawing.Image)
        Me.ButtonItem15.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem15.Name = "ButtonItem15"
        Me.ButtonItem15.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem16, Me.ButtonItem17})
        Me.ButtonItem15.SubItemsExpandWidth = 14
        Me.ButtonItem15.Text = "Realisasi Gross Profit"
        '
        'ButtonItem16
        '
        Me.ButtonItem16.Name = "ButtonItem16"
        Me.ButtonItem16.Text = "Per Cabang"
        '
        'ButtonItem17
        '
        Me.ButtonItem17.Name = "ButtonItem17"
        Me.ButtonItem17.Text = "Per Outlet"
        '
        'RibbonBar14
        '
        Me.RibbonBar14.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar14.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar14.ContainerControlProcessDialogKey = True
        Me.RibbonBar14.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar14.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem8})
        Me.RibbonBar14.Location = New System.Drawing.Point(168, 0)
        Me.RibbonBar14.Name = "RibbonBar14"
        Me.RibbonBar14.Size = New System.Drawing.Size(55, 97)
        Me.RibbonBar14.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar14.TabIndex = 5
        '
        '
        '
        Me.RibbonBar14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar14.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem8
        '
        Me.ButtonItem8.Image = CType(resources.GetObject("ButtonItem8.Image"), System.Drawing.Image)
        Me.ButtonItem8.ImageAlt = CType(resources.GetObject("ButtonItem8.ImageAlt"), System.Drawing.Image)
        Me.ButtonItem8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem8.Name = "ButtonItem8"
        Me.ButtonItem8.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem9, Me.ButtonItem10, Me.ButtonItem11, Me.ButtonItem12, Me.ButtonItem13, Me.ButtonItem14})
        Me.ButtonItem8.SubItemsExpandWidth = 14
        Me.ButtonItem8.Text = "Piutang"
        '
        'ButtonItem9
        '
        Me.ButtonItem9.Name = "ButtonItem9"
        Me.ButtonItem9.Text = "Tunai"
        '
        'ButtonItem10
        '
        Me.ButtonItem10.Name = "ButtonItem10"
        Me.ButtonItem10.Text = "Lain-lain"
        '
        'ButtonItem11
        '
        Me.ButtonItem11.Name = "ButtonItem11"
        Me.ButtonItem11.Text = "Sewa Beli Per Cabang"
        '
        'ButtonItem12
        '
        Me.ButtonItem12.Name = "ButtonItem12"
        Me.ButtonItem12.Text = "Sewa Beli Per Outlet"
        '
        'ButtonItem13
        '
        Me.ButtonItem13.Name = "ButtonItem13"
        Me.ButtonItem13.Text = "Afiliasi"
        '
        'ButtonItem14
        '
        Me.ButtonItem14.Name = "ButtonItem14"
        Me.ButtonItem14.Text = "Pinjaman Karyawan"
        '
        'RibbonBar18
        '
        Me.RibbonBar18.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar18.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar18.ContainerControlProcessDialogKey = True
        Me.RibbonBar18.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar18.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnDistToko})
        Me.RibbonBar18.Location = New System.Drawing.Point(117, 0)
        Me.RibbonBar18.Name = "RibbonBar18"
        Me.RibbonBar18.Size = New System.Drawing.Size(51, 97)
        Me.RibbonBar18.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar18.TabIndex = 4
        '
        '
        '
        Me.RibbonBar18.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar18.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnDistToko
        '
        Me.BtnDistToko.Image = CType(resources.GetObject("BtnDistToko.Image"), System.Drawing.Image)
        Me.BtnDistToko.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnDistToko.Name = "BtnDistToko"
        Me.BtnDistToko.SubItemsExpandWidth = 14
        Me.BtnDistToko.Text = "Lost Profit"
        '
        'rb1
        '
        Me.rb1.AutoOverflowEnabled = True
        '
        '
        '
        Me.rb1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.rb1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.rb1.ContainerControlProcessDialogKey = True
        Me.rb1.Dock = System.Windows.Forms.DockStyle.Left
        Me.rb1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnNeraca})
        Me.rb1.Location = New System.Drawing.Point(64, 0)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(53, 97)
        Me.rb1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.rb1.TabIndex = 1
        '
        '
        '
        Me.rb1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.rb1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnNeraca
        '
        Me.BtnNeraca.Image = CType(resources.GetObject("BtnNeraca.Image"), System.Drawing.Image)
        Me.BtnNeraca.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnNeraca.Name = "BtnNeraca"
        Me.BtnNeraca.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem3, Me.ButtonItem5, Me.ButtonItem6})
        Me.BtnNeraca.SubItemsExpandWidth = 14
        Me.BtnNeraca.Text = "Neraca"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.Text = "Neraca"
        '
        'ButtonItem5
        '
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.Text = "Neraca Lajur"
        '
        'ButtonItem6
        '
        Me.ButtonItem6.Name = "ButtonItem6"
        Me.ButtonItem6.Text = "Trend Neraca"
        '
        'RibbonBar8
        '
        Me.RibbonBar8.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar8.ContainerControlProcessDialogKey = True
        Me.RibbonBar8.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar8.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnRepBalance})
        Me.RibbonBar8.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar8.Name = "RibbonBar8"
        Me.RibbonBar8.Size = New System.Drawing.Size(61, 97)
        Me.RibbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar8.TabIndex = 0
        '
        '
        '
        Me.RibbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'BtnRepBalance
        '
        Me.BtnRepBalance.Image = CType(resources.GetObject("BtnRepBalance.Image"), System.Drawing.Image)
        Me.BtnRepBalance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnRepBalance.Name = "BtnRepBalance"
        Me.BtnRepBalance.SubItemsExpandWidth = 14
        Me.BtnRepBalance.Text = "Rekap Perkiraan"
        '
        'ApplicationButton1
        '
        Me.ApplicationButton1.AutoExpandOnClick = True
        Me.ApplicationButton1.CanCustomize = False
        Me.ApplicationButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image
        Me.ApplicationButton1.Image = CType(resources.GetObject("ApplicationButton1.Image"), System.Drawing.Image)
        Me.ApplicationButton1.ImageFixedSize = New System.Drawing.Size(16, 16)
        Me.ApplicationButton1.ImagePaddingHorizontal = 0
        Me.ApplicationButton1.ImagePaddingVertical = 0
        Me.ApplicationButton1.Name = "ApplicationButton1"
        Me.ApplicationButton1.ShowSubItems = False
        Me.ApplicationButton1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer1})
        Me.ApplicationButton1.Text = "&File"
        '
        'ItemContainer1
        '
        '
        '
        '
        Me.ItemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer"
        Me.ItemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer1.Name = "ItemContainer1"
        Me.ItemContainer1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer2, Me.ItemContainer4})
        '
        '
        '
        Me.ItemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer2
        '
        '
        '
        '
        Me.ItemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer2.ItemSpacing = 0
        Me.ItemContainer2.Name = "ItemContainer2"
        Me.ItemContainer2.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer3})
        '
        '
        '
        Me.ItemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer3
        '
        '
        '
        '
        Me.ItemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer3.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer3.Name = "ItemContainer3"
        Me.ItemContainer3.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem7})
        '
        '
        '
        Me.ItemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem7
        '
        Me.ButtonItem7.BeginGroup = True
        Me.ButtonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem7.Image = CType(resources.GetObject("ButtonItem7.Image"), System.Drawing.Image)
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.SubItemsExpandWidth = 24
        Me.ButtonItem7.Text = "&Exit"
        '
        'ItemContainer4
        '
        '
        '
        '
        Me.ItemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer4.Name = "ItemContainer4"
        Me.ItemContainer4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem2})
        '
        '
        '
        Me.ItemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem2
        '
        Me.ButtonItem2.BeginGroup = True
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.Image = CType(resources.GetObject("ButtonItem2.Image"), System.Drawing.Image)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.SubItemsExpandWidth = 24
        Me.ButtonItem2.Text = "&LogOff"
        '
        'RTMaster
        '
        Me.RTMaster.Checked = True
        Me.RTMaster.Name = "RTMaster"
        Me.RTMaster.Panel = Me.RibbonPanel1
        Me.RTMaster.Text = "Master"
        '
        'RTTransaction
        '
        Me.RTTransaction.Name = "RTTransaction"
        Me.RTTransaction.Panel = Me.RibbonPanel2
        Me.RTTransaction.Text = "Transcation"
        '
        'RibbonTabItem1
        '
        Me.RibbonTabItem1.Name = "RibbonTabItem1"
        Me.RibbonTabItem1.Panel = Me.RibbonPanel5
        Me.RibbonTabItem1.Text = "Human Resource"
        '
        'RTReport
        '
        Me.RTReport.Name = "RTReport"
        Me.RTReport.Panel = Me.RibbonPanel3
        Me.RTReport.Text = "Report"
        '
        'RTTools
        '
        Me.RTTools.Name = "RTTools"
        Me.RTTools.Panel = Me.RibbonPanel4
        Me.RTTools.Text = "Tools"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.Image = CType(resources.GetObject("ButtonItem1.Image"), System.Drawing.Image)
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BtnThemes})
        Me.ButtonItem1.Text = "ButtonItem1"
        '
        'BtnThemes
        '
        Me.BtnThemes.Image = CType(resources.GetObject("BtnThemes.Image"), System.Drawing.Image)
        Me.BtnThemes.ImageFixedSize = New System.Drawing.Size(16, 16)
        Me.BtnThemes.Name = "BtnThemes"
        Me.BtnThemes.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.Office2007Blue, Me.Office2007Black, Me.Office2007Silver, Me.Office2007VistaGlass, Me.Office2010Black, Me.Office2010Blue, Me.Office2010Silver, Me.Office2013, Me.VisualStudio2010Blue, Me.VisualStudio2012Dark, Me.VisualStudio2012Light, Me.Windows7Blue})
        Me.BtnThemes.Text = "Themes"
        '
        'Office2007Blue
        '
        Me.Office2007Blue.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2007Blue.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2007Blue.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2007Blue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2007Blue.Name = "Office2007Blue"
        Me.Office2007Blue.PaddingBottom = 1
        Me.Office2007Blue.PaddingLeft = 10
        Me.Office2007Blue.PaddingTop = 1
        Me.Office2007Blue.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2007Blue.Text = "Office 2007 Blue"
        '
        'Office2007Black
        '
        Me.Office2007Black.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2007Black.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2007Black.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2007Black.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2007Black.Name = "Office2007Black"
        Me.Office2007Black.PaddingBottom = 1
        Me.Office2007Black.PaddingLeft = 10
        Me.Office2007Black.PaddingTop = 1
        Me.Office2007Black.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2007Black.Text = "Office 2007 Black"
        '
        'Office2007Silver
        '
        Me.Office2007Silver.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2007Silver.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2007Silver.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2007Silver.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2007Silver.Name = "Office2007Silver"
        Me.Office2007Silver.PaddingBottom = 1
        Me.Office2007Silver.PaddingLeft = 10
        Me.Office2007Silver.PaddingTop = 1
        Me.Office2007Silver.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2007Silver.Text = "Office 2007 Silver"
        '
        'Office2007VistaGlass
        '
        Me.Office2007VistaGlass.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2007VistaGlass.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2007VistaGlass.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2007VistaGlass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2007VistaGlass.Name = "Office2007VistaGlass"
        Me.Office2007VistaGlass.PaddingBottom = 1
        Me.Office2007VistaGlass.PaddingLeft = 10
        Me.Office2007VistaGlass.PaddingTop = 1
        Me.Office2007VistaGlass.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2007VistaGlass.Text = "Office 2007 Vista Glass"
        '
        'Office2010Black
        '
        Me.Office2010Black.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2010Black.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2010Black.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2010Black.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2010Black.Name = "Office2010Black"
        Me.Office2010Black.PaddingBottom = 1
        Me.Office2010Black.PaddingLeft = 10
        Me.Office2010Black.PaddingTop = 1
        Me.Office2010Black.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2010Black.Text = "Office 2010 Black"
        '
        'Office2010Blue
        '
        Me.Office2010Blue.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2010Blue.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2010Blue.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2010Blue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2010Blue.Name = "Office2010Blue"
        Me.Office2010Blue.PaddingBottom = 1
        Me.Office2010Blue.PaddingLeft = 10
        Me.Office2010Blue.PaddingTop = 1
        Me.Office2010Blue.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2010Blue.Text = "Office 2010 Blue"
        '
        'Office2010Silver
        '
        Me.Office2010Silver.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2010Silver.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2010Silver.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2010Silver.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2010Silver.Name = "Office2010Silver"
        Me.Office2010Silver.PaddingBottom = 1
        Me.Office2010Silver.PaddingLeft = 10
        Me.Office2010Silver.PaddingTop = 1
        Me.Office2010Silver.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2010Silver.Text = "Office 2010 Silver"
        '
        'Office2013
        '
        Me.Office2013.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Office2013.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Office2013.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Office2013.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Office2013.Name = "Office2013"
        Me.Office2013.PaddingBottom = 1
        Me.Office2013.PaddingLeft = 10
        Me.Office2013.PaddingTop = 1
        Me.Office2013.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Office2013.Text = "Office 2013"
        '
        'VisualStudio2010Blue
        '
        Me.VisualStudio2010Blue.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.VisualStudio2010Blue.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.VisualStudio2010Blue.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.VisualStudio2010Blue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.VisualStudio2010Blue.Name = "VisualStudio2010Blue"
        Me.VisualStudio2010Blue.PaddingBottom = 1
        Me.VisualStudio2010Blue.PaddingLeft = 10
        Me.VisualStudio2010Blue.PaddingTop = 1
        Me.VisualStudio2010Blue.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.VisualStudio2010Blue.Text = "Visual Studio 2010 Blue"
        '
        'VisualStudio2012Dark
        '
        Me.VisualStudio2012Dark.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.VisualStudio2012Dark.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.VisualStudio2012Dark.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.VisualStudio2012Dark.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.VisualStudio2012Dark.Name = "VisualStudio2012Dark"
        Me.VisualStudio2012Dark.PaddingBottom = 1
        Me.VisualStudio2012Dark.PaddingLeft = 10
        Me.VisualStudio2012Dark.PaddingTop = 1
        Me.VisualStudio2012Dark.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.VisualStudio2012Dark.Text = "Visual Studio 2012 Dark"
        '
        'VisualStudio2012Light
        '
        Me.VisualStudio2012Light.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.VisualStudio2012Light.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.VisualStudio2012Light.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.VisualStudio2012Light.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.VisualStudio2012Light.Name = "VisualStudio2012Light"
        Me.VisualStudio2012Light.PaddingBottom = 1
        Me.VisualStudio2012Light.PaddingLeft = 10
        Me.VisualStudio2012Light.PaddingTop = 1
        Me.VisualStudio2012Light.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.VisualStudio2012Light.Text = "Visual Studio 2012 Light"
        '
        'Windows7Blue
        '
        Me.Windows7Blue.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Windows7Blue.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.Windows7Blue.BorderType = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.Windows7Blue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Windows7Blue.Name = "Windows7Blue"
        Me.Windows7Blue.PaddingBottom = 1
        Me.Windows7Blue.PaddingLeft = 10
        Me.Windows7Blue.PaddingTop = 1
        Me.Windows7Blue.SingleLineColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.Windows7Blue.Text = "Windows 7 Blue"
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(26, Byte), Integer)))
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(5, 155)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(959, 231)
        Me.TabControl1.TabIndex = 2
        '
        'TabPage2
        '
        Me.TabPage2.BackgroundImage = CType(resources.GetObject("TabPage2.BackgroundImage"), System.Drawing.Image)
        Me.TabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TabPage2.Controls.Add(Me.PictureBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(951, 205)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Dashboard"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(675, 277)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(316, 63)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 26
        Me.PictureBox2.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolUser, Me.ToolDept, Me.ToolBulan})
        Me.StatusStrip1.Location = New System.Drawing.Point(5, 364)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(959, 22)
        Me.StatusStrip1.TabIndex = 17
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolUser
        '
        Me.ToolUser.Name = "ToolUser"
        Me.ToolUser.Size = New System.Drawing.Size(120, 17)
        Me.ToolUser.Text = "ToolStripStatusLabel1"
        '
        'ToolDept
        '
        Me.ToolDept.Name = "ToolDept"
        Me.ToolDept.Size = New System.Drawing.Size(120, 17)
        Me.ToolDept.Text = "ToolStripStatusLabel1"
        '
        'ToolBulan
        '
        Me.ToolBulan.Name = "ToolBulan"
        Me.ToolBulan.Size = New System.Drawing.Size(120, 17)
        Me.ToolBulan.Text = "ToolStripStatusLabel1"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = CType(resources.GetObject("Panel1.BackgroundImage"), System.Drawing.Image)
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.btnLogin)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txUser)
        Me.Panel1.Controls.Add(Me.txPassword)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Location = New System.Drawing.Point(509, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(336, 354)
        Me.Panel1.TabIndex = 18
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(183, 205)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(142, 34)
        Me.Button1.TabIndex = 24
        Me.Button1.Text = "CANCEL"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnLogin
        '
        Me.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin.ForeColor = System.Drawing.Color.Black
        Me.btnLogin.Image = CType(resources.GetObject("btnLogin.Image"), System.Drawing.Image)
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogin.Location = New System.Drawing.Point(23, 205)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(140, 34)
        Me.btnLogin.TabIndex = 2
        Me.btnLogin.Text = "LOGIN"
        Me.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, CType((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label5.Location = New System.Drawing.Point(217, 329)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 18)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Change connection"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-1, 245)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(352, 10)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 22
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(19, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Username"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label4.Location = New System.Drawing.Point(73, 282)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(188, 15)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "PT. PRIORITAS GROUP @ 2019"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(19, 137)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Password"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label3.Location = New System.Drawing.Point(80, 258)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(174, 15)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Application For General Ledger"
        '
        'txUser
        '
        Me.txUser.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txUser.Location = New System.Drawing.Point(23, 102)
        Me.txUser.Name = "txUser"
        Me.txUser.Size = New System.Drawing.Size(190, 22)
        Me.txUser.TabIndex = 0
        Me.txUser.Text = "ian.cuah.stekom@gmail.com"
        '
        'txPassword
        '
        Me.txPassword.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txPassword.Location = New System.Drawing.Point(23, 159)
        Me.txPassword.Name = "txPassword"
        Me.txPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txPassword.Size = New System.Drawing.Size(190, 22)
        Me.txPassword.TabIndex = 1
        Me.txPassword.Text = "malesmaen"
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(169, 10)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(156, 171)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 19
        Me.PictureBox4.TabStop = False
        '
        'BtnRepStock
        '
        Me.BtnRepStock.Image = CType(resources.GetObject("BtnRepStock.Image"), System.Drawing.Image)
        Me.BtnRepStock.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnRepStock.Name = "BtnRepStock"
        Me.BtnRepStock.SubItemsExpandWidth = 14
        Me.BtnRepStock.Text = "Neraca"
        '
        'ItemContainer5
        '
        '
        '
        '
        Me.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer5.Name = "ItemContainer5"
        '
        '
        '
        Me.ItemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(969, 388)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Name = "frmMain"
        Me.Text = "General Ledger Ver 1.1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.RibbonControl1.ResumeLayout(False)
        Me.RibbonControl1.PerformLayout()
        Me.RibbonPanel4.ResumeLayout(False)
        Me.RibbonPanel2.ResumeLayout(False)
        Me.RibbonPanel1.ResumeLayout(False)
        Me.RibbonPanel5.ResumeLayout(False)
        Me.RibbonPanel3.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RibbonControl1 As DevComponents.DotNetBar.RibbonControl
    Friend WithEvents RibbonPanel1 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonBar2 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnCoa As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnCoaGroup As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel3 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonBar18 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnDistToko As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar8 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnRepBalance As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel2 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonBar21 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnJurnalUmum As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar16 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnJurnalSesuai As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar6 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents RibbonBar5 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnKasIn As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel4 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonBar9 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnPassword As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RTMaster As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents RTTransaction As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents RTReport As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents RTTools As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents ApplicationButton1 As DevComponents.DotNetBar.ApplicationButton
    Friend WithEvents ItemContainer1 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer2 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer3 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemContainer4 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents BtnThemes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Office2007Blue As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2007Black As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2007Silver As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2007VistaGlass As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2010Black As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2010Blue As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2010Silver As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Office2013 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents VisualStudio2010Blue As DevComponents.DotNetBar.LabelItem
    Friend WithEvents VisualStudio2012Dark As DevComponents.DotNetBar.LabelItem
    Friend WithEvents VisualStudio2012Light As DevComponents.DotNetBar.LabelItem
    Friend WithEvents Windows7Blue As DevComponents.DotNetBar.LabelItem
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolDept As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txUser As System.Windows.Forms.TextBox
    Friend WithEvents txPassword As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents RibbonBar7 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents RibbonBar10 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnSettingPerAcct As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar11 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnSaldoAwal As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents BtnBankOut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents rb1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnNeraca As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents BtnRepStock As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemContainer5 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ToolBulan As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents RibbonBar13 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents RibbonBar12 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents BtnUnclosing As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents BtnClosing As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem6 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar14 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem8 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem9 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem10 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem11 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem12 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem13 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem14 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar17 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem15 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem16 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem17 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar19 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem18 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem19 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem20 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem21 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem22 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar20 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem23 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar22 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem24 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem25 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem26 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem27 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem28 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem29 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem30 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem31 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem32 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar23 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem33 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem34 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem35 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem36 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem37 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel5 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonBar29 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem43 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar28 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem42 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar27 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem41 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar26 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem40 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar25 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem39 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar24 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem38 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonTabItem1 As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents ButtonItem44 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem45 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem46 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem47 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem48 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem50 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem51 As DevComponents.DotNetBar.ButtonItem
End Class
