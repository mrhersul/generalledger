﻿Imports System.IO

Public Class frmSettingCoaBrowse
    Dim FlNm As String

    Sub frmClose()
        fscb.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Sub ShowDataGrid()

        SQL = ""
        SQL = " select a.nama_tabel,a.nama_field,a.kd_akun_perkiraan,b.nama,a.type_perkiraan,a.acc_setting,a.tanggal_buat,a.id_acc_perkiraan_setting "
        SQL = SQL & " from acc_perkiraan_setting2 a left join acc_perkiraan_nama b on a.id_perkiraan_nama=b.id where 0=0"


        If txtSearch.Text <> "" Then

            SQL = SQL & " and (a.kd_akun_perkiraan like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or b.nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or a.acc_setting like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If

        SQL = SQL & " order by 1"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 11)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 11)
        DGView.Columns(0).Width = 250
        DGView.Columns(1).Width = 200
        DGView.Columns(2).Width = 150
        DGView.Columns(3).Width = 250
        DGView.Columns(4).Width = 80
        DGView.Columns(5).Width = 200
        DGView.Columns(6).Width = 150
        DGView.Columns(7).Width = 100

        DGView.Columns(0).HeaderText = "NAMA TABEL"
        DGView.Columns(1).HeaderText = "NAMA FIELD"
        DGView.Columns(2).HeaderText = "KODE AKUN"
        DGView.Columns(3).HeaderText = "NAMA AKUN"
        DGView.Columns(4).HeaderText = "TIPE"
        DGView.Columns(5).HeaderText = "ACC SETTING"
        DGView.Columns(6).HeaderText = "TANGGAL BUAT"
        DGView.Columns(7).HeaderText = "ID"

        DGView.Columns(7).Visible = False

        ColorChange()

    End Sub

    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next

    End Sub

    Private Sub frmSettingCoaBrowse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ShowDataGrid()
    End Sub

    Private Sub BtnShow_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnShow.Click
        ShowDataGrid()
        txtSearch.Text = ""
        txtSearch.Focus()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Private Sub frmSettingCoaBrowse_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 680
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnAdd.Click
        blnAdd = True
        blnEdit = False

        frmSettingCoaDetail.ShowDialog()
        ShowDataGrid()
    End Sub

    Private Sub ExportToExcel(ByVal DGV As DataGridView)
        Dim fs As New StreamWriter(FlNm, False)
        With fs
            .WriteLine("<?xml version=""1.0""?>")
            .WriteLine("<?mso-application progid=""Excel.Sheet""?>")
            .WriteLine("<Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet"">")
            .WriteLine("    <Styles>")
            .WriteLine("        <Style ss:ID=""hdr"">")
            .WriteLine("            <Alignment ss:Horizontal=""Center""/>")
            .WriteLine("            <Borders>")
            .WriteLine("                <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("                <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("                <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("            </Borders>")
            .WriteLine("            <Font ss:FontName=""Calibri"" ss:Size=""11"" ss:Bold=""1""/>") 'SET FONT
            .WriteLine("        </Style>")
            .WriteLine("        <Style ss:ID=""ksg"">")
            .WriteLine("            <Alignment ss:Vertical=""Bottom""/>")
            .WriteLine("            <Borders/>")
            .WriteLine("            <Font ss:FontName=""Calibri""/>") 'SET FONT
            .WriteLine("        </Style>")
            .WriteLine("        <Style ss:ID=""isi"">")
            .WriteLine("            <Borders>")
            .WriteLine("                <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("                <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("                <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("                <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>")
            .WriteLine("            </Borders>")
            .WriteLine("            <Font ss:FontName=""Calibri"" ss:Size=""10""/>") 'SET FONT
            .WriteLine("        </Style>")
            .WriteLine("    </Styles>")
            If DGV.Name = "Student" Then
                .WriteLine("    <Worksheet ss:Name=""NamaPerkiraan"">") 'SET NAMA SHEET
                .WriteLine("        <Table>")
                .WriteLine("            <Column ss:Width=""27.75""/>") 'No
                .WriteLine("            <Column ss:Width=""200""/>") 'Tabel
                .WriteLine("            <Column ss:Width=""200""/>") 'Field
                .WriteLine("            <Column ss:Width=""150""/>") 'Kode Group
                .WriteLine("            <Column ss:Width=""250""/>") 'Nama Group
                .WriteLine("            <Column ss:Width=""150""/>") 'Tipe
                .WriteLine("            <Column ss:Width=""150""/>") 'Tipe
                .WriteLine("            <Column ss:Width=""150""/>") 'Tanggal Buat
            End If


            'AUTO SET HEADER
            .WriteLine("            <Row ss:StyleID=""ksg"">")
            For i As Integer = 0 To DGV.Columns.Count - 1 'SET HEADER
                Application.DoEvents()
                .WriteLine("            <Cell ss:StyleID=""hdr"">")
                .WriteLine("                <Data ss:Type=""String"">{0}</Data>", DGV.Columns.Item(i).HeaderText)
                .WriteLine("            </Cell>")
            Next
            .WriteLine("            </Row>")
            For intRow As Integer = 0 To DGV.RowCount - 1
                Application.DoEvents()
                .WriteLine("        <Row ss:StyleID=""ksg"" ss:utoFitHeight =""0"">")
                For intCol As Integer = 0 To DGV.Columns.Count - 1
                    Application.DoEvents()
                    .WriteLine("        <Cell ss:StyleID=""isi"">")
                    .WriteLine("            <Data ss:Type=""String"">{0}</Data>", DGV.Item(intCol, intRow).Value.ToString)
                    .WriteLine("        </Cell>")
                Next
                .WriteLine("        </Row>")
            Next
            .WriteLine("        </Table>")
            .WriteLine("    </Worksheet>")
            .WriteLine("</Workbook>")
            .Close()
        End With
    End Sub


    Private Sub BtnExcel_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnExcel.Click

        If DGView.RowCount = 0 Then Return

        Application.DoEvents()

        Dim DGV As New DataGridView

        With DGV
            .AllowUserToAddRows = False
            .Name = "Student"
            .Visible = False
            .Columns.Clear()
            .Columns.Add("No", "No")
            .Columns.Add("NIK", "Nama Tabel")
            .Columns.Add("No", "Nama Field")
            .Columns.Add("NIK", "Kode AKun")
            .Columns.Add("Nama", "Nama Akun")
            .Columns.Add("Nama", "Tipe Akun")
            .Columns.Add("Nama", "Acc Setting")
            .Columns.Add("tanggal_buat", "Tanggal Buat")
        End With
        With DGView
            If .Rows.Count > 0 Then
                For i As Integer = 0 To .Rows.Count - 1
                    Application.DoEvents()
                    DGV.Rows.Add(IIf(i = 0, 1, i + 1), .Rows(i).Cells("nama_tabel").Value, .Rows(i).Cells("nama_field").Value, .Rows(i).Cells("kd_akun_perkiraan").Value, _
                                 .Rows(i).Cells("nama").Value, .Rows(i).Cells("type_perkiraan").Value, .Rows(i).Cells("acc_setting").Value, _
                                 .Rows(i).Cells("tanggal_buat").Value)
                Next
            End If
        End With


        FlNm = "D:\SettingCoa " & Now.Day & "-" & Now.Month & "-" & Now.Year & ".xls"
        'FlNm = Application.StartupPath & "\Student " _
        '        & Now.Day & "-" & Now.Month & "-" & Now.Year & ".xls"
        If File.Exists(FlNm) Then File.Delete(FlNm)
        ExportToExcel(DGV)

        DGV.Dispose()
        DGV = Nothing

        Process.Start("D:\SettingCoa " & Now.Day & "-" & Now.Month & "-" & Now.Year & ".xls")

    End Sub

    Private Sub BtnEdit_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnEdit.Click
        blnAdd = False
        blnEdit = True

        TakeDGView()

        frmSettingCoaDetail.ShowDialog()
        ShowDataGrid()
    End Sub

    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                xyzID = IIf(.Item("id_acc_perkiraan_setting", i).Value.Equals(DBNull.Value), "", .Item("id_acc_perkiraan_setting", i).Value)
            End With
        End If

    End Sub

    Private Sub DGView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.DoubleClick
        blnAdd = False
        blnEdit = False

        TakeDGView()
        frmSettingCoaDetail.ShowDialog()
    End Sub
End Class

