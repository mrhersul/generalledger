﻿Imports System.IO

Public Class frmCoaBrowse
    Dim FlNm As String
    Dim Row1 As Janus.Windows.GridEX.GridEXRow

    Sub frmClose()
        fcoab.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub BtnShow_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnShow.Click
        ShowDataAKun()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Private Sub frmCoaBrowse_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmCoaBrowse_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowDataAKun()

    End Sub

    Private Sub frmCoaBrowse_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
    End Sub

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnAdd.Click
        blnAdd = True
        blnEdit = False

        frmCoaDetail.ShowDialog()
        ShowDataAKun()
    End Sub


    Private Sub BtnEdit_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnEdit.Click
        blnAdd = False
        blnEdit = True


        On Error Resume Next

        Select TabControlAkun.SelectedTabIndex
            Case 0
                Row1 = GridEX1.CurrentRow
            Case 1
                Row1 = GridEX2.CurrentRow
            Case 2
                Row1 = GridEX3.CurrentRow
            Case 3
                Row1 = GridEX4.CurrentRow
            Case 4
                Row1 = GridEX5.CurrentRow
            Case 5
                Row1 = GridEX6.CurrentRow
            Case 6
                Row1 = GridEX7.CurrentRow
            Case 7
                Row1 = GridEX8.CurrentRow
            Case 8
                Row1 = GridEX9.CurrentRow

        End Select

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()

    End Sub

    Private Sub DGView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        blnAdd = False
        blnEdit = False

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub TabControlAkun_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles TabControlAkun.SelectedTabChanged
        ShowDataAKun()
    End Sub

    Sub ShowDataAKun()
        On Error Resume Next

        Select Case TabControlAkun.SelectedTabIndex
            'Case 0

            '' --- Group ---
            'SQL = ""
            'SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=1"


            'Cmd = New SqlClient.SqlCommand(SQL, Cn)
            'dr = Cmd.ExecuteReader()

            'Dim dt = New DataTable
            'dt.Load(dr)

            'dscoa.Tables("group_akun").Clear()
            'For Each dr As DataRow In dt.Rows
            '    dscoa.Tables("group_akun").ImportRow(dr)
            'Next

            '' --- Sub Group ---
            'SQL = ""
            'SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=1"


            'Cmd = New SqlClient.SqlCommand(SQL, Cn)
            'dr2 = Cmd.ExecuteReader()

            'Dim dt2 = New DataTable
            'dt2.Load(dr2)

            'dscoa.Tables("subgroup_akun").Clear()
            'For Each dr2 As DataRow In dt2.Rows
            '    dscoa.Tables("subgroup_akun").ImportRow(dr2)
            'Next


            '' --- Coa ---
            'SQL = ""
            'SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=1"


            'Cmd = New SqlClient.SqlCommand(SQL, Cn)
            'dr3 = Cmd.ExecuteReader()

            'Dim dt3 = New DataTable
            'dt3.Load(dr3)

            'dscoa.Tables("nama_akun").Clear()
            'For Each dr3 As DataRow In dt3.Rows
            '    dscoa.Tables("nama_akun").ImportRow(dr3)
            'Next

            'GridEX1.DataSource = dscoa
            'GridEX1.DataMember = "group_akun"
            'GridEX1.RootTable.DataMember = "subgroup_akun"
            'GridEX1.RootTable.Key = "group_akun_subgroup_akun"

            Case 1 'HUTANG

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=2"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=2"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=2"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX2.DataSource = dscoa
                GridEX2.DataMember = "group_akun"
                GridEX2.RootTable.DataMember = "subgroup_akun"
                GridEX2.RootTable.Key = "group_akun_subgroup_akun"

            Case 2 'MODAL


                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=3"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=3"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=3"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX3.DataSource = dscoa
                GridEX3.DataMember = "group_akun"
                GridEX3.RootTable.DataMember = "subgroup_akun"
                GridEX3.RootTable.Key = "group_akun_subgroup_akun"


            Case 3 'PENGHASILAN

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=4"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=4"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=4"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX4.DataSource = dscoa
                GridEX4.DataMember = "group_akun"
                GridEX4.RootTable.DataMember = "subgroup_akun"
                GridEX4.RootTable.Key = "group_akun_subgroup_akun"


            Case 4 'HARGA POKOK


                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=5"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=5"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=5"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX5.DataSource = dscoa
                GridEX5.DataMember = "group_akun"
                GridEX5.RootTable.DataMember = "subgroup_akun"
                GridEX5.RootTable.Key = "group_akun_subgroup_akun"



            Case 5 'BIAYA

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=6"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=6"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=6"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX6.DataSource = dscoa
                GridEX6.DataMember = "group_akun"
                GridEX6.RootTable.DataMember = "subgroup_akun"
                GridEX6.RootTable.Key = "group_akun_subgroup_akun"

              


            Case 6 'PENDAPATAN LAIN

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=7"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=7"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=7"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX7.DataSource = dscoa
                GridEX7.DataMember = "group_akun"
                GridEX7.RootTable.DataMember = "subgroup_akun"
                GridEX7.RootTable.Key = "group_akun_subgroup_akun"


            Case 7 'BEBAN LAIN

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=8"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=8"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=8"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX8.DataSource = dscoa
                GridEX8.DataMember = "group_akun"
                GridEX8.RootTable.DataMember = "subgroup_akun"
                GridEX8.RootTable.Key = "group_akun_subgroup_akun"


            Case 8 'PAJAK

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=9"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id<>0 and type_perkiraan='H' and acc_perkiraan_group_id=9"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa.Tables("subgroup_akun").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa.Tables("subgroup_akun").ImportRow(dr2)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=9"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa.Tables("nama_akun").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa.Tables("nama_akun").ImportRow(dr3)
                Next

                GridEX9.DataSource = dscoa
                GridEX9.DataMember = "group_akun"
                GridEX9.RootTable.DataMember = "subgroup_akun"
                GridEX9.RootTable.Key = "group_akun_subgroup_akun"

            Case 0

                ' --- Group ---
                SQL = ""
                SQL = " select id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=0 and type_perkiraan='H' and acc_perkiraan_group_id=1"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr = Cmd.ExecuteReader()

                Dim dt = New DataTable
                dt.Load(dr)

                dscoa_aktiva.Tables("group_akun").Clear()
                For Each dr As DataRow In dt.Rows
                    dscoa_aktiva.Tables("group_akun").ImportRow(dr)
                Next

                ' --- Sub Group 1 ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id=1 and type_perkiraan='H' and acc_perkiraan_group_id=1"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr2 = Cmd.ExecuteReader()

                Dim dt2 = New DataTable
                dt2.Load(dr2)

                dscoa_aktiva.Tables("subgroup_akun1").Clear()
                For Each dr2 As DataRow In dt2.Rows
                    dscoa_aktiva.Tables("subgroup_akun1").ImportRow(dr2)
                Next


                ' --- Sub Group 2 ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where parent_id not in (0,1) and type_perkiraan='H' and acc_perkiraan_group_id=1"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr3 = Cmd.ExecuteReader()

                Dim dt3 = New DataTable
                dt3.Load(dr3)

                dscoa_aktiva.Tables("subgroup_akun2").Clear()
                For Each dr3 As DataRow In dt3.Rows
                    dscoa_aktiva.Tables("subgroup_akun2").ImportRow(dr3)
                Next


                ' --- Coa ---
                SQL = ""
                SQL = " select id, parent_id, kode,nama,type_perkiraan from acc_perkiraan_nama where type_perkiraan='D' and acc_perkiraan_group_id=1"


                Cmd = New SqlClient.SqlCommand(SQL, Cn)
                dr4 = Cmd.ExecuteReader()

                Dim dt4 = New DataTable
                dt4.Load(dr4)

                dscoa_aktiva.Tables("nama_akun").Clear()
                For Each dr4 As DataRow In dt4.Rows
                    dscoa_aktiva.Tables("nama_akun").ImportRow(dr4)
                Next

                GridEX1.DataSource = dscoa_aktiva
                GridEX1.DataMember = "group_akun"
                GridEX1.RootTable.DataMember = "group_akun_subgroup_akun1"
                GridEX1.RootTable.Key = "subgroup_akun2_nama_akun"


        End Select

    End Sub

    Private Sub GridEX1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX1.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()

    End Sub

    Private Sub GridEX2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX2.DoubleClick
        Row1 = GridEX2.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX3_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX3.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX4_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX4.DoubleClick
        Row1 = GridEX4.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX5_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX5.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX6_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX6.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX7_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX7.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX8_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX8.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub

    Private Sub GridEX9_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Row1 = GridEX9.CurrentRow

        xyzCoa = Row1.Cells("kode").Value.ToString

        frmCoaDetail.ShowDialog()
    End Sub
End Class

