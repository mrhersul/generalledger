﻿Public Class frmConnection
    Dim encServerName As String
    Dim encLogin As String
    Dim encPass As String
    Dim encData As String
    Dim encPort As String

    Dim decServerName As String
    Dim decLogin As String
    Dim decPass As String
    Dim decData As String
    Dim decPort As String

    Private Sub FrmKoneksi_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim FilePath As String = Application.StartupPath & "\setting.ini"
        Dim FileName As String = System.IO.Path.GetFileName(FilePath)
        If System.IO.File.Exists(FileName) Then
            decServerName = Konek.readini(FilePath, "Koneksi Local", "ServerName", "")
            decLogin = Konek.readini(FilePath, "Koneksi Local", "Login", "")
            decPass = Konek.readini(FilePath, "Koneksi Local", "Password", "")
            decData = Konek.readini(FilePath, "Koneksi Local", "Database", "")
            decPort = Konek.readini(FilePath, "Koneksi Local", "Port", "")

            txtServerName.Text = Konek.DecryptText(decServerName)
            txtLogin.Text = Konek.DecryptText(decLogin)
            txtPassword.Text = Konek.DecryptText(decPass)
            txtDatabase.Text = Konek.DecryptText(decData)
            txtPort.Text = Konek.DecryptText(decPort)

        Else

            txtServerName.Text = ""
            txtLogin.Text = ""
            txtPassword.Text = ""
            txtDatabase.Text = ""
            txtPort.Text = ""

        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        encServerName = Konek.EncryptText(txtServerName.Text)
        encLogin = Konek.EncryptText(txtLogin.Text)
        encPass = Konek.EncryptText(txtPassword.Text)
        encData = Konek.EncryptText(txtDatabase.Text)
        encPort = Konek.EncryptText(txtPort.Text)


        Dim FilePath As String = Application.StartupPath & "\setting.ini"

        Konek.writeini(FilePath, "Koneksi Local", "ServerName", encServerName)
        Konek.writeini(FilePath, "Koneksi Local", "Login", encLogin)
        Konek.writeini(FilePath, "Koneksi Local", "Password", encPass)
        Konek.writeini(FilePath, "Koneksi Local", "Database", encData)
        Konek.writeini(FilePath, "Koneksi Local", "Port", encPort)

        MsgBox("Konfigurasi berhasil di simpan.", MsgBoxStyle.Information, "Informasi")
        Me.Close()
    End Sub
End Class