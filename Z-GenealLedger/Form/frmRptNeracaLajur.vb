﻿Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmRptNeracaLajur
    Dim xythn
    Dim xythnBef
    Dim xybln
    Dim bulan

    Private Sub frmRptNeracaLajur_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        txtDate.Value = Now
    End Sub

    Private Sub frmRptNeracaLajur_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim objRpt As New CR_NeracaLajur

        bulan = Month(txtDate.Value)
        xythn = Format(txtDate.Value, "yyyy-MM-dd")
        xythnBef = Format(DateAdd(DateInterval.Year, -1, txtDate.Value), "yyyy-MM-dd")
        xybln = Format(txtDate.Value, "MMMM-yyyy")


        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE T1.parent_id IS NOT NULL )"
        SQL = SQL & " select xyz.unitbisnis_id,xyz.bln,xyz.namaunitbisnis,xyz.nama_perkiraan_group,"
        SQL = SQL & " xyz.subnama,xyz.kode,xyz.nama,sum(xyz.debet1) as debet1,sum(xyz.kredit1) as kredit1,"
        SQL = SQL & " sum(xyz.debet2) as debet2,sum(xyz.kredit2) as kredit2,sum(xyz.debet3) as debet3,sum(xyz.kredit3) as kredit3,"
        SQL = SQL & " case when sum(xyz.debet1 - xyz.kredit1) + sum(xyz.debet2 - xyz.kredit2)>0 then sum(xyz.debet1 - xyz.kredit1) + sum(xyz.debet2 - xyz.kredit2) else 0 end as debetfinal,"
        SQL = SQL & " case when sum(xyz.debet1 - xyz.kredit1) + sum(xyz.debet2 - xyz.kredit2)<0 then abs(sum(xyz.debet1 - xyz.kredit1) + sum(xyz.debet2 - xyz.kredit2)) else 0 end as kreditfinal"
        SQL = SQL & " from"
        SQL = SQL & " (select x.unitbisnis_id,'" & xybln & "' as bln,'" & txtNamaUnit.Text & "' as namaunitbisnis,x.nama_perkiraan_group,"
        SQL = SQL & " z.nama as subnama,x.kode,x.nama,x.debet1,x.kredit1,0 as debet2, 0 as kredit2,0 as debet3,0 as kredit3"
        SQL = SQL & " from"
        SQL = SQL & " (select a.unitbisnis_id,c.nama_perkiraan_group,b.kode,b.nama,sum(a.nilai_debet) as debet1,sum(nilai_kredit) as kredit1,b.parent_id from acc_jurnal a"
        SQL = SQL & " left join acc_perkiraan_nama b on a.acc_coa_id=b.id"
        SQL = SQL & " left join acc_perkiraan_group c on b.acc_perkiraan_group_id=c.id"
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.acc_coa_id=d.id"
        SQL = SQL & " where datediff(month,a.tanggal,'" & xythn & "')=0 and d.neraca=1 and left(no_transaksi_kas,3)<>'JUP'"
        SQL = SQL & " group by a.unitbisnis_id,b.kode,b.nama,c.nama_perkiraan_group,b.parent_id) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('AKTIVA', 'HUTANG')"
        SQL = SQL & " union all"
        SQL = SQL & " select x.unitbisnis_id,'" & xybln & "' as bln,'" & txtNamaUnit.Text & "' as namaunitbisnis,x.nama_perkiraan_group,"
        SQL = SQL & " z.nama as subnama,x.kode,x.nama,0 as debet1,0 as kredit1,x.debet2, x.kredit2,0 as debet3,0 as kredit3"
        SQL = SQL & " from"
        SQL = SQL & " (select a.unitbisnis_id,c.nama_perkiraan_group,b.kode,b.nama,sum(a.nilai_debet) as debet2,sum(a.nilai_kredit) as kredit2,b.parent_id from acc_jurnal a"
        SQL = SQL & " left join acc_perkiraan_nama b on a.acc_coa_id=b.id"
        SQL = SQL & " left join acc_perkiraan_group c on b.acc_perkiraan_group_id=c.id"
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.acc_coa_id=d.id"
        SQL = SQL & " where datediff(month,a.tanggal,'" & xythn & "')=0 and d.neraca=1 and left(no_transaksi_kas,3)='JUP'"
        SQL = SQL & " group by a.unitbisnis_id,b.kode,b.nama,c.nama_perkiraan_group,b.parent_id) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('AKTIVA', 'HUTANG')"
        SQL = SQL & " union all"
        SQL = SQL & " select w.unitbisnis_id,'" & xybln & "' as bln,'" & txtNamaUnit.Text & "' as namaunitbisnis,w.nama_perkiraan_group,"
        SQL = SQL & " w.subnama,w.kode,w.nama,0 as debet1,0 as kredit1,0 as debet2, 0 as kredit2,w.debet3,w.kredit3 from"
        SQL = SQL & " (select x.unitbisnis_id,x.nama_perkiraan_group,z.nama as subnama,x.kode,x.nama,x.debet3,x.kredit3"
        SQL = SQL & " from"
        SQL = SQL & " (select b.unitbisnis_id,c.nama_perkiraan_group,a.kode,a.nama,"
        SQL = SQL & " case when DATEPART(month, tanggal) = " & bulan & "  then sum(b.nilai_debet) else 0 end as debet3,"
        SQL = SQL & " case when DATEPART(month, tanggal) = " & bulan & "  then sum(b.nilai_kredit) else 0 end as kredit3,a.parent_id "
        SQL = SQL & " from acc_perkiraan_nama a "
        SQL = SQL & " left join acc_jurnal b on a.id=b.acc_coa_id"
        SQL = SQL & " left join acc_perkiraan_group c on a.acc_perkiraan_group_id=c.id"
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.id=d.id"
        SQL = SQL & " where a.type_perkiraan='D' and d.profit=1"
        SQL = SQL & " group by b.unitbisnis_id,a.parent_id,a.kode,a.nama,c.nama_perkiraan_group,DATEPART(month, b.tanggal)) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('PENGHASILAN','HARGA POKOK','MODAL')) as w) as xyz"
        SQL = SQL & " where xyz.unitbisnis_id in (SELECT id FROM ParentChildCTE)"
        SQL = SQL & " group by  xyz.unitbisnis_id,xyz.bln,xyz.namaunitbisnis,xyz.nama_perkiraan_group,xyz.subnama,xyz.kode,xyz.nama"
        SQL = SQL & " order by xyz.nama_perkiraan_group,xyz.subnama,xyz.kode"


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_neracalajur").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_neracalajur").ImportRow(dr)
        Next

        objRpt.SetDataSource(ds1.Tables("ds_neracalajur"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt

        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default

    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitRptNeraca2 = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub
End Class