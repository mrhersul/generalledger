﻿Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmRptLostProfit
    Dim xythn
    Dim xythnBef
    Dim xybln

    Private Sub frmRptLostProfit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        txtDate.Value = Now
    End Sub

    Private Sub frmRptLostProfit_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim objRpt As New CR_LostProfit
        Dim xyzbln As Integer


        xythn = Format(txtDate.Value, "yyyy-MM-dd")
        xythnBef = Format(DateAdd(DateInterval.Year, -1, txtDate.Value), "yyyy-MM-dd")
        xybln = Format(txtDate.Value, "MMMM-yyyy")
        xyzbln = Month(txtDate.Value)

        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & txtIdUnit.Text & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
        SQL = SQL & " T1.parent_id IS NOT NULL )"
        SQL = SQL & " select '" & xybln & "' as bln,'" & txtNamaUnit.Text & "' as namaunitbisnis,w.nama_perkiraan_group,w.subnama,w.kode,w.nama,w.nilai from"
        SQL = SQL & " (select x.unitbisnis_id,x.nama_perkiraan_group,z.nama as subnama,x.kode,x.nama,x.nilai"
        SQL = SQL & " from"
        SQL = SQL & " (select b.unitbisnis_id,c.nama_perkiraan_group,a.kode,a.nama,"
        SQL = SQL & " case when DATEPART(month, b.tanggal) =" & xyzbln & " then sum(b.nilai_debet + b.nilai_kredit) else 0 end as nilai,a.parent_id "
        SQL = SQL & " from acc_perkiraan_nama a "
        SQL = SQL & " left join acc_jurnal b on a.id=b.acc_coa_id"
        SQL = SQL & " left join acc_perkiraan_group c on a.acc_perkiraan_group_id=c.id"
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.id=d.id"
        SQL = SQL & " where a.type_perkiraan='D' and d.profit=1"
        SQL = SQL & " group by b.unitbisnis_id,a.parent_id,a.kode,a.nama,c.nama_perkiraan_group,DATEPART(month, b.tanggal)) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('PENGHASILAN','HARGA POKOK','PENDAPATAN LAIN','BEBAN LAIN')"
        SQL = SQL & " union all"
        SQL = SQL & " select x.unitbisnis_id,x.nama_perkiraan_group,z.nama as subnama,x.kode,x.nama,x.nilai"
        SQL = SQL & " from"
        SQL = SQL & " (select b.unitbisnis_id,c.nama_perkiraan_group,a.kode,a.nama,"
        SQL = SQL & " case when DATEPART(month, b.tanggal) =" & xyzbln & " then sum(b.nilai_debet + b.nilai_kredit) else 0 end as nilai,a.parent_id "
        SQL = SQL & " from acc_perkiraan_nama a "
        SQL = SQL & " left join acc_jurnal b on a.id=b.acc_coa_id"
        SQL = SQL & " left join acc_perkiraan_group c on a.acc_perkiraan_group_id=c.id"
        SQL = SQL & " left join acc_perkiraan_setting_print d on a.id=d.id"
        SQL = SQL & " where a.type_perkiraan='D' and d.profit=1"
        SQL = SQL & " group by b.unitbisnis_id,a.parent_id,a.kode,a.nama,c.nama_perkiraan_group,DATEPART(month, b.tanggal)) as x"
        SQL = SQL & " left join (select * from acc_perkiraan_nama where type_perkiraan='H') as z on z.id=x.parent_id"
        SQL = SQL & " where x.nama_perkiraan_group in ('MODAL')) as w"
        SQL = SQL & " where w.unitbisnis_id in (SELECT id FROM ParentChildCTE)"


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_lostprofit").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_lostprofit").ImportRow(dr)
        Next

        objRpt.SetDataSource(ds1.Tables("ds_lostprofit"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt

        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default

    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitRptRL = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub
End Class