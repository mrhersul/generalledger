﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmPosting
    Dim strNamaTabel As String
    Dim strIDPerkiraan As Long
    Dim xyIDHd As Long
    Dim tglbeli
    Dim xyTotNilai As Double
    Dim xyKet As String
    Dim xyNoTrans As String
    Dim R As Integer
    Dim Rmax As Integer
    Dim brs As Long

    Private Sub frmPosting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDate1.Value = "01/" & Format(Now, "MM/yyyy")
        txtDate2.Value = Format(Now, "dd/MM/yyyy")

        ProgressBar1.Visible = False
        ProgressBar1.Value = 0
    End Sub

    Sub CallDataAwal()

        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & myIDCabang & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE T1.parent_id IS NOT NULL )"
        SQL = SQL & " select x.transaksi_id,x.jenis,x.tanggal,x.unitbisnis_id,x.kd_unitbisnis,x.nama_unitbisnis,x.posting from"
        SQL = SQL & " (select a.transaksi_id,a.jenis,a.tanggal,a.unitbisnis_id,b.kd_unitbisnis,b.nama_unitbisnis,a.posting from acc_jurnal a"
        SQL = SQL & " left join unitbisnis_nama b on a.unitbisnis_id=b.id where a.posting=0 and a.tanggal between '" & Format(txtDate1.Value, "yyyy-MM-dd") & "' and '" & Format(txtDate2.Value, "yyyy-MM-dd") & "'"
        SQL = SQL & " group by a.transaksi_id,a.jenis,a.tanggal,a.unitbisnis_id,b.kd_unitbisnis,b.nama_unitbisnis,a.posting) as x"
        SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        DGView.Rows.Clear()
        If dr.HasRows Then
            While (dr.Read)

                DGView.Rows.Add(dr.Item("transaksi_id").ToString, dr.Item("jenis").ToString, Format(dr.Item("tanggal"), "dd/MM/yyyy"), dr.Item("kd_unitbisnis").ToString, dr.Item("nama_unitbisnis").ToString, dr.Item("posting").ToString)

            End While
        Else
            MsgBox("Tidak Ada Data Yang Belum Di Posting Di Periode tsb", vbInformation)
            Exit Sub
        End If
        dr.Close()

    End Sub

    Private Sub BtnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnShow.Click
        CallDataAwal()
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        ProgressBar1.Visible = True
        ProgressBar1.Maximum = DGView.Rows.Count
        brs = 1
        For x As Integer = 0 To DGView.Rows.Count - 1
            If (DGView.Rows(x).Cells(0).Value) <> "" Then

                ProgressBar1.Value = brs

                strNamaTabel = DGView.Rows(x).Cells(0).Value

                SQL = "update acc_jurnal set posting=1 where transaksi_id='" & strNamaTabel & "'"
                Konek.IUDQuery(SQL)

                SQL = "update acc_jurnal_hd set posting=1 where no_transaksi='" & strNamaTabel & "'"
                Konek.IUDQuery(SQL)

                SQL = "update acc_kas_hd set posting=1 where no_transaksi='" & strNamaTabel & "'"
                Konek.IUDQuery(SQL)

                brs = brs + 1
            End If
            dr1.Close()
        Next
        MsgBox("Posting Sukses", vbInformation)
        ProgressBar1.Visible = False

    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub
End Class
