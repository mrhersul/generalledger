﻿Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmRptpendapatan
    Dim xytgl1
    Dim xytglsaldo
    Dim strcode
    Dim xystrcode As String

    Private Sub frmRptpendapatan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        txtDate1.Value = Now
    End Sub

    Private Sub frmRptpendapatan_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim objRpt As New CR_Pendapatan

        xytgl1 = Format(txtDate1.Value, "yyyy-MM-dd")
        xytglsaldo = Format(txtDate1.Value, "yyyy-MM") & "-01"
        strcode = cboCoa.SelectedValue
        xystrcode = cboCoa.SelectedValue & " - " & cboCoa.Text

        SQL = ""
        SQL = " WITH ParentChildCTE AS (SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id =" & txtIdUnit.Text & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
        SQL = SQL & " T1.parent_id IS NOT NULL )"
        SQL = SQL & " select '" & Format(txtDate1.Value, "MMMM-yyyy") & "' as bln,'" & txtNamaUnit.Text & "' as namaunitbisnis,'" & xystrcode & "' as coa_group, m.tanggal,m.no_transaksi_kas,m.nama,sum(m.debet) as debet,sum(m.kredit) as kredit, sum(m.nominal) as nominal from"
        SQL = SQL & " (select a.unitbisnis_id,a.tanggal,a.no_transaksi_kas,b.nama,sum(a.nilai_debet) as debet,sum(a.nilai_kredit) as kredit, 0 as nominal"
        SQL = SQL & " from acc_jurnal a left join acc_perkiraan_nama b on a.acc_coa_id=b.id"
        SQL = SQL & " where b.acc_perkiraan_group_id=" & strcode & " and datediff(month,tanggal,'" & xytgl1 & "')=0 "
        SQL = SQL & " group by a.tanggal,a.no_transaksi_kas,b.nama,a.unitbisnis_id"
        SQL = SQL & " UNION ALL"
        SQL = SQL & " select a.unitbisnis_id,'" & xytglsaldo & "' as tanggal,'' as no_transaksi_kas,'SALDO_AWAL' as nama,0 as debet,0 as kredit,sum(a.nominal) as nominal"
        SQL = SQL & " from acc_saldo_awal a left join acc_perkiraan_nama b on a.coa_code=b.id"
        SQL = SQL & " where b.acc_perkiraan_group_id=" & strcode & " and datediff(month,tgl,'" & xytgl1 & "')=0 "
        SQL = SQL & " group by a.unitbisnis_id) as m"
        SQL = SQL & " where m.unitbisnis_id in (SELECT id FROM ParentChildCTE)"
        SQL = SQL & " group by m.tanggal,m.no_transaksi_kas,m.nama"
        SQL = SQL & " order by 1,2"


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_pendapatan").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_pendapatan").ImportRow(dr)
        Next

        objRpt.SetDataSource(ds1.Tables("ds_pendapatan"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt

        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default


    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitRptNeraca = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub

    Sub showcbocoa()
        dr.Close()
        SQL = "select kd_perkiraan_group,nama_perkiraan_group,id from acc_perkiraan_group order by id "

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)
        cboCoa.DataSource = dt

        cboCoa.DisplayMember = "nama_perkiraan_group"
        cboCoa.ValueMember = "id"
        cboCoa.DropDownColumns = "kd_perkiraan_group,nama_perkiraan_group"
    End Sub

    Private Sub cboCoa_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCoa.DropDown
        showcbocoa()
    End Sub

End Class