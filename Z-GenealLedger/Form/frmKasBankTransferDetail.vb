﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmKasBankTransferDetail
    Public curcol, currow As Integer
    Dim strIDHdr As Integer

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitTransfer = True
        frmFindCoa.ShowDialog()
        CallNoTransaksi()
    End Sub

    Private Sub btnAkun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAkun.Click
        blnCoaTransfer = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub frmKasOutDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DGView.Rows.Clear()
        ClearData()

        If blnAdd Then
            txtkdUnit.Text = myKdBisnis
            txtNamaUnit.Text = myNmBisnis
            txtIdUnit.Text = myIDUnit
            txtDate.Value = Now
            BtnSave.Enabled = True
            DGView.Rows.Add()

            CallNoTransaksi()
        Else
            If blnEdit Then
                BtnSave.Enabled = True
            Else
                BtnSave.Enabled = False
            End If
            ShowData()
        End If

        SettingGrid()
    End Sub

    Sub CallNoTransaksi()
        blnz = txtDate.Value.ToString("MM")
        thnz = txtDate.Value.ToString("yy")
        tglz = txtDate.Value.ToString("dd")
        xystrkdunit = txtIdUnit.Text

        blnzx = Month(txtDate.Value)
        thnzx = Year(txtDate.Value)

        strNoTransaksi = "KBT" & "/" & txtkdUnit.Text & "/" & thnz & "/" & blnz & "/" & tglz & "/" & Konek.GenerateKasBankMasuk("KBT", "acc_kas_hd", "tgl_transaksi") & ""
        txtNoTrans.Text = strNoTransaksi

    End Sub

    Sub ClearData()
        txtNoTrans.Text = ""
        txtIDHdr.Text = ""
        txtkdUnit.Text = ""
        txtNamaUnit.Text = ""
        txtIdUnit.Text = ""
        txtNoCoa.Text = ""
        txtNamaCoa.Text = ""
        txtIdAkun.Text = ""
        txtKet.Text = ""
        txtNilai.Text = 0
    End Sub

    Sub SettingGrid()
        DGView.Columns(0).ReadOnly = False
        DGView.Columns(2).ReadOnly = True
        DGView.Columns(3).ReadOnly = False
        DGView.Columns(4).ReadOnly = False
        DGView.Columns(5).ReadOnly = False
    End Sub

    Sub ShowData()

        SQL = ""
        SQL = " select a.id,a.no_transaksi,a.tgl_transaksi,a.tipe_transaksi,a.unitbisnis_id,c.kd_unitbisnis,c.nama_unitbisnis,"
        SQL = SQL & " a.acc_coa_id,d.kode,d.nama,a.ttl_nilai,a.keterangan,a.status,b.acc_coa_id as acc_codedtl,e.kode as kodedtl,e.nama as nanmadtl,"
        SQL = SQL & " b.nilai,b.keterangan as keterangandtl"
        SQL = SQL & " from acc_kas_hd a inner join acc_kas_dt b on a.id=b.header_id"
        SQL = SQL & " left join unitbisnis_nama c on a.unitbisnis_id=c.id"
        SQL = SQL & " left join acc_perkiraan_nama d on a.acc_coa_id=d.id"
        SQL = SQL & " left join acc_perkiraan_nama e on b.acc_coa_id=e.id"
        SQL = SQL & " where a.is_trash = 0 And b.is_trash = 0 and a.id='" & Trim(strID) & "'"


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read
                txtNoTrans.Text = Trim(dr.Item("no_transaksi").ToString())
                txtIDHdr.Text = Trim(dr.Item("id").ToString())
                txtDate.Value = dr.Item("tgl_transaksi").ToString()
                txtkdUnit.Text = Trim(dr.Item("kd_unitbisnis").ToString())
                txtNamaUnit.Text = Trim(dr.Item("nama_unitbisnis").ToString())
                txtIdUnit.Text = Trim(dr.Item("unitbisnis_id").ToString())
                txtNoCoa.Text = Trim(dr.Item("kode").ToString())
                txtNamaCoa.Text = Trim(dr.Item("nama").ToString())
                txtIdAkun.Text = Trim(dr.Item("acc_coa_id").ToString())
                txtKet.Text = Trim(dr.Item("keterangan").ToString())
                txtNilai.Text = dr.Item("ttl_nilai").ToString()

                DGView.Rows.Add(dr.Item("kodedtl").ToString, "", dr.Item("nanmadtl").ToString, dr.Item("keterangandtl").ToString, dr.Item("nilai").ToString, "", dr.Item("acc_codedtl").ToString)

            End While
        End If

        DGView.Rows.Add()

        hitungtotal()

        dr.Close()

    End Sub

    Private Sub DGView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGView.CellClick
        On Error Resume Next

        If DGView.Columns(e.ColumnIndex).HeaderText = "FIND" Then
            blnCoaGridTransfer = True
            frmFindCoa.ShowDialog()
        ElseIf DGView.Columns(e.ColumnIndex).HeaderText = "ACTION" Then
            DGView.Rows.Remove(DGView.CurrentRow)
            hitungtotal()
        End If
    End Sub

    Private Sub DGView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGView.CellFormatting
        If e.ColumnIndex = 4 AndAlso e.RowIndex <> DGView.NewRowIndex Then
            e.Value = CDec(e.Value).ToString("N2")
            e.FormattingApplied = True
        End If
    End Sub

    Private Sub DGView_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.CurrentCellChanged
        Try
            curcol = DGView.CurrentCell.ColumnIndex
            currow = DGView.CurrentCell.RowIndex
        Catch ex As Exception
            curcol = 0
            currow = 0
        End Try
    End Sub

    Private Sub DGView_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles DGView.EditingControlShowing
        RemoveHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress

        If DGView.CurrentCell.ColumnIndex = 4 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress
        Else
            RemoveHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress
        End If
    End Sub

    Private Sub TextBox_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

        If (Not Char.IsControl(e.KeyChar) _
                   AndAlso (Not Char.IsDigit(e.KeyChar) _
                   AndAlso (e.KeyChar <> Microsoft.VisualBasic.ChrW(46)))) Then
            e.Handled = True
        End If

    End Sub

    Private Sub DGView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGView.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                DGView.ClearSelection()
                Try
                    If curcol = 0 Then
                        SQL = "select * from acc_perkiraan_nama where kode='" & Trim(DGView.Item(0, currow).Value) & "' and type_perkiraan='D'"
                        Cmd = New SqlClient.SqlCommand(SQL, Cn)
                        dr = Cmd.ExecuteReader()

                        If dr.HasRows Then
                            With DGView
                                While dr.Read
                                    .Item(2, currow).Value = Trim(dr.Item("nama").ToString())
                                    .Item(3, currow).Value = Trim(dr.Item("keterangan").ToString())
                                    .Item(6, currow).Value = Trim(dr.Item("id").ToString())
                                    .CurrentCell = DGView(3, currow)
                                End While
                            End With
                            dr.Close()
                        Else
                            MsgBox("Data dengan kode tsb tidak ada", vbInformation)
                            DGView.CurrentCell = DGView(0, currow)
                            Exit Sub
                        End If

                    ElseIf curcol = 3 Then
                        DGView.CurrentCell = DGView(4, currow)

                    ElseIf curcol = 4 Then
                        DGView.Rows.Add()
                        hitungtotal()
                        DGView.CurrentCell = DGView(0, currow + 1)
                    End If



                    'If curcol = DGView.Columns.Count - 1 - 2 Then
                    '    If currow < DGView.Rows.Count - 1 Then
                    '        DGView.CurrentCell = DGView(0, currow + 1)
                    '    ElseIf currow = DGView.Rows.Count - 1 Then
                    '        DGView.CurrentCell = DGView(0, currow + 1)
                    '    End If
                    'Else
                    '    DGView.CurrentCell = DGView(curcol + 1, currow)
                    'End If
                Catch ex As Exception
                    Exit Try
                End Try
        End Select
    End Sub

    Sub hitungtotal()
        Dim tnilai As Double = 0
        txtNilai.Text = 0

        For t As Integer = 0 To DGView.Rows.Count - 1
            tnilai = tnilai + (DGView.Rows(t).Cells(4).Value)
        Next

        txtNilai.Text = Format(tnilai, "N2")
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        blnz = txtDate.Value.ToString("MM")
        thnz = txtDate.Value.ToString("yy")
        xystrkdunit = txtIdUnit.Text

        If txtkdUnit.Text = "" Or txtNoCoa.Text = "" Then
            MsgBox("Masih Ada Data Yang Kosong...", MsgBoxStyle.Critical)
            Exit Sub
        End If

        '--- Cek Nilai Total ---
        Dim tnilaix As Double = 0

        For t As Integer = 0 To DGView.Rows.Count - 1
            tnilaix = tnilaix + (DGView.Rows(t).Cells(4).Value)
        Next

        If tnilaix <> txtNilai.Text Then
            MsgBox("Nilai Total Masih Berbeda, silakan Tekan Tombol ENTER pada kolom Nominal...", MsgBoxStyle.Critical)
            Exit Sub
        End If
        '---------------------------

        '=== Dimatikan sementara tunggu konfirmasi prioritas
        'SQL = ""
        'SQL = " Select * From acc_periode_setting Where id_unitbisnis = " & txtIdUnit.Text & " and datediff(month,tgl_berjalan,'" & Format(txtDate.Value, "yyyy-MM-dd") & "') >= 0"

        'Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
        'dr1 = Cmd1.ExecuteReader()

        'If dr1.HasRows Then
        '    dr1.Close()
        'Else
        '    MsgBox("Bulan yang anda input sudah di closing, Silakan ganti tangal menjadi bulan berjalan", vbInformation)
        '    dr1.Close()
        '    Exit Sub
        'End If

        If blnAdd = True Then

            strNoTransaksi = Trim(txtNoTrans.Text)

            'strNoTransaksi = "KBT" & "/" & txtkdUnit.Text & "/" & thnz & "/" & Konek.GenerateJurnal("KBT", "acc_kas_hd", "no_transaksi") & ""

            IsConError = True

            dr.Close()
            SQL = "Insert Into acc_kas_hd (confirm,no_transaksi,tgl_transaksi,tipe_transaksi,unitbisnis_id,unitbisnis_level,acc_coa_id,acc_coa_kode,ttl_nilai,tanggal_buat,user_buat,keterangan,status,is_trash,posting)"
            SQL = SQL & " Values(0,'" & strNoTransaksi & "','" & Format(txtDate.Value, "yyyy-MM-dd") & "','KAS_BANK_TRANSFER'," & xystrkdunit & ",0," & txtIdAkun.Text & ",'" & txtNoCoa.Text & "',"
            SQL = SQL & " " & CDbl(txtNilai.Text) & ",'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "','" & myID & "','" & Konek.BuatKomaSatu(txtKet.Text) & "',0,0,0)"
            Konek.IUDQuery(SQL)


            SQL = ""
            SQL = "Select * From acc_kas_hd Where no_transaksi = '" & strNoTransaksi & "'"

            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()

            If dr.HasRows Then

                While dr.Read
                    strIDHdr = Trim(dr.Item("id").ToString())
                End While
                dr.Close()
            End If


            dr.Close()
            SQL = ""
            SQL = "Insert Into acc_jurnal (transaksi_id,jenis,tanggal,unitbisnis_id,unitbisnis_id_level,acc_coa_id,acc_coa_kode,nilai_total,nilai_debet,nilai_kredit,"
            SQL = SQL & " no_urut,keterangan,status,is_trash,tanggal_buat,user_buat,posting,no_transaksi_kas) "
            SQL = SQL & " Values(" & strIDHdr & ",'KAS_BANK_TRANSFER','" & Format(txtDate.Value, "yyyy-MM-dd") & "'," & xystrkdunit & "," & myIDOrganisasi & "," & txtIdAkun.Text & ",'" & txtNoCoa.Text & "'," & CDbl(txtNilai.Text) & ","
            SQL = SQL & " 0," & CDbl(txtNilai.Text) & ",0,'" & Konek.BuatKomaSatu(txtKet.Text) & "',0,0,'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",0,'" & strNoTransaksi & "')"
            Konek.IUDQuery(SQL)


        ElseIf blnEdit = True Then
            IsConError = True

            strIDHdr = txtIDHdr.Text
            strNoTransaksi = Trim(txtNoTrans.Text)

            dr.Close()
            SQL = "update acc_kas_hd set tgl_transaksi='" & Format(txtDate.Value, "yyyy-MM-dd HH:mm:ss") & "',acc_coa_id=" & txtIdAkun.Text & ",acc_coa_kode='" & txtNoCoa.Text & "',ttl_nilai=" & CDbl(txtNilai.Text) & ","
            SQL = SQL & " keterangan='" & Konek.BuatKomaSatu(txtKet.Text) & "',tanggal_ubah='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',user_ubah='" & myID & "'"
            SQL = SQL & " where no_transaksi='" & Trim(txtNoTrans.Text) & "'"
            Konek.IUDQuery(SQL)

            dr.Close()
            SQL = " delete from acc_kas_dt where header_id='" & strIDHdr & "'"
            Konek.IUDQuery(SQL)

            dr.Close()
            SQL = " delete from acc_jurnal where no_transaksi_kas='" & Trim(txtNoTrans.Text) & "'"
            Konek.IUDQuery(SQL)


            dr.Close()
            SQL = ""
            SQL = "Insert Into acc_jurnal (transaksi_id,jenis,tanggal,unitbisnis_id,unitbisnis_id_level,acc_coa_id,acc_coa_kode,nilai_total,nilai_debet,nilai_kredit,"
            SQL = SQL & " no_urut,keterangan,status,is_trash,tanggal_buat,user_buat,posting,no_transaksi_kas) "
            SQL = SQL & " Values(" & strIDHdr & ",'KAS_BANK_TRANSFER','" & Format(txtDate.Value, "yyyy-MM-dd") & "'," & xystrkdunit & "," & myIDOrganisasi & "," & txtIdAkun.Text & ",'" & txtNoCoa.Text & "'," & CDbl(txtNilai.Text) & ","
            SQL = SQL & " 0," & CDbl(txtNilai.Text) & ",0,'" & Konek.BuatKomaSatu(txtKet.Text) & "',0,0,'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",0,'" & strNoTransaksi & "')"
            Konek.IUDQuery(SQL)


        End If


        For x As Integer = 0 To DGView.Rows.Count - 1
            If DGView.Rows(x).Cells(0).Value <> "" Then
                svCoaKode = DGView.Rows(x).Cells(0).Value
                svID = DGView.Rows(x).Cells(6).Value
                svKet = Konek.BuatKomaSatu(IIf(DGView.Rows(x).Cells(3).Value.Equals(DBNull.Value), "", (DGView.Rows(x).Cells(3).Value)))
                svTotal = DGView.Rows(x).Cells(4).Value

                'Menyimpan Data Ke Tabel Detail
                dr.Close()
                SQL = ""
                SQL = "Insert Into acc_kas_dt (header_id,transaksi_id,tipe_transaksi,acc_coa_id,acc_coa_kode,nilai,tanggal_buat,user_buat,keterangan,status,is_trash) " & _
                      "Values(" & strIDHdr & "," & strIDHdr & ",'KAS_BANK_TRANSFER'," & svID & ",'" & svCoaKode & "'," & svTotal & ",'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",'" & svKet & "',0,0)"
                Konek.IUDQuery(SQL)


                dr.Close()
                SQL = ""
                SQL = "Insert Into acc_jurnal (transaksi_id,jenis,tanggal,unitbisnis_id,unitbisnis_id_level,acc_coa_id,acc_coa_kode,nilai_total,nilai_debet,nilai_kredit,"
                SQL = SQL & " no_urut,keterangan,status,is_trash,tanggal_buat,user_buat,posting,no_transaksi_kas) "
                SQL = SQL & " Values(" & strIDHdr & ",'KAS_BANK_TRANSFER','" & Format(txtDate.Value, "yyyy-MM-dd") & "'," & xystrkdunit & "," & myIDOrganisasi & "," & svID & ",'" & svCoaKode & "'," & CDbl(txtNilai.Text) & ","
                SQL = SQL & " " & svTotal & ",0," & x + 1 & ",'" & Konek.BuatKomaSatu(svKet) & "',0,0,'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",0,'" & strNoTransaksi & "')"
                Konek.IUDQuery(SQL)

            End If
        Next


        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        blnAdd = False
        blnEdit = False

        Me.Close()
        Exit Sub

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

End Class