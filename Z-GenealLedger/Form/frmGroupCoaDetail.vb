﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmGroupCoaDetail
    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        blnAdd = False
        blnEdit = False
        Me.Close()
    End Sub

    Private Sub ChkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkStatus.CheckedChanged
        If ChkStatus.Checked = True Then
            ChkStatus.Text = "Aktif"
        Else
            ChkStatus.Text = "Non Aktif"
        End If
    End Sub

    Private Sub frmGroupCoaDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If blnAdd = True Then
            txtkdgroup.Text = ""
            txtnamagroup.Text = ""
            txtKet.Text = ""
            ChkStatus.Checked = True
        Else
            ShowData()

            If strStatus = "AKTIF" Then
                ChkStatus.Checked = True
                ChkStatus.Text = "Aktif"
            Else
                ChkStatus.Checked = False
                ChkStatus.Text = "Non Aktif"
            End If

        End If

        If blnAdd = True Or blnEdit = True Then
            BtnSave.Enabled = True
        Else
            BtnSave.Enabled = False
        End If
    End Sub

    Sub ShowData()
        Dim shActive As Integer

        SQL = ""
        SQL = "Select * From acc_perkiraan_group Where kd_perkiraan_group = '" & Trim(strID) & "'"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read
                txtkdgroup.Text = Trim(dr.Item("kd_perkiraan_group").ToString())
                txtnamagroup.Text = Trim(dr.Item("nama_perkiraan_group").ToString())
                txtIdGroup.Text = Trim(dr.Item("id").ToString())
                txtKet.Text = Trim(dr.Item("keterangan").ToString())

                shActive = Trim(dr.Item("status").ToString())
                If shActive = 1 Then
                    ChkStatus.Checked = True
                    ChkStatus.Text = "Aktif"
                Else
                    ChkStatus.Checked = False
                    ChkStatus.Text = "Non Aktif"
                End If

            End While
        End If

        dr.Close()

    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        If txtkdgroup.Text = "" Or txtnamagroup.Text = "" Then
            MsgBox("Masih Ada Data Yang Kosong...", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If ChkStatus.Checked = True Then
            strActive = "1"
        Else
            strActive = "0"
        End If

        IsConError = True

        If blnAdd = True Then
            dr.Close()
            SQL = "Select * From acc_perkiraan_group where kd_perkiraan_group='" & Trim(txtkdgroup.Text) & "' or nama_perkiraan_group='" & Konek.BuatKomaSatu(txtnamagroup.Text) & "'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            dr.Read()

            If dr.HasRows Then
                MsgBox("Data tersebut sudah ada...", MsgBoxStyle.Critical)
                Exit Sub
            Else
                dr.Close()
                SQL = "Insert Into acc_perkiraan_group (tanggal_buat,user_buat,kd_perkiraan_group,nama_perkiraan_group,"
                SQL = SQL & " keterangan,status,is_trash) "
                SQL = SQL & " Values('" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ",'" & txtkdgroup.Text & "','" & Konek.BuatKomaSatu(txtnamagroup.Text) & "',"
                SQL = SQL & " '" & Konek.BuatKomaSatu(txtKet.Text) & "'," & strActive & ",0)"
                Konek.IUDQuery(SQL)

            End If

        ElseIf blnEdit = True Then
            dr.Close()
            SQL = "update acc_perkiraan_group set nama_perkiraan_group= '" & Konek.BuatKomaSatu(txtnamagroup.Text) & "',"
            SQL = SQL & " keterangan='" & Konek.BuatKomaSatu(txtKet.Text) & "',status=" & strActive & ", tanggal_ubah='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',"
            SQL = SQL & " user_ubah=" & myID & ""
            SQL = SQL & " where kd_perkiraan_group='" & txtkdgroup.Text & "'"
            Konek.IUDQuery(SQL)

        End If

        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        blnAdd = False
        blnEdit = False

        Me.Close()
        Exit Sub

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub
End Class