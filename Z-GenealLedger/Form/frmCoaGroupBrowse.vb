﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Reflection
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.SuperGrid
Imports DevComponents.DotNetBar.SuperGrid.Style
Imports System.Data.SqlClient


Public Class frmCoaGroupBrowse
    'Inherits Office2007Form
    Private _DataSet As DataSet

    Private _Background1 As New Background(Color.White, Color.FromArgb(238, 244, 251), 45)

    Private _Background2 As New Background(Color.FromArgb(249, 249, 234))
    Private _Background3 As New Background(Color.FromArgb(255, 247, 250))

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Sub frmClose()
        fcoag.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub BtnShow_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnShow.Click
        BindCustomerData()
        txtSearch.Text = ""
        txtSearch.Focus()
    End Sub

    Private Sub frmCoaGroupBrowse_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5

        GroupPanel2.Left = Me.Width - 680
        GroupPanel2.Top = 2
    End Sub

    'Public Sub New()
    '    InitializeComponent()

    '    ' Initialize the grid, bind to our grid data
    '    ' and set the sample description text

    '    InitializeGrid()
    '    BindCustomerData()

    '    'ShellServices.LoadRtbText(richTextBox1, "SuperGridDemo.DemoMasterDetail.rtf")
    'End Sub

#Region "InitializeGrid"

    ''' <summary>
    ''' Initializes the default grid
    ''' </summary>
    Private Sub InitializeGrid()
        Dim panel As GridPanel = superGridControl1.PrimaryGrid

        panel.Name = "Customers"
        panel.ShowToolTips = True

        panel.MinRowHeight = 20
        panel.AutoGenerateColumns = True

        panel.DefaultVisualStyles.GroupByStyles.Default.Background = _Background1

        panel.SelectionGranularity = SelectionGranularity.Cell

        AddHandler superGridControl1.CellValueChanged, AddressOf SuperGridControl1CellValueChanged
        AddHandler superGridControl1.GetCellStyle, AddressOf SuperGridControl1GetCellStyle
        AddHandler superGridControl1.DataBindingComplete, AddressOf SuperGridControl1DataBindingComplete
    End Sub

#End Region

#Region "BindCustomerData"

    ''' <summary>
    ''' Binds our data to the grid
    ''' </summary>
    Private Sub BindCustomerData()
        'INSTANT VB NOTE: The variable location was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim location_Renamed As String = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & "\Resources"

        If location_Renamed IsNot Nothing Then
            _DataSet = New DataSet()

            'Dim adapter As New OleDbDataAdapter

            Using Da As New SqlDataAdapter("select id,kd_perkiraan_group,nama_perkiraan_group,case [status] when 1 then 'AKTIF' else 'NON AKTIF' end as status,tanggal_buat from acc_perkiraan_group WHERE 0=0;", Cn)
                Da.Fill(_DataSet, "acc_perkiraan_group")

                CType(New SqlDataAdapter("select kode,nama,parent_id,case [status] when 1 then 'AKTIF' else 'NON AKTIF' end as status,acc_perkiraan_group_id from acc_perkiraan_nama;", Cn), SqlDataAdapter).Fill(_DataSet, "acc_perkiraan_nama")

                _DataSet.Relations.Add("1", _DataSet.Tables("acc_perkiraan_group").Columns("id"), _DataSet.Tables("acc_perkiraan_nama").Columns("acc_perkiraan_group_id"), False)

                '_DataSet.Relations.Add("2", _DataSet.Tables("Orders").Columns("OrderID"), _DataSet.Tables("Order Details").Columns("OrderID"), False)
            End Using

            superGridControl1.PrimaryGrid.DataSource = _DataSet
            superGridControl1.PrimaryGrid.DataMember = "acc_perkiraan_group"
        End If
    End Sub

#End Region

#Region "SuperGridControl1CellValueChanged"

    ''' <summary>
    ''' Handles cell value change events
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SuperGridControl1CellValueChanged(ByVal sender As Object, ByVal e As GridCellValueChangedEventArgs)
        Dim panel As GridPanel = e.GridPanel

        ' If a cell value in the "Order Details" panel has changed
        ' then update its footer to reflect the change

  
    End Sub

#End Region

#Region "SuperGridControl1GetCellStyle"

    ''' <summary>
    ''' This routine is called to retrieve application provided
    ''' cell style information. The style being presented in this
    ''' call is the Effective Style (style used after applying
    ''' all base styles).
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SuperGridControl1GetCellStyle(ByVal sender As Object, ByVal e As GridGetCellStyleEventArgs)
        Dim panel As GridPanel = e.GridPanel

        If panel.Name.Equals("Customers") = True Then
            If e.GridCell.GridColumn.Name.Equals("ContactTitle") = True Then
                If CStr(e.GridCell.Value).Equals("Owner") = True Then
                    e.Style.TextColor = Color.Red
                End If
            End If
        End If
    End Sub

#End Region

#Region "SuperGridControl1DataBindingComplete"

    ''' <summary>
    ''' This routine is called after each bindable data portion has
    ''' been completed. This callout lets you customize the display
    ''' or visibility of the data however the application needs.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SuperGridControl1DataBindingComplete(ByVal sender As Object, ByVal e As GridDataBindingCompleteEventArgs)
        Dim panel As GridPanel = e.GridPanel

        panel.GroupByRow.Visible = True

        Select Case panel.DataMember
            Case "Customers"
                CustomizeCustomerPanel(panel)

            Case "Orders"
                CustomizeOrdersPanel(panel)

            Case "Order Details"
                CustomizeDetailsPanel(panel)
        End Select
    End Sub

#Region "CustomizeCustomerPanel"

    ''' <summary>
    ''' Customizes the CustomerPanel
    ''' </summary>
    ''' <param name="panel"></param>
    Private Sub CustomizeCustomerPanel(ByVal panel As GridPanel)
        panel.FrozenColumnCount = 1
        panel.ColumnHeader.RowHeight = 30

        panel.Columns(0).GroupBoxEffects = GroupBoxEffects.None
        panel.Columns("Region").NullString = "<no locale>"

        panel.Columns(0).CellStyles.Default.Background = New Background(Color.AliceBlue)

        For Each column As GridColumn In panel.Columns
            column.ColumnSortMode = ColumnSortMode.Multiple
        Next column
    End Sub

#End Region

#Region "CustomizeOrdersPanel"

    ''' <summary>
    ''' Customizes the OrdersPanel
    ''' </summary>
    ''' <param name="panel"></param>
    Private Sub CustomizeOrdersPanel(ByVal panel As GridPanel)
        panel.ShowRowGridIndex = True
        panel.ShowRowDirtyMarker = True
        panel.ColumnHeader.RowHeight = 30

        panel.Columns(0).CellStyles.Default.Background = New Background(Color.Beige)

        panel.Caption = New GridCaption()

        panel.Caption.Text = String.Format("Orders ({0}) for customer <font color=""Maroon""><i>""{1}</i>""</font>", panel.Rows.Count, CType(panel.Parent, GridRow)("CompanyName").Value)

        panel.DefaultVisualStyles.CaptionStyles.Default.Alignment = Alignment.MiddleLeft
        panel.DefaultVisualStyles.GroupByStyles.Default.Background = _Background2
    End Sub

#End Region

#Region "CustomizeDetailsPanel"

    ''' <summary>
    ''' Customizes the DetailsPanel
    ''' </summary>
    ''' <param name="panel"></param>
    Private Sub CustomizeDetailsPanel(ByVal panel As GridPanel)
        panel.ColumnHeader.RowHeight = 30

        panel.Columns(0).CellStyles.Default.Background = New Background(Color.LavenderBlush)

        panel.Columns("OrderID").CellStyles.Default.Alignment = Alignment.MiddleLeft

        panel.DefaultVisualStyles.CaptionStyles.Default.Alignment = Alignment.MiddleLeft
        panel.DefaultVisualStyles.CellStyles.Default.Alignment = Alignment.MiddleRight
        panel.DefaultVisualStyles.GroupByStyles.Default.Background = _Background3

    End Sub

#End Region


#End Region

    Private Sub frmCoaGroupBrowse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitializeComponent()

        ' Initialize the grid, bind to our grid data
        ' and set the sample description text

        InitializeGrid()
        BindCustomerData()

        'ShellServices.LoadRtbText(richTextBox1, "SuperGridDemo.DemoMasterDetail.rtf")
    End Sub
End Class