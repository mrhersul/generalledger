﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettingCoaDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettingCoaDetail))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.RBK = New System.Windows.Forms.RadioButton()
        Me.RBD = New System.Windows.Forms.RadioButton()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.CboNamaField = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.CboNamaTabel = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.txtID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtIdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btngroup = New DevComponents.DotNetBar.ButtonX()
        Me.txtkdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtAccSetting = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Controls.Add(Me.txtAccSetting)
        Me.GroupPanel1.Controls.Add(Me.RBK)
        Me.GroupPanel1.Controls.Add(Me.RBD)
        Me.GroupPanel1.Controls.Add(Me.LabelX4)
        Me.GroupPanel1.Controls.Add(Me.CboNamaField)
        Me.GroupPanel1.Controls.Add(Me.CboNamaTabel)
        Me.GroupPanel1.Controls.Add(Me.txtID)
        Me.GroupPanel1.Controls.Add(Me.LabelX3)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.txtIdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNamaUnit)
        Me.GroupPanel1.Controls.Add(Me.btngroup)
        Me.GroupPanel1.Controls.Add(Me.txtkdUnit)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 12)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(664, 236)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 4
        '
        'RBK
        '
        Me.RBK.AutoSize = True
        Me.RBK.BackColor = System.Drawing.Color.Transparent
        Me.RBK.Location = New System.Drawing.Point(295, 175)
        Me.RBK.Name = "RBK"
        Me.RBK.Size = New System.Drawing.Size(65, 17)
        Me.RBK.TabIndex = 5
        Me.RBK.Text = "KREDIT"
        Me.RBK.UseVisualStyleBackColor = False
        '
        'RBD
        '
        Me.RBD.AutoSize = True
        Me.RBD.BackColor = System.Drawing.Color.Transparent
        Me.RBD.Checked = True
        Me.RBD.Location = New System.Drawing.Point(184, 175)
        Me.RBD.Name = "RBD"
        Me.RBD.Size = New System.Drawing.Size(61, 17)
        Me.RBD.TabIndex = 4
        Me.RBD.TabStop = True
        Me.RBD.Text = "DEBET"
        Me.RBD.UseVisualStyleBackColor = False
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(31, 172)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(105, 23)
        Me.LabelX4.TabIndex = 55
        Me.LabelX4.Text = "TIPE AKUN"
        '
        'CboNamaField
        '
        Me.CboNamaField.DisplayMember = "Text"
        Me.CboNamaField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CboNamaField.FormattingEnabled = True
        Me.CboNamaField.ItemHeight = 14
        Me.CboNamaField.Location = New System.Drawing.Point(188, 63)
        Me.CboNamaField.Name = "CboNamaField"
        Me.CboNamaField.Size = New System.Drawing.Size(379, 20)
        Me.CboNamaField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CboNamaField.TabIndex = 1
        '
        'CboNamaTabel
        '
        Me.CboNamaTabel.DisplayMember = "Text"
        Me.CboNamaTabel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CboNamaTabel.FormattingEnabled = True
        Me.CboNamaTabel.ItemHeight = 14
        Me.CboNamaTabel.Location = New System.Drawing.Point(188, 27)
        Me.CboNamaTabel.Name = "CboNamaTabel"
        Me.CboNamaTabel.Size = New System.Drawing.Size(379, 20)
        Me.CboNamaTabel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CboNamaTabel.TabIndex = 0
        '
        'txtID
        '
        '
        '
        '
        Me.txtID.Border.Class = "TextBoxBorder"
        Me.txtID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtID.Location = New System.Drawing.Point(582, 24)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(39, 20)
        Me.txtID.TabIndex = 52
        Me.txtID.Visible = False
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(31, 60)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(105, 23)
        Me.LabelX3.TabIndex = 51
        Me.LabelX3.Text = "NAMA FIELD"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(31, 24)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(105, 23)
        Me.LabelX2.TabIndex = 50
        Me.LabelX2.Text = "NAMA TABEL"
        '
        'txtIdUnit
        '
        '
        '
        '
        Me.txtIdUnit.Border.Class = "TextBoxBorder"
        Me.txtIdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdUnit.Location = New System.Drawing.Point(613, 124)
        Me.txtIdUnit.Name = "txtIdUnit"
        Me.txtIdUnit.Size = New System.Drawing.Size(39, 20)
        Me.txtIdUnit.TabIndex = 47
        Me.txtIdUnit.Visible = False
        '
        'txtNamaUnit
        '
        '
        '
        '
        Me.txtNamaUnit.Border.Class = "TextBoxBorder"
        Me.txtNamaUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaUnit.Enabled = False
        Me.txtNamaUnit.Location = New System.Drawing.Point(276, 98)
        Me.txtNamaUnit.Name = "txtNamaUnit"
        Me.txtNamaUnit.Size = New System.Drawing.Size(339, 20)
        Me.txtNamaUnit.TabIndex = 46
        '
        'btngroup
        '
        Me.btngroup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btngroup.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btngroup.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btngroup.Image = CType(resources.GetObject("btngroup.Image"), System.Drawing.Image)
        Me.btngroup.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btngroup.Location = New System.Drawing.Point(613, 98)
        Me.btngroup.Name = "btngroup"
        Me.btngroup.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btngroup.Size = New System.Drawing.Size(34, 20)
        Me.btngroup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btngroup.TabIndex = 2
        '
        'txtkdUnit
        '
        '
        '
        '
        Me.txtkdUnit.Border.Class = "TextBoxBorder"
        Me.txtkdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtkdUnit.Enabled = False
        Me.txtkdUnit.Location = New System.Drawing.Point(188, 98)
        Me.txtkdUnit.Name = "txtkdUnit"
        Me.txtkdUnit.Size = New System.Drawing.Size(82, 20)
        Me.txtkdUnit.TabIndex = 2
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(583, 149)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(70, 72)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 7
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(507, 149)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(70, 72)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 6
        Me.BtnSave.Text = "SAVE"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(31, 98)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(105, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "KODE AKUN"
        '
        'txtAccSetting
        '
        '
        '
        '
        Me.txtAccSetting.Border.Class = "TextBoxBorder"
        Me.txtAccSetting.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAccSetting.Location = New System.Drawing.Point(188, 135)
        Me.txtAccSetting.Name = "txtAccSetting"
        Me.txtAccSetting.Size = New System.Drawing.Size(300, 20)
        Me.txtAccSetting.TabIndex = 3
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(31, 135)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(105, 23)
        Me.LabelX5.TabIndex = 59
        Me.LabelX5.Text = "ACC SETTING"
        '
        'frmSettingCoaDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 260)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSettingCoaDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Setting Akun Perkiraan Detail"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtIdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNamaUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btngroup As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtkdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents CboNamaTabel As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents CboNamaField As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents RBK As System.Windows.Forms.RadioButton
    Friend WithEvents RBD As System.Windows.Forms.RadioButton
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAccSetting As DevComponents.DotNetBar.Controls.TextBoxX
End Class
