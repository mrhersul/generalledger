﻿Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmRekapJurnal
    Sub frmClose()
        frj.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub frmRekapJurnal_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        GridEX.Width = Me.Width - 40
        GridEX.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        GridEX.Left = 10
        GridEX.Top = 40
        GroupPanel2.Left = Me.Width - 980
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Sub ShowData()
        ' --- Header ---

        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & myIDCabang & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE T1.parent_id IS NOT NULL )"
        SQL = SQL & " select x.transaksi_id,x.jenis,x.tanggal,x.unitbisnis_id,x.kd_unitbisnis,x.nama_unitbisnis,x.status,x.posting from"
        SQL = SQL & " (select a.transaksi_id,a.jenis,a.tanggal,a.unitbisnis_id,b.kd_unitbisnis,b.nama_unitbisnis,case a.status when 0 then 'OPEN' when 1 then 'CONFIRM' end as status,case a.posting when 0 then 'NO' when 1 then 'YES' end as posting from acc_jurnal a"
        SQL = SQL & " left join unitbisnis_nama b on a.unitbisnis_id=b.id where a.is_trash=0 "

        If ChkBln.Checked = True Then
            SQL = SQL & " and datediff(month,a.tanggal,'" & Format(txtDate.Value, "yyyy-MM-dd") & "')=0"
        End If

        If txtSearch.Text <> "" Then
            SQL = SQL & " and (a.transaksi_id like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or b.nama_unitbisnis like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
        End If

        If UCase(cboStatus.Text) = "OPEN" Then
            SQL = SQL & " and a.status = 0"
        ElseIf UCase(cboStatus.Text) = "CONFIRM" Then
            SQL = SQL & " and a.status = 1"
        ElseIf UCase(cboStatus.Text) = "CANCEL" Then
            SQL = SQL & " and a.status = 2"
        End If

        SQL = SQL & " group by a.transaksi_id,a.jenis,a.tanggal,a.unitbisnis_id,b.kd_unitbisnis,b.nama_unitbisnis,a.status,a.posting) as x"
        SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        dscoa3.Tables("Data_Header").Clear()
        For Each dr As DataRow In dt.Rows
            dscoa3.Tables("Data_Header").ImportRow(dr)
        Next


        ' --- Coa ---
        SQL = ""
        SQL = " select a.transaksi_id,b.kode,b.nama,a.nilai_debet,a.nilai_kredit,a.keterangan from acc_jurnal a left join acc_perkiraan_nama b on a.acc_coa_id=b.id"
        SQL = SQL & " where a.is_trash=0 and datediff(month,a.tanggal,'" & Format(txtDate.Value, "yyyy-MM-dd") & "')=0"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr3 = Cmd.ExecuteReader()

        Dim dt3 = New DataTable
        dt3.Load(dr3)

        dscoa3.Tables("Data_Detail").Clear()
        For Each dr3 As DataRow In dt3.Rows
            dscoa3.Tables("Data_Detail").ImportRow(dr3)
        Next

        GridEX.DataSource = dscoa3
        GridEX.DataMember = "Data_Header"
        GridEX.RootTable.Key = "Data_Header_Data_Detail"

    End Sub

    Private Sub frmRekapJurnal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDate.Value = Format(Now, "MMMM-yyyy")
        ChkBln.Checked = True
        txtDate.Enabled = True
        ShowData()
    End Sub

    Private Sub BubbleButton1_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BubbleButton1.Click
        ShowData()
        txtSearch.Text = ""
        cboStatus.Text = ""
    End Sub

    Private Sub ChkBln_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkBln.CheckedChanged
        If ChkBln.Checked = True Then
            txtDate.Enabled = True
        Else
            txtDate.Enabled = False
        End If
    End Sub
End Class