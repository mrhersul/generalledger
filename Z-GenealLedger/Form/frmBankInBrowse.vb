﻿Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmBankInBrowse

    Sub frmClose()
        fbib.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub frmBankInBrowse_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmBankInBrowse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDate.Value = Format(Now, "dd/MM/yyyy")
        txtDate.Enabled = False
        ShowDataGrid()
    End Sub

    Private Sub frmBankInBrowse_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        DGView.Width = Me.Width - 40
        DGView.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        DGView.Left = 10
        DGView.Top = 40
        GroupPanel2.Left = Me.Width - 980
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnAdd.Click
        blnAdd = True
        blnEdit = False
        TakeDGView()
        frmBankInDetail.ShowDialog()
        ShowDataGrid()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Private Sub BtnEdit_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnEdit.Click
        TakeDGView()
        If stsposting = 0 Then

            blnAdd = False
            blnEdit = True
            TakeDGView()
            frmBankInDetail.ShowDialog()
            ShowDataGrid()
        Else
            MsgBox("Data tidak bisa diedit, Karena status sudah POSTING", vbExclamation)
            Exit Sub
        End If
    End Sub

    Private Sub DGView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGView.CellFormatting
        ColorChange()
        If e.ColumnIndex = 4 Then e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        If e.ColumnIndex = 4 Then e.Value = CDec(e.Value).ToString("N2")
    End Sub

    Private Sub DGView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.DoubleClick
        blnAdd = False
        blnEdit = False
        TakeDGView()
        frmBankInDetail.ShowDialog()
    End Sub

    Sub ShowDataGrid()

        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & myIDCabang & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE"
        SQL = SQL & " T1.parent_id IS NOT NULL )"
        SQL = SQL & " select x.no_transaksi,x.tgl_transaksi,x.nama_unitbisnis,x.acc_nama,x.ttl_nilai,x.confirm,x.id,x.posting from"
        SQL = SQL & " (select top 500 a.no_transaksi,a.tgl_transaksi,a.tipe_transaksi,a.unitbisnis_id,b.nama_unitbisnis,c.kode + ' - ' + c.nama as acc_nama,a.ttl_nilai,case a.confirm when 0 then 'OPEN'"
        SQL = SQL & " when '1' then 'CONFIRM' else 'CANCEL' end as confirm,a.id,a.posting"
        SQL = SQL & " from acc_kas_hd a left join unitbisnis_nama b on a.unitbisnis_id=b.id "
        SQL = SQL & " left join acc_perkiraan_nama c on a.acc_coa_id=c.id"
        SQL = SQL & " where a.is_trash=0 and a.tipe_transaksi='BANK_MASUK') as x"
        SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"


        If txtSearch.Text <> "" Then

            SQL = SQL & " and (x.no_transaksi like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%'"
            SQL = SQL & " or x.nama_unitbisnis like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or x.acc_nama like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"

        End If

        If UCase(cboStatus.Text) = "OPEN" Then
            SQL = SQL & " and x.confirm = 'OPEN'"
        ElseIf UCase(cboStatus.Text) = "CONFIRM" Then
            SQL = SQL & " and x.confirm = 'CONFIRM'"
        End If

        If ChkBln.Checked = True Then
            SQL = SQL & " and datediff(month,x.tgl_transaksi,'" & Format(txtDate.Value, "yyyy-MM-dd") & "') =0"
        End If


        SQL = SQL & " order by 1 desc"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 300
        DGView.Columns(1).Width = 200
        DGView.Columns(2).Width = 260
        DGView.Columns(3).Width = 300
        DGView.Columns(4).Width = 120
        DGView.Columns(5).Width = 80
        DGView.Columns(6).Width = 50
        DGView.Columns(7).Width = 50

        DGView.Columns(0).HeaderText = "NO TRANSAKSI"
        DGView.Columns(1).HeaderText = "TANGGAL TRANSAKSI"
        DGView.Columns(2).HeaderText = "UNIT BISNIS"
        DGView.Columns(3).HeaderText = "MASUK KE AKUN"
        DGView.Columns(4).HeaderText = "TOTAL"
        DGView.Columns(5).HeaderText = "STATUS"
        DGView.Columns(6).HeaderText = "ID"
        DGView.Columns(7).HeaderText = "POSTING"

        DGView.Columns(6).Visible = False
        DGView.Columns(7).Visible = False

    End Sub

    Sub ColorChange()
        For Each iniRow As DataGridViewRow In DGView.Rows
            For Each iniCell As DataGridViewCell In iniRow.Cells
                If iniRow.Index Mod 2 = 0 Then
                    iniCell.Style.BackColor = Color.WhiteSmoke
                Else
                    iniCell.Style.BackColor = Color.CornflowerBlue
                End If
            Next
        Next
    End Sub

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        ShowDataGrid()
        txtSearch.Text = ""
        txtSearch.Focus()
    End Sub

    Sub TakeDGView()
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                strID = IIf(.Item("id", i).Value.Equals(DBNull.Value), "", .Item("id", i).Value)
                xyzNotrans = IIf(.Item("no_transaksi", i).Value.Equals(DBNull.Value), "", .Item("no_transaksi", i).Value)
                strStatus = Trim(.Item("confirm", i).Value)
                stsposting = .Item("posting", i).Value
            End With
        End If
    End Sub

    Private Sub BtnHapus_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnHapus.Click
        TakeDGView()
        If stsposting = 0 Then
            If MessageBox.Show("Anda Yakin Data Bank Masuk dengan No. " & xyzNotrans & " akan di Hapus", "Delete Data", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then

                SQL = "update acc_kas_hd set confirm=2, is_trash=1,tanggal_hapus='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', user_hapus='" & myID & "' where id=" & strID & " and is_trash=0"
                Konek.IUDQuery(SQL)

                SQL = "update acc_kas_dt set is_trash=1,tanggal_hapus='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', user_hapus='" & myID & "' where header_id=" & strID & ""
                Konek.IUDQuery(SQL)

                SQL = "update acc_jurnal set is_trash=1,tanggal_hapus='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "', user_hapus='" & myID & "' where no_transaksi_kas='" & xyzNotrans & "'"
                Konek.IUDQuery(SQL)


                MsgBox("Data berhasil dihapus", vbExclamation)
                ShowDataGrid()
            End If
        Else
            MsgBox("Data tidak bisa dihapus, Karena status sudah POSTING", vbExclamation)
            Exit Sub
        End If
    End Sub

    Private Sub BtnConfirm_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnConfirm.Click
        TakeDGView()
        If strStatus = "OPEN" Then
            SQL = "update acc_kas_hd set confirm=1 where id=" & strID & " and is_trash=0"
            Konek.IUDQuery(SQL)

            'SQL = "update acc_jurnal set status=1 where no_transaksi_kas='" & xyzNotrans & "'"
            'Konek.IUDQuery(SQL)


            MsgBox("Proses Konfirmasi Berhasil", vbExclamation)
            ShowDataGrid()
        Else
            MsgBox("Anda Sudah Melakukan proses Konfirmamsi sebelumnya, Proses Gagal", vbExclamation)
            Exit Sub
        End If
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnPrint.Click
        Dim objRpt As New CR_KBIn

        TakeDGView()

        SQL = ""
        SQL = " select a.id,a.no_transaksi,a.tgl_transaksi,a.tipe_transaksi,a.unitbisnis_id,c.kd_unitbisnis,c.nama_unitbisnis,"
        SQL = SQL & " a.acc_coa_id,d.kode,d.nama,a.ttl_nilai,a.keterangan,a.confirm,b.acc_coa_id as acc_codedtl,e.kode as kodedtl,e.nama as nanmadtl,"
        SQL = SQL & " b.nilai,b.keterangan as keterangandtl"
        SQL = SQL & " from acc_kas_hd a inner join acc_kas_dt b on a.id=b.header_id"
        SQL = SQL & " left join unitbisnis_nama c on a.unitbisnis_id=c.id"
        SQL = SQL & " left join acc_perkiraan_nama d on a.acc_coa_id=d.id"
        SQL = SQL & " left join acc_perkiraan_nama e on b.acc_coa_id=e.id"
        SQL = SQL & " where a.is_trash = 0 And b.is_trash = 0 and a.id='" & Trim(strID) & "'"


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_inkasbank").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_inkasbank").ImportRow(dr)
        Next


        objRpt.SetDataSource(ds1.Tables("ds_inkasbank"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt
        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default

    End Sub


    Private Sub ChkBln_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkBln.CheckedChanged
        If ChkBln.Checked = True Then
            txtDate.Enabled = True
        Else
            txtDate.Enabled = False
        End If
    End Sub
End Class

