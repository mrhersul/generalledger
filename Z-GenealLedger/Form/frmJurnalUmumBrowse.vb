﻿Imports System.IO
Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmJurnalUmumBrowse
    Dim Row1 As Janus.Windows.GridEX.GridEXRow

    Sub frmClose()
        fjub.Close()
        frmMain.TabControl1.TabPages.Remove(frmMain.TabControl1.SelectedTab)
    End Sub

    Private Sub frmJurnalUmumBrowse_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmClose()
    End Sub

    Private Sub frmJurnalUmumBrowse_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        GroupPanel1.Width = Me.Width
        GroupPanel1.Height = Me.Height - (frmMain.RibbonPanel1.Height * 0.5)
        GroupPanel1.Left = 5
        GroupPanel1.Top = 5
        GridEX.Width = Me.Width - 40
        GridEX.Height = Me.Height - (frmMain.RibbonPanel1.Height * 1.5)
        GridEX.Left = 10
        GridEX.Top = 40
        GroupPanel2.Left = Me.Width - 1250
        GroupPanel2.Top = 2
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles BtnClose.Click
        frmClose()
    End Sub

    Sub ShowData()
        ' --- Header ---

        SQL = ""
        SQL = " WITH ParentChildCTE AS ("
        SQL = SQL & " SELECT id,parent_id,kd_unitbisnis,nama_unitbisnis FROM unitbisnis_nama WHERE id = " & myIDCabang & ""
        SQL = SQL & " UNION ALL"
        SQL = SQL & " SELECT T1.id,T1.parent_id,T1.kd_unitbisnis,T1.nama_unitbisnis FROM unitbisnis_nama T1"
        SQL = SQL & " INNER JOIN ParentChildCTE T ON T.id = T1.parent_id WHERE T1.parent_id IS NOT NULL )"
        SQL = SQL & " select x.tanggal,x.no_transaksi_kas,x.kode,x.nama,x.keterangan,x.nilai_debet,x.nilai_kredit from"
        SQL = SQL & " (select a.tanggal,a.no_transaksi_kas,b.kode,b.nama,a.keterangan,a.nilai_debet,a.nilai_kredit from acc_jurnal a"
        SQL = SQL & " left join acc_perkiraan_nama b on a.acc_coa_id=b.id where a.is_trash=0 and a.tanggal between '" & Format(txtDate1.Value, "yyyy-MM-dd") & "' and '" & Format(txtDate2.Value, "yyyy-MM-dd") & "'"


        If txtSearch.Text <> "" Then
            SQL = SQL & " and (a.no_transaksi_kas like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
        End If

        If cboTrans.Text <> "" Then
            SQL = SQL & " and a.jenis='" & cboTrans.Text & "'"
        End If

        'If UCase(cboStatus.Text) = "OPEN" Then
        '    SQL = SQL & " and a.status = 0"
        'ElseIf UCase(cboStatus.Text) = "CONFIRM" Then
        '    SQL = SQL & " and a.status = 1"
        'ElseIf UCase(cboStatus.Text) = "CANCEL" Then
        '    SQL = SQL & " and a.status = 2"
        'End If

        SQL = SQL & " group by a.tanggal,a.no_transaksi_kas,b.kode,b.nama,a.keterangan,a.nilai_debet,a.nilai_kredit) as x"
        'SQL = SQL & " where x.unitbisnis_id in (SELECT id FROM ParentChildCTE)"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        dscoa3.Tables("Data_Header").Clear()
        For Each dr As DataRow In dt.Rows
            dscoa3.Tables("Data_Header").ImportRow(dr)
        Next


        GridEX.DataSource = dscoa3
        GridEX.DataMember = "Data_Header"

    End Sub

    Sub ComboJenis()
        SQL = ""
        SQL = "select distinct(jenis) as jenis from acc_jurnal order by jenis"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Do While dr.Read = True
            cboTrans.Items.Add(dr.GetString(0))
        Loop

    End Sub

    Private Sub frmJurnalUmumBrowse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDate1.Value = New DateTime(Now.Year, Now.Month, 1)
        txtDate2.Value = Format(Now, "dd/MM/yyyy")
        ShowData()
        ComboJenis()
    End Sub

    Private Sub btnEdit_Click(sender As System.Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles btnEdit.Click
        blnAdd = False
        blnEdit = True


        On Error Resume Next

        Row1 = GridEX.CurrentRow
        xyzNoTransaksi = Row1.Cells("no_transaksi_kas").Value.ToString

        If Mid(xyzNoTransaksi, 1, 3) <> "JUM" Then
            MsgBox("Data tsb tidak bisa di EDIT melalui modul Jurnal Umum Karena Sebelumnya tidak di Input di Modul Jurnal Umum, Silakan Edit Di Modul Awal nya", vbExclamation)
            Exit Sub
        End If

        frmJurnalUmumDetail.ShowDialog()
        ShowData()
    End Sub

    Private Sub btnAdd2_Click(sender As System.Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles btnAdd2.Click
        blnAdd = True
        blnEdit = False
        frmJurnalUmumDetail.ShowDialog()
        ShowData()
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As DevComponents.DotNetBar.ClickEventArgs) Handles btnPrint.Click
        Dim objRpt As New CR_JurnalUmum

        On Error Resume Next

        Row1 = GridEX.CurrentRow
        xyzNoTransaksi = Row1.Cells("no_transaksi_kas").Value.ToString


        SQL = ""
        SQL = " select a.no_transaksi_kas,a.tanggal,a.acc_coa_id,a.unitbisnis_id,b.kd_unitbisnis,b.nama_unitbisnis,a.nilai_total,"
        SQL = SQL & " a.nilai_debet,a.nilai_kredit,a.keterangan,c.kode,c.nama"
        SQL = SQL & " from acc_jurnal a"
        SQL = SQL & " left join unitbisnis_nama b on a.unitbisnis_id=b.id "
        SQL = SQL & " left join acc_perkiraan_nama c on a.acc_coa_id=c.id "
        SQL = SQL & " where a.no_transaksi_kas='" & Trim(xyzNoTransaksi) & "'"


        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        Dim dt = New DataTable
        dt.Load(dr)

        ds1.Tables("ds_jurnalumum").Clear()
        For Each dr As DataRow In dt.Rows
            ds1.Tables("ds_jurnalumum").ImportRow(dr)
        Next


        objRpt.SetDataSource(ds1.Tables("ds_jurnalumum"))
        frmReport.CrystalReportViewer1.ReportSource = objRpt
        frmReport.CrystalReportViewer1.Refresh()
        frmReport.Show()
        Cursor = Cursors.Default
    End Sub

    Private Sub BtnSearch_Click(sender As System.Object, e As System.EventArgs) Handles BtnSearch.Click
        ShowData()
        txtSearch.Text = ""
    End Sub

End Class