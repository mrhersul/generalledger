﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.IO.Ports
Imports System.Drawing.Printing
Imports Microsoft.VisualBasic
Imports System.IO

Public Class frmPembayaran
    Dim xacccode As String
    Dim xaccnama As String
    Dim xtype As String
    Dim xacccodeID As Long

    Sub MyCariData()

        If blnbayarbeli Then
            dr.Close()
            SQL = ""
            SQL = " select a.no_transaksi_do,a.tgl_pembelian as tgl,b.nama_supplier + ' - ' + b.nama_pt as supplier,a.ttl_nilai_akhir,a.ttl_nilai_bayar,(a.ttl_nilai_akhir - a.ttl_nilai_bayar) as sisa,a.id "
            SQL = SQL & " from pembelian_beli_hd a left join supplier_nama b on a.supplier_nama_id=b.id where 0=0"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (a.no_transaksi_do like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or b.nama_pt like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or b.nama_supplier like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by a.no_transaksi_do "

        ElseIf blnbayarjual Then
            dr.Close()
            SQL = ""
            SQL = " select a.no_transaksi_do,a.tgl_penjualan as tgl,b.kd_konsumen as supplier,a.ttl_nilai_akhir,a.ttl_nilai_bayar,(a.ttl_nilai_akhir - a.ttl_nilai_bayar) as sisa,a.id "
            SQL = SQL & " from penjualan_jual_hd a left join konsumen b on a.konsumen_id=b.id where 0=0"

            If txtSearch.Text <> "" Then
                SQL = SQL & " and (a.no_transaksi_do like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%' or b.kd_konsumen like '%" & Konek.BuatKomaSatu(txtSearch.Text) & "%')"
            End If

            SQL = SQL & " order by a.no_transaksi_do "

        End If



        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        Dim dt = New DataTable
        dt.Load(dr)
        DGView.DataSource = dt


        DGView.RowsDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.ColumnHeadersDefaultCellStyle.Font = New Font("Calibri", 10)
        DGView.Columns(0).Width = 200
        DGView.Columns(1).Width = 110
        DGView.Columns(2).Width = 230
        DGView.Columns(3).Width = 100
        DGView.Columns(4).Width = 140
        DGView.Columns(5).Width = 100
        DGView.Columns(6).Width = 100

        DGView.Columns(0).HeaderText = "NO TRANSAKSI"
        DGView.Columns(1).HeaderText = "TANGGAL"
        DGView.Columns(2).HeaderText = "SUPPLIER"
        DGView.Columns(3).HeaderText = "JUMLAH"
        DGView.Columns(4).HeaderText = "TOTAL NILAI BAYAR"
        DGView.Columns(5).HeaderText = "SISA"
        DGView.Columns(6).HeaderText = "ID"


    End Sub

    Private Sub frmPembayaran_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MyCariData()
    End Sub

    Private Sub DGView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGView.Click
        If DGView.RowCount > 0 Then
            With DGView
                Dim i = .CurrentRow.Index
                txtNoTrans.Text = .Item("no_transaksi_do", i).Value
                txtDate.Value = .Item("tgl", i).Value
                txtSuppCust.Text = IIf(.Item("supplier", i).Value.Equals(DBNull.Value), "", .Item("supplier", i).Value)
                txtJumlah.Text = .Item("ttl_nilai_akhir", i).Value
                txtBayar.Text = .Item("ttl_nilai_bayar", i).Value
                txtSisa.Text = .Item("sisa", i).Value
                txtID.Text = .Item("id", i).Value
            End With
        End If
    End Sub

    Private Sub BtnInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnInsert.Click
        If txtNoTrans.Text = "" Or txtJumlah.Text = "" Or txtBayar.Text = "" Then
            MsgBox("Jumlah dan Nilai Bayar  Harga Tidak Boleh Kosong", vbExclamation)
            Exit Sub
        End If

        If blnbayarbeli = True Then
            BayarBeli()
        ElseIf blnbayarjual = True Then
            BayarJual()
        End If

        Close()

    End Sub

    Sub BayarBeli()
        With frmJurnalUmumDetail.DGView
            Dim i = .CurrentRow.Index

            'Debet
            dr.Close()
            SQL = ""
            SQL = " select a.*,b.nama from acc_perkiraan_setting2 a left join acc_perkiraan_nama b on a.id_perkiraan_nama=b.id where a.nama_tabel='pembelian_byr_hutang_hd' and a.acc_setting='ACC_BELI_BIAYA'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read
                    xacccodeID = dr.Item("id_perkiraan_nama")
                    xacccode = dr.Item("kd_akun_perkiraan")
                    xaccnama = dr.Item("nama")
                    xtype = dr.Item("type_perkiraan")

                End While
            End If

            frmJurnalUmumDetail.DGView.Item(0, i).Value = xacccode
            frmJurnalUmumDetail.DGView.Item(2, i).Value = xaccnama
            frmJurnalUmumDetail.DGView.Item(3, i).Value = txtJumlah.Text
            frmJurnalUmumDetail.DGView.Item(4, i).Value = 0
            frmJurnalUmumDetail.DGView.Item(6, i).Value = xacccodeID

            'Kredit
            dr.Close()
            SQL = ""
            SQL = " select a.*,b.nama from acc_perkiraan_setting2 a left join acc_perkiraan_nama b on a.id_perkiraan_nama=b.id where a.nama_tabel='pembelian_byr_hutang_hd' and a.acc_setting='ACC_KAS'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read
                    xacccodeID = dr.Item("id_perkiraan_nama")
                    xacccode = dr.Item("kd_akun_perkiraan")
                    xaccnama = dr.Item("nama")
                    xtype = dr.Item("type_perkiraan")

                End While
            End If


            frmJurnalUmumDetail.DGView.Rows.Add(xacccode, "", xaccnama, 0, CDbl(txtBayar.Text), "", xacccodeID)

            frmJurnalUmumDetail.hitungtotal()
        End With
    End Sub

    Sub BayarJual()
        With frmJurnalUmumDetail.DGView
            Dim i = .CurrentRow.Index

            'Debet
            dr.Close()
            SQL = ""
            SQL = " select a.*,b.nama from acc_perkiraan_setting2 a left join acc_perkiraan_nama b on a.id_perkiraan_nama=b.id where a.nama_tabel='penjualan_sb_byr_piutang_hd' and a.acc_setting='ACC_KAS'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read
                    xacccodeID = dr.Item("id_perkiraan_nama")
                    xacccode = dr.Item("kd_akun_perkiraan")
                    xaccnama = dr.Item("nama")
                    xtype = dr.Item("type_perkiraan")

                End While
            End If

            frmJurnalUmumDetail.DGView.Item(0, i).Value = xacccode
            frmJurnalUmumDetail.DGView.Item(2, i).Value = xaccnama
            frmJurnalUmumDetail.DGView.Item(3, i).Value = txtJumlah.Text
            frmJurnalUmumDetail.DGView.Item(4, i).Value = 0
            frmJurnalUmumDetail.DGView.Item(6, i).Value = xacccodeID

            'Kredit
            dr.Close()
            SQL = ""
            SQL = " select a.*,b.nama from acc_perkiraan_setting2 a left join acc_perkiraan_nama b on a.id_perkiraan_nama=b.id where a.nama_tabel='penjualan_sb_byr_piutang_hd' and a.acc_setting='ACC_JUAL_BIAYA'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read
                    xacccodeID = dr.Item("id_perkiraan_nama")
                    xacccode = dr.Item("kd_akun_perkiraan")
                    xaccnama = dr.Item("nama")
                    xtype = dr.Item("type_perkiraan")

                End While
            End If


            frmJurnalUmumDetail.DGView.Rows.Add(xacccode, "", xaccnama, 0, CDbl(txtBayar.Text), "", xacccodeID)

            frmJurnalUmumDetail.hitungtotal()
        End With
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub txtBayar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBayar.KeyPress
        On Error Resume Next

        'JIKA YG DITEKAN ADALAH HURUF PEMISAH RIBUAN ATAU DESIMAL
        If e.KeyChar = System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator OrElse _
        e.KeyChar = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator Then
            'UBAH HURUF TERSEBUT MENJADI HURUF PEMISAH DESIMAL
            e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
            'NB : NUMBERGROUPSEPARATOR ADALAH PEMISAH RIBUAN PADA PENULISAN ANGKA
            'NB : NUMBERDECIMALSEPARATOR ADALAH PEMISAH DESIMAL PADA PENULISAN ANGKA
        End If


        If e.KeyChar = "ENTER" Then
            txtSisa.Text = txtJumlah.Text - txtBayar.Text
        End If
    End Sub

    Private Sub txtBayar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBayar.TextChanged
        On Error Resume Next
        txtSisa.Text = txtJumlah.Text - txtBayar.Text
    End Sub
End Class