﻿Public Class frmUnClosingBulanan
    Dim strbulan As String
    Dim xybln
    Dim xyblnbef
    Dim xyblnclosing

    Private Sub frmUnClosingBulanan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        showbulanclosing()
        LabelX5.Text = strbulan
        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        LabelX1.Text = "Note : Un-Closing bulanan dilakukan jika terdapat kesalahan" & Environment.NewLine & "transaksi di bulan sebelumnya . Unclosing bulanan akan membuka closingan" & Environment.NewLine & "hanya 1 bulan yang sudah di closing"
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

        xybln = Format(DateAdd("m", 1, xyblnclosing), "yyyy-MM-dd")


        SQL = ""
        SQL = "delete from acc_saldo_awal where datediff(month,tgl,'" & xybln & "') = 0 and unitbisnis_id='" & txtIdUnit.Text & "'"
        Konek.IUDQuery(SQL)

        SQL = ""
        SQL = " update acc_periode_setting set tgl_berjalan=DateAdd(month,-1,tgl_berjalan), tgl_ubah='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',"
        SQL = SQL & "user_ubah=" & myID & " where id_unitbisnis=" & txtIdUnit.Text & ""
        Konek.IUDQuery(SQL)

        MsgBox("Data Sudah Tersimpan", vbInformation)
        showbulanclosing()
        Close()
    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitUnCLosing = True
        frmFindCoa.ShowDialog()
    End Sub

    Sub showbulanclosing()
        SQL = ""
        SQL = " Select * From acc_periode_setting Where id_unitbisnis = " & myIDUnit & ""

        Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
        dr1 = Cmd1.ExecuteReader()

        If dr1.HasRows Then
            While dr1.Read
                frmMain.ToolBulan.Text = " PERIODE BULAN  : " & Format(dr1.Item("tgl_berjalan"), "MMMM-yyyy")
                strbulan = "PERIODE CLOSING TERAKHIR  :  " & Format(dr1.Item("tgl_berjalan"), "MMMM-yyyy")
                xyblnclosing = Format(dr1.Item("tgl_berjalan"), "yyyy-MM-dd")
            End While
        End If
        dr1.Close()
    End Sub
End Class