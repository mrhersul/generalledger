﻿Imports System.Drawing
Imports System.IO
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data
'Imports System.Data.Sqlimpo
Imports BCrypt.Net.BCrypt

Public Class frmMain
    Dim strEncPass As String

    Sub ChildOpen(ByVal frm As Form, ByVal xtabtxt As String)
        'Dim tab As New TabPage
        'frm.TopLevel = False
        'tab.Text = xtabtxt
        'tab.Controls.Add(frm)
        'TabControl1.TabPages.Add(tab)
        'TabControl1.SelectedTab = tab
        'frm.Show()

        Dim notFound As Boolean = True
        Dim idxTab As Integer = 0

        frm.TopLevel = False

        For Each ti As TabPage In TabControl1.TabPages
            If ti.Name = frm.Name.ToString Then
                notFound = False
                Exit For
            End If
            idxTab += 1
        Next
        If notFound Then
            Dim tab As New TabPage

            tab.Text = xtabtxt
            tab.Name = frm.Name.ToString
            tab.Controls.Add(frm)

            TabControl1.TabPages.Add(tab)
            TabControl1.SelectedTab = tab
            frm.Show()
        Else
            Dim tab As TabPage = TabControl1.TabPages(frm.Name.ToString)
            TabControl1.SelectedTab = tab
        End If
    End Sub

    Private Sub Office2007Blue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2007Blue.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Blue
    End Sub

    Private Sub Office2007Black_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2007Black.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Black
    End Sub


    Private Sub Office2007Silver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2007Silver.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Silver
    End Sub

    Private Sub Office2007VistaGlass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2007VistaGlass.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007VistaGlass
    End Sub

    Private Sub Office2010Black_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2010Black.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Black
    End Sub

    Private Sub Office2010Blue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2010Blue.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue
    End Sub

    Private Sub Office2010Silver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2010Silver.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Silver
    End Sub

    Private Sub Office2013_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2013.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2013
    End Sub

    Private Sub VisualStudio2010Blue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisualStudio2010Blue.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2010Blue
    End Sub

    Private Sub VisualStudio2012Dark_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisualStudio2012Dark.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2012Dark
    End Sub

    Private Sub VisualStudio2012Light_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisualStudio2012Light.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2012Light
    End Sub

    Private Sub Windows7Blue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Windows7Blue.Click
        StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolUser.Text = ""
        ToolDept.Text = ""

        txUser.Focus()
        RibbonPanel1.Enabled = False
        RibbonControl1.Enabled = False

    End Sub

    Private Sub frmMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Panel1.Top = (Me.ClientSize.Height - Panel1.Height) / 2
        Panel1.Left = (Me.ClientSize.Width - Panel1.Width) / 2
        PictureBox2.Top = (Me.ClientSize.Height - Panel1.Width + 50)
        PictureBox2.Left = (Me.ClientSize.Width - Panel1.Width - 30)
    End Sub

    Private Sub Login()
        Try

            OpenConn()

            blnfrmCoa = False

            SQL = ""
            SQL = " select a.id,a.username,a.fullname,a.password,a.id_relasi_cabang,a.status_karyawan,a.active,"
            SQL = SQL & " b.id as idunit,b.type_organisasi_id, b.parent_id, b.kd_unitbisnis, b.nama_unitbisnis"
            SQL = SQL & " from users a left join unitbisnis_nama b on a.id_relasi_cabang=b.id"
            SQL = SQL & " Where username = '" & txUser.Text & "' And password = '" & txPassword.Text & "' and status_karyawan='AKTIF'"

            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()


            If dr.HasRows Then

                While dr.Read
                    myUser = Trim(dr.Item("username").ToString())
                    MyPass = Trim(dr.Item("password").ToString())
                    myName = Trim(dr.Item("fullname").ToString())
                    myIDCabang = Trim(dr.Item("id_relasi_cabang").ToString())
                    myStsKary = dr.Item("status_karyawan").ToString()
                    myActive = dr.Item("active").ToString()
                    myIDOrganisasi = dr.Item("type_organisasi_id").ToString()
                    myIDParent = dr.Item("parent_id").ToString()
                    myKdBisnis = dr.Item("kd_unitbisnis").ToString()
                    myNmBisnis = dr.Item("nama_unitbisnis").ToString()
                    myID = dr.Item("id").ToString()
                    myIDUnit = dr.Item("idunit").ToString()

                    ToolUser.Text = "Nama" & "  :  " & myName
                    ToolDept.Text = "Cabang" & "  :  " & myNmBisnis

                End While



                SQL = ""
                SQL = " Select * From acc_periode_setting Where id_unitbisnis = " & myIDUnit & ""

                Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
                dr1 = Cmd1.ExecuteReader()

                If dr1.HasRows Then
                    While dr1.Read
                        ToolBulan.Text = " PERIODE BULAN  : " & Format(dr1.Item("tgl_berjalan"), "MMMM-yyyy")
                    End While
                End If

                dr.Close()
                dr1.Close()


                Panel1.Visible = False
                RibbonControl1.Enabled = True
                RibbonPanel1.Enabled = True


            Else
                MsgBox("Username atau Password anda salah...!", MsgBoxStyle.Critical)
                Exit Sub
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub BLogin()
        Dim bUser, bPass As String
        Dim Granted As Boolean


        Try

            OpenConn()

            blnfrmCoa = False

            SQL = ""
            SQL = " select a.id,a.username,a.fullname,a.password,a.id_relasi_cabang,a.status_karyawan,a.active,"
            SQL = SQL & " b.id as idunit,b.type_organisasi_id, b.parent_id, b.kd_unitbisnis, b.nama_unitbisnis"
            SQL = SQL & " from users a left join unitbisnis_nama b on a.id_relasi_cabang=b.id"
            SQL = SQL & " Where username = '" & txUser.Text & "' and status_karyawan='AKTIF'"

            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            dr.Read()

            'strEncPass = BCrypt.Net.BCrypt.HashPassword(txPassword.Text)
            Granted = BCrypt.Net.BCrypt.Verify(txPassword.Text, dr.Item("password").ToString())

            If Granted Then
                myUser = Trim(dr.Item("username").ToString())
                MyPass = Trim(dr.Item("password").ToString())
                myName = Trim(dr.Item("fullname").ToString())
                myIDCabang = Trim(dr.Item("id_relasi_cabang").ToString())
                myStsKary = dr.Item("status_karyawan").ToString()
                myActive = dr.Item("active")
                myIDOrganisasi = dr.Item("type_organisasi_id").ToString()
                myIDParent = dr.Item("parent_id").ToString()
                myKdBisnis = dr.Item("kd_unitbisnis").ToString()
                myNmBisnis = dr.Item("nama_unitbisnis").ToString()
                myID = dr.Item("id").ToString()
                myIDUnit = dr.Item("idunit").ToString()

                ToolUser.Text = "Nama" & "  :  " & myName
                ToolDept.Text = "Cabang" & "  :  " & myNmBisnis

                SQL = ""
                SQL = " Select * From acc_periode_setting Where id_unitbisnis = " & myIDUnit & ""

                Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
                dr1 = Cmd1.ExecuteReader()

                If dr1.HasRows Then
                    While dr1.Read
                        ToolBulan.Text = " PERIODE BULAN  : " & Format(dr1.Item("tgl_berjalan"), "MMMM-yyyy")
                    End While
                End If

                dr.Close()
                dr1.Close()


                Panel1.Visible = False
                RibbonControl1.Enabled = True
                RibbonPanel1.Enabled = True
            Else
                MsgBox("Username atau Password anda salah...!", MsgBoxStyle.Critical)
                Exit Sub
            End If

            'If dr.HasRows Then

            '    While dr.Read
            '        myUser = Trim(dr.Item("username").ToString())
            '        MyPass = Trim(dr.Item("password").ToString())
            '        myName = Trim(dr.Item("fullname").ToString())
            '        myIDCabang = Trim(dr.Item("id_relasi_cabang").ToString())
            '        myStsKary = dr.Item("status_karyawan").ToString()
            '        myActive = dr.Item("active").ToString()
            '        myIDOrganisasi = dr.Item("type_organisasi_id").ToString()
            '        myIDParent = dr.Item("parent_id").ToString()
            '        myKdBisnis = dr.Item("kd_unitbisnis").ToString()
            '        myNmBisnis = dr.Item("nama_unitbisnis").ToString()
            '        myID = dr.Item("id").ToString()
            '        myIDUnit = dr.Item("idunit").ToString()

            '        ToolUser.Text = "Nama" & "  :  " & myName
            '        ToolDept.Text = "Cabang" & "  :  " & myNmBisnis

            '    End While



            '    SQL = ""
            '    SQL = " Select * From acc_periode_setting Where id_unitbisnis = " & myIDUnit & ""

            '    Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
            '    dr1 = Cmd1.ExecuteReader()

            '    If dr1.HasRows Then
            '        While dr1.Read
            '            ToolBulan.Text = " PERIODE BULAN  : " & Format(dr1.Item("tgl_berjalan"), "MMMM-yyyy")
            '        End While
            '    End If

            '    dr.Close()
            '    dr1.Close()


            '    Panel1.Visible = False
            '    RibbonControl1.Enabled = True
            '    RibbonPanel1.Enabled = True


            'Else
            '    MsgBox("Username atau Password anda salah...!", MsgBoxStyle.Critical)
            '    Exit Sub
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem2.Click
        Dim FilePath As String = Application.StartupPath & "\Z-GenealLedger.exe"

        Application.Exit()

        Dim myProcess As New Process()
        myProcess.StartInfo.FileName = FilePath
        myProcess.Start()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Application.Exit()
    End Sub

    Private Sub ButtonItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem7.Click
        Application.Exit()
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Call BLogin()
    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click
        Dim pwd As String = "admin234"
        Dim coba As String = InputBox("Password : ", "Database Connection")
        If coba = pwd Then
            frmConnection.ShowDialog()
        Else
            MsgBox("Wrong Password ", vbExclamation)
        End If
    End Sub

    Private Sub txPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txPassword.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            btnLogin.Focus()
        End If
    End Sub

    Private Sub txUser_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txUser.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            txPassword.Focus()
        End If
    End Sub

    Private Sub ButtonItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem4.Click
        frmConnection.ShowDialog()
    End Sub

    Private Sub BtnMasterItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCoaGroup.Click
        Dim frm As New frmGroupCoaBrowse
        Dim xtabtxt As String = "Group Perkiraan"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub BtnCoa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCoa.Click
        Dim frm As New frmCoaBrowse
        Dim xtabtxt As String = "Perkiraaan"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New SuperGridDemo.DemoMasterDetail
        Dim xtabtxt As String = "Perkiraaan"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub BtnPassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPassword.Click
        Dim frm As New frmSettingCoaprint
        Dim xtabtxt As String = "Setting Print Akun Perkiraan"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub BtnUnPosting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmUnposting.ShowDialog()
    End Sub

    Private Sub BtnSettingPerAcct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSettingPerAcct.Click
        frmSettingPeriodeAcct.ShowDialog()
    End Sub

    Private Sub BtnPosting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmPosting.ShowDialog()
    End Sub

    Private Sub BtnDistToko_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDistToko.Click
        frmRptLostProfit.ShowDialog()
    End Sub

    Private Sub BtnRepBalance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRepBalance.Click
        frmRptRekapPerkiraan.ShowDialog()
    End Sub

    Private Sub BtnClosing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClosing.Click
        frmClosingBulanan.ShowDialog()
    End Sub

    Private Sub BtnUnclosing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnUnclosing.Click
        frmUnClosingBulanan.ShowDialog()
    End Sub

    Private Sub ButtonItem3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem3.Click
        frmRptTotalNeraca.ShowDialog()
    End Sub

    Private Sub ButtonItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem6.Click
        frmRptNeraca.ShowDialog()
    End Sub

    Private Sub ButtonItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem5.Click
        frmRptNeracaLajur.ShowDialog()
    End Sub

    Private Sub ButtonItem25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem25.Click
        blnRptKasMasuk = True
        frmRptJurnalEntris.Text = "Laporan Jurnal Entris - KAS MASUK"
        frmRptJurnalEntris.ShowDialog()
    End Sub

    Private Sub ButtonItem26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem26.Click
        blnRptKasKeluar = True
        frmRptJurnalEntris.Text = "Laporan Jurnal Entris - KAS KELUAR"
        frmRptJurnalEntris.ShowDialog()
    End Sub

    Private Sub ButtonItem27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem27.Click
        blnRptBankMasuk = True
        frmRptJurnalEntris.Text = "Laporan Jurnal Entris - BANK MASUK"
        frmRptJurnalEntris.ShowDialog()
    End Sub

    Private Sub ButtonItem28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem28.Click
        blnRptBankKeluar = True
        frmRptJurnalEntris.Text = "Laporan Jurnal Entris - BANK KELUAR"
        frmRptJurnalEntris.ShowDialog()
    End Sub

    Private Sub ButtonItem23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem23.Click
        frmRptpendapatan.ShowDialog()
    End Sub

    Private Sub ButtonItem38_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem38.Click
        Dim frm As New frmDaftarKaryawan
        Dim xtabtxt As String = "Informasi Karyawan"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem44_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem44.Click
        Dim frm As New frmInputCuti
        Dim xtabtxt As String = "Daftar Cuti"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem45_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem45.Click
        Dim frm As New frmBrowseIzin
        Dim xtabtxt As String = "Daftar Izin"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem40_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem40.Click
        Dim frm As New frmBrowseCatatan
        Dim xtabtxt As String = "Daftar Catatan"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem41_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem41.Click
        Dim frm As New frmDaftarAbsensivb
        Dim xtabtxt As String = "Daftar Absensi"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem46_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem46.Click
        Dim frm As New frmTugasDinas
        Dim xtabtxt As String = "Tugas Dinas"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem42_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem42.Click
        Dim frm As New frmPayroll
        Dim xtabtxt As String = "Payroll"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem47_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem47.Click
        Dim frm As New frmKasInBrowse
        Dim xtabtxt As String = "Kas Masuk"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem48_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem48.Click
        Dim frm As New frmKasOutBrowse
        Dim xtabtxt As String = "Kas Keluar"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub BtnJurnalUmum_Click(sender As System.Object, e As System.EventArgs) Handles BtnJurnalUmum.Click
        Dim frm As New frmJurnalUmumBrowse
        Dim xtabtxt As String = "Jurnal Umum"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub BtnJurnalSesuai_Click(sender As System.Object, e As System.EventArgs) Handles BtnJurnalSesuai.Click
        Dim frm As New frmJurnalSesuaiBrowse
        Dim xtabtxt As String = "Jurnal Penyesuaian"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem50_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem50.Click
        Dim frm As New frmBankInBrowse
        Dim xtabtxt As String = "Bank Masuk"
        ChildOpen(frm, xtabtxt)
    End Sub

    Private Sub ButtonItem51_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem51.Click
        Dim frm As New frmBankOutBrowse
        Dim xtabtxt As String = "Bank Keluar"
        ChildOpen(frm, xtabtxt)
    End Sub
End Class

