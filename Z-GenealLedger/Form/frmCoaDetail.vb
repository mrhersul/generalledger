﻿Imports System.Data
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Rendering
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frmCoaDetail
    Dim svTipe As String
    Dim svKB As Integer
    Dim svKode As String

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        blnAdd = False
        blnEdit = False
        Me.Close()
    End Sub

    Private Sub ChkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkStatus.CheckedChanged
        If ChkStatus.Checked = True Then
            ChkStatus.Text = "Aktif"
        Else
            ChkStatus.Text = "Non Aktif"
        End If
    End Sub

    Private Sub frmCoaDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If blnAdd = True Then
            txtkdgroup.Text = ""
            txtnamagroup.Text = ""
            txtIdGroup.Text = ""
            txtkdParent.Text = ""
            txtNamaParent.Text = ""
            txtIdParent.Text = ""
            txtKodeK.Text = ""
            txtNoCoa.Text = ""
            txtNamaCoa.Text = ""
            txtIdCoa.Text = ""
            txtKet.Text = ""
            ChkStatus.Checked = True
        Else

            ShowData()

        End If

        If blnAdd = True Or blnEdit = True Then
            BtnSave.Enabled = True
        Else
            BtnSave.Enabled = False
        End If
    End Sub

    Sub ShowData()

        Dim shtipe As String
        Dim shBK As Integer
        Dim shstatus As Integer

        SQL = ""
        SQL = "Select a.*,b.kd_perkiraan_group,b.nama_perkiraan_group,c.kd_unitbisnis,c.nama_unitbisnis,d.kode as kode_parent,d.nama as nama_parent From acc_perkiraan_nama a left join acc_perkiraan_group b on a.acc_perkiraan_group_id=b.id"
        SQL = SQL & " left join unitbisnis_nama c on a.unitbisnis_nama_id=c.id"
        SQL = SQL & " left join acc_perkiraan_nama d on a.parent_id=d.id "
        SQL = SQL & " Where a.kode = '" & xyzCoa & "'"

        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()

        If dr.HasRows Then
            dr.Read()

            shtipe = Trim(dr.Item("type_perkiraan").ToString())
            If shtipe = "H" Then
                RBH.Checked = True
            Else
                RBD.Checked = True
            End If


            txtkdgroup.Text = Trim(dr.Item("kd_perkiraan_group").ToString())
            txtnamagroup.Text = Trim(dr.Item("nama_perkiraan_group").ToString())
            txtIdGroup.Text = Trim(dr.Item("acc_perkiraan_group_id").ToString())
            txtkdParent.Text = Trim(dr.Item("kode_parent").ToString())
            txtNamaParent.Text = Trim(dr.Item("nama_parent").ToString())
            txtIdParent.Text = Trim(dr.Item("parent_id").ToString())
            txtKodeK.Text = (Trim(dr.Item("kd_perkiraan_group").ToString()).Substring(0, 1))
            txtNoCoa.Text = (Trim(dr.Item("kode").ToString()).Substring(2, 4))
            txtNamaCoa.Text = Trim(dr.Item("nama").ToString())
            txtIdCoa.Text = dr.Item("id").ToString()
            txtKet.Text = Trim(dr.Item("keterangan").ToString())

            shstatus = Trim(dr.Item("status").ToString())
            If shstatus = "0" Then
                ChkStatus.Checked = False
                ChkStatus.Text = "Non Aktif"
            Else
                ChkStatus.Checked = True
                ChkStatus.Text = "Aktif"
            End If

        End If

        dr.Close()

    End Sub

    Private Sub btngroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngroup.Click
        blngroup = True
        frmFindCoa.ShowDialog()
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim IsConError As Boolean
        On Error GoTo err

        If txtkdgroup.Text = "" Or txtNoCoa.Text = "" Or txtNamaCoa.Text = "" Then
            MsgBox("Masih Ada Data Yang Kosong...", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If ChkStatus.Checked = True Then
            strActive = "1"
        Else
            strActive = "0"
        End If

        If RBD.Checked = True Then
            svTipe = "D"
        Else
            svTipe = "H"
        End If

        svKode = txtKodeK.Text & txtSep.Text & txtNoCoa.Text

        If blnAdd = True Then

            SQL = ""
            SQL = "  select kode from acc_perkiraan_nama where kode='" & svKode & "'"

            Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
            dr1 = Cmd1.ExecuteReader()

            If dr1.HasRows Then
                MsgBox("Kode COA  " & svKode & "  Sudah Ada, Silakan Ganti Kode COA nya", vbInformation)
                Exit Sub
            End If
            dr1.Close()


            dr.Close()
            SQL = "Select * From acc_perkiraan_nama where kode='" & Trim(txtNoCoa.Text) & "' or nama='" & Konek.BuatKomaSatu(txtNamaCoa.Text) & "'"
            Cmd = New SqlClient.SqlCommand(SQL, Cn)
            dr = Cmd.ExecuteReader()
            dr.Read()

            If dr.HasRows Then
                MsgBox("Data tersebut sudah ada...", MsgBoxStyle.Critical)
                Exit Sub
            Else
                IsConError = True
                dr.Close()
                SQL = "Insert Into acc_perkiraan_nama (tanggal_buat,user_buat,unitbisnis_nama_id,acc_perkiraan_group_id,"
                SQL = SQL & " type_perkiraan,parent_id,kode,nama,keterangan,status,is_trash) "
                SQL = SQL & " Values('" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & "," & myIDCabang & "," & txtIdGroup.Text & ",'" & svTipe & "'," & txtIdParent.Text & ",'" & svKode & "',"
                SQL = SQL & " '" & Konek.BuatKomaSatu(txtNamaCoa.Text) & "','" & Konek.BuatKomaSatu(txtKet.Text) & "'," & strActive & ",0)"
                Konek.IUDQuery(SQL)

            End If

        ElseIf blnEdit = True Then
            IsConError = True
            dr.Close()
            SQL = "update acc_perkiraan_nama set type_perkiraan='" & svTipe & "',"
            SQL = SQL & " nama='" & Konek.BuatKomaSatu(txtNamaCoa.Text) & "',keterangan='" & Konek.BuatKomaSatu(txtKet.Text) & "',status=" & strActive & ", tanggal_ubah='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',"
            SQL = SQL & " user_ubah=" & myID & ""
            SQL = SQL & " where id=" & txtIdCoa.Text & ""
            Konek.IUDQuery(SQL)

        End If

        MsgBox("Data sudah tersimpan...", MsgBoxStyle.Information)
        IsConError = False

        blnAdd = False
        blnEdit = False

        Me.Close()
        Exit Sub

err:
        Select Case IsConError
            Case True
                MsgBox(Err.Number & " : " & Err.Description, vbExclamation, "Warning")
            Case Else
        End Select
    End Sub

    Private Sub btnParent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParent.Click

        blnParent = True
        frmFindCoa.ShowDialog()

        If txtIdParent.Text <> "" Then
            dr1.Close()
            SQL = ""
            SQL = "select max(kode) as kode from acc_perkiraan_nama where acc_perkiraan_group_id=" & txtIdGroup.Text & " and parent_id=" & txtIdParent.Text & ""

            Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
            dr1 = Cmd1.ExecuteReader()

            If dr1.HasRows Then
                While dr1.Read
                    txtNoCoa.Text = dr1.Item("kode").ToString().Substring(2, 4)
                End While
            End If
            dr1.Close()
        End If
    End Sub
End Class