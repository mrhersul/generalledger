﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClosingBulanan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClosingBulanan))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtIdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtNamaUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.btnUnit = New DevComponents.DotNetBar.ButtonX()
        Me.txtkdUnit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.BtnClose = New DevComponents.DotNetBar.ButtonX()
        Me.BtnSave = New DevComponents.DotNetBar.ButtonX()
        Me.txtDate = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.txtIdUnit)
        Me.GroupPanel1.Controls.Add(Me.txtNamaUnit)
        Me.GroupPanel1.Controls.Add(Me.LabelX6)
        Me.GroupPanel1.Controls.Add(Me.btnUnit)
        Me.GroupPanel1.Controls.Add(Me.txtkdUnit)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Controls.Add(Me.BtnClose)
        Me.GroupPanel1.Controls.Add(Me.BtnSave)
        Me.GroupPanel1.Controls.Add(Me.txtDate)
        Me.GroupPanel1.Controls.Add(Me.LabelX5)
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 12)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(562, 262)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 0
        '
        'txtIdUnit
        '
        '
        '
        '
        Me.txtIdUnit.Border.Class = "TextBoxBorder"
        Me.txtIdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIdUnit.Location = New System.Drawing.Point(502, 54)
        Me.txtIdUnit.Name = "txtIdUnit"
        Me.txtIdUnit.Size = New System.Drawing.Size(37, 20)
        Me.txtIdUnit.TabIndex = 152
        Me.txtIdUnit.Visible = False
        '
        'txtNamaUnit
        '
        '
        '
        '
        Me.txtNamaUnit.Border.Class = "TextBoxBorder"
        Me.txtNamaUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNamaUnit.Enabled = False
        Me.txtNamaUnit.Location = New System.Drawing.Point(148, 77)
        Me.txtNamaUnit.Name = "txtNamaUnit"
        Me.txtNamaUnit.Size = New System.Drawing.Size(353, 20)
        Me.txtNamaUnit.TabIndex = 151
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(28, 51)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(114, 23)
        Me.LabelX6.TabIndex = 150
        Me.LabelX6.Text = "NAMA UNIT BISNIS"
        '
        'btnUnit
        '
        Me.btnUnit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUnit.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.btnUnit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnUnit.Image = CType(resources.GetObject("btnUnit.Image"), System.Drawing.Image)
        Me.btnUnit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUnit.Location = New System.Drawing.Point(494, 77)
        Me.btnUnit.Name = "btnUnit"
        Me.btnUnit.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.btnUnit.Size = New System.Drawing.Size(45, 22)
        Me.btnUnit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnUnit.TabIndex = 149
        '
        'txtkdUnit
        '
        '
        '
        '
        Me.txtkdUnit.Border.Class = "TextBoxBorder"
        Me.txtkdUnit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtkdUnit.Enabled = False
        Me.txtkdUnit.Location = New System.Drawing.Point(148, 49)
        Me.txtkdUnit.Name = "txtkdUnit"
        Me.txtkdUnit.Size = New System.Drawing.Size(241, 20)
        Me.txtkdUnit.TabIndex = 148
        '
        'LabelX1
        '
        Me.LabelX1.AutoSize = True
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Red
        Me.LabelX1.Location = New System.Drawing.Point(28, 116)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(78, 19)
        Me.LabelX1.TabIndex = 138
        Me.LabelX1.Text = "keterangan"
        Me.LabelX1.TextLineAlignment = System.Drawing.StringAlignment.Far
        Me.LabelX1.WordWrap = True
        '
        'BtnClose
        '
        Me.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnClose.BackColor = System.Drawing.Color.Transparent
        Me.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnClose.Image = CType(resources.GetObject("BtnClose.Image"), System.Drawing.Image)
        Me.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnClose.Location = New System.Drawing.Point(478, 181)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnClose.Size = New System.Drawing.Size(70, 72)
        Me.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnClose.TabIndex = 137
        Me.BtnClose.Text = "CLOSE"
        '
        'BtnSave
        '
        Me.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BtnSave.Image = CType(resources.GetObject("BtnSave.Image"), System.Drawing.Image)
        Me.BtnSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.BtnSave.Location = New System.Drawing.Point(402, 181)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Shape = New DevComponents.DotNetBar.EllipticalShapeDescriptor()
        Me.BtnSave.Size = New System.Drawing.Size(70, 72)
        Me.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BtnSave.TabIndex = 136
        Me.BtnSave.Text = "PROCESS"
        '
        'txtDate
        '
        '
        '
        '
        Me.txtDate.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.txtDate.ButtonDropDown.Visible = True
        Me.txtDate.IsPopupCalendarOpen = False
        Me.txtDate.Location = New System.Drawing.Point(148, 6)
        '
        '
        '
        Me.txtDate.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.txtDate.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.txtDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.DisplayMonth = New Date(2018, 4, 1, 0, 0, 0, 0)
        Me.txtDate.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.txtDate.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.txtDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDate.MonthCalendar.TodayButtonVisible = True
        Me.txtDate.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(141, 20)
        Me.txtDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtDate.TabIndex = 135
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(28, 3)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(114, 23)
        Me.LabelX5.TabIndex = 134
        Me.LabelX5.Text = "BULAN"
        '
        'frmClosingBulanan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(583, 284)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmClosingBulanan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Closing Bulanan - Posting"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel1.PerformLayout()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents txtDate As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BtnClose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtIdUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtNamaUnit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnUnit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtkdUnit As DevComponents.DotNetBar.Controls.TextBoxX
End Class
