﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCombo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboDetail = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.GL = New DevComponents.Editors.ComboItem()
        Me.PIUTANG_USAHA = New DevComponents.Editors.ComboItem()
        Me.SuspendLayout()
        '
        'cboDetail
        '
        Me.cboDetail.DisplayMember = "Text"
        Me.cboDetail.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboDetail.FormattingEnabled = True
        Me.cboDetail.ItemHeight = 16
        Me.cboDetail.Items.AddRange(New Object() {Me.GL, Me.PIUTANG_USAHA})
        Me.cboDetail.Location = New System.Drawing.Point(12, 12)
        Me.cboDetail.Name = "cboDetail"
        Me.cboDetail.Size = New System.Drawing.Size(183, 22)
        Me.cboDetail.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cboDetail.TabIndex = 7
        '
        'GL
        '
        Me.GL.Text = "GL"
        '
        'PIUTANG_USAHA
        '
        Me.PIUTANG_USAHA.Text = "PIUTANG USAHA"
        '
        'frmCombo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(211, 44)
        Me.Controls.Add(Me.cboDetail)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmCombo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Filter"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboDetail As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents GL As DevComponents.Editors.ComboItem
    Friend WithEvents PIUTANG_USAHA As DevComponents.Editors.ComboItem
End Class
