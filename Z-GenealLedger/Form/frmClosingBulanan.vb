﻿
Public Class frmClosingBulanan
    Dim xybln
    Dim xyblnbef
    Dim xyperiode

    Private Sub frmClosingBulanan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDate.Value = Now
        txtDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        txtDate.CustomFormat = "MMMM yyyy"

        txtkdUnit.Text = myKdBisnis
        txtNamaUnit.Text = myNmBisnis
        txtIdUnit.Text = myIDUnit
        LabelX1.Text = "Note : Closing bulanan dilakukan jika  semua transaksi " & Environment.NewLine & "di bulan berjalan sudah benar dan confirm semua" & Environment.NewLine & "Cek semua transaksi sebelum anda melakukan closing"
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

        xybln = Format(DateAdd("m", 1, txtDate.Value), "yyyy-MM-dd")
        xyblnbef = Format(txtDate.Value, "yyyy-MM-dd")
        xyperiode = Format(txtDate.Value, "yyyy-MM") & "-" & "01"

        dr.Close()
        SQL = "Select * From acc_saldo_awal where unitbisnis_id=" & txtIdUnit.Text & " and datediff(month,periode,'" & xyblnbef & "')=0 "
        Cmd = New SqlClient.SqlCommand(SQL, Cn)
        dr = Cmd.ExecuteReader()
        dr.Read()

        If dr.HasRows Then
            MsgBox("Anda Sudah Melakukan CLosing Bulanan / Posting Periode  " & Format(xyblnbef, "MMMM yyyy") & "  ...", MsgBoxStyle.Critical)
            Exit Sub
        End If


        SQL = ""
        SQL = "insert into acc_saldo_awal (periode,unitbisnis_id,acc_coa_id,saldo,tanggal_buat,user_buat)"
        SQL = SQL & " select '" & xyperiode & "',x.unitbisnis_id,x.acc_coa_id,x.saldo + x.trans as saldo,'" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "','" & myID & "' from"
        SQL = SQL & " (select unitbisnis_id,acc_coa_id,ISNULL(saldo,0) as saldo,0 as trans from acc_saldo_awal"
        SQL = SQL & " where datediff(month,periode,'" & xybln & "')=0 and unitbisnis_id=" & txtIdUnit.Text & ""
        SQL = SQL & " union all"
        SQL = SQL & " select unitbisnis_id,acc_coa_id,0 as saldo,sum(nilai_debet - nilai_kredit) as trans from acc_jurnal"
        SQL = SQL & " where datediff(month,tanggal,'" & xyblnbef & "')=0 and unitbisnis_id=" & txtIdUnit.Text & ""
        SQL = SQL & " group by unitbisnis_id,acc_coa_id) as x"
        Konek.IUDQuery(SQL)


        SQL = ""
        SQL = "select * from acc_periode_setting where id_unitbisnis=" & txtIdUnit.Text & ""

        Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
        dr1 = Cmd1.ExecuteReader()

        If dr1.HasRows Then
            SQL = ""
            SQL = " update acc_periode_setting set tgl_berjalan='" & Format(txtDate.Value, "yyyy-MM-dd") & "', tgl_ubah='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "',"
            SQL = SQL & "user_ubah=" & myID & " where id_unitbisnis=" & txtIdUnit.Text & ""
            Konek.IUDQuery(SQL)
        Else
            SQL = ""
            SQL = " insert into acc_periode_setting (id_unitbisnis,tgl_berjalan,tgl_buat,user_buat)"
            SQL = SQL & " values(" & txtIdUnit.Text & ",'" & Format(txtDate.Value, "yyyy-MM-dd") & "','" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "'," & myID & ")"
            Konek.IUDQuery(SQL)
        End If

        MsgBox("Data Sudah Tersimpan", vbInformation)
        dr1.Close()
        showbulanclosing()
        Close()
    End Sub

    Private Sub btnUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnit.Click
        blnUnitCLosing = True
        frmFindCoa.ShowDialog()
    End Sub

    Sub showbulanclosing()
        SQL = ""
        SQL = " Select * From acc_periode_setting Where id_unitbisnis= " & myIDUnit & ""

        Cmd1 = New SqlClient.SqlCommand(SQL, Cn)
        dr1 = Cmd1.ExecuteReader()

        If dr1.HasRows Then
            While dr1.Read
                frmMain.ToolBulan.Text = " PERIODE BULAN  : " & Format(dr1.Item("tgl_berjalan"), "MMMM-yyyy")
            End While
        End If
        dr1.Close()
    End Sub

End Class